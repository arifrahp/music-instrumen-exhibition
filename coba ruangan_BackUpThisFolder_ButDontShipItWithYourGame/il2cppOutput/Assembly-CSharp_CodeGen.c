﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Angklung::Start()
extern void Angklung_Start_mF6239B39C4065663D8D170ECDFF2F19A417CC4BA (void);
// 0x00000002 System.Void Angklung::Animate(System.Int32)
extern void Angklung_Animate_m9DCC2E855D2F2C235E35E3EBF20A2BC478546CBB (void);
// 0x00000003 System.Void Angklung::.ctor()
extern void Angklung__ctor_m96B99B22230B9A79E224E5B5C30136A0CA39E54A (void);
// 0x00000004 System.Void BtnSound::Start()
extern void BtnSound_Start_m1AF53386BA04AB246E760721330B2839D7730F14 (void);
// 0x00000005 System.Void BtnSound::Update()
extern void BtnSound_Update_m745746B117A02DD08DBDA4C4D93620E77F7F6AA7 (void);
// 0x00000006 System.Void BtnSound::.ctor()
extern void BtnSound__ctor_mBA6E99A3249FA5658FBDC6C792D32105F45EC801 (void);
// 0x00000007 System.Void BtnSound1::Start()
extern void BtnSound1_Start_mD88297CBEBC42E83E5FD3EF8A7DBB35D1433693F (void);
// 0x00000008 System.Void BtnSound1::Update()
extern void BtnSound1_Update_m2273A7FBE6730762421FEFDAC8D83762E4EF7D7D (void);
// 0x00000009 System.Void BtnSound1::.ctor()
extern void BtnSound1__ctor_m16D4383E8A9EBD38EF64E42C3832E0A72818FE89 (void);
// 0x0000000A System.Void MainMenu::ExitButton()
extern void MainMenu_ExitButton_mF86100263A10A1331A2EAED63F76C85D3321D397 (void);
// 0x0000000B System.Void MainMenu::StartButton()
extern void MainMenu_StartButton_m89CA27F2754BA12C13BCCB9C0C106133FE6830E3 (void);
// 0x0000000C System.Void MainMenu::StartCamera()
extern void MainMenu_StartCamera_m8DB49E995FBACBF64AC0C3A435EC01492B971106 (void);
// 0x0000000D System.Void MainMenu::.ctor()
extern void MainMenu__ctor_m8209CEC1D907C87A96D777961F4D0536E6E948DD (void);
// 0x0000000E System.Void playerMove::Start()
extern void playerMove_Start_m02ECAB6243F3A534E8AE21959824A4099BF19858 (void);
// 0x0000000F System.Void playerMove::Update()
extern void playerMove_Update_m6F2E105ACD135F40E009F1E729E3D09034092163 (void);
// 0x00000010 System.Void playerMove::.ctor()
extern void playerMove__ctor_m2407BF3CFD6A09637D4E31A702F13C0FF5D1F508 (void);
// 0x00000011 System.Void CameraPointer::Update()
extern void CameraPointer_Update_mF19D284A593E4627CD4F22A58C11FB87B6627FF4 (void);
// 0x00000012 System.Void CameraPointer::.ctor()
extern void CameraPointer__ctor_m5C4B443EB7C8A518306BF27BD947D938FDF5162D (void);
// 0x00000013 System.Void CardboardStartup::Start()
extern void CardboardStartup_Start_m35E3138C09D62411C4C8503DD4D47DA2AF9BF6E2 (void);
// 0x00000014 System.Void CardboardStartup::Update()
extern void CardboardStartup_Update_m4D4D770B4F4BA807AB64DB40B6730226BB821883 (void);
// 0x00000015 System.Void CardboardStartup::.ctor()
extern void CardboardStartup__ctor_m4C459294D28BDCFC64FECC657FB860844F52D9F7 (void);
// 0x00000016 System.Void GraphicsAPITextController::Start()
extern void GraphicsAPITextController_Start_mCD9DF9F31DF9E93A29126C6CD0B979F7F01F72C4 (void);
// 0x00000017 System.Void GraphicsAPITextController::.ctor()
extern void GraphicsAPITextController__ctor_m29A2AD5AE282F114E816F84A166F5BA39BEB1685 (void);
// 0x00000018 System.Void ObjectController::Start()
extern void ObjectController_Start_m50CBBCE72AB6C2A5C8BC1977D520CF3856A91E89 (void);
// 0x00000019 System.Void ObjectController::TeleportRandomly()
extern void ObjectController_TeleportRandomly_m45F23374832369044CBD4F60F4F976D33C97C181 (void);
// 0x0000001A System.Void ObjectController::OnPointerEnter()
extern void ObjectController_OnPointerEnter_m5D028066A6B28C2770924AE0C5A2A926E807FC42 (void);
// 0x0000001B System.Void ObjectController::OnPointerExit()
extern void ObjectController_OnPointerExit_mB2F968E3432A4B5E90ABFCE7E6B025A39041809F (void);
// 0x0000001C System.Void ObjectController::OnPointerClick()
extern void ObjectController_OnPointerClick_m42836A892DD52FE0C121F5E37231BF198052017E (void);
// 0x0000001D System.Void ObjectController::SetMaterial(System.Boolean)
extern void ObjectController_SetMaterial_mF8B3638C2382300F585FCFAE6EC5626AAE5EC772 (void);
// 0x0000001E System.Void ObjectController::.ctor()
extern void ObjectController__ctor_mA008C8DFDEF1B6A05926338921FE1FD30BCEA9BC (void);
// 0x0000001F System.Boolean VrModeController::get__isScreenTouched()
extern void VrModeController_get__isScreenTouched_mD72F14326F1D19F3AC970FD0ED9D0C5CD5C6CFB5 (void);
// 0x00000020 System.Boolean VrModeController::get__isVrModeEnabled()
extern void VrModeController_get__isVrModeEnabled_m8D8D68536E09FF2DEAD3795AF30A57BA6DF7C95F (void);
// 0x00000021 System.Void VrModeController::Start()
extern void VrModeController_Start_m10BF77FE8B825D9640800D5DF7EF791A391FA49B (void);
// 0x00000022 System.Void VrModeController::Update()
extern void VrModeController_Update_m43F984D5BF13F2C545B508D93645C1C3AC9262D5 (void);
// 0x00000023 System.Void VrModeController::EnterVR()
extern void VrModeController_EnterVR_m5FD79BF38C6AF621AF9992378BC5C3618FDE5EFB (void);
// 0x00000024 System.Void VrModeController::ExitVR()
extern void VrModeController_ExitVR_m1FF25DFE5B3BC15CEDE5C42F56CC399693867397 (void);
// 0x00000025 System.Collections.IEnumerator VrModeController::StartXR()
extern void VrModeController_StartXR_m57D80B4AD6B28A7C4E36052563B6227E824CDDB4 (void);
// 0x00000026 System.Void VrModeController::StopXR()
extern void VrModeController_StopXR_m0338E8D8035C06E1A46F17C16BBC7933EE9CAA3A (void);
// 0x00000027 System.Void VrModeController::.ctor()
extern void VrModeController__ctor_m94505259847FB7C54346D7CF02AD85793393D141 (void);
// 0x00000028 System.Void VrModeController/<StartXR>d__10::.ctor(System.Int32)
extern void U3CStartXRU3Ed__10__ctor_mDA431E1365B30DA2F64F1E7875CD387F2231E138 (void);
// 0x00000029 System.Void VrModeController/<StartXR>d__10::System.IDisposable.Dispose()
extern void U3CStartXRU3Ed__10_System_IDisposable_Dispose_m3DC1735AA5EC0D61A72803BB3B33B9499A373336 (void);
// 0x0000002A System.Boolean VrModeController/<StartXR>d__10::MoveNext()
extern void U3CStartXRU3Ed__10_MoveNext_m9A6340456306ACFADD0D458946C82448A862E68B (void);
// 0x0000002B System.Object VrModeController/<StartXR>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartXRU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD0B4CE8CBE980FD132B19F40FF2172502B1D0566 (void);
// 0x0000002C System.Void VrModeController/<StartXR>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartXRU3Ed__10_System_Collections_IEnumerator_Reset_mD99AEFDB38CFAC11731CDB371647891816288600 (void);
// 0x0000002D System.Object VrModeController/<StartXR>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartXRU3Ed__10_System_Collections_IEnumerator_get_Current_mD84D9ACDB0A04C619509BA51590975DB84C30B0F (void);
static Il2CppMethodPointer s_methodPointers[45] = 
{
	Angklung_Start_mF6239B39C4065663D8D170ECDFF2F19A417CC4BA,
	Angklung_Animate_m9DCC2E855D2F2C235E35E3EBF20A2BC478546CBB,
	Angklung__ctor_m96B99B22230B9A79E224E5B5C30136A0CA39E54A,
	BtnSound_Start_m1AF53386BA04AB246E760721330B2839D7730F14,
	BtnSound_Update_m745746B117A02DD08DBDA4C4D93620E77F7F6AA7,
	BtnSound__ctor_mBA6E99A3249FA5658FBDC6C792D32105F45EC801,
	BtnSound1_Start_mD88297CBEBC42E83E5FD3EF8A7DBB35D1433693F,
	BtnSound1_Update_m2273A7FBE6730762421FEFDAC8D83762E4EF7D7D,
	BtnSound1__ctor_m16D4383E8A9EBD38EF64E42C3832E0A72818FE89,
	MainMenu_ExitButton_mF86100263A10A1331A2EAED63F76C85D3321D397,
	MainMenu_StartButton_m89CA27F2754BA12C13BCCB9C0C106133FE6830E3,
	MainMenu_StartCamera_m8DB49E995FBACBF64AC0C3A435EC01492B971106,
	MainMenu__ctor_m8209CEC1D907C87A96D777961F4D0536E6E948DD,
	playerMove_Start_m02ECAB6243F3A534E8AE21959824A4099BF19858,
	playerMove_Update_m6F2E105ACD135F40E009F1E729E3D09034092163,
	playerMove__ctor_m2407BF3CFD6A09637D4E31A702F13C0FF5D1F508,
	CameraPointer_Update_mF19D284A593E4627CD4F22A58C11FB87B6627FF4,
	CameraPointer__ctor_m5C4B443EB7C8A518306BF27BD947D938FDF5162D,
	CardboardStartup_Start_m35E3138C09D62411C4C8503DD4D47DA2AF9BF6E2,
	CardboardStartup_Update_m4D4D770B4F4BA807AB64DB40B6730226BB821883,
	CardboardStartup__ctor_m4C459294D28BDCFC64FECC657FB860844F52D9F7,
	GraphicsAPITextController_Start_mCD9DF9F31DF9E93A29126C6CD0B979F7F01F72C4,
	GraphicsAPITextController__ctor_m29A2AD5AE282F114E816F84A166F5BA39BEB1685,
	ObjectController_Start_m50CBBCE72AB6C2A5C8BC1977D520CF3856A91E89,
	ObjectController_TeleportRandomly_m45F23374832369044CBD4F60F4F976D33C97C181,
	ObjectController_OnPointerEnter_m5D028066A6B28C2770924AE0C5A2A926E807FC42,
	ObjectController_OnPointerExit_mB2F968E3432A4B5E90ABFCE7E6B025A39041809F,
	ObjectController_OnPointerClick_m42836A892DD52FE0C121F5E37231BF198052017E,
	ObjectController_SetMaterial_mF8B3638C2382300F585FCFAE6EC5626AAE5EC772,
	ObjectController__ctor_mA008C8DFDEF1B6A05926338921FE1FD30BCEA9BC,
	VrModeController_get__isScreenTouched_mD72F14326F1D19F3AC970FD0ED9D0C5CD5C6CFB5,
	VrModeController_get__isVrModeEnabled_m8D8D68536E09FF2DEAD3795AF30A57BA6DF7C95F,
	VrModeController_Start_m10BF77FE8B825D9640800D5DF7EF791A391FA49B,
	VrModeController_Update_m43F984D5BF13F2C545B508D93645C1C3AC9262D5,
	VrModeController_EnterVR_m5FD79BF38C6AF621AF9992378BC5C3618FDE5EFB,
	VrModeController_ExitVR_m1FF25DFE5B3BC15CEDE5C42F56CC399693867397,
	VrModeController_StartXR_m57D80B4AD6B28A7C4E36052563B6227E824CDDB4,
	VrModeController_StopXR_m0338E8D8035C06E1A46F17C16BBC7933EE9CAA3A,
	VrModeController__ctor_m94505259847FB7C54346D7CF02AD85793393D141,
	U3CStartXRU3Ed__10__ctor_mDA431E1365B30DA2F64F1E7875CD387F2231E138,
	U3CStartXRU3Ed__10_System_IDisposable_Dispose_m3DC1735AA5EC0D61A72803BB3B33B9499A373336,
	U3CStartXRU3Ed__10_MoveNext_m9A6340456306ACFADD0D458946C82448A862E68B,
	U3CStartXRU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD0B4CE8CBE980FD132B19F40FF2172502B1D0566,
	U3CStartXRU3Ed__10_System_Collections_IEnumerator_Reset_mD99AEFDB38CFAC11731CDB371647891816288600,
	U3CStartXRU3Ed__10_System_Collections_IEnumerator_get_Current_mD84D9ACDB0A04C619509BA51590975DB84C30B0F,
};
static const int32_t s_InvokerIndices[45] = 
{
	3576,
	2906,
	3576,
	3576,
	3576,
	3576,
	3576,
	3576,
	3576,
	3576,
	3576,
	3576,
	3576,
	3576,
	3576,
	3576,
	3576,
	3576,
	3576,
	3576,
	3576,
	3576,
	3576,
	3576,
	3576,
	3576,
	3576,
	3576,
	2859,
	3576,
	3421,
	3421,
	3576,
	3576,
	3576,
	3576,
	3483,
	3576,
	3576,
	2906,
	3576,
	3421,
	3483,
	3576,
	3483,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	45,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
