using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerMove : MonoBehaviour
{
    public CharacterController controller;
    public CharacterController CharacterController;
    public float speed = 2f;
    public float gravity = -9.181f;
    public float Gravity = 9.8f;
    private float velocity = 0;
    // Start is called before the first frame update
    void Start()
    {
        CharacterController = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");

        Vector3 move = transform.right * x + transform.forward * y;
        controller.Move(move * speed * Time.deltaTime);

        if(CharacterController.isGrounded)
        {
            velocity = 0;
            velocity -= Gravity * Time.deltaTime;
        }
        else
        {
            CharacterController.Move(new Vector3(0, velocity, 0));
        }
    }
}
