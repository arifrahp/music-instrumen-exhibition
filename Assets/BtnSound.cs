using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BtnSound : MonoBehaviour
{
    private AudioSource audioSrcBtn;
    // Start is called before the first frame update
    void Start()
    {
        audioSrcBtn = GetComponent<AudioSource>();

        if (audioSrcBtn==null)
        {
            Debug.Log("The Audio is missing");
        }  
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            audioSrcBtn.Play();
        }
    }
}
