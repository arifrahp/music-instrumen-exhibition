using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Angklung : MonoBehaviour
{
    // Start is called before the first frame update
    private Animator animator;

    void Start()
    {
        animator = GetComponent<Animator> ();
    }

    public void Animate(int gerakan){
        if(gerakan == 1)animator.SetTrigger ("move");
    }

}
