using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BtnSound1 : MonoBehaviour
{
    private AudioSource audioSrcBtn1;
    // Start is called before the first frame update
    void Start()
    {
        audioSrcBtn1 = GetComponent<AudioSource>();

        if (audioSrcBtn1==null)
        {
            Debug.Log("The Audio is missing");
        }  
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            audioSrcBtn1.Play();
        }
    }
}
