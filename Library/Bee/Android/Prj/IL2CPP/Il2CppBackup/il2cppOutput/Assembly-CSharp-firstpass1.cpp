﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>


struct VirtualActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct VirtualActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// System.Action`1<Lean.Common.LeanSelectable>
struct Action_1_tAD366A485E434448836F3A4108059A1618C980AF;
// System.Action`1<System.Object>
struct Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87;
// System.Action`2<Lean.Common.LeanSelect,Lean.Common.LeanSelectable>
struct Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190;
// System.Action`2<System.Object,System.Object>
struct Action_2_t156C43F079E7E68155FCDCD12DC77DD11AEF7E3C;
// System.Comparison`1<UnityEngine.EventSystems.RaycastResult>
struct Comparison_1_t9FCAC8C8CE160A96C5AAD2DE1D353DCE8A2FEEFC;
// System.Collections.Generic.LinkedListNode`1<Lean.Common.LeanSelect>
struct LinkedListNode_1_t7D0DA7355A32A333D2AB6F41BC1E57C6EF2AE9F7;
// System.Collections.Generic.LinkedListNode`1<Lean.Common.LeanSelectable>
struct LinkedListNode_1_tC4B0FFBEE56621B13FC8502EB49ADCA6A7E69B72;
// System.Collections.Generic.LinkedListNode`1<System.Object>
struct LinkedListNode_1_t293BB098D459DDAE6A26977D0731A997186D1D4C;
// System.Collections.Generic.LinkedList`1<Lean.Common.LeanSelect>
struct LinkedList_1_t74DD3E2689E35FC0DF8DF1971B5CD529EEE8CAA1;
// System.Collections.Generic.LinkedList`1<Lean.Common.LeanSelectable>
struct LinkedList_1_t25474EE5265E341D73136D6D26BA9381A1E5361C;
// System.Collections.Generic.LinkedList`1<System.Object>
struct LinkedList_1_t49DC5CF34D4D642E6417F1245CDEC26A32F60C76;
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.GameObject,System.Int32>>
struct List_1_tC2267FBE21298A38669F48AD07AE8540B7AA80AF;
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct List_1_tA61AD775C53D69BA8C4CB85F6A1A7866AAC6997B;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>
struct List_1_tA5BDE435C735A082941CD33D212F97F4AE9FA55F;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventSystem>
struct List_1_tF2FE88545EFEC788CAAE6C74EC2F78E937FCCAC3;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_tB951CE80B58D1BF9650862451D8DAD8C231F207B;
// System.Collections.Generic.List`1<Lean.Common.LeanSelectable>
struct List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3;
// System.Collections.Generic.List`1<System.Object>
struct List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t8292C421BBB00D7661DC07462822936152BAB446;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t5BB0582F926E75E2FE795492679A6CF55A4B4BC4;
// UnityEngine.Events.UnityAction`1<Lean.Common.LeanSelect>
struct UnityAction_1_tA601885BAAC49BD9D535DC344F67685863BD4573;
// UnityEngine.Events.UnityAction`1<System.Object>
struct UnityAction_1_t9C30BCD020745BF400CBACF22C6F34ADBA2DDA6A;
// UnityEngine.Events.UnityEvent`1<Lean.Common.LeanSelect>
struct UnityEvent_1_tE3623D0B5417AD4C7F446399441E2E6214BD3108;
// UnityEngine.Events.UnityEvent`1<Lean.Common.LeanSelectable>
struct UnityEvent_1_t0C1738C60D92A522CB1FF78F9CE867EE6CD8CD31;
// UnityEngine.Events.UnityEvent`1<System.Object>
struct UnityEvent_1_t3CE03B42D5873C0C0E0692BEE72E1E6D5399F205;
// UnityEngine.Events.UnityEvent`1<UnityEngine.SpriteRenderer>
struct UnityEvent_1_t8ABE5544759145B8D7A09F1C54FFCB6907EDD56E;
// UnityEngine.Events.UnityEvent`1<System.String>
struct UnityEvent_1_tC9859540CF1468306CAB6D758C0A0D95DBCEC257;
// System.Collections.Generic.KeyValuePair`2<UnityEngine.GameObject,System.Int32>[]
struct KeyValuePair_2U5BU5D_t8F573E762700321B5950A4ACCC53564FBE075358;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>[]
struct KeyValuePair_2U5BU5D_tF5EFD20ACC3BB6CE90DBB51F3461B2C8F1D2E2F3;
// System.Delegate[]
struct DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771;
// System.IntPtr[]
struct IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832;
// Lean.Common.LeanSelectable[]
struct LeanSelectableU5BU5D_t53DEC4F400E60F6A978C470F939227BE2303931A;
// System.Object[]
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t008B8309DE422FE7567068D743D68054D5EBF1A8;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t28739C686586993113318B63C84927FD43063FC7;
// UnityEngine.EventSystems.RaycastResult[]
struct RaycastResultU5BU5D_tEAF6B3C3088179304676571328CBB001D8CECBC7;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C;
// System.ArgumentOutOfRangeException
struct ArgumentOutOfRangeException_tEA2822DAF62B10EEED00E0E3A341D4BAF78CF85F;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_tE03A848325C0AE8E76C6CA15FD86395EBF83364F;
// UnityEngine.EventSystems.BaseInputModule
struct BaseInputModule_tF3B7C22AF1419B2AC9ECE6589357DC1B88ED96B1;
// UnityEngine.EventSystems.BaseRaycaster
struct BaseRaycaster_t7DC8158FD3CA0193455344379DD5FF7CD5F1F832;
// UnityEngine.Behaviour
struct Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA;
// UnityEngine.Camera
struct Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184;
// UnityEngine.Canvas
struct Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tAB9A55A976C4E3B2B37D0CE5616E5685A8B43860;
// UnityEngine.Collider
struct Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76;
// UnityEngine.Component
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3;
// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t61C51380B105BE9D2C39C4F15B7E655659957707;
// System.Exception
struct Exception_t;
// UnityEngine.GameObject
struct GameObject_t76FEDD663AB33C991A9C9A23129337651094216F;
// UnityEngine.UI.Graphic
struct Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931;
// System.Collections.IDictionary
struct IDictionary_t6D03155AF1FA9083817AA5B6AD7DEEACC26AB220;
// System.IndexOutOfRangeException
struct IndexOutOfRangeException_t7ECB35264FB6CA8FAA516BD958F4B2ADC78E8A82;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t309E1C8C7CE885A0D2F98C84CEA77A8935688382;
// Lean.Common.LeanDestroy
struct LeanDestroy_tB9D1797F553C4D87243711A8C9777654CE71E9B6;
// Lean.Common.LeanFormatString
struct LeanFormatString_tA7B453B58D2C7E96D7ECF908A884299F3F6563EE;
// Lean.Common.LeanPath
struct LeanPath_t52FC47E81D073093417CE48BADFF80E5182D132F;
// Lean.Common.LeanPlane
struct LeanPlane_tDB7C59EE99114AFD2E4745F6ADE3D448FCC76E31;
// Lean.Common.LeanRoll
struct LeanRoll_t205306FB01D404E1DBC7D81AC3D66A5796097934;
// Lean.Common.LeanSelect
struct LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F;
// Lean.Common.LeanSelectable
struct LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E;
// Lean.Common.LeanSelectableBehaviour
struct LeanSelectableBehaviour_t07650DA7B5AA5AEACF3FA60433E5C36E1A679CD9;
// Lean.Common.LeanSelectableGraphicColor
struct LeanSelectableGraphicColor_tFDDBE08F9483FC2CB8E1511BEEE1AF8F5601345A;
// Lean.Common.LeanSelectableRendererColor
struct LeanSelectableRendererColor_tDD4EBBF7DAB508DD4FC536B35A7F8DB48142433E;
// Lean.Common.LeanSelectableSpriteRendererColor
struct LeanSelectableSpriteRendererColor_tAD5AA41024B15A0A25E574475377A2669143886E;
// Lean.Common.LeanSpawn
struct LeanSpawn_t9B3DF6A3500CC180061ACF23149F40C2F2439801;
// UnityEngine.LineRenderer
struct LineRenderer_tEFEF960672DB69CB14B6D181FAE6292F0CF8B63D;
// UnityEngine.Material
struct Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3;
// UnityEngine.MaterialPropertyBlock
struct MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D;
// UnityEngine.Mesh
struct Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71;
// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_tB826EDF15DC80F71BCBCD8E410FD959A04C33F25;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t9670F3C7D823CCB738A1604C72A1EB90292396FB;
// UnityEngine.RectTransform
struct RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5;
// UnityEngine.Renderer
struct Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t3C47F63E24BEB9FCE2DC6309E027F238DC5C5E37;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B;
// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4;
// UnityEngine.Transform
struct Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1;
// UnityEngine.Events.UnityAction
struct UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7;
// UnityEngine.Events.UnityEvent
struct UnityEvent_tDC2C3548799DBC91D1E3F3DE60083A66F4751977;
// UnityEngine.UI.VertexHelper
struct VertexHelper_tB905FCB02AE67CBEE5F265FE37A5938FC5D136FE;
// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t844E527BFE37BC0495E7F67993E43C07642DA9DD;
// Lean.Common.LeanFormatString/StringEvent
struct StringEvent_t12A6CB6ADC4D0C58126DC29CA2AB35D8FD93E161;
// Lean.Common.LeanSelect/LeanSelectableEvent
struct LeanSelectableEvent_tE1BC7E60B77ABC37DF10F63761C513AB4EBEC09D;
// Lean.Common.LeanSelectable/LeanSelectEvent
struct LeanSelectEvent_tE1B7F7125473C6F40463EC6C88B8842A0404C34F;

IL2CPP_EXTERN_C RuntimeClass* Action_1_tAD366A485E434448836F3A4108059A1618C980AF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ArgumentOutOfRangeException_tEA2822DAF62B10EEED00E0E3A341D4BAF78CF85F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CwHelper_tCEAA370BDC6A41945D0FEDD0D8D490440F1C18F7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* EventSystem_t61C51380B105BE9D2C39C4F15B7E655659957707_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Exception_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IndexOutOfRangeException_t7ECB35264FB6CA8FAA516BD958F4B2ADC78E8A82_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* LeanPath_t52FC47E81D073093417CE48BADFF80E5182D132F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* LeanSelectEvent_tE1B7F7125473C6F40463EC6C88B8842A0404C34F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* LeanSelectableEvent_tE1BC7E60B77ABC37DF10F63761C513AB4EBEC09D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* LinkedList_1_t25474EE5265E341D73136D6D26BA9381A1E5361C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* LinkedList_1_t74DD3E2689E35FC0DF8DF1971B5CD529EEE8CAA1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t8292C421BBB00D7661DC07462822936152BAB446_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_tC2267FBE21298A38669F48AD07AE8540B7AA80AF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Mathf_tE284D016E3B297B72311AAD9EB8F0E643F6A4682_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Physics2D_t64C0DB5246067DAC2E83A52558A0AC68AF3BE94D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PointerEventData_t9670F3C7D823CCB738A1604C72A1EB90292396FB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* RaycastHit2DU5BU5D_t28739C686586993113318B63C84927FD43063FC7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* RaycastHitU5BU5D_t008B8309DE422FE7567068D743D68054D5EBF1A8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringEvent_t12A6CB6ADC4D0C58126DC29CA2AB35D8FD93E161_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UnityAction_1_tA601885BAAC49BD9D535DC344F67685863BD4573_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UnityEvent_tDC2C3548799DBC91D1E3F3DE60083A66F4751977_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE;
IL2CPP_EXTERN_C String_t* _stringLiteral5D1DD7CD3FED08FC2AEEF290CBF04FF642A87F37;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponentInParent_TisLeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_mCB62694EE43FA4026B1B4D8D8C9B372BDEE1DD51_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisGraphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931_mFE18E20FC92395F90E776DBC4CD214A4F2D97D90_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_mC91ACC92AD57CA6CA00991DAF1DB3830BCE07AF8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisSpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B_m6181F10C09FC1650DAE0EF2308D344A2F170AA45_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_m15AE326ED9AE5BBF6AD71F57A8B15B6D10B75F1A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_m3CC6549F7A60846A9D156C06EDCF724D21D0763C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_m78D479DDD8F3214C93E9BE0B062995D6E182E773_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_mE3FB1E26BA0B10EAB4C06CC56F1C78002726865C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_m22E22E0108683303C9E8F060E9C970D6AECF313C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_m3C9B7035E15808178030F1D80430BAFA582C5C32_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_m89C7C4FA562ABDDA990D8F12AFAB5714BC74927C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_mAC2A0346E0E1D3D9AC42A6CDFB77A42E2EF081CF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_m357A4F354FD535F202C1DAECEB1427B848D781D7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_m53EF02F206B7F773103FC51D50AC1B974AE692F4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_m5BCF5CFF717F6739116402C6E75944686E15AD0D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_m97354C43156F3E8FF03CC8D54C4151C9992C605C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* KeyValuePair_2__ctor_m882B36949BE2FB86948D3198EA9BC21D5AED8330_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* KeyValuePair_2_get_Key_m3AB1FFB220CFA62C8913F62DF7F79F0D2F39A314_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* KeyValuePair_2_get_Value_mB01CEBC5D69BFC0A878DB1ACBF3DE05B8EC1B964_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* LeanPath_GetPoint_m809DDF4B617B43CB30CA6EF2C33AA8AD3A4D6659_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* LeanPath_GetSmoothedPoint_m7284BFFDC1350CD161F27992008413A28EB6A309_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* LinkedList_1_AddLast_m22A0173682AA7EC7831D043A8AD267DFEA1446B9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* LinkedList_1_AddLast_mDC5C4929E156AC04B13A0D54368063664D3A861A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* LinkedList_1_GetEnumerator_mB93F43BCCD2C6A1C3A285DFD6BBCCA71529E58D1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* LinkedList_1_GetEnumerator_mDB5737AFFB871DAB61A205446EC59B28F4A810D3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* LinkedList_1_Remove_mC7399940C9CFA33D74D4C0DAC876404129DF8562_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* LinkedList_1_Remove_mC8D384262E80C141DEA34CEC4DC755F5BCB953FC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* LinkedList_1__ctor_m7E3FF3E1520AA5DE2DBE33B6A1E7657EC4FE65BF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* LinkedList_1__ctor_mC7E25FA9B50FCAED34E6C53704CBCDAE4BE0BB46_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m79E50C4F592B1703F4B76A8BE7B4855515460CA1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m879794CD85E22D7F3E18EB8223A87D702B9E74C8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_mB5F4B5AC39AACE323FD5679252895AE6CF289E08_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Clear_m455780C5A45049F9BDC25EAD3BA10A681D16385D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Clear_m6D7EC60334D1F90BC2538200034022903CF7DE50_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Contains_mCB5517DFCB5671A1504FF59C499DA2B27EE1B8AE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_GetEnumerator_m5032C7535D90065B4FA0A5942E7F92F4D72B8D41_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_GetEnumerator_m552F956B853F080F1BB08A652725EAA51DE82680_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_IndexOf_mCDCF06F86563E158C15328FB5D36286E68D653B6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_RemoveAt_m3D273B83DA1DA3CA02F13C3D3ECB2DDE052CFD2C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m4444F647B6C0C03F0F6CD67236CF2D7F76202E7A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m50607A2028B005421BBF8F137494E6877D6CAE5A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mB89D13B8B739042E3841C5DFAF30C3C51A797EA7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mC54E2BCBE43279A96FC082F5CDE2D76388BD8F9C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m45E012A15BC6389D71A4A103287549989EDC4DF2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m46EEFFA770BE665EA0CB3A5332E941DA4B3C1D37_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m0712D8C4200A8A2294457480069FDD039D529E1E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m8F2E15FC96DA75186C51228128A0660709E4E810_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_FindObjectOfType_TisEventSystem_t61C51380B105BE9D2C39C4F15B7E655659957707_m35D4A88CE80EF52117B3256977C521D1E9F2E7E4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_Instantiate_TisTransform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1_mD6EFBE8E5CFC309C748E284B0798BF1C184F49ED_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityEvent_1_AddListener_m41D8372E4AB1C156FF3CA895F0B66CBD484E6C57_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityEvent_1_Invoke_m14D167DBD029D5C80FE4F7F6DFEDDA46937311C4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityEvent_1_Invoke_mA633B48B5D287DA856FB954AC3E4012487E63C15_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityEvent_1_Invoke_mE535439F54E8EA413F15E7C4EF538598058BBD9A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityEvent_1_RemoveListener_m93A6005ABE25250DBD0E2497D329D839BC2C92D8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityEvent_1__ctor_m0F7D790DACD6F3D18E865D01FE4427603C1B0CC6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityEvent_1__ctor_mA2FAFACB7630DDA1C8722C0684B8E14E2517CD57_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityEvent_1__ctor_mA971530EC4BACA70B45C39F76AD5A0BE118CF63C_RuntimeMethod_var;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct KeyValuePair_2U5BU5D_tF5EFD20ACC3BB6CE90DBB51F3461B2C8F1D2E2F3;
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918;
struct RaycastHitU5BU5D_t008B8309DE422FE7567068D743D68054D5EBF1A8;
struct RaycastHit2DU5BU5D_t28739C686586993113318B63C84927FD43063FC7;
struct Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.LinkedListNode`1<Lean.Common.LeanSelect>
struct LinkedListNode_1_t7D0DA7355A32A333D2AB6F41BC1E57C6EF2AE9F7  : public RuntimeObject
{
	// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1::list
	LinkedList_1_t74DD3E2689E35FC0DF8DF1971B5CD529EEE8CAA1* ___list_0;
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1::next
	LinkedListNode_1_t7D0DA7355A32A333D2AB6F41BC1E57C6EF2AE9F7* ___next_1;
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1::prev
	LinkedListNode_1_t7D0DA7355A32A333D2AB6F41BC1E57C6EF2AE9F7* ___prev_2;
	// T System.Collections.Generic.LinkedListNode`1::item
	LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* ___item_3;
};

// System.Collections.Generic.LinkedListNode`1<Lean.Common.LeanSelectable>
struct LinkedListNode_1_tC4B0FFBEE56621B13FC8502EB49ADCA6A7E69B72  : public RuntimeObject
{
	// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1::list
	LinkedList_1_t25474EE5265E341D73136D6D26BA9381A1E5361C* ___list_0;
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1::next
	LinkedListNode_1_tC4B0FFBEE56621B13FC8502EB49ADCA6A7E69B72* ___next_1;
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1::prev
	LinkedListNode_1_tC4B0FFBEE56621B13FC8502EB49ADCA6A7E69B72* ___prev_2;
	// T System.Collections.Generic.LinkedListNode`1::item
	LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* ___item_3;
};

// System.Collections.Generic.LinkedList`1<Lean.Common.LeanSelect>
struct LinkedList_1_t74DD3E2689E35FC0DF8DF1971B5CD529EEE8CAA1  : public RuntimeObject
{
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1::head
	LinkedListNode_1_t7D0DA7355A32A333D2AB6F41BC1E57C6EF2AE9F7* ___head_0;
	// System.Int32 System.Collections.Generic.LinkedList`1::count
	int32_t ___count_1;
	// System.Int32 System.Collections.Generic.LinkedList`1::version
	int32_t ___version_2;
	// System.Object System.Collections.Generic.LinkedList`1::_syncRoot
	RuntimeObject* ____syncRoot_3;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.LinkedList`1::_siInfo
	SerializationInfo_t3C47F63E24BEB9FCE2DC6309E027F238DC5C5E37* ____siInfo_4;
};

// System.Collections.Generic.LinkedList`1<Lean.Common.LeanSelectable>
struct LinkedList_1_t25474EE5265E341D73136D6D26BA9381A1E5361C  : public RuntimeObject
{
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1::head
	LinkedListNode_1_tC4B0FFBEE56621B13FC8502EB49ADCA6A7E69B72* ___head_0;
	// System.Int32 System.Collections.Generic.LinkedList`1::count
	int32_t ___count_1;
	// System.Int32 System.Collections.Generic.LinkedList`1::version
	int32_t ___version_2;
	// System.Object System.Collections.Generic.LinkedList`1::_syncRoot
	RuntimeObject* ____syncRoot_3;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.LinkedList`1::_siInfo
	SerializationInfo_t3C47F63E24BEB9FCE2DC6309E027F238DC5C5E37* ____siInfo_4;
};

// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.GameObject,System.Int32>>
struct List_1_tC2267FBE21298A38669F48AD07AE8540B7AA80AF  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	KeyValuePair_2U5BU5D_t8F573E762700321B5950A4ACCC53564FBE075358* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct List_1_tA61AD775C53D69BA8C4CB85F6A1A7866AAC6997B  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	KeyValuePair_2U5BU5D_tF5EFD20ACC3BB6CE90DBB51F3461B2C8F1D2E2F3* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

// System.Collections.Generic.List`1<Lean.Common.LeanSelectable>
struct List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	LeanSelectableU5BU5D_t53DEC4F400E60F6A978C470F939227BE2303931A* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

// System.Collections.Generic.List`1<System.Object>
struct List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t8292C421BBB00D7661DC07462822936152BAB446  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	RaycastResultU5BU5D_tEAF6B3C3088179304676571328CBB001D8CECBC7* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

// UnityEngine.EventSystems.AbstractEventData
struct AbstractEventData_tAE1A127ED657117548181D29FFE4B1B14D8E67F7  : public RuntimeObject
{
	// System.Boolean UnityEngine.EventSystems.AbstractEventData::m_Used
	bool ___m_Used_0;
};

// Lean.Common.LeanCommon
struct LeanCommon_t9BCB87B352B5F36A416D913062E1348E1ED489F7  : public RuntimeObject
{
};

// System.String
struct String_t  : public RuntimeObject
{
	// System.Int32 System.String::_stringLength
	int32_t ____stringLength_4;
	// System.Char System.String::_firstChar
	Il2CppChar ____firstChar_5;
};

// UnityEngine.Events.UnityEventBase
struct UnityEventBase_t4968A4C72559F35C0923E4BD9C042C3A842E1DB8  : public RuntimeObject
{
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t309E1C8C7CE885A0D2F98C84CEA77A8935688382* ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_tB826EDF15DC80F71BCBCD8E410FD959A04C33F25* ___m_PersistentCalls_1;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_2;
};

// System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_com
{
};

// System.Collections.Generic.LinkedList`1/Enumerator<Lean.Common.LeanSelect>
struct Enumerator_t3F4E85BE6F8338474FC5FB1BD081F64E1ECC5A1E 
{
	// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedList`1/Enumerator::_list
	LinkedList_1_t74DD3E2689E35FC0DF8DF1971B5CD529EEE8CAA1* ____list_0;
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1/Enumerator::_node
	LinkedListNode_1_t7D0DA7355A32A333D2AB6F41BC1E57C6EF2AE9F7* ____node_1;
	// System.Int32 System.Collections.Generic.LinkedList`1/Enumerator::_version
	int32_t ____version_2;
	// T System.Collections.Generic.LinkedList`1/Enumerator::_current
	LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* ____current_3;
	// System.Int32 System.Collections.Generic.LinkedList`1/Enumerator::_index
	int32_t ____index_4;
};

// System.Collections.Generic.LinkedList`1/Enumerator<Lean.Common.LeanSelectable>
struct Enumerator_t2DDE01BDFB37E13974A8807CCD46D71DEA3E8F33 
{
	// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedList`1/Enumerator::_list
	LinkedList_1_t25474EE5265E341D73136D6D26BA9381A1E5361C* ____list_0;
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1/Enumerator::_node
	LinkedListNode_1_tC4B0FFBEE56621B13FC8502EB49ADCA6A7E69B72* ____node_1;
	// System.Int32 System.Collections.Generic.LinkedList`1/Enumerator::_version
	int32_t ____version_2;
	// T System.Collections.Generic.LinkedList`1/Enumerator::_current
	LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* ____current_3;
	// System.Int32 System.Collections.Generic.LinkedList`1/Enumerator::_index
	int32_t ____index_4;
};

// System.Collections.Generic.LinkedList`1/Enumerator<System.Object>
struct Enumerator_tC25D6382B2C7E2606E12FC6637F714A98D52DE22 
{
	// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedList`1/Enumerator::_list
	LinkedList_1_t49DC5CF34D4D642E6417F1245CDEC26A32F60C76* ____list_0;
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1/Enumerator::_node
	LinkedListNode_1_t293BB098D459DDAE6A26977D0731A997186D1D4C* ____node_1;
	// System.Int32 System.Collections.Generic.LinkedList`1/Enumerator::_version
	int32_t ____version_2;
	// T System.Collections.Generic.LinkedList`1/Enumerator::_current
	RuntimeObject* ____current_3;
	// System.Int32 System.Collections.Generic.LinkedList`1/Enumerator::_index
	int32_t ____index_4;
};

// System.Collections.Generic.KeyValuePair`2<UnityEngine.GameObject,System.Int32>
struct KeyValuePair_2_t9BD14587548B372830B82C49C122A2710251FD58 
{
	// TKey System.Collections.Generic.KeyValuePair`2::key
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	int32_t ___value_1;
};

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>
struct KeyValuePair_2_tF11CA6D20F09EC4DAB7CB3C2C394F6F2C394E6B8 
{
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	int32_t ___value_1;
};

// UnityEngine.Events.UnityEvent`1<Lean.Common.LeanSelect>
struct UnityEvent_1_tE3623D0B5417AD4C7F446399441E2E6214BD3108  : public UnityEventBase_t4968A4C72559F35C0923E4BD9C042C3A842E1DB8
{
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* ___m_InvokeArray_3;
};

// UnityEngine.Events.UnityEvent`1<Lean.Common.LeanSelectable>
struct UnityEvent_1_t0C1738C60D92A522CB1FF78F9CE867EE6CD8CD31  : public UnityEventBase_t4968A4C72559F35C0923E4BD9C042C3A842E1DB8
{
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* ___m_InvokeArray_3;
};

// UnityEngine.Events.UnityEvent`1<System.String>
struct UnityEvent_1_tC9859540CF1468306CAB6D758C0A0D95DBCEC257  : public UnityEventBase_t4968A4C72559F35C0923E4BD9C042C3A842E1DB8
{
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* ___m_InvokeArray_3;
};

// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_tE03A848325C0AE8E76C6CA15FD86395EBF83364F  : public AbstractEventData_tAE1A127ED657117548181D29FFE4B1B14D8E67F7
{
	// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.BaseEventData::m_EventSystem
	EventSystem_t61C51380B105BE9D2C39C4F15B7E655659957707* ___m_EventSystem_1;
};

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22 
{
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;
};

// UnityEngine.Color
struct Color_tD001788D726C3A7F1379BEED0260B9591F440C1F 
{
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;
};

// System.Double
struct Double_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F 
{
	// System.Double System.Double::m_value
	double ___m_value_0;
};

// System.Int32
struct Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C 
{
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;
};

// System.IntPtr
struct IntPtr_t 
{
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;
};

// UnityEngine.LayerMask
struct LayerMask_t97CB6BDADEDC3D6423C7BCFEA7F86DA2EC6241DB 
{
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;
};

// UnityEngine.Mathf
struct Mathf_tE284D016E3B297B72311AAD9EB8F0E643F6A4682 
{
	union
	{
		struct
		{
		};
		uint8_t Mathf_tE284D016E3B297B72311AAD9EB8F0E643F6A4682__padding[1];
	};
};

// UnityEngine.Quaternion
struct Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 
{
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;
};

// System.Single
struct Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C 
{
	// System.Single System.Single::m_value
	float ___m_value_0;
};

// UnityEngine.Events.UnityEvent
struct UnityEvent_tDC2C3548799DBC91D1E3F3DE60083A66F4751977  : public UnityEventBase_t4968A4C72559F35C0923E4BD9C042C3A842E1DB8
{
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* ___m_InvokeArray_3;
};

// UnityEngine.Vector2
struct Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 
{
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;
};

// UnityEngine.Vector3
struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 
{
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;
};

// UnityEngine.Vector4
struct Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 
{
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;
};

// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915 
{
	union
	{
		struct
		{
		};
		uint8_t Void_t4861ACF8F4594C3437BB48B6E56783494B843915__padding[1];
	};
};

// UnityEngine.EventSystems.EventSystem/UIToolkitOverrideConfig
struct UIToolkitOverrideConfig_t4E6B4528E38BCA7DA72C45424634806200A50182 
{
	// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.EventSystem/UIToolkitOverrideConfig::activeEventSystem
	EventSystem_t61C51380B105BE9D2C39C4F15B7E655659957707* ___activeEventSystem_0;
	// System.Boolean UnityEngine.EventSystems.EventSystem/UIToolkitOverrideConfig::sendEvents
	bool ___sendEvents_1;
	// System.Boolean UnityEngine.EventSystems.EventSystem/UIToolkitOverrideConfig::createPanelGameObjectsOnStart
	bool ___createPanelGameObjectsOnStart_2;
};
// Native definition for P/Invoke marshalling of UnityEngine.EventSystems.EventSystem/UIToolkitOverrideConfig
struct UIToolkitOverrideConfig_t4E6B4528E38BCA7DA72C45424634806200A50182_marshaled_pinvoke
{
	EventSystem_t61C51380B105BE9D2C39C4F15B7E655659957707* ___activeEventSystem_0;
	int32_t ___sendEvents_1;
	int32_t ___createPanelGameObjectsOnStart_2;
};
// Native definition for COM marshalling of UnityEngine.EventSystems.EventSystem/UIToolkitOverrideConfig
struct UIToolkitOverrideConfig_t4E6B4528E38BCA7DA72C45424634806200A50182_marshaled_com
{
	EventSystem_t61C51380B105BE9D2C39C4F15B7E655659957707* ___activeEventSystem_0;
	int32_t ___sendEvents_1;
	int32_t ___createPanelGameObjectsOnStart_2;
};

// System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<UnityEngine.GameObject,System.Int32>>
struct Enumerator_tE49F457C3EA7DD2E164F160F3A3D19CC62191D80 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::_list
	List_1_tC2267FBE21298A38669F48AD07AE8540B7AA80AF* ____list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::_index
	int32_t ____index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::_version
	int32_t ____version_2;
	// T System.Collections.Generic.List`1/Enumerator::_current
	KeyValuePair_2_t9BD14587548B372830B82C49C122A2710251FD58 ____current_3;
};

// System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct Enumerator_t7F9BA7E481A1947D155FE5B42578F1117860DC39 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::_list
	List_1_tA61AD775C53D69BA8C4CB85F6A1A7866AAC6997B* ____list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::_index
	int32_t ____index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::_version
	int32_t ____version_2;
	// T System.Collections.Generic.List`1/Enumerator::_current
	KeyValuePair_2_tF11CA6D20F09EC4DAB7CB3C2C394F6F2C394E6B8 ____current_3;
};

// System.Delegate
struct Delegate_t  : public RuntimeObject
{
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject* ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.IntPtr System.Delegate::interp_method
	intptr_t ___interp_method_7;
	// System.IntPtr System.Delegate::interp_invoke_impl
	intptr_t ___interp_invoke_impl_8;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t* ___method_info_9;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t* ___original_method_info_10;
	// System.DelegateData System.Delegate::data
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_12;
};
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};

// System.Exception
struct Exception_t  : public RuntimeObject
{
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t* ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject* ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject* ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832* ___native_trace_ips_15;
	// System.Int32 System.Exception::caught_in_unmanaged
	int32_t ___caught_in_unmanaged_16;
};
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
	int32_t ___caught_in_unmanaged_16;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
	int32_t ___caught_in_unmanaged_16;
};

// Lean.Common.LeanScreenQuery
struct LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535 
{
	// Lean.Common.LeanScreenQuery/MethodType Lean.Common.LeanScreenQuery::Method
	int32_t ___Method_0;
	// UnityEngine.LayerMask Lean.Common.LeanScreenQuery::Layers
	LayerMask_t97CB6BDADEDC3D6423C7BCFEA7F86DA2EC6241DB ___Layers_1;
	// Lean.Common.LeanScreenQuery/SearchType Lean.Common.LeanScreenQuery::Search
	int32_t ___Search_2;
	// System.String Lean.Common.LeanScreenQuery::RequiredTag
	String_t* ___RequiredTag_3;
	// UnityEngine.Camera Lean.Common.LeanScreenQuery::Camera
	Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* ___Camera_4;
	// System.Single Lean.Common.LeanScreenQuery::Distance
	float ___Distance_5;
};
// Native definition for P/Invoke marshalling of Lean.Common.LeanScreenQuery
struct LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_marshaled_pinvoke
{
	int32_t ___Method_0;
	LayerMask_t97CB6BDADEDC3D6423C7BCFEA7F86DA2EC6241DB ___Layers_1;
	int32_t ___Search_2;
	char* ___RequiredTag_3;
	Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* ___Camera_4;
	float ___Distance_5;
};
// Native definition for COM marshalling of Lean.Common.LeanScreenQuery
struct LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_marshaled_com
{
	int32_t ___Method_0;
	LayerMask_t97CB6BDADEDC3D6423C7BCFEA7F86DA2EC6241DB ___Layers_1;
	int32_t ___Search_2;
	Il2CppChar* ___RequiredTag_3;
	Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* ___Camera_4;
	float ___Distance_5;
};

// UnityEngine.MaterialPropertyBlock
struct MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D  : public RuntimeObject
{
	// System.IntPtr UnityEngine.MaterialPropertyBlock::m_Ptr
	intptr_t ___m_Ptr_0;
};

// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.Ray
struct Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 
{
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Direction_1;
};

// UnityEngine.RaycastHit
struct RaycastHit_t6F30BD0B38B56401CA833A1B87BD74F2ACD2F2B5 
{
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Normal_1;
	// System.UInt32 UnityEngine.RaycastHit::m_FaceID
	uint32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_UV_4;
	// System.Int32 UnityEngine.RaycastHit::m_Collider
	int32_t ___m_Collider_5;
};

// UnityEngine.RaycastHit2D
struct RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA 
{
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Centroid
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_Centroid_0;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Point
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_Point_1;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Normal
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_Normal_2;
	// System.Single UnityEngine.RaycastHit2D::m_Distance
	float ___m_Distance_3;
	// System.Single UnityEngine.RaycastHit2D::m_Fraction
	float ___m_Fraction_4;
	// System.Int32 UnityEngine.RaycastHit2D::m_Collider
	int32_t ___m_Collider_5;
};

// UnityEngine.EventSystems.RaycastResult
struct RaycastResult_tEC6A7B7CABA99C386F054F01E498AEC426CF8023 
{
	// UnityEngine.GameObject UnityEngine.EventSystems.RaycastResult::m_GameObject
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___m_GameObject_0;
	// UnityEngine.EventSystems.BaseRaycaster UnityEngine.EventSystems.RaycastResult::module
	BaseRaycaster_t7DC8158FD3CA0193455344379DD5FF7CD5F1F832* ___module_1;
	// System.Single UnityEngine.EventSystems.RaycastResult::distance
	float ___distance_2;
	// System.Single UnityEngine.EventSystems.RaycastResult::index
	float ___index_3;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::depth
	int32_t ___depth_4;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingLayer
	int32_t ___sortingLayer_5;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingOrder
	int32_t ___sortingOrder_6;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldPosition
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___worldPosition_7;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldNormal
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___worldNormal_8;
	// UnityEngine.Vector2 UnityEngine.EventSystems.RaycastResult::screenPosition
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___screenPosition_9;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::displayIndex
	int32_t ___displayIndex_10;
};
// Native definition for P/Invoke marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_tEC6A7B7CABA99C386F054F01E498AEC426CF8023_marshaled_pinvoke
{
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___m_GameObject_0;
	BaseRaycaster_t7DC8158FD3CA0193455344379DD5FF7CD5F1F832* ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___worldPosition_7;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___worldNormal_8;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___screenPosition_9;
	int32_t ___displayIndex_10;
};
// Native definition for COM marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_tEC6A7B7CABA99C386F054F01E498AEC426CF8023_marshaled_com
{
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___m_GameObject_0;
	BaseRaycaster_t7DC8158FD3CA0193455344379DD5FF7CD5F1F832* ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___worldPosition_7;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___worldNormal_8;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___screenPosition_9;
	int32_t ___displayIndex_10;
};

// Lean.Common.LeanFormatString/StringEvent
struct StringEvent_t12A6CB6ADC4D0C58126DC29CA2AB35D8FD93E161  : public UnityEvent_1_tC9859540CF1468306CAB6D758C0A0D95DBCEC257
{
};

// Lean.Common.LeanSelect/LeanSelectableEvent
struct LeanSelectableEvent_tE1BC7E60B77ABC37DF10F63761C513AB4EBEC09D  : public UnityEvent_1_t0C1738C60D92A522CB1FF78F9CE867EE6CD8CD31
{
};

// Lean.Common.LeanSelectable/LeanSelectEvent
struct LeanSelectEvent_tE1B7F7125473C6F40463EC6C88B8842A0404C34F  : public UnityEvent_1_tE3623D0B5417AD4C7F446399441E2E6214BD3108
{
};

// System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>
struct Enumerator_tFEA671794CD7ED9545DB6E9B1D2E744410E5EB1C 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::_list
	List_1_t8292C421BBB00D7661DC07462822936152BAB446* ____list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::_index
	int32_t ____index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::_version
	int32_t ____version_2;
	// T System.Collections.Generic.List`1/Enumerator::_current
	RaycastResult_tEC6A7B7CABA99C386F054F01E498AEC426CF8023 ____current_3;
};

// UnityEngine.Component
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// UnityEngine.GameObject
struct GameObject_t76FEDD663AB33C991A9C9A23129337651094216F  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771* ___delegates_13;
};
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_13;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_13;
};

// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t9670F3C7D823CCB738A1604C72A1EB90292396FB  : public BaseEventData_tE03A848325C0AE8E76C6CA15FD86395EBF83364F
{
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<pointerEnter>k__BackingField
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___U3CpointerEnterU3Ek__BackingField_2;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::m_PointerPress
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___m_PointerPress_3;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<lastPress>k__BackingField
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___U3ClastPressU3Ek__BackingField_4;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<rawPointerPress>k__BackingField
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___U3CrawPointerPressU3Ek__BackingField_5;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<pointerDrag>k__BackingField
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___U3CpointerDragU3Ek__BackingField_6;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<pointerClick>k__BackingField
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___U3CpointerClickU3Ek__BackingField_7;
	// UnityEngine.EventSystems.RaycastResult UnityEngine.EventSystems.PointerEventData::<pointerCurrentRaycast>k__BackingField
	RaycastResult_tEC6A7B7CABA99C386F054F01E498AEC426CF8023 ___U3CpointerCurrentRaycastU3Ek__BackingField_8;
	// UnityEngine.EventSystems.RaycastResult UnityEngine.EventSystems.PointerEventData::<pointerPressRaycast>k__BackingField
	RaycastResult_tEC6A7B7CABA99C386F054F01E498AEC426CF8023 ___U3CpointerPressRaycastU3Ek__BackingField_9;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> UnityEngine.EventSystems.PointerEventData::hovered
	List_1_tB951CE80B58D1BF9650862451D8DAD8C231F207B* ___hovered_10;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<eligibleForClick>k__BackingField
	bool ___U3CeligibleForClickU3Ek__BackingField_11;
	// System.Int32 UnityEngine.EventSystems.PointerEventData::<pointerId>k__BackingField
	int32_t ___U3CpointerIdU3Ek__BackingField_12;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<position>k__BackingField
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___U3CpositionU3Ek__BackingField_13;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<delta>k__BackingField
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___U3CdeltaU3Ek__BackingField_14;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<pressPosition>k__BackingField
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___U3CpressPositionU3Ek__BackingField_15;
	// UnityEngine.Vector3 UnityEngine.EventSystems.PointerEventData::<worldPosition>k__BackingField
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___U3CworldPositionU3Ek__BackingField_16;
	// UnityEngine.Vector3 UnityEngine.EventSystems.PointerEventData::<worldNormal>k__BackingField
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___U3CworldNormalU3Ek__BackingField_17;
	// System.Single UnityEngine.EventSystems.PointerEventData::<clickTime>k__BackingField
	float ___U3CclickTimeU3Ek__BackingField_18;
	// System.Int32 UnityEngine.EventSystems.PointerEventData::<clickCount>k__BackingField
	int32_t ___U3CclickCountU3Ek__BackingField_19;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<scrollDelta>k__BackingField
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___U3CscrollDeltaU3Ek__BackingField_20;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<useDragThreshold>k__BackingField
	bool ___U3CuseDragThresholdU3Ek__BackingField_21;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<dragging>k__BackingField
	bool ___U3CdraggingU3Ek__BackingField_22;
	// UnityEngine.EventSystems.PointerEventData/InputButton UnityEngine.EventSystems.PointerEventData::<button>k__BackingField
	int32_t ___U3CbuttonU3Ek__BackingField_23;
	// System.Single UnityEngine.EventSystems.PointerEventData::<pressure>k__BackingField
	float ___U3CpressureU3Ek__BackingField_24;
	// System.Single UnityEngine.EventSystems.PointerEventData::<tangentialPressure>k__BackingField
	float ___U3CtangentialPressureU3Ek__BackingField_25;
	// System.Single UnityEngine.EventSystems.PointerEventData::<altitudeAngle>k__BackingField
	float ___U3CaltitudeAngleU3Ek__BackingField_26;
	// System.Single UnityEngine.EventSystems.PointerEventData::<azimuthAngle>k__BackingField
	float ___U3CazimuthAngleU3Ek__BackingField_27;
	// System.Single UnityEngine.EventSystems.PointerEventData::<twist>k__BackingField
	float ___U3CtwistU3Ek__BackingField_28;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<radius>k__BackingField
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___U3CradiusU3Ek__BackingField_29;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<radiusVariance>k__BackingField
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___U3CradiusVarianceU3Ek__BackingField_30;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<fullyExited>k__BackingField
	bool ___U3CfullyExitedU3Ek__BackingField_31;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<reentered>k__BackingField
	bool ___U3CreenteredU3Ek__BackingField_32;
};

// System.SystemException
struct SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295  : public Exception_t
{
};

// System.Action`1<Lean.Common.LeanSelectable>
struct Action_1_tAD366A485E434448836F3A4108059A1618C980AF  : public MulticastDelegate_t
{
};

// System.Action`1<System.Object>
struct Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87  : public MulticastDelegate_t
{
};

// System.Action`2<Lean.Common.LeanSelect,Lean.Common.LeanSelectable>
struct Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190  : public MulticastDelegate_t
{
};

// System.Action`2<System.Object,System.Object>
struct Action_2_t156C43F079E7E68155FCDCD12DC77DD11AEF7E3C  : public MulticastDelegate_t
{
};

// UnityEngine.Events.UnityAction`1<Lean.Common.LeanSelect>
struct UnityAction_1_tA601885BAAC49BD9D535DC344F67685863BD4573  : public MulticastDelegate_t
{
};

// System.ArgumentException
struct ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263  : public SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295
{
	// System.String System.ArgumentException::_paramName
	String_t* ____paramName_18;
};

// UnityEngine.Behaviour
struct Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// UnityEngine.Collider
struct Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// System.IndexOutOfRangeException
struct IndexOutOfRangeException_t7ECB35264FB6CA8FAA516BD958F4B2ADC78E8A82  : public SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295
{
};

// UnityEngine.Renderer
struct Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// UnityEngine.Transform
struct Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// System.ArgumentOutOfRangeException
struct ArgumentOutOfRangeException_tEA2822DAF62B10EEED00E0E3A341D4BAF78CF85F  : public ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263
{
	// System.Object System.ArgumentOutOfRangeException::_actualValue
	RuntimeObject* ____actualValue_19;
};

// UnityEngine.Camera
struct Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

// UnityEngine.LineRenderer
struct LineRenderer_tEFEF960672DB69CB14B6D181FAE6292F0CF8B63D  : public Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF
{
};

// UnityEngine.MonoBehaviour
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B  : public Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF
{
	// UnityEngine.Events.UnityEvent`1<UnityEngine.SpriteRenderer> UnityEngine.SpriteRenderer::m_SpriteChangeEvent
	UnityEvent_1_t8ABE5544759145B8D7A09F1C54FFCB6907EDD56E* ___m_SpriteChangeEvent_4;
};

// Lean.Common.LeanDestroy
struct LeanDestroy_tB9D1797F553C4D87243711A8C9777654CE71E9B6  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// Lean.Common.LeanDestroy/ExecuteType Lean.Common.LeanDestroy::execute
	int32_t ___execute_4;
	// UnityEngine.GameObject Lean.Common.LeanDestroy::target
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___target_5;
	// System.Single Lean.Common.LeanDestroy::seconds
	float ___seconds_6;
};

// Lean.Common.LeanFormatString
struct LeanFormatString_tA7B453B58D2C7E96D7ECF908A884299F3F6563EE  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.String Lean.Common.LeanFormatString::format
	String_t* ___format_4;
	// Lean.Common.LeanFormatString/StringEvent Lean.Common.LeanFormatString::onString
	StringEvent_t12A6CB6ADC4D0C58126DC29CA2AB35D8FD93E161* ___onString_5;
};

// Lean.Common.LeanPath
struct LeanPath_t52FC47E81D073093417CE48BADFF80E5182D132F  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Lean.Common.LeanPath::Points
	List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B* ___Points_4;
	// System.Boolean Lean.Common.LeanPath::Loop
	bool ___Loop_5;
	// UnityEngine.Space Lean.Common.LeanPath::Space
	int32_t ___Space_6;
	// System.Int32 Lean.Common.LeanPath::Smoothing
	int32_t ___Smoothing_7;
	// UnityEngine.LineRenderer Lean.Common.LeanPath::Visual
	LineRenderer_tEFEF960672DB69CB14B6D181FAE6292F0CF8B63D* ___Visual_8;
};

// Lean.Common.LeanPlane
struct LeanPlane_tDB7C59EE99114AFD2E4745F6ADE3D448FCC76E31  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Boolean Lean.Common.LeanPlane::ClampX
	bool ___ClampX_4;
	// System.Single Lean.Common.LeanPlane::MinX
	float ___MinX_5;
	// System.Single Lean.Common.LeanPlane::MaxX
	float ___MaxX_6;
	// System.Boolean Lean.Common.LeanPlane::ClampY
	bool ___ClampY_7;
	// System.Single Lean.Common.LeanPlane::MinY
	float ___MinY_8;
	// System.Single Lean.Common.LeanPlane::MaxY
	float ___MaxY_9;
	// System.Single Lean.Common.LeanPlane::SnapX
	float ___SnapX_10;
	// System.Single Lean.Common.LeanPlane::SnapY
	float ___SnapY_11;
};

// Lean.Common.LeanRoll
struct LeanRoll_t205306FB01D404E1DBC7D81AC3D66A5796097934  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Single Lean.Common.LeanRoll::angle
	float ___angle_4;
	// System.Boolean Lean.Common.LeanRoll::clamp
	bool ___clamp_5;
	// System.Single Lean.Common.LeanRoll::clampMin
	float ___clampMin_6;
	// System.Single Lean.Common.LeanRoll::clampMax
	float ___clampMax_7;
	// System.Single Lean.Common.LeanRoll::damping
	float ___damping_8;
	// System.Single Lean.Common.LeanRoll::currentAngle
	float ___currentAngle_9;
};

// Lean.Common.LeanSelect
struct LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Collections.Generic.LinkedListNode`1<Lean.Common.LeanSelect> Lean.Common.LeanSelect::instancesNode
	LinkedListNode_1_t7D0DA7355A32A333D2AB6F41BC1E57C6EF2AE9F7* ___instancesNode_5;
	// System.Boolean Lean.Common.LeanSelect::deselectWithNothing
	bool ___deselectWithNothing_6;
	// Lean.Common.LeanSelect/LimitType Lean.Common.LeanSelect::limit
	int32_t ___limit_7;
	// System.Int32 Lean.Common.LeanSelect::maxSelectables
	int32_t ___maxSelectables_8;
	// Lean.Common.LeanSelect/ReselectType Lean.Common.LeanSelect::reselect
	int32_t ___reselect_9;
	// System.Collections.Generic.List`1<Lean.Common.LeanSelectable> Lean.Common.LeanSelect::selectables
	List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3* ___selectables_10;
	// Lean.Common.LeanSelect/LeanSelectableEvent Lean.Common.LeanSelect::onSelected
	LeanSelectableEvent_tE1BC7E60B77ABC37DF10F63761C513AB4EBEC09D* ___onSelected_11;
	// Lean.Common.LeanSelect/LeanSelectableEvent Lean.Common.LeanSelect::onDeselected
	LeanSelectableEvent_tE1BC7E60B77ABC37DF10F63761C513AB4EBEC09D* ___onDeselected_12;
	// UnityEngine.Events.UnityEvent Lean.Common.LeanSelect::onNothing
	UnityEvent_tDC2C3548799DBC91D1E3F3DE60083A66F4751977* ___onNothing_13;
};

// Lean.Common.LeanSelectable
struct LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Collections.Generic.LinkedListNode`1<Lean.Common.LeanSelectable> Lean.Common.LeanSelectable::instancesNode
	LinkedListNode_1_tC4B0FFBEE56621B13FC8502EB49ADCA6A7E69B72* ___instancesNode_5;
	// System.Boolean Lean.Common.LeanSelectable::selfSelected
	bool ___selfSelected_6;
	// Lean.Common.LeanSelectable/LeanSelectEvent Lean.Common.LeanSelectable::onSelected
	LeanSelectEvent_tE1B7F7125473C6F40463EC6C88B8842A0404C34F* ___onSelected_7;
	// Lean.Common.LeanSelectable/LeanSelectEvent Lean.Common.LeanSelectable::onDeselected
	LeanSelectEvent_tE1B7F7125473C6F40463EC6C88B8842A0404C34F* ___onDeselected_8;
};

// Lean.Common.LeanSelectableBehaviour
struct LeanSelectableBehaviour_t07650DA7B5AA5AEACF3FA60433E5C36E1A679CD9  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// Lean.Common.LeanSelectable Lean.Common.LeanSelectableBehaviour::selectable
	LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* ___selectable_4;
};

// Lean.Common.LeanSpawn
struct LeanSpawn_t9B3DF6A3500CC180061ACF23149F40C2F2439801  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.Transform Lean.Common.LeanSpawn::prefab
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___prefab_4;
	// Lean.Common.LeanSpawn/SourceType Lean.Common.LeanSpawn::defaultPosition
	int32_t ___defaultPosition_5;
	// Lean.Common.LeanSpawn/SourceType Lean.Common.LeanSpawn::defaultRotation
	int32_t ___defaultRotation_6;
};

// UnityEngine.EventSystems.UIBehaviour
struct UIBehaviour_tB9D4295827BD2EEDEF0749200C6CA7090C742A9D  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
};

// UnityEngine.EventSystems.EventSystem
struct EventSystem_t61C51380B105BE9D2C39C4F15B7E655659957707  : public UIBehaviour_tB9D4295827BD2EEDEF0749200C6CA7090C742A9D
{
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule> UnityEngine.EventSystems.EventSystem::m_SystemInputModules
	List_1_tA5BDE435C735A082941CD33D212F97F4AE9FA55F* ___m_SystemInputModules_4;
	// UnityEngine.EventSystems.BaseInputModule UnityEngine.EventSystems.EventSystem::m_CurrentInputModule
	BaseInputModule_tF3B7C22AF1419B2AC9ECE6589357DC1B88ED96B1* ___m_CurrentInputModule_5;
	// UnityEngine.GameObject UnityEngine.EventSystems.EventSystem::m_FirstSelected
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___m_FirstSelected_7;
	// System.Boolean UnityEngine.EventSystems.EventSystem::m_sendNavigationEvents
	bool ___m_sendNavigationEvents_8;
	// System.Int32 UnityEngine.EventSystems.EventSystem::m_DragThreshold
	int32_t ___m_DragThreshold_9;
	// UnityEngine.GameObject UnityEngine.EventSystems.EventSystem::m_CurrentSelected
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___m_CurrentSelected_10;
	// System.Boolean UnityEngine.EventSystems.EventSystem::m_HasFocus
	bool ___m_HasFocus_11;
	// System.Boolean UnityEngine.EventSystems.EventSystem::m_SelectionGuard
	bool ___m_SelectionGuard_12;
	// UnityEngine.EventSystems.BaseEventData UnityEngine.EventSystems.EventSystem::m_DummyData
	BaseEventData_tE03A848325C0AE8E76C6CA15FD86395EBF83364F* ___m_DummyData_13;
};

// UnityEngine.UI.Graphic
struct Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931  : public UIBehaviour_tB9D4295827BD2EEDEF0749200C6CA7090C742A9D
{
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipLayoutUpdate
	bool ___m_SkipLayoutUpdate_8;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipMaterialUpdate
	bool ___m_SkipMaterialUpdate_9;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_10;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTargetCache
	bool ___m_RaycastTargetCache_11;
	// UnityEngine.Vector4 UnityEngine.UI.Graphic::m_RaycastPadding
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___m_RaycastPadding_12;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___m_RectTransform_13;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tAB9A55A976C4E3B2B37D0CE5616E5685A8B43860* ___m_CanvasRenderer_14;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26* ___m_Canvas_15;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_16;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_17;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___m_OnDirtyLayoutCallback_18;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___m_OnDirtyVertsCallback_19;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___m_OnDirtyMaterialCallback_20;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::m_CachedMesh
	Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* ___m_CachedMesh_23;
	// UnityEngine.Vector2[] UnityEngine.UI.Graphic::m_CachedUvs
	Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* ___m_CachedUvs_24;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t5BB0582F926E75E2FE795492679A6CF55A4B4BC4* ___m_ColorTweenRunner_25;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_26;
};

// Lean.Common.LeanSelectableGraphicColor
struct LeanSelectableGraphicColor_tFDDBE08F9483FC2CB8E1511BEEE1AF8F5601345A  : public LeanSelectableBehaviour_t07650DA7B5AA5AEACF3FA60433E5C36E1A679CD9
{
	// UnityEngine.Color Lean.Common.LeanSelectableGraphicColor::defaultColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___defaultColor_5;
	// UnityEngine.Color Lean.Common.LeanSelectableGraphicColor::selectedColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___selectedColor_6;
	// UnityEngine.UI.Graphic Lean.Common.LeanSelectableGraphicColor::cachedGraphic
	Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931* ___cachedGraphic_7;
};

// Lean.Common.LeanSelectableRendererColor
struct LeanSelectableRendererColor_tDD4EBBF7DAB508DD4FC536B35A7F8DB48142433E  : public LeanSelectableBehaviour_t07650DA7B5AA5AEACF3FA60433E5C36E1A679CD9
{
	// UnityEngine.Color Lean.Common.LeanSelectableRendererColor::defaultColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___defaultColor_5;
	// UnityEngine.Color Lean.Common.LeanSelectableRendererColor::selectedColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___selectedColor_6;
	// UnityEngine.Renderer Lean.Common.LeanSelectableRendererColor::cachedRenderer
	Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* ___cachedRenderer_7;
	// UnityEngine.MaterialPropertyBlock Lean.Common.LeanSelectableRendererColor::properties
	MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D* ___properties_8;
};

// Lean.Common.LeanSelectableSpriteRendererColor
struct LeanSelectableSpriteRendererColor_tAD5AA41024B15A0A25E574475377A2669143886E  : public LeanSelectableBehaviour_t07650DA7B5AA5AEACF3FA60433E5C36E1A679CD9
{
	// UnityEngine.Color Lean.Common.LeanSelectableSpriteRendererColor::defaultColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___defaultColor_5;
	// UnityEngine.Color Lean.Common.LeanSelectableSpriteRendererColor::selectedColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___selectedColor_6;
	// UnityEngine.SpriteRenderer Lean.Common.LeanSelectableSpriteRendererColor::cachedSpriteRenderer
	SpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B* ___cachedSpriteRenderer_7;
};

// System.Collections.Generic.LinkedListNode`1<Lean.Common.LeanSelect>

// System.Collections.Generic.LinkedListNode`1<Lean.Common.LeanSelect>

// System.Collections.Generic.LinkedListNode`1<Lean.Common.LeanSelectable>

// System.Collections.Generic.LinkedListNode`1<Lean.Common.LeanSelectable>

// System.Collections.Generic.LinkedList`1<Lean.Common.LeanSelect>

// System.Collections.Generic.LinkedList`1<Lean.Common.LeanSelect>

// System.Collections.Generic.LinkedList`1<Lean.Common.LeanSelectable>

// System.Collections.Generic.LinkedList`1<Lean.Common.LeanSelectable>

// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.GameObject,System.Int32>>
struct List_1_tC2267FBE21298A38669F48AD07AE8540B7AA80AF_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	KeyValuePair_2U5BU5D_t8F573E762700321B5950A4ACCC53564FBE075358* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.GameObject,System.Int32>>

// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct List_1_tA61AD775C53D69BA8C4CB85F6A1A7866AAC6997B_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	KeyValuePair_2U5BU5D_tF5EFD20ACC3BB6CE90DBB51F3461B2C8F1D2E2F3* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>

// System.Collections.Generic.List`1<Lean.Common.LeanSelectable>
struct List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	LeanSelectableU5BU5D_t53DEC4F400E60F6A978C470F939227BE2303931A* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<Lean.Common.LeanSelectable>

// System.Collections.Generic.List`1<System.Object>
struct List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<System.Object>

// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t8292C421BBB00D7661DC07462822936152BAB446_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	RaycastResultU5BU5D_tEAF6B3C3088179304676571328CBB001D8CECBC7* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>

// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<UnityEngine.Vector3>

// UnityEngine.EventSystems.AbstractEventData

// UnityEngine.EventSystems.AbstractEventData

// Lean.Common.LeanCommon

// Lean.Common.LeanCommon

// System.String
struct String_t_StaticFields
{
	// System.String System.String::Empty
	String_t* ___Empty_6;
};

// System.String

// UnityEngine.Events.UnityEventBase

// UnityEngine.Events.UnityEventBase

// System.ValueType

// System.ValueType

// System.Collections.Generic.LinkedList`1/Enumerator<Lean.Common.LeanSelect>

// System.Collections.Generic.LinkedList`1/Enumerator<Lean.Common.LeanSelect>

// System.Collections.Generic.LinkedList`1/Enumerator<Lean.Common.LeanSelectable>

// System.Collections.Generic.LinkedList`1/Enumerator<Lean.Common.LeanSelectable>

// System.Collections.Generic.LinkedList`1/Enumerator<System.Object>

// System.Collections.Generic.LinkedList`1/Enumerator<System.Object>

// System.Collections.Generic.KeyValuePair`2<UnityEngine.GameObject,System.Int32>

// System.Collections.Generic.KeyValuePair`2<UnityEngine.GameObject,System.Int32>

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>

// UnityEngine.Events.UnityEvent`1<Lean.Common.LeanSelect>

// UnityEngine.Events.UnityEvent`1<Lean.Common.LeanSelect>

// UnityEngine.Events.UnityEvent`1<Lean.Common.LeanSelectable>

// UnityEngine.Events.UnityEvent`1<Lean.Common.LeanSelectable>

// UnityEngine.Events.UnityEvent`1<System.String>

// UnityEngine.Events.UnityEvent`1<System.String>

// UnityEngine.EventSystems.BaseEventData

// UnityEngine.EventSystems.BaseEventData

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_StaticFields
{
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;
};

// System.Boolean

// UnityEngine.Color

// UnityEngine.Color

// System.Double

// System.Double

// System.Int32

// System.Int32

// System.IntPtr
struct IntPtr_t_StaticFields
{
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;
};

// System.IntPtr

// UnityEngine.LayerMask

// UnityEngine.LayerMask

// UnityEngine.Mathf
struct Mathf_tE284D016E3B297B72311AAD9EB8F0E643F6A4682_StaticFields
{
	// System.Single UnityEngine.Mathf::Epsilon
	float ___Epsilon_0;
};

// UnityEngine.Mathf

// UnityEngine.Quaternion
struct Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_StaticFields
{
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___identityQuaternion_4;
};

// UnityEngine.Quaternion

// System.Single

// System.Single

// UnityEngine.Events.UnityEvent

// UnityEngine.Events.UnityEvent

// UnityEngine.Vector2
struct Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_StaticFields
{
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___negativeInfinityVector_9;
};

// UnityEngine.Vector2

// UnityEngine.Vector3
struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_StaticFields
{
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___negativeInfinityVector_14;
};

// UnityEngine.Vector3

// UnityEngine.Vector4
struct Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3_StaticFields
{
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___negativeInfinityVector_8;
};

// UnityEngine.Vector4

// System.Void

// System.Void

// UnityEngine.EventSystems.EventSystem/UIToolkitOverrideConfig

// UnityEngine.EventSystems.EventSystem/UIToolkitOverrideConfig

// System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<UnityEngine.GameObject,System.Int32>>

// System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<UnityEngine.GameObject,System.Int32>>

// System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>

// System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>

// System.Delegate

// System.Delegate

// System.Exception
struct Exception_t_StaticFields
{
	// System.Object System.Exception::s_EDILock
	RuntimeObject* ___s_EDILock_0;
};

// System.Exception

// Lean.Common.LeanScreenQuery
struct LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_StaticFields
{
	// UnityEngine.RaycastHit[] Lean.Common.LeanScreenQuery::raycastHits
	RaycastHitU5BU5D_t008B8309DE422FE7567068D743D68054D5EBF1A8* ___raycastHits_6;
	// UnityEngine.RaycastHit2D[] Lean.Common.LeanScreenQuery::raycastHit2Ds
	RaycastHit2DU5BU5D_t28739C686586993113318B63C84927FD43063FC7* ___raycastHit2Ds_7;
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> Lean.Common.LeanScreenQuery::tempRaycastResults
	List_1_t8292C421BBB00D7661DC07462822936152BAB446* ___tempRaycastResults_8;
	// UnityEngine.EventSystems.PointerEventData Lean.Common.LeanScreenQuery::tempPointerEventData
	PointerEventData_t9670F3C7D823CCB738A1604C72A1EB90292396FB* ___tempPointerEventData_9;
	// UnityEngine.EventSystems.EventSystem Lean.Common.LeanScreenQuery::tempEventSystem
	EventSystem_t61C51380B105BE9D2C39C4F15B7E655659957707* ___tempEventSystem_10;
	// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.GameObject,System.Int32>> Lean.Common.LeanScreenQuery::tempLayers
	List_1_tC2267FBE21298A38669F48AD07AE8540B7AA80AF* ___tempLayers_11;
};

// Lean.Common.LeanScreenQuery

// UnityEngine.MaterialPropertyBlock

// UnityEngine.MaterialPropertyBlock

// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_StaticFields
{
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;
};

// UnityEngine.Object

// UnityEngine.Ray

// UnityEngine.Ray

// UnityEngine.RaycastHit

// UnityEngine.RaycastHit

// UnityEngine.RaycastHit2D

// UnityEngine.RaycastHit2D

// UnityEngine.EventSystems.RaycastResult

// UnityEngine.EventSystems.RaycastResult

// Lean.Common.LeanFormatString/StringEvent

// Lean.Common.LeanFormatString/StringEvent

// Lean.Common.LeanSelect/LeanSelectableEvent

// Lean.Common.LeanSelect/LeanSelectableEvent

// Lean.Common.LeanSelectable/LeanSelectEvent

// Lean.Common.LeanSelectable/LeanSelectEvent

// System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>

// System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>

// UnityEngine.Component

// UnityEngine.Component

// UnityEngine.GameObject

// UnityEngine.GameObject

// System.MulticastDelegate

// System.MulticastDelegate

// UnityEngine.EventSystems.PointerEventData

// UnityEngine.EventSystems.PointerEventData

// System.SystemException

// System.SystemException

// System.Action`1<Lean.Common.LeanSelectable>

// System.Action`1<Lean.Common.LeanSelectable>

// System.Action`1<System.Object>

// System.Action`1<System.Object>

// System.Action`2<Lean.Common.LeanSelect,Lean.Common.LeanSelectable>

// System.Action`2<Lean.Common.LeanSelect,Lean.Common.LeanSelectable>

// System.Action`2<System.Object,System.Object>

// System.Action`2<System.Object,System.Object>

// UnityEngine.Events.UnityAction`1<Lean.Common.LeanSelect>

// UnityEngine.Events.UnityAction`1<Lean.Common.LeanSelect>

// System.ArgumentException

// System.ArgumentException

// UnityEngine.Behaviour

// UnityEngine.Behaviour

// UnityEngine.Collider

// UnityEngine.Collider

// System.IndexOutOfRangeException

// System.IndexOutOfRangeException

// UnityEngine.Renderer

// UnityEngine.Renderer

// UnityEngine.Transform

// UnityEngine.Transform

// System.ArgumentOutOfRangeException

// System.ArgumentOutOfRangeException

// UnityEngine.Camera
struct Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184_StaticFields
{
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t844E527BFE37BC0495E7F67993E43C07642DA9DD* ___onPreCull_4;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t844E527BFE37BC0495E7F67993E43C07642DA9DD* ___onPreRender_5;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t844E527BFE37BC0495E7F67993E43C07642DA9DD* ___onPostRender_6;
};

// UnityEngine.Camera

// UnityEngine.LineRenderer

// UnityEngine.LineRenderer

// UnityEngine.MonoBehaviour

// UnityEngine.MonoBehaviour

// UnityEngine.SpriteRenderer

// UnityEngine.SpriteRenderer

// Lean.Common.LeanDestroy

// Lean.Common.LeanDestroy

// Lean.Common.LeanFormatString

// Lean.Common.LeanFormatString

// Lean.Common.LeanPath
struct LeanPath_t52FC47E81D073093417CE48BADFF80E5182D132F_StaticFields
{
	// UnityEngine.Vector3 Lean.Common.LeanPath::LastWorldNormal
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___LastWorldNormal_9;
};

// Lean.Common.LeanPath

// Lean.Common.LeanPlane

// Lean.Common.LeanPlane

// Lean.Common.LeanRoll

// Lean.Common.LeanRoll

// Lean.Common.LeanSelect
struct LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_StaticFields
{
	// System.Collections.Generic.LinkedList`1<Lean.Common.LeanSelect> Lean.Common.LeanSelect::Instances
	LinkedList_1_t74DD3E2689E35FC0DF8DF1971B5CD529EEE8CAA1* ___Instances_4;
	// System.Action`2<Lean.Common.LeanSelect,Lean.Common.LeanSelectable> Lean.Common.LeanSelect::OnAnySelected
	Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* ___OnAnySelected_14;
	// System.Action`2<Lean.Common.LeanSelect,Lean.Common.LeanSelectable> Lean.Common.LeanSelect::OnAnyDeselected
	Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* ___OnAnyDeselected_15;
};

// Lean.Common.LeanSelect

// Lean.Common.LeanSelectable
struct LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_StaticFields
{
	// System.Collections.Generic.LinkedList`1<Lean.Common.LeanSelectable> Lean.Common.LeanSelectable::Instances
	LinkedList_1_t25474EE5265E341D73136D6D26BA9381A1E5361C* ___Instances_4;
	// System.Action`1<Lean.Common.LeanSelectable> Lean.Common.LeanSelectable::OnAnyEnabled
	Action_1_tAD366A485E434448836F3A4108059A1618C980AF* ___OnAnyEnabled_9;
	// System.Action`1<Lean.Common.LeanSelectable> Lean.Common.LeanSelectable::OnAnyDisabled
	Action_1_tAD366A485E434448836F3A4108059A1618C980AF* ___OnAnyDisabled_10;
	// System.Action`2<Lean.Common.LeanSelect,Lean.Common.LeanSelectable> Lean.Common.LeanSelectable::OnAnySelected
	Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* ___OnAnySelected_11;
	// System.Action`2<Lean.Common.LeanSelect,Lean.Common.LeanSelectable> Lean.Common.LeanSelectable::OnAnyDeselected
	Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* ___OnAnyDeselected_12;
	// System.Collections.Generic.List`1<Lean.Common.LeanSelectable> Lean.Common.LeanSelectable::tempSelectables
	List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3* ___tempSelectables_13;
};

// Lean.Common.LeanSelectable

// Lean.Common.LeanSelectableBehaviour

// Lean.Common.LeanSelectableBehaviour

// Lean.Common.LeanSpawn

// Lean.Common.LeanSpawn

// UnityEngine.EventSystems.UIBehaviour

// UnityEngine.EventSystems.UIBehaviour

// UnityEngine.EventSystems.EventSystem
struct EventSystem_t61C51380B105BE9D2C39C4F15B7E655659957707_StaticFields
{
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventSystem> UnityEngine.EventSystems.EventSystem::m_EventSystems
	List_1_tF2FE88545EFEC788CAAE6C74EC2F78E937FCCAC3* ___m_EventSystems_6;
	// System.Comparison`1<UnityEngine.EventSystems.RaycastResult> UnityEngine.EventSystems.EventSystem::s_RaycastComparer
	Comparison_1_t9FCAC8C8CE160A96C5AAD2DE1D353DCE8A2FEEFC* ___s_RaycastComparer_14;
	// UnityEngine.EventSystems.EventSystem/UIToolkitOverrideConfig UnityEngine.EventSystems.EventSystem::s_UIToolkitOverride
	UIToolkitOverrideConfig_t4E6B4528E38BCA7DA72C45424634806200A50182 ___s_UIToolkitOverride_15;
};

// UnityEngine.EventSystems.EventSystem

// UnityEngine.UI.Graphic
struct Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931_StaticFields
{
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* ___s_Mesh_21;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_tB905FCB02AE67CBEE5F265FE37A5938FC5D136FE* ___s_VertexHelper_22;
};

// UnityEngine.UI.Graphic

// Lean.Common.LeanSelectableGraphicColor

// Lean.Common.LeanSelectableGraphicColor

// Lean.Common.LeanSelectableRendererColor

// Lean.Common.LeanSelectableRendererColor

// Lean.Common.LeanSelectableSpriteRendererColor

// Lean.Common.LeanSelectableSpriteRendererColor
#ifdef __clang__
#pragma clang diagnostic pop
#endif
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t008B8309DE422FE7567068D743D68054D5EBF1A8  : public RuntimeArray
{
	ALIGN_FIELD (8) RaycastHit_t6F30BD0B38B56401CA833A1B87BD74F2ACD2F2B5 m_Items[1];

	inline RaycastHit_t6F30BD0B38B56401CA833A1B87BD74F2ACD2F2B5 GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RaycastHit_t6F30BD0B38B56401CA833A1B87BD74F2ACD2F2B5* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RaycastHit_t6F30BD0B38B56401CA833A1B87BD74F2ACD2F2B5 value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline RaycastHit_t6F30BD0B38B56401CA833A1B87BD74F2ACD2F2B5 GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RaycastHit_t6F30BD0B38B56401CA833A1B87BD74F2ACD2F2B5* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RaycastHit_t6F30BD0B38B56401CA833A1B87BD74F2ACD2F2B5 value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t28739C686586993113318B63C84927FD43063FC7  : public RuntimeArray
{
	ALIGN_FIELD (8) RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA m_Items[1];

	inline RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C  : public RuntimeArray
{
	ALIGN_FIELD (8) Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 m_Items[1];

	inline Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918  : public RuntimeArray
{
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>[]
struct KeyValuePair_2U5BU5D_tF5EFD20ACC3BB6CE90DBB51F3461B2C8F1D2E2F3  : public RuntimeArray
{
	ALIGN_FIELD (8) KeyValuePair_2_tF11CA6D20F09EC4DAB7CB3C2C394F6F2C394E6B8 m_Items[1];

	inline KeyValuePair_2_tF11CA6D20F09EC4DAB7CB3C2C394F6F2C394E6B8 GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline KeyValuePair_2_tF11CA6D20F09EC4DAB7CB3C2C394F6F2C394E6B8* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_tF11CA6D20F09EC4DAB7CB3C2C394F6F2C394E6B8 value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___key_0), (void*)NULL);
	}
	inline KeyValuePair_2_tF11CA6D20F09EC4DAB7CB3C2C394F6F2C394E6B8 GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline KeyValuePair_2_tF11CA6D20F09EC4DAB7CB3C2C394F6F2C394E6B8* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, KeyValuePair_2_tF11CA6D20F09EC4DAB7CB3C2C394F6F2C394E6B8 value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___key_0), (void*)NULL);
	}
};


// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::Invoke(T0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_1_Invoke_m6CDC8B0639CE8935E2E13D10B2C8E500968130B6_gshared (UnityEvent_1_t3CE03B42D5873C0C0E0692BEE72E1E6D5399F205* __this, RuntimeObject* ___0_arg0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_1__ctor_m8D77F4F05F69D0E52E8A445322811EEC25987525_gshared (UnityEvent_1_t3CE03B42D5873C0C0E0692BEE72E1E6D5399F205* __this, const RuntimeMethod* method) ;
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m46EEFFA770BE665EA0CB3A5332E941DA4B3C1D37_gshared_inline (List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B* __this, const RuntimeMethod* method) ;
// T System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 List_1_get_Item_m8F2E15FC96DA75186C51228128A0660709E4E810_gshared (List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B* __this, int32_t ___0_index, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_mC54E2BCBE43279A96FC082F5CDE2D76388BD8F9C_gshared (List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Clear()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Clear_m455780C5A45049F9BDC25EAD3BA10A681D16385D_gshared_inline (List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(T)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Add_m79E50C4F592B1703F4B76A8BE7B4855515460CA1_gshared_inline (List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_item, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m7F078BB342729BDF11327FD89D7872265328F690_gshared (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.List`1<System.Object>::Contains(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool List_1_Contains_m4C9139C2A6B23E9343D3F87807B32C6E2CFE660D_gshared (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, RuntimeObject* ___0_item, const RuntimeMethod* method) ;
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m4407E4C389F22B8CEC282C15D56516658746C383_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, const RuntimeMethod* method) ;
// T System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* List_1_get_Item_m33561245D64798C2AB07584C0EC4F240E4839A38_gshared (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, int32_t ___0_index, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.Object>::Add(T)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Add_mEBCF994CC3814631017F46A387B1A192ED6C85C7_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, RuntimeObject* ___0_item, const RuntimeMethod* method) ;
// System.Void System.Action`2<System.Object,System.Object>::Invoke(T1,T2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Action_2_Invoke_m7BFCE0BBCF67689D263059B56A8D79161B698587_gshared_inline (Action_2_t156C43F079E7E68155FCDCD12DC77DD11AEF7E3C* __this, RuntimeObject* ___0_arg1, RuntimeObject* ___1_arg2, const RuntimeMethod* method) ;
// System.Int32 System.Collections.Generic.List`1<System.Object>::IndexOf(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t List_1_IndexOf_m378F61BA812B79DEE58D86FE8AA9F20E3FC7D85F_gshared (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, RuntimeObject* ___0_item, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.Object>::RemoveAt(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_RemoveAt_m54F62297ADEE4D4FDA697F49ED807BF901201B54_gshared (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, int32_t ___0_index, const RuntimeMethod* method) ;
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::AddLast(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR LinkedListNode_1_t293BB098D459DDAE6A26977D0731A997186D1D4C* LinkedList_1_AddLast_mF5239C871EADC44D51C6B621592A9CAC43449A2E_gshared (LinkedList_1_t49DC5CF34D4D642E6417F1245CDEC26A32F60C76* __this, RuntimeObject* ___0_value, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LinkedList_1_Remove_m6B592B94D9AEF003EAE59FCB5455DA67AB4E423C_gshared (LinkedList_1_t49DC5CF34D4D642E6417F1245CDEC26A32F60C76* __this, LinkedListNode_1_t293BB098D459DDAE6A26977D0731A997186D1D4C* ___0_node, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LinkedList_1__ctor_m2732A2EC5597469086D48C79A12B3563DEA501C5_gshared (LinkedList_1_t49DC5CF34D4D642E6417F1245CDEC26A32F60C76* __this, const RuntimeMethod* method) ;
// System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1<System.Object>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Enumerator_tC25D6382B2C7E2606E12FC6637F714A98D52DE22 LinkedList_1_GetEnumerator_m2522814971CB421FDF996D386A650A3F8FA0E30C_gshared (LinkedList_1_t49DC5CF34D4D642E6417F1245CDEC26A32F60C76* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enumerator_Dispose_m9E60B472E2E61202E9CFBED3DA607374973EA744_gshared (Enumerator_tC25D6382B2C7E2606E12FC6637F714A98D52DE22* __this, const RuntimeMethod* method) ;
// T System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* Enumerator_get_Current_mCD39BA1871E5D5BE52D8AA0B27886D9B5B10BBF9_gshared_inline (Enumerator_tC25D6382B2C7E2606E12FC6637F714A98D52DE22* __this, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m5148B3CE498B6E84B883D3EB4A7A080238A2609D_gshared (Enumerator_tC25D6382B2C7E2606E12FC6637F714A98D52DE22* __this, const RuntimeMethod* method) ;
// System.Void System.Action`1<System.Object>::Invoke(T)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Action_1_Invoke_mF2422B2DD29F74CE66F791C3F68E288EC7C3DB9E_gshared_inline (Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* __this, RuntimeObject* ___0_obj, const RuntimeMethod* method) ;
// T UnityEngine.Component::GetComponentInParent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Component_GetComponentInParent_TisRuntimeObject_m6746D6BB99912B1B509746C993906492F86CD119_gshared (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Events.UnityAction`1<System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityAction_1__ctor_m0C2FC6B483B474AE9596A43EBA7FF6E85503A92A_gshared (UnityAction_1_t9C30BCD020745BF400CBACF22C6F34ADBA2DDA6A* __this, RuntimeObject* ___0_object, intptr_t ___1_method, const RuntimeMethod* method) ;
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_1_AddListener_m055233246714700E4BDAA62635BC0AA49E8165CC_gshared (UnityEvent_1_t3CE03B42D5873C0C0E0692BEE72E1E6D5399F205* __this, UnityAction_1_t9C30BCD020745BF400CBACF22C6F34ADBA2DDA6A* ___0_call, const RuntimeMethod* method) ;
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_1_RemoveListener_m904FA6BDD0D33FDF8650EF816FF5C131867E693E_gshared (UnityEvent_1_t3CE03B42D5873C0C0E0692BEE72E1E6D5399F205* __this, UnityAction_1_t9C30BCD020745BF400CBACF22C6F34ADBA2DDA6A* ___0_call, const RuntimeMethod* method) ;
// T UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Component_GetComponent_TisRuntimeObject_m7181F81CAEC2CF53F5D2BC79B7425C16E1F80D33_gshared (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method) ;
// T UnityEngine.Object::Instantiate<System.Object>(T,UnityEngine.Vector3,UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Object_Instantiate_TisRuntimeObject_m249A6BA4F2F19C2D3CE217D4D31847DF0EF03EFE_gshared (RuntimeObject* ___0_original, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___1_position, Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___2_rotation, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::.ctor(TKey,TValue)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void KeyValuePair_2__ctor_mD4B5AD912DE40BAACCC9A814640E4E3386E51BA8_gshared (KeyValuePair_2_tF11CA6D20F09EC4DAB7CB3C2C394F6F2C394E6B8* __this, RuntimeObject* ___0_key, int32_t ___1_value, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::Add(T)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Add_m9BAF9FC9B01B86AB9E16C2A54BAB69042D04B974_gshared_inline (List_1_tA61AD775C53D69BA8C4CB85F6A1A7866AAC6997B* __this, KeyValuePair_2_tF11CA6D20F09EC4DAB7CB3C2C394F6F2C394E6B8 ___0_item, const RuntimeMethod* method) ;
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Enumerator_t7F9BA7E481A1947D155FE5B42578F1117860DC39 List_1_GetEnumerator_mA470FE029F16C9C2CF4CF4B94F44C54DE0EDE84A_gshared (List_1_tA61AD775C53D69BA8C4CB85F6A1A7866AAC6997B* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enumerator_Dispose_mA741B664CFF7AF6C68A6B638319CA64348911DFD_gshared (Enumerator_t7F9BA7E481A1947D155FE5B42578F1117860DC39* __this, const RuntimeMethod* method) ;
// T System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR KeyValuePair_2_tF11CA6D20F09EC4DAB7CB3C2C394F6F2C394E6B8 Enumerator_get_Current_m7362387DF032E42E572F7041F4C1A3CD21679DCD_gshared_inline (Enumerator_t7F9BA7E481A1947D155FE5B42578F1117860DC39* __this, const RuntimeMethod* method) ;
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Key()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* KeyValuePair_2_get_Key_mADC45FA05C759E6F88D7DADDFE0C0E1ADBB3E501_gshared_inline (KeyValuePair_2_tF11CA6D20F09EC4DAB7CB3C2C394F6F2C394E6B8* __this, const RuntimeMethod* method) ;
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Value()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t KeyValuePair_2_get_Value_m7A836D9634814B22DF33AD801EA10741ABFBDFE2_gshared_inline (KeyValuePair_2_tF11CA6D20F09EC4DAB7CB3C2C394F6F2C394E6B8* __this, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_mDE2B6C6EB1965BF7A51BC0785BAA55A818EDD46F_gshared (Enumerator_t7F9BA7E481A1947D155FE5B42578F1117860DC39* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::Clear()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Clear_mB511E4F0FAD5BBBFD635CE5E636119104E14A316_gshared_inline (List_1_tA61AD775C53D69BA8C4CB85F6A1A7866AAC6997B* __this, const RuntimeMethod* method) ;
// T UnityEngine.Object::FindObjectOfType<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Object_FindObjectOfType_TisRuntimeObject_m02DFBF011F3B59F777A5E521DB2A116DD496E968_gshared (const RuntimeMethod* method) ;
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Enumerator_tFEA671794CD7ED9545DB6E9B1D2E744410E5EB1C List_1_GetEnumerator_m5032C7535D90065B4FA0A5942E7F92F4D72B8D41_gshared (List_1_t8292C421BBB00D7661DC07462822936152BAB446* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enumerator_Dispose_mE3FB1E26BA0B10EAB4C06CC56F1C78002726865C_gshared (Enumerator_tFEA671794CD7ED9545DB6E9B1D2E744410E5EB1C* __this, const RuntimeMethod* method) ;
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RaycastResult_tEC6A7B7CABA99C386F054F01E498AEC426CF8023 Enumerator_get_Current_m53EF02F206B7F773103FC51D50AC1B974AE692F4_gshared_inline (Enumerator_tFEA671794CD7ED9545DB6E9B1D2E744410E5EB1C* __this, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m22E22E0108683303C9E8F060E9C970D6AECF313C_gshared (Enumerator_tFEA671794CD7ED9545DB6E9B1D2E744410E5EB1C* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_mB89D13B8B739042E3841C5DFAF30C3C51A797EA7_gshared (List_1_t8292C421BBB00D7661DC07462822936152BAB446* __this, int32_t ___0_capacity, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m28EFF96ED442DBB1D7387E7591CC371F0087B937_gshared (List_1_tA61AD775C53D69BA8C4CB85F6A1A7866AAC6997B* __this, const RuntimeMethod* method) ;

// System.Void Lean.Common.LeanDestroy::DestroyNow()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanDestroy_DestroyNow_m2F3B8BCB8ED431FCC7EE6E7317A1FD84FF92BC9E (LeanDestroy_tB9D1797F553C4D87243711A8C9777654CE71E9B6* __this, const RuntimeMethod* method) ;
// System.Single UnityEngine.Time::get_deltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_deltaTime_mC3195000401F0FD167DD2F948FD2BC58330D0865 (const RuntimeMethod* method) ;
// System.Single UnityEngine.Time::get_unscaledDeltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_unscaledDeltaTime_mF057EECA857E5C0F90A3F910D26D3EE59F27C4B5 (const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_mD0BE578448EAA61948F25C32F8DD55AB1F778602 (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___0_x, Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___1_y, const RuntimeMethod* method) ;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_mE97D0A766419A81296E8D4E5C23D01D3FE91ACBB (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___0_obj, const RuntimeMethod* method) ;
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E (MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71* __this, const RuntimeMethod* method) ;
// System.Void Lean.Common.LeanFormatString/StringEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StringEvent__ctor_m0EFECAE65155F17A84EA7E5F220261C6E03ABA0B (StringEvent_t12A6CB6ADC4D0C58126DC29CA2AB35D8FD93E161* __this, const RuntimeMethod* method) ;
// System.Void Lean.Common.LeanFormatString::SendString(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanFormatString_SendString_m9851DD3FFE8B07C743265B961F78D4F8065B9A52 (LeanFormatString_tA7B453B58D2C7E96D7ECF908A884299F3F6563EE* __this, RuntimeObject* ___0_a, const RuntimeMethod* method) ;
// System.Void Lean.Common.LeanFormatString::SendString(System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanFormatString_SendString_m07D565F1CE9F6A76EE31FFC46C3B344AD653C419 (LeanFormatString_tA7B453B58D2C7E96D7ECF908A884299F3F6563EE* __this, RuntimeObject* ___0_a, RuntimeObject* ___1_b, const RuntimeMethod* method) ;
// System.String System.String::Format(System.String,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_mA8DBB4C2516B9723C5A41E6CB1E2FAF4BBE96DD8 (String_t* ___0_format, RuntimeObject* ___1_arg0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Events.UnityEvent`1<System.String>::Invoke(T0)
inline void UnityEvent_1_Invoke_mA633B48B5D287DA856FB954AC3E4012487E63C15 (UnityEvent_1_tC9859540CF1468306CAB6D758C0A0D95DBCEC257* __this, String_t* ___0_arg0, const RuntimeMethod* method)
{
	((  void (*) (UnityEvent_1_tC9859540CF1468306CAB6D758C0A0D95DBCEC257*, String_t*, const RuntimeMethod*))UnityEvent_1_Invoke_m6CDC8B0639CE8935E2E13D10B2C8E500968130B6_gshared)(__this, ___0_arg0, method);
}
// System.String System.String::Format(System.String,System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_mFB7DA489BD99F4670881FF50EC017BFB0A5C0987 (String_t* ___0_format, RuntimeObject* ___1_arg0, RuntimeObject* ___2_arg1, const RuntimeMethod* method) ;
// System.Void UnityEngine.Events.UnityEvent`1<System.String>::.ctor()
inline void UnityEvent_1__ctor_m0F7D790DACD6F3D18E865D01FE4427603C1B0CC6 (UnityEvent_1_tC9859540CF1468306CAB6D758C0A0D95DBCEC257* __this, const RuntimeMethod* method)
{
	((  void (*) (UnityEvent_1_tC9859540CF1468306CAB6D758C0A0D95DBCEC257*, const RuntimeMethod*))UnityEvent_1__ctor_m8D77F4F05F69D0E52E8A445322811EEC25987525_gshared)(__this, method);
}
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Count()
inline int32_t List_1_get_Count_m46EEFFA770BE665EA0CB3A5332E941DA4B3C1D37_inline (List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B* __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B*, const RuntimeMethod*))List_1_get_Count_m46EEFFA770BE665EA0CB3A5332E941DA4B3C1D37_gshared_inline)(__this, method);
}
// System.Void System.IndexOutOfRangeException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IndexOutOfRangeException__ctor_m270ED9671475CE680EEA8C62A7A43308AE4188EF (IndexOutOfRangeException_t7ECB35264FB6CA8FAA516BD958F4B2ADC78E8A82* __this, const RuntimeMethod* method) ;
// System.Void System.Exception::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Exception__ctor_m203319D1EA1274689B380A947B4ADC8445662B8F (Exception_t* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector3 Lean.Common.LeanPath::GetPointRaw(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 LeanPath_GetPointRaw_mEFEC1A35269B12C264B32A53883F27E347327D19 (LeanPath_t52FC47E81D073093417CE48BADFF80E5182D132F* __this, int32_t ___0_index, int32_t ___1_count, const RuntimeMethod* method) ;
// System.Single Lean.Common.LeanPath::CubicInterpolate(System.Single,System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float LeanPath_CubicInterpolate_m1AA1DCBB91FEE95D41E5082FD32446ED979B4D84 (LeanPath_t52FC47E81D073093417CE48BADFF80E5182D132F* __this, float ___0_a, float ___1_b, float ___2_c, float ___3_d, float ___4_t, const RuntimeMethod* method) ;
// System.Void System.ArgumentOutOfRangeException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentOutOfRangeException__ctor_mB596C51BFA864B65C2CED275458FAE90F7CD29C9 (ArgumentOutOfRangeException_tEA2822DAF62B10EEED00E0E3A341D4BAF78CF85F* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector3 Lean.Common.LeanPath::GetSmoothedPoint(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 LeanPath_GetSmoothedPoint_m7284BFFDC1350CD161F27992008413A28EB6A309 (LeanPath_t52FC47E81D073093417CE48BADFF80E5182D132F* __this, float ___0_index, const RuntimeMethod* method) ;
// System.Int32 Lean.Common.LeanPath::Mod(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t LeanPath_Mod_mA52316BA69AE1DC6A8B5F15D76589B4055DCFA66 (LeanPath_t52FC47E81D073093417CE48BADFF80E5182D132F* __this, int32_t ___0_a, int32_t ___1_b, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.Mathf::Clamp(System.Int32,System.Int32,System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Mathf_Clamp_m4DC36EEFDBE5F07C16249DA568023C5ECCFF0E7B_inline (int32_t ___0_value, int32_t ___1_min, int32_t ___2_max, const RuntimeMethod* method) ;
// T System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32)
inline Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 List_1_get_Item_m8F2E15FC96DA75186C51228128A0660709E4E810 (List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B* __this, int32_t ___0_index, const RuntimeMethod* method)
{
	return ((  Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 (*) (List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B*, int32_t, const RuntimeMethod*))List_1_get_Item_m8F2E15FC96DA75186C51228128A0660709E4E810_gshared)(__this, ___0_index, method);
}
// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371 (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Transform::TransformPoint(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Transform_TransformPoint_m05BFF013DB830D7BFE44A007703694AE1062EE44 (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_position, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor()
inline void List_1__ctor_mC54E2BCBE43279A96FC082F5CDE2D76388BD8F9C (List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B* __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B*, const RuntimeMethod*))List_1__ctor_mC54E2BCBE43279A96FC082F5CDE2D76388BD8F9C_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Clear()
inline void List_1_Clear_m455780C5A45049F9BDC25EAD3BA10A681D16385D_inline (List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B* __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B*, const RuntimeMethod*))List_1_Clear_m455780C5A45049F9BDC25EAD3BA10A681D16385D_gshared_inline)(__this, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(T)
inline void List_1_Add_m79E50C4F592B1703F4B76A8BE7B4855515460CA1_inline (List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_item, const RuntimeMethod* method)
{
	((  void (*) (List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B*, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2, const RuntimeMethod*))List_1_Add_m79E50C4F592B1703F4B76A8BE7B4855515460CA1_gshared_inline)(__this, ___0_item, method);
}
// System.Int32 Lean.Common.LeanPath::GetPointCount(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t LeanPath_GetPointCount_m904AC0FF40B2AE9750C9541C035CF128F9FB6094 (LeanPath_t52FC47E81D073093417CE48BADFF80E5182D132F* __this, int32_t ___0_smoothing, const RuntimeMethod* method) ;
// UnityEngine.Vector3 Lean.Common.LeanPath::GetPoint(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 LeanPath_GetPoint_m809DDF4B617B43CB30CA6EF2C33AA8AD3A4D6659 (LeanPath_t52FC47E81D073093417CE48BADFF80E5182D132F* __this, int32_t ___0_index, int32_t ___1_smoothing, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_Subtraction_mE42023FF80067CB44A1D4A27EB7CF2B24CABB828_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_a, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___1_b, const RuntimeMethod* method) ;
// UnityEngine.Vector3 Lean.Common.LeanPath::GetClosestPoint(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 LeanPath_GetClosestPoint_m35BCB88CADA5172B750A9C1DC015CF57704CDA95 (LeanPath_t52FC47E81D073093417CE48BADFF80E5182D132F* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_position, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___1_origin, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___2_direction, const RuntimeMethod* method) ;
// System.Single UnityEngine.Vector3::Distance(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector3_Distance_m2314DB9B8BD01157E013DF87BEA557375C7F9FF9_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_a, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___1_b, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::Normalize(UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_Normalize_mEF8349CC39674236CFC694189AFD36E31F89AC8F_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_value, const RuntimeMethod* method) ;
// System.Boolean Lean.Common.LeanPath::TryGetClosest(UnityEngine.Vector3,UnityEngine.Vector3&,System.Int32&,System.Int32&,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool LeanPath_TryGetClosest_m961C2E4329FDA0D8F6B54898725ACBD69317C659 (LeanPath_t52FC47E81D073093417CE48BADFF80E5182D132F* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_position, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* ___1_closestPoint, int32_t* ___2_closestIndexA, int32_t* ___3_closestIndexB, int32_t ___4_smoothing, const RuntimeMethod* method) ;
// UnityEngine.Vector3 Lean.Common.LeanPath::GetClosestPoint(UnityEngine.Ray,UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 LeanPath_GetClosestPoint_m8C1E2BD9CAB8AD2A3D281FADAA705D2A9D3224C4 (LeanPath_t52FC47E81D073093417CE48BADFF80E5182D132F* __this, Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 ___0_ray, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___1_origin, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___2_direction, const RuntimeMethod* method) ;
// System.Single Lean.Common.LeanPath::GetClosestDistance(UnityEngine.Ray,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float LeanPath_GetClosestDistance_m25EDF5A03B96A0615B55298C1458859FC6253AC8 (LeanPath_t52FC47E81D073093417CE48BADFF80E5182D132F* __this, Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 ___0_ray, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___1_point, const RuntimeMethod* method) ;
// System.Boolean Lean.Common.LeanPath::TryGetClosest(UnityEngine.Ray,UnityEngine.Vector3&,System.Int32&,System.Int32&,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool LeanPath_TryGetClosest_mC5CDF1FF80E51741388D1DE012E07E52B722485B (LeanPath_t52FC47E81D073093417CE48BADFF80E5182D132F* __this, Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 ___0_ray, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* ___1_closestPoint, int32_t* ___2_closestIndexA, int32_t* ___3_closestIndexB, int32_t ___4_smoothing, const RuntimeMethod* method) ;
// System.Boolean Lean.Common.LeanPath::TryGetClosest(UnityEngine.Ray,UnityEngine.Vector3&,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool LeanPath_TryGetClosest_m150E1C0790AF8C436ED99E02AD665914ED0E54D8 (LeanPath_t52FC47E81D073093417CE48BADFF80E5182D132F* __this, Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 ___0_ray, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* ___1_currentPoint, int32_t ___2_smoothing, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::MoveTowards(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_MoveTowards_m0363264647799F3173AC37F8E819F98298249B08_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_current, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___1_target, float ___2_maxDistanceDelta, const RuntimeMethod* method) ;
// System.Boolean Lean.Common.LeanPath::TryGetClosest(UnityEngine.Vector3,UnityEngine.Vector3&,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool LeanPath_TryGetClosest_m445515693D3D0C0908FB0533F01DC6E59238C2B9 (LeanPath_t52FC47E81D073093417CE48BADFF80E5182D132F* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_position, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* ___1_closestPoint, int32_t ___2_smoothing, const RuntimeMethod* method) ;
// System.Single UnityEngine.Vector3::Dot(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector3_Dot_mBB86BB940AA0A32FA7D3C02AC42E5BC7095A5D52_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_lhs, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___1_rhs, const RuntimeMethod* method) ;
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Mathf_Clamp01_mA7E048DBDA832D399A581BE4D6DED9FA44CE0F14_inline (float ___0_value, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_Multiply_m87BA7C578F96C8E49BB07088DAAC4649F83B0353_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_a, float ___1_d, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_Addition_m78C0EC70CB66E8DCAC225743D82B268DAEE92067_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_a, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___1_b, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Ray::get_direction()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Ray_get_direction_m21C2D22D3BD4A683BD4DC191AB22DD05F5EC2086 (Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::Cross(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_Cross_mF93A280558BCE756D13B6CC5DCD7DE8A43148987_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_lhs, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___1_rhs, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Ray::get_origin()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Ray_get_origin_m97604A8F180316A410DCD77B7D74D04522FA1BA6 (Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Ray::GetPoint(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Ray_GetPoint_mAF4E1D38026156E6434EF2BED2420ED5236392AF (Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00* __this, float ___0_distance, const RuntimeMethod* method) ;
// System.Void UnityEngine.LineRenderer::set_positionCount(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LineRenderer_set_positionCount_m2001FB4044053895ECBE897AB833284F3300B205 (LineRenderer_tEFEF960672DB69CB14B6D181FAE6292F0CF8B63D* __this, int32_t ___0_value, const RuntimeMethod* method) ;
// System.Void UnityEngine.LineRenderer::SetPosition(System.Int32,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LineRenderer_SetPosition_m84C4AD9ADC6AC62B33DB4D7E4C9F066DFF8440C1 (LineRenderer_tEFEF960672DB69CB14B6D181FAE6292F0CF8B63D* __this, int32_t ___0_index, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___1_position, const RuntimeMethod* method) ;
// System.Void Lean.Common.LeanPath::UpdateVisual()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanPath_UpdateVisual_mC4B1C0DD129E0D88C1ED43B4F29B4A1B5DEE8202 (LeanPath_t52FC47E81D073093417CE48BADFF80E5182D132F* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::get_forward()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_get_forward_mAA55A7034304DF8B2152EAD49AE779FC4CA2EB4A_inline (const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Transform::InverseTransformPoint(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Transform_InverseTransformPoint_m18CD395144D9C78F30E15A5B82B6670E792DBA5D (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_position, const RuntimeMethod* method) ;
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Mathf_Clamp_mEB9AEA827D27D20FCC787F7375156AF46BB12BBF_inline (float ___0_value, float ___1_min, float ___2_max, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Transform::get_forward()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Transform_get_forward_mFCFACF7165FDAB21E80E384C494DF278386CEE2F (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1 (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, const RuntimeMethod* method) ;
// System.Boolean Lean.Common.LeanPlane::RayToPlane(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Ray,System.Single&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool LeanPlane_RayToPlane_mBD9C38BE30C7D51157BEAE1270C63709A453D8BB (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_point, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___1_normal, Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 ___2_ray, float* ___3_distance, const RuntimeMethod* method) ;
// UnityEngine.Vector3 Lean.Common.LeanPlane::GetClosest(UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 LeanPlane_GetClosest_m31305D784A9F385A4030A64D6145AB7E9E7B1848 (LeanPlane_tDB7C59EE99114AFD2E4745F6ADE3D448FCC76E31* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_position, float ___1_offset, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Mathf::Approximately(System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Mathf_Approximately_m1DADD012A8FC82E11FB282501AE2EBBF9A77150B_inline (float ___0_a, float ___1_b, const RuntimeMethod* method) ;
// System.Single UnityEngine.Vector2::get_sqrMagnitude()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector2_get_sqrMagnitude_mA16336720C14EEF8BA9B55AE33B98C9EE2082BDC_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* __this, const RuntimeMethod* method) ;
// System.Single CW.Common.CwHelper::DampenFactor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float CwHelper_DampenFactor_m730A8463F0CBE628CD66EF69E1B5969091015C9A (float ___0_speed, float ___1_elapsed, const RuntimeMethod* method) ;
// System.Single UnityEngine.Mathf::LerpAngle(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Mathf_LerpAngle_m0653422E15193C2E4A4E5AF05236B6315C789C23_inline (float ___0_a, float ___1_b, float ___2_t, const RuntimeMethod* method) ;
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 Quaternion_Euler_m9262AB29E3E9CE94EF71051F38A28E82AEC73F90_inline (float ___0_x, float ___1_y, float ___2_z, const RuntimeMethod* method) ;
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_rotation_m61340DE74726CF0F9946743A727C4D444397331D (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___0_value, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<Lean.Common.LeanSelectable>::.ctor()
inline void List_1__ctor_m4444F647B6C0C03F0F6CD67236CF2D7F76202E7A (List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3* __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3*, const RuntimeMethod*))List_1__ctor_m7F078BB342729BDF11327FD89D7872265328F690_gshared)(__this, method);
}
// System.Void Lean.Common.LeanSelect/LeanSelectableEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectableEvent__ctor_mF85F656B71FB189CE41973B9F90133305E4889DF (LeanSelectableEvent_tE1BC7E60B77ABC37DF10F63761C513AB4EBEC09D* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Events.UnityEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent__ctor_m03D3E5121B9A6100351984D0CE3050B909CD3235 (UnityEvent_tDC2C3548799DBC91D1E3F3DE60083A66F4751977* __this, const RuntimeMethod* method) ;
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Delegate_t* Delegate_Combine_m1F725AEF318BE6F0426863490691A6F4606E7D00 (Delegate_t* ___0_a, Delegate_t* ___1_b, const RuntimeMethod* method) ;
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Delegate_t* Delegate_Remove_m8B7DD5661308FA972E23CA1CC3FC9CEB355504E3 (Delegate_t* ___0_source, Delegate_t* ___1_value, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.List`1<Lean.Common.LeanSelectable>::Contains(T)
inline bool List_1_Contains_mCB5517DFCB5671A1504FF59C499DA2B27EE1B8AE (List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3* __this, LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* ___0_item, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3*, LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E*, const RuntimeMethod*))List_1_Contains_m4C9139C2A6B23E9343D3F87807B32C6E2CFE660D_gshared)(__this, ___0_item, method);
}
// System.Boolean Lean.Common.LeanSelect::TrySelect(Lean.Common.LeanSelectable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool LeanSelect_TrySelect_mAA0CABFECC04FE41D594CFF3B1D944E738AE35E9 (LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* __this, LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* ___0_selectable, const RuntimeMethod* method) ;
// System.Boolean Lean.Common.LeanSelect::TryDeselect(Lean.Common.LeanSelectable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool LeanSelect_TryDeselect_m908988308BE386DB0EE1E3FC7D7C465CDBBEBB5F (LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* __this, LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* ___0_selectable, const RuntimeMethod* method) ;
// System.Boolean CW.Common.CwHelper::Enabled(UnityEngine.Behaviour)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CwHelper_Enabled_mD9E9A832413D64688E133F17F26B9F071F1C5272 (Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA* ___0_b, const RuntimeMethod* method) ;
// System.Boolean Lean.Common.LeanSelect::TryReselect(Lean.Common.LeanSelectable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool LeanSelect_TryReselect_m2F88C4B33A2F49E72EAC9FF4321E905D10A06D2D (LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* __this, LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* ___0_selectable, const RuntimeMethod* method) ;
// System.Collections.Generic.List`1<Lean.Common.LeanSelectable> Lean.Common.LeanSelect::get_Selectables()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3* LeanSelect_get_Selectables_m0541739B28A2D6DE8DDB291B67F36C3D4BB7255C (LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* __this, const RuntimeMethod* method) ;
// System.Int32 System.Collections.Generic.List`1<Lean.Common.LeanSelectable>::get_Count()
inline int32_t List_1_get_Count_m45E012A15BC6389D71A4A103287549989EDC4DF2_inline (List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3* __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3*, const RuntimeMethod*))List_1_get_Count_m4407E4C389F22B8CEC282C15D56516658746C383_gshared_inline)(__this, method);
}
// T System.Collections.Generic.List`1<Lean.Common.LeanSelectable>::get_Item(System.Int32)
inline LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* List_1_get_Item_m0712D8C4200A8A2294457480069FDD039D529E1E (List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3* __this, int32_t ___0_index, const RuntimeMethod* method)
{
	return ((  LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* (*) (List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3*, int32_t, const RuntimeMethod*))List_1_get_Item_m33561245D64798C2AB07584C0EC4F240E4839A38_gshared)(__this, ___0_index, method);
}
// System.Void System.Collections.Generic.List`1<Lean.Common.LeanSelectable>::Add(T)
inline void List_1_Add_m879794CD85E22D7F3E18EB8223A87D702B9E74C8_inline (List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3* __this, LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* ___0_item, const RuntimeMethod* method)
{
	((  void (*) (List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3*, LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E*, const RuntimeMethod*))List_1_Add_mEBCF994CC3814631017F46A387B1A192ED6C85C7_gshared_inline)(__this, ___0_item, method);
}
// System.Void UnityEngine.Events.UnityEvent`1<Lean.Common.LeanSelectable>::Invoke(T0)
inline void UnityEvent_1_Invoke_mE535439F54E8EA413F15E7C4EF538598058BBD9A (UnityEvent_1_t0C1738C60D92A522CB1FF78F9CE867EE6CD8CD31* __this, LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* ___0_arg0, const RuntimeMethod* method)
{
	((  void (*) (UnityEvent_1_t0C1738C60D92A522CB1FF78F9CE867EE6CD8CD31*, LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E*, const RuntimeMethod*))UnityEvent_1_Invoke_m6CDC8B0639CE8935E2E13D10B2C8E500968130B6_gshared)(__this, ___0_arg0, method);
}
// System.Void System.Action`2<Lean.Common.LeanSelect,Lean.Common.LeanSelectable>::Invoke(T1,T2)
inline void Action_2_Invoke_m92A2047F7B6E70B622B357B63B29E4FB7BD96336_inline (Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* __this, LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* ___0_arg1, LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* ___1_arg2, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190*, LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F*, LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E*, const RuntimeMethod*))Action_2_Invoke_m7BFCE0BBCF67689D263059B56A8D79161B698587_gshared_inline)(__this, ___0_arg1, ___1_arg2, method);
}
// System.Void Lean.Common.LeanSelectable::InvokeOnSelected(Lean.Common.LeanSelect)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectable_InvokeOnSelected_m3C81335F17D41C2F14CEBF38F31B275E59D6FAE9 (LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* __this, LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* ___0_select, const RuntimeMethod* method) ;
// System.Void UnityEngine.Events.UnityEvent::Invoke()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_Invoke_mFBF80D59B03C30C5FE6A06F897D954ACADE061D2 (UnityEvent_tDC2C3548799DBC91D1E3F3DE60083A66F4751977* __this, const RuntimeMethod* method) ;
// System.Void Lean.Common.LeanSelect::DeselectAll()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelect_DeselectAll_mFE5E913DADC65729C87BE27A69D04658F3785E18 (LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* __this, const RuntimeMethod* method) ;
// System.Void Lean.Common.LeanSelect::Deselect(Lean.Common.LeanSelectable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelect_Deselect_mD29FDB8833FDBCE9CD91833CBE489E52D001C628 (LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* __this, LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* ___0_selectable, const RuntimeMethod* method) ;
// System.Int32 System.Collections.Generic.List`1<Lean.Common.LeanSelectable>::IndexOf(T)
inline int32_t List_1_IndexOf_mCDCF06F86563E158C15328FB5D36286E68D653B6 (List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3* __this, LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* ___0_item, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3*, LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E*, const RuntimeMethod*))List_1_IndexOf_m378F61BA812B79DEE58D86FE8AA9F20E3FC7D85F_gshared)(__this, ___0_item, method);
}
// System.Boolean Lean.Common.LeanSelect::TryDeselect(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool LeanSelect_TryDeselect_m1F043BCFE0AF219FE3D252F4F5E63F7BD25E455D (LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* __this, int32_t ___0_index, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<Lean.Common.LeanSelectable>::RemoveAt(System.Int32)
inline void List_1_RemoveAt_m3D273B83DA1DA3CA02F13C3D3ECB2DDE052CFD2C (List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3* __this, int32_t ___0_index, const RuntimeMethod* method)
{
	((  void (*) (List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3*, int32_t, const RuntimeMethod*))List_1_RemoveAt_m54F62297ADEE4D4FDA697F49ED807BF901201B54_gshared)(__this, ___0_index, method);
}
// System.Void Lean.Common.LeanSelectable::InvokeOnDeslected(Lean.Common.LeanSelect)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectable_InvokeOnDeslected_m6B165447EB743395D368821C3E598822DFDEA7DF (LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* __this, LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* ___0_select, const RuntimeMethod* method) ;
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<Lean.Common.LeanSelect>::AddLast(T)
inline LinkedListNode_1_t7D0DA7355A32A333D2AB6F41BC1E57C6EF2AE9F7* LinkedList_1_AddLast_m22A0173682AA7EC7831D043A8AD267DFEA1446B9 (LinkedList_1_t74DD3E2689E35FC0DF8DF1971B5CD529EEE8CAA1* __this, LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* ___0_value, const RuntimeMethod* method)
{
	return ((  LinkedListNode_1_t7D0DA7355A32A333D2AB6F41BC1E57C6EF2AE9F7* (*) (LinkedList_1_t74DD3E2689E35FC0DF8DF1971B5CD529EEE8CAA1*, LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F*, const RuntimeMethod*))LinkedList_1_AddLast_mF5239C871EADC44D51C6B621592A9CAC43449A2E_gshared)(__this, ___0_value, method);
}
// System.Void System.Collections.Generic.LinkedList`1<Lean.Common.LeanSelect>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
inline void LinkedList_1_Remove_mC8D384262E80C141DEA34CEC4DC755F5BCB953FC (LinkedList_1_t74DD3E2689E35FC0DF8DF1971B5CD529EEE8CAA1* __this, LinkedListNode_1_t7D0DA7355A32A333D2AB6F41BC1E57C6EF2AE9F7* ___0_node, const RuntimeMethod* method)
{
	((  void (*) (LinkedList_1_t74DD3E2689E35FC0DF8DF1971B5CD529EEE8CAA1*, LinkedListNode_1_t7D0DA7355A32A333D2AB6F41BC1E57C6EF2AE9F7*, const RuntimeMethod*))LinkedList_1_Remove_m6B592B94D9AEF003EAE59FCB5455DA67AB4E423C_gshared)(__this, ___0_node, method);
}
// System.Void System.Collections.Generic.LinkedList`1<Lean.Common.LeanSelect>::.ctor()
inline void LinkedList_1__ctor_mC7E25FA9B50FCAED34E6C53704CBCDAE4BE0BB46 (LinkedList_1_t74DD3E2689E35FC0DF8DF1971B5CD529EEE8CAA1* __this, const RuntimeMethod* method)
{
	((  void (*) (LinkedList_1_t74DD3E2689E35FC0DF8DF1971B5CD529EEE8CAA1*, const RuntimeMethod*))LinkedList_1__ctor_m2732A2EC5597469086D48C79A12B3563DEA501C5_gshared)(__this, method);
}
// System.Void UnityEngine.Events.UnityEvent`1<Lean.Common.LeanSelectable>::.ctor()
inline void UnityEvent_1__ctor_mA2FAFACB7630DDA1C8722C0684B8E14E2517CD57 (UnityEvent_1_t0C1738C60D92A522CB1FF78F9CE867EE6CD8CD31* __this, const RuntimeMethod* method)
{
	((  void (*) (UnityEvent_1_t0C1738C60D92A522CB1FF78F9CE867EE6CD8CD31*, const RuntimeMethod*))UnityEvent_1__ctor_m8D77F4F05F69D0E52E8A445322811EEC25987525_gshared)(__this, method);
}
// System.Void Lean.Common.LeanSelectable/LeanSelectEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectEvent__ctor_m298B432186C5138B38B93C71D379689F44C0B204 (LeanSelectEvent_tE1B7F7125473C6F40463EC6C88B8842A0404C34F* __this, const RuntimeMethod* method) ;
// System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1<Lean.Common.LeanSelect>::GetEnumerator()
inline Enumerator_t3F4E85BE6F8338474FC5FB1BD081F64E1ECC5A1E LinkedList_1_GetEnumerator_mB93F43BCCD2C6A1C3A285DFD6BBCCA71529E58D1 (LinkedList_1_t74DD3E2689E35FC0DF8DF1971B5CD529EEE8CAA1* __this, const RuntimeMethod* method)
{
	return ((  Enumerator_t3F4E85BE6F8338474FC5FB1BD081F64E1ECC5A1E (*) (LinkedList_1_t74DD3E2689E35FC0DF8DF1971B5CD529EEE8CAA1*, const RuntimeMethod*))LinkedList_1_GetEnumerator_m2522814971CB421FDF996D386A650A3F8FA0E30C_gshared)(__this, method);
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<Lean.Common.LeanSelect>::Dispose()
inline void Enumerator_Dispose_m15AE326ED9AE5BBF6AD71F57A8B15B6D10B75F1A (Enumerator_t3F4E85BE6F8338474FC5FB1BD081F64E1ECC5A1E* __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_t3F4E85BE6F8338474FC5FB1BD081F64E1ECC5A1E*, const RuntimeMethod*))Enumerator_Dispose_m9E60B472E2E61202E9CFBED3DA607374973EA744_gshared)(__this, method);
}
// T System.Collections.Generic.LinkedList`1/Enumerator<Lean.Common.LeanSelect>::get_Current()
inline LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* Enumerator_get_Current_m5BCF5CFF717F6739116402C6E75944686E15AD0D_inline (Enumerator_t3F4E85BE6F8338474FC5FB1BD081F64E1ECC5A1E* __this, const RuntimeMethod* method)
{
	return ((  LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* (*) (Enumerator_t3F4E85BE6F8338474FC5FB1BD081F64E1ECC5A1E*, const RuntimeMethod*))Enumerator_get_Current_mCD39BA1871E5D5BE52D8AA0B27886D9B5B10BBF9_gshared_inline)(__this, method);
}
// System.Boolean Lean.Common.LeanSelect::IsSelected(Lean.Common.LeanSelectable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool LeanSelect_IsSelected_m882CF8026A3B01FE1F1A5374536481252A04FC16 (LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* __this, LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* ___0_selectable, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<Lean.Common.LeanSelect>::MoveNext()
inline bool Enumerator_MoveNext_mAC2A0346E0E1D3D9AC42A6CDFB77A42E2EF081CF (Enumerator_t3F4E85BE6F8338474FC5FB1BD081F64E1ECC5A1E* __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t3F4E85BE6F8338474FC5FB1BD081F64E1ECC5A1E*, const RuntimeMethod*))Enumerator_MoveNext_m5148B3CE498B6E84B883D3EB4A7A080238A2609D_gshared)(__this, method);
}
// System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1<Lean.Common.LeanSelectable>::GetEnumerator()
inline Enumerator_t2DDE01BDFB37E13974A8807CCD46D71DEA3E8F33 LinkedList_1_GetEnumerator_mDB5737AFFB871DAB61A205446EC59B28F4A810D3 (LinkedList_1_t25474EE5265E341D73136D6D26BA9381A1E5361C* __this, const RuntimeMethod* method)
{
	return ((  Enumerator_t2DDE01BDFB37E13974A8807CCD46D71DEA3E8F33 (*) (LinkedList_1_t25474EE5265E341D73136D6D26BA9381A1E5361C*, const RuntimeMethod*))LinkedList_1_GetEnumerator_m2522814971CB421FDF996D386A650A3F8FA0E30C_gshared)(__this, method);
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<Lean.Common.LeanSelectable>::Dispose()
inline void Enumerator_Dispose_m3CC6549F7A60846A9D156C06EDCF724D21D0763C (Enumerator_t2DDE01BDFB37E13974A8807CCD46D71DEA3E8F33* __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_t2DDE01BDFB37E13974A8807CCD46D71DEA3E8F33*, const RuntimeMethod*))Enumerator_Dispose_m9E60B472E2E61202E9CFBED3DA607374973EA744_gshared)(__this, method);
}
// T System.Collections.Generic.LinkedList`1/Enumerator<Lean.Common.LeanSelectable>::get_Current()
inline LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* Enumerator_get_Current_m357A4F354FD535F202C1DAECEB1427B848D781D7_inline (Enumerator_t2DDE01BDFB37E13974A8807CCD46D71DEA3E8F33* __this, const RuntimeMethod* method)
{
	return ((  LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* (*) (Enumerator_t2DDE01BDFB37E13974A8807CCD46D71DEA3E8F33*, const RuntimeMethod*))Enumerator_get_Current_mCD39BA1871E5D5BE52D8AA0B27886D9B5B10BBF9_gshared_inline)(__this, method);
}
// System.Boolean Lean.Common.LeanSelectable::get_IsSelected()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool LeanSelectable_get_IsSelected_m55508746D046549000EB2085D5EA859B5ED855CB (LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* __this, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<Lean.Common.LeanSelectable>::MoveNext()
inline bool Enumerator_MoveNext_m89C7C4FA562ABDDA990D8F12AFAB5714BC74927C (Enumerator_t2DDE01BDFB37E13974A8807CCD46D71DEA3E8F33* __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t2DDE01BDFB37E13974A8807CCD46D71DEA3E8F33*, const RuntimeMethod*))Enumerator_MoveNext_m5148B3CE498B6E84B883D3EB4A7A080238A2609D_gshared)(__this, method);
}
// System.Void Lean.Common.LeanSelectable::set_SelfSelected(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectable_set_SelfSelected_m3B119374E382EB78103F159BFC08C1DDB8220186 (LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* __this, bool ___0_value, const RuntimeMethod* method) ;
// System.Void UnityEngine.Events.UnityEvent`1<Lean.Common.LeanSelect>::Invoke(T0)
inline void UnityEvent_1_Invoke_m14D167DBD029D5C80FE4F7F6DFEDDA46937311C4 (UnityEvent_1_tE3623D0B5417AD4C7F446399441E2E6214BD3108* __this, LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* ___0_arg0, const RuntimeMethod* method)
{
	((  void (*) (UnityEvent_1_tE3623D0B5417AD4C7F446399441E2E6214BD3108*, LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F*, const RuntimeMethod*))UnityEvent_1_Invoke_m6CDC8B0639CE8935E2E13D10B2C8E500968130B6_gshared)(__this, ___0_arg0, method);
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<Lean.Common.LeanSelectable>::AddLast(T)
inline LinkedListNode_1_tC4B0FFBEE56621B13FC8502EB49ADCA6A7E69B72* LinkedList_1_AddLast_mDC5C4929E156AC04B13A0D54368063664D3A861A (LinkedList_1_t25474EE5265E341D73136D6D26BA9381A1E5361C* __this, LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* ___0_value, const RuntimeMethod* method)
{
	return ((  LinkedListNode_1_tC4B0FFBEE56621B13FC8502EB49ADCA6A7E69B72* (*) (LinkedList_1_t25474EE5265E341D73136D6D26BA9381A1E5361C*, LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E*, const RuntimeMethod*))LinkedList_1_AddLast_mF5239C871EADC44D51C6B621592A9CAC43449A2E_gshared)(__this, ___0_value, method);
}
// System.Void System.Action`1<Lean.Common.LeanSelectable>::Invoke(T)
inline void Action_1_Invoke_m155488EBEB85B08AEF82D89E9EB046BFB10AA612_inline (Action_1_tAD366A485E434448836F3A4108059A1618C980AF* __this, LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* ___0_obj, const RuntimeMethod* method)
{
	((  void (*) (Action_1_tAD366A485E434448836F3A4108059A1618C980AF*, LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E*, const RuntimeMethod*))Action_1_Invoke_mF2422B2DD29F74CE66F791C3F68E288EC7C3DB9E_gshared_inline)(__this, ___0_obj, method);
}
// System.Void System.Collections.Generic.LinkedList`1<Lean.Common.LeanSelectable>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
inline void LinkedList_1_Remove_mC7399940C9CFA33D74D4C0DAC876404129DF8562 (LinkedList_1_t25474EE5265E341D73136D6D26BA9381A1E5361C* __this, LinkedListNode_1_tC4B0FFBEE56621B13FC8502EB49ADCA6A7E69B72* ___0_node, const RuntimeMethod* method)
{
	((  void (*) (LinkedList_1_t25474EE5265E341D73136D6D26BA9381A1E5361C*, LinkedListNode_1_tC4B0FFBEE56621B13FC8502EB49ADCA6A7E69B72*, const RuntimeMethod*))LinkedList_1_Remove_m6B592B94D9AEF003EAE59FCB5455DA67AB4E423C_gshared)(__this, ___0_node, method);
}
// System.Void Lean.Common.LeanSelectable::Deselect()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectable_Deselect_mCB84D48D0F71C8345F70321595197109042D2A27 (LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.LinkedList`1<Lean.Common.LeanSelectable>::.ctor()
inline void LinkedList_1__ctor_m7E3FF3E1520AA5DE2DBE33B6A1E7657EC4FE65BF (LinkedList_1_t25474EE5265E341D73136D6D26BA9381A1E5361C* __this, const RuntimeMethod* method)
{
	((  void (*) (LinkedList_1_t25474EE5265E341D73136D6D26BA9381A1E5361C*, const RuntimeMethod*))LinkedList_1__ctor_m2732A2EC5597469086D48C79A12B3563DEA501C5_gshared)(__this, method);
}
// System.Void UnityEngine.Events.UnityEvent`1<Lean.Common.LeanSelect>::.ctor()
inline void UnityEvent_1__ctor_mA971530EC4BACA70B45C39F76AD5A0BE118CF63C (UnityEvent_1_tE3623D0B5417AD4C7F446399441E2E6214BD3108* __this, const RuntimeMethod* method)
{
	((  void (*) (UnityEvent_1_tE3623D0B5417AD4C7F446399441E2E6214BD3108*, const RuntimeMethod*))UnityEvent_1__ctor_m8D77F4F05F69D0E52E8A445322811EEC25987525_gshared)(__this, method);
}
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mB6120F782D83091EF56A198FCEBCF066DB4A9605 (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___0_x, Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___1_y, const RuntimeMethod* method) ;
// System.Void Lean.Common.LeanSelectableBehaviour::Register()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectableBehaviour_Register_m59BCA4108A01A335DA517DC30A64C29ADB64DB80 (LeanSelectableBehaviour_t07650DA7B5AA5AEACF3FA60433E5C36E1A679CD9* __this, const RuntimeMethod* method) ;
// T UnityEngine.Component::GetComponentInParent<Lean.Common.LeanSelectable>()
inline LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* Component_GetComponentInParent_TisLeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_mCB62694EE43FA4026B1B4D8D8C9B372BDEE1DD51 (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method)
{
	return ((  LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* (*) (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3*, const RuntimeMethod*))Component_GetComponentInParent_TisRuntimeObject_m6746D6BB99912B1B509746C993906492F86CD119_gshared)(__this, method);
}
// System.Void Lean.Common.LeanSelectableBehaviour::Register(Lean.Common.LeanSelectable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectableBehaviour_Register_mDADF1B6384CBA28AC26D2161AA5831FC1D7B9747 (LeanSelectableBehaviour_t07650DA7B5AA5AEACF3FA60433E5C36E1A679CD9* __this, LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* ___0_newSelectable, const RuntimeMethod* method) ;
// System.Void Lean.Common.LeanSelectableBehaviour::Unregister()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectableBehaviour_Unregister_m21AA701DA1065421693A87AA7CD10DB32AD9318C (LeanSelectableBehaviour_t07650DA7B5AA5AEACF3FA60433E5C36E1A679CD9* __this, const RuntimeMethod* method) ;
// Lean.Common.LeanSelectable/LeanSelectEvent Lean.Common.LeanSelectable::get_OnSelected()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR LeanSelectEvent_tE1B7F7125473C6F40463EC6C88B8842A0404C34F* LeanSelectable_get_OnSelected_mEE84613146ACD47032B93CB4E8988F526A15988B (LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Events.UnityAction`1<Lean.Common.LeanSelect>::.ctor(System.Object,System.IntPtr)
inline void UnityAction_1__ctor_mA72F946150482FECDC3C04BED2CBF062038EE470 (UnityAction_1_tA601885BAAC49BD9D535DC344F67685863BD4573* __this, RuntimeObject* ___0_object, intptr_t ___1_method, const RuntimeMethod* method)
{
	((  void (*) (UnityAction_1_tA601885BAAC49BD9D535DC344F67685863BD4573*, RuntimeObject*, intptr_t, const RuntimeMethod*))UnityAction_1__ctor_m0C2FC6B483B474AE9596A43EBA7FF6E85503A92A_gshared)(__this, ___0_object, ___1_method, method);
}
// System.Void UnityEngine.Events.UnityEvent`1<Lean.Common.LeanSelect>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
inline void UnityEvent_1_AddListener_m41D8372E4AB1C156FF3CA895F0B66CBD484E6C57 (UnityEvent_1_tE3623D0B5417AD4C7F446399441E2E6214BD3108* __this, UnityAction_1_tA601885BAAC49BD9D535DC344F67685863BD4573* ___0_call, const RuntimeMethod* method)
{
	((  void (*) (UnityEvent_1_tE3623D0B5417AD4C7F446399441E2E6214BD3108*, UnityAction_1_tA601885BAAC49BD9D535DC344F67685863BD4573*, const RuntimeMethod*))UnityEvent_1_AddListener_m055233246714700E4BDAA62635BC0AA49E8165CC_gshared)(__this, ___0_call, method);
}
// Lean.Common.LeanSelectable/LeanSelectEvent Lean.Common.LeanSelectable::get_OnDeselected()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR LeanSelectEvent_tE1B7F7125473C6F40463EC6C88B8842A0404C34F* LeanSelectable_get_OnDeselected_m055BD9B88E71CAB5E6C4D429CA271308F56EBC29 (LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Events.UnityEvent`1<Lean.Common.LeanSelect>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
inline void UnityEvent_1_RemoveListener_m93A6005ABE25250DBD0E2497D329D839BC2C92D8 (UnityEvent_1_tE3623D0B5417AD4C7F446399441E2E6214BD3108* __this, UnityAction_1_tA601885BAAC49BD9D535DC344F67685863BD4573* ___0_call, const RuntimeMethod* method)
{
	((  void (*) (UnityEvent_1_tE3623D0B5417AD4C7F446399441E2E6214BD3108*, UnityAction_1_tA601885BAAC49BD9D535DC344F67685863BD4573*, const RuntimeMethod*))UnityEvent_1_RemoveListener_m904FA6BDD0D33FDF8650EF816FF5C131867E693E_gshared)(__this, ___0_call, method);
}
// System.Void Lean.Common.LeanSelectableGraphicColor::UpdateColor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectableGraphicColor_UpdateColor_m8C4B31A043489DD4DA3BFC8DAA3CC0F985E3FE41 (LeanSelectableGraphicColor_tFDDBE08F9483FC2CB8E1511BEEE1AF8F5601345A* __this, const RuntimeMethod* method) ;
// T UnityEngine.Component::GetComponent<UnityEngine.UI.Graphic>()
inline Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931* Component_GetComponent_TisGraphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931_mFE18E20FC92395F90E776DBC4CD214A4F2D97D90 (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method)
{
	return ((  Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931* (*) (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3*, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m7181F81CAEC2CF53F5D2BC79B7425C16E1F80D33_gshared)(__this, method);
}
// Lean.Common.LeanSelectable Lean.Common.LeanSelectableBehaviour::get_Selectable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* LeanSelectableBehaviour_get_Selectable_mDA4E928E7FE887A93CF29BBC0EDD11483D52B97E (LeanSelectableBehaviour_t07650DA7B5AA5AEACF3FA60433E5C36E1A679CD9* __this, const RuntimeMethod* method) ;
// UnityEngine.Color UnityEngine.Color::get_white()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_white_m068F5AF879B0FCA584E3693F762EA41BB65532C6_inline (const RuntimeMethod* method) ;
// UnityEngine.Color UnityEngine.Color::get_green()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_green_mEB001F2CD8C68C6BBAEF9101990B779D3AA2A6EF_inline (const RuntimeMethod* method) ;
// System.Void Lean.Common.LeanSelectableBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectableBehaviour__ctor_m24001ACBC3695134BE8306864960F29F6A4573B9 (LeanSelectableBehaviour_t07650DA7B5AA5AEACF3FA60433E5C36E1A679CD9* __this, const RuntimeMethod* method) ;
// System.Void Lean.Common.LeanSelectableRendererColor::UpdateColor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectableRendererColor_UpdateColor_mBB4BFB9173F24C7C758D291DC6AAFA77F91F484C (LeanSelectableRendererColor_tDD4EBBF7DAB508DD4FC536B35A7F8DB48142433E* __this, const RuntimeMethod* method) ;
// System.Void Lean.Common.LeanSelectableBehaviour::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectableBehaviour_Start_mCC6D17C6D1520EA1C4BAF1C4F3BB0801ECE7402B (LeanSelectableBehaviour_t07650DA7B5AA5AEACF3FA60433E5C36E1A679CD9* __this, const RuntimeMethod* method) ;
// T UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
inline Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* Component_GetComponent_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_mC91ACC92AD57CA6CA00991DAF1DB3830BCE07AF8 (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method)
{
	return ((  Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* (*) (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3*, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m7181F81CAEC2CF53F5D2BC79B7425C16E1F80D33_gshared)(__this, method);
}
// System.Void UnityEngine.MaterialPropertyBlock::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MaterialPropertyBlock__ctor_m14C3432585F7BB65028BCD64A0FD6607A1B490FB (MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Renderer::GetPropertyBlock(UnityEngine.MaterialPropertyBlock)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Renderer_GetPropertyBlock_mD062F90343D70151CA060AE7EBEF2E85146A9FBA (Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* __this, MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D* ___0_properties, const RuntimeMethod* method) ;
// System.Void UnityEngine.MaterialPropertyBlock::SetColor(System.String,UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MaterialPropertyBlock_SetColor_m5B4E910B5E42518BBD0088055EB68E4A3A609DDE (MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D* __this, String_t* ___0_name, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___1_value, const RuntimeMethod* method) ;
// System.Void UnityEngine.Renderer::SetPropertyBlock(UnityEngine.MaterialPropertyBlock)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Renderer_SetPropertyBlock_mF565698782FE54580B17CC0BFF9B0C4F0D68DF50 (Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* __this, MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D* ___0_properties, const RuntimeMethod* method) ;
// System.Void Lean.Common.LeanSelectableSpriteRendererColor::UpdateColor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectableSpriteRendererColor_UpdateColor_mB959E44683EB3D85A96956DB84A4AEB79A124D2B (LeanSelectableSpriteRendererColor_tAD5AA41024B15A0A25E574475377A2669143886E* __this, const RuntimeMethod* method) ;
// T UnityEngine.Component::GetComponent<UnityEngine.SpriteRenderer>()
inline SpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B* Component_GetComponent_TisSpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B_m6181F10C09FC1650DAE0EF2308D344A2F170AA45 (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method)
{
	return ((  SpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B* (*) (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3*, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m7181F81CAEC2CF53F5D2BC79B7425C16E1F80D33_gshared)(__this, method);
}
// System.Void UnityEngine.SpriteRenderer::set_color(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpriteRenderer_set_color_mB0EEC2845A0347E296C01C831F967731D2804546 (SpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B* __this, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___0_value, const RuntimeMethod* method) ;
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 Transform_get_rotation_m32AF40CA0D50C797DA639A696F8EAEC7524C179C (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, const RuntimeMethod* method) ;
// T UnityEngine.Object::Instantiate<UnityEngine.Transform>(T,UnityEngine.Vector3,UnityEngine.Quaternion)
inline Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* Object_Instantiate_TisTransform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1_mD6EFBE8E5CFC309C748E284B0798BF1C184F49ED (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___0_original, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___1_position, Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___2_rotation, const RuntimeMethod* method)
{
	return ((  Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* (*) (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1*, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2, Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974, const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_m249A6BA4F2F19C2D3CE217D4D31847DF0EF03EFE_gshared)(___0_original, ___1_position, ___2_rotation, method);
}
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, bool ___0_value, const RuntimeMethod* method) ;
// System.Single Lean.Common.LeanCommon::HermiteInterpolate(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float LeanCommon_HermiteInterpolate_m18965FC0DCD5D7A0F11486C2497BCDCD4CC205B2 (float ___0_y0, float ___1_y1, float ___2_y2, float ___3_y3, float ___4_mu, float ___5_mu2, float ___6_mu3, const RuntimeMethod* method) ;
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* __this, float ___0_x, float ___1_y, const RuntimeMethod* method) ;
// UnityEngine.LayerMask UnityEngine.LayerMask::op_Implicit(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR LayerMask_t97CB6BDADEDC3D6423C7BCFEA7F86DA2EC6241DB LayerMask_op_Implicit_m01C8996A2CB2085328B9C33539C43139660D8222 (int32_t ___0_intVal, const RuntimeMethod* method) ;
// System.Void Lean.Common.LeanScreenQuery::.ctor(Lean.Common.LeanScreenQuery/MethodType,UnityEngine.LayerMask)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanScreenQuery__ctor_m96FE813B432186035779FED836415F1E1E16B23A (LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535* __this, int32_t ___0_newMethod, LayerMask_t97CB6BDADEDC3D6423C7BCFEA7F86DA2EC6241DB ___1_layers, const RuntimeMethod* method) ;
// System.Void Lean.Common.LeanScreenQuery::.ctor(Lean.Common.LeanScreenQuery/MethodType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanScreenQuery__ctor_m59065C77ACB27AC2072C31D92B4D418EC9C60A85 (LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535* __this, int32_t ___0_newMethod, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.GameObject::get_layer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t GameObject_get_layer_m108902B9C89E9F837CE06B9942AA42307450FEAF (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.GameObject,System.Int32>::.ctor(TKey,TValue)
inline void KeyValuePair_2__ctor_m882B36949BE2FB86948D3198EA9BC21D5AED8330 (KeyValuePair_2_t9BD14587548B372830B82C49C122A2710251FD58* __this, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___0_key, int32_t ___1_value, const RuntimeMethod* method)
{
	((  void (*) (KeyValuePair_2_t9BD14587548B372830B82C49C122A2710251FD58*, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*, int32_t, const RuntimeMethod*))KeyValuePair_2__ctor_mD4B5AD912DE40BAACCC9A814640E4E3386E51BA8_gshared)(__this, ___0_key, ___1_value, method);
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.GameObject,System.Int32>>::Add(T)
inline void List_1_Add_mB5F4B5AC39AACE323FD5679252895AE6CF289E08_inline (List_1_tC2267FBE21298A38669F48AD07AE8540B7AA80AF* __this, KeyValuePair_2_t9BD14587548B372830B82C49C122A2710251FD58 ___0_item, const RuntimeMethod* method)
{
	((  void (*) (List_1_tC2267FBE21298A38669F48AD07AE8540B7AA80AF*, KeyValuePair_2_t9BD14587548B372830B82C49C122A2710251FD58, const RuntimeMethod*))List_1_Add_m9BAF9FC9B01B86AB9E16C2A54BAB69042D04B974_gshared_inline)(__this, ___0_item, method);
}
// System.Void UnityEngine.GameObject::set_layer(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_set_layer_m6E1AF478A2CC86BD222B96317BEB78B7D89B18D0 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, int32_t ___0_value, const RuntimeMethod* method) ;
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method) ;
// UnityEngine.Transform UnityEngine.Transform::get_parent()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* Transform_get_parent_m65354E28A4C94EC00EBCF03532F7B0718380791E (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, const RuntimeMethod* method) ;
// System.Void Lean.Common.LeanScreenQuery::ChangeLayers(UnityEngine.GameObject,System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanScreenQuery_ChangeLayers_m0FE445BF645A2E5B913D4ABE847CCE70C1F87D25 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___0_root, bool ___1_ancestors, bool ___2_children, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.Transform::get_childCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Transform_get_childCount_mE9C29C702AB662CC540CA053EDE48BDAFA35B4B0 (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, const RuntimeMethod* method) ;
// UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* Transform_GetChild_mE686DF0C7AAC1F7AEF356967B1C04D8B8E240EAF (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, int32_t ___0_index, const RuntimeMethod* method) ;
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.GameObject,System.Int32>>::GetEnumerator()
inline Enumerator_tE49F457C3EA7DD2E164F160F3A3D19CC62191D80 List_1_GetEnumerator_m552F956B853F080F1BB08A652725EAA51DE82680 (List_1_tC2267FBE21298A38669F48AD07AE8540B7AA80AF* __this, const RuntimeMethod* method)
{
	return ((  Enumerator_tE49F457C3EA7DD2E164F160F3A3D19CC62191D80 (*) (List_1_tC2267FBE21298A38669F48AD07AE8540B7AA80AF*, const RuntimeMethod*))List_1_GetEnumerator_mA470FE029F16C9C2CF4CF4B94F44C54DE0EDE84A_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<UnityEngine.GameObject,System.Int32>>::Dispose()
inline void Enumerator_Dispose_m78D479DDD8F3214C93E9BE0B062995D6E182E773 (Enumerator_tE49F457C3EA7DD2E164F160F3A3D19CC62191D80* __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_tE49F457C3EA7DD2E164F160F3A3D19CC62191D80*, const RuntimeMethod*))Enumerator_Dispose_mA741B664CFF7AF6C68A6B638319CA64348911DFD_gshared)(__this, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<UnityEngine.GameObject,System.Int32>>::get_Current()
inline KeyValuePair_2_t9BD14587548B372830B82C49C122A2710251FD58 Enumerator_get_Current_m97354C43156F3E8FF03CC8D54C4151C9992C605C_inline (Enumerator_tE49F457C3EA7DD2E164F160F3A3D19CC62191D80* __this, const RuntimeMethod* method)
{
	return ((  KeyValuePair_2_t9BD14587548B372830B82C49C122A2710251FD58 (*) (Enumerator_tE49F457C3EA7DD2E164F160F3A3D19CC62191D80*, const RuntimeMethod*))Enumerator_get_Current_m7362387DF032E42E572F7041F4C1A3CD21679DCD_gshared_inline)(__this, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.GameObject,System.Int32>::get_Key()
inline GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* KeyValuePair_2_get_Key_m3AB1FFB220CFA62C8913F62DF7F79F0D2F39A314_inline (KeyValuePair_2_t9BD14587548B372830B82C49C122A2710251FD58* __this, const RuntimeMethod* method)
{
	return ((  GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* (*) (KeyValuePair_2_t9BD14587548B372830B82C49C122A2710251FD58*, const RuntimeMethod*))KeyValuePair_2_get_Key_mADC45FA05C759E6F88D7DADDFE0C0E1ADBB3E501_gshared_inline)(__this, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.GameObject,System.Int32>::get_Value()
inline int32_t KeyValuePair_2_get_Value_mB01CEBC5D69BFC0A878DB1ACBF3DE05B8EC1B964_inline (KeyValuePair_2_t9BD14587548B372830B82C49C122A2710251FD58* __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (KeyValuePair_2_t9BD14587548B372830B82C49C122A2710251FD58*, const RuntimeMethod*))KeyValuePair_2_get_Value_m7A836D9634814B22DF33AD801EA10741ABFBDFE2_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<UnityEngine.GameObject,System.Int32>>::MoveNext()
inline bool Enumerator_MoveNext_m3C9B7035E15808178030F1D80430BAFA582C5C32 (Enumerator_tE49F457C3EA7DD2E164F160F3A3D19CC62191D80* __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tE49F457C3EA7DD2E164F160F3A3D19CC62191D80*, const RuntimeMethod*))Enumerator_MoveNext_mDE2B6C6EB1965BF7A51BC0785BAA55A818EDD46F_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.GameObject,System.Int32>>::Clear()
inline void List_1_Clear_m6D7EC60334D1F90BC2538200034022903CF7DE50_inline (List_1_tC2267FBE21298A38669F48AD07AE8540B7AA80AF* __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tC2267FBE21298A38669F48AD07AE8540B7AA80AF*, const RuntimeMethod*))List_1_Clear_mB511E4F0FAD5BBBFD635CE5E636119104E14A316_gshared_inline)(__this, method);
}
// System.Single UnityEngine.RaycastHit::get_distance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float RaycastHit_get_distance_m035194B0E9BB6229259CFC43B095A9C8E5011C78 (RaycastHit_t6F30BD0B38B56401CA833A1B87BD74F2ACD2F2B5* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector2_op_Implicit_m6D9CABB2C791A192867D7A4559D132BE86DD3EB7_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___0_v, const RuntimeMethod* method) ;
// UnityEngine.Ray UnityEngine.Camera::ScreenPointToRay(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 Camera_ScreenPointToRay_m2887B9A49880B7AB670C57D66B67D6A6689FE315 (Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_pos, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.LayerMask::op_Implicit(UnityEngine.LayerMask)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t LayerMask_op_Implicit_m7F5A5B9D079281AC445ED39DEE1FCFA9D795810D (LayerMask_t97CB6BDADEDC3D6423C7BCFEA7F86DA2EC6241DB ___0_mask, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit[],System.Single,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Physics_RaycastNonAlloc_m2BFEE9072E390ED6ACD500FD0AE4E714DE9549BC (Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 ___0_ray, RaycastHitU5BU5D_t008B8309DE422FE7567068D743D68054D5EBF1A8* ___1_results, float ___2_maxDistance, int32_t ___3_layerMask, const RuntimeMethod* method) ;
// System.Int32 Lean.Common.LeanScreenQuery::GetClosestRaycastHitsIndex(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t LeanScreenQuery_GetClosestRaycastHitsIndex_mCA37AA7178583AABC833765E4D9434EBA5BC91E5 (int32_t ___0_count, const RuntimeMethod* method) ;
// UnityEngine.Collider UnityEngine.RaycastHit::get_collider()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* RaycastHit_get_collider_m84B160439BBEAB6D9E94B799F720E25C9E2D444D (RaycastHit_t6F30BD0B38B56401CA833A1B87BD74F2ACD2F2B5* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_point()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 RaycastHit_get_point_m02B764612562AFE0F998CC7CFB2EEDE41BA47F39 (RaycastHit_t6F30BD0B38B56401CA833A1B87BD74F2ACD2F2B5* __this, const RuntimeMethod* method) ;
// System.Void Lean.Common.LeanScreenQuery::DoRaycast3D(UnityEngine.Camera,UnityEngine.Vector2,UnityEngine.Component&,System.Single&,UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanScreenQuery_DoRaycast3D_m79EEB1F96BF3BB4CC4BCB70E9F1667E4CA7D993E (LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535* __this, Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* ___0_camera, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___1_screenPosition, Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3** ___2_bestResult, float* ___3_bestDistance, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* ___4_bestPosition, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.Physics2D::GetRayIntersectionNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit2D[],System.Single,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Physics2D_GetRayIntersectionNonAlloc_mB7942B73C8B86F369262FC3B87F080132E7A369C (Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 ___0_ray, RaycastHit2DU5BU5D_t28739C686586993113318B63C84927FD43063FC7* ___1_results, float ___2_distance, int32_t ___3_layerMask, const RuntimeMethod* method) ;
// System.Single UnityEngine.RaycastHit2D::get_distance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float RaycastHit2D_get_distance_mD0FE1482E2768CF587AFB65488459697EAB64613 (RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA* __this, const RuntimeMethod* method) ;
// UnityEngine.Transform UnityEngine.RaycastHit2D::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* RaycastHit2D_get_transform_mA5E3F8DC9914E79D3C9F6F3F2515B49EEBB4564A (RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_point()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 RaycastHit2D_get_point_mB35E988E9E04328EFE926228A18334326721A36B (RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA* __this, const RuntimeMethod* method) ;
// System.Void Lean.Common.LeanScreenQuery::DoRaycast2D(UnityEngine.Camera,UnityEngine.Vector2,UnityEngine.Component&,System.Single&,UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanScreenQuery_DoRaycast2D_m57CB1F6A5464D8D88EB1E5E83DA7BA15BAB854B7 (LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535* __this, Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* ___0_camera, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___1_screenPosition, Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3** ___2_bestResult, float* ___3_bestDistance, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* ___4_bestPosition, const RuntimeMethod* method) ;
// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.EventSystem::get_current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR EventSystem_t61C51380B105BE9D2C39C4F15B7E655659957707* EventSystem_get_current_mC87C69FB418563DC2A571A10E2F9DB59A6785016 (const RuntimeMethod* method) ;
// T UnityEngine.Object::FindObjectOfType<UnityEngine.EventSystems.EventSystem>()
inline EventSystem_t61C51380B105BE9D2C39C4F15B7E655659957707* Object_FindObjectOfType_TisEventSystem_t61C51380B105BE9D2C39C4F15B7E655659957707_m35D4A88CE80EF52117B3256977C521D1E9F2E7E4 (const RuntimeMethod* method)
{
	return ((  EventSystem_t61C51380B105BE9D2C39C4F15B7E655659957707* (*) (const RuntimeMethod*))Object_FindObjectOfType_TisRuntimeObject_m02DFBF011F3B59F777A5E521DB2A116DD496E968_gshared)(method);
}
// System.Void UnityEngine.EventSystems.PointerEventData::.ctor(UnityEngine.EventSystems.EventSystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PointerEventData__ctor_m63837790B68893F0022CCEFEF26ADD55A975F71C (PointerEventData_t9670F3C7D823CCB738A1604C72A1EB90292396FB* __this, EventSystem_t61C51380B105BE9D2C39C4F15B7E655659957707* ___0_eventSystem, const RuntimeMethod* method) ;
// System.Void UnityEngine.EventSystems.PointerEventData::set_position(UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PointerEventData_set_position_m66E8DFE693F550372E6B085C6E2F887FDB092FAA_inline (PointerEventData_t9670F3C7D823CCB738A1604C72A1EB90292396FB* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___0_value, const RuntimeMethod* method) ;
// System.Void UnityEngine.EventSystems.EventSystem::RaycastAll(UnityEngine.EventSystems.PointerEventData,System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EventSystem_RaycastAll_mE93CC75909438D20D17A0EF98348A064FBFEA528 (EventSystem_t61C51380B105BE9D2C39C4F15B7E655659957707* __this, PointerEventData_t9670F3C7D823CCB738A1604C72A1EB90292396FB* ___0_eventData, List_1_t8292C421BBB00D7661DC07462822936152BAB446* ___1_raycastResults, const RuntimeMethod* method) ;
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::GetEnumerator()
inline Enumerator_tFEA671794CD7ED9545DB6E9B1D2E744410E5EB1C List_1_GetEnumerator_m5032C7535D90065B4FA0A5942E7F92F4D72B8D41 (List_1_t8292C421BBB00D7661DC07462822936152BAB446* __this, const RuntimeMethod* method)
{
	return ((  Enumerator_tFEA671794CD7ED9545DB6E9B1D2E744410E5EB1C (*) (List_1_t8292C421BBB00D7661DC07462822936152BAB446*, const RuntimeMethod*))List_1_GetEnumerator_m5032C7535D90065B4FA0A5942E7F92F4D72B8D41_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::Dispose()
inline void Enumerator_Dispose_mE3FB1E26BA0B10EAB4C06CC56F1C78002726865C (Enumerator_tFEA671794CD7ED9545DB6E9B1D2E744410E5EB1C* __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_tFEA671794CD7ED9545DB6E9B1D2E744410E5EB1C*, const RuntimeMethod*))Enumerator_Dispose_mE3FB1E26BA0B10EAB4C06CC56F1C78002726865C_gshared)(__this, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::get_Current()
inline RaycastResult_tEC6A7B7CABA99C386F054F01E498AEC426CF8023 Enumerator_get_Current_m53EF02F206B7F773103FC51D50AC1B974AE692F4_inline (Enumerator_tFEA671794CD7ED9545DB6E9B1D2E744410E5EB1C* __this, const RuntimeMethod* method)
{
	return ((  RaycastResult_tEC6A7B7CABA99C386F054F01E498AEC426CF8023 (*) (Enumerator_tFEA671794CD7ED9545DB6E9B1D2E744410E5EB1C*, const RuntimeMethod*))Enumerator_get_Current_m53EF02F206B7F773103FC51D50AC1B974AE692F4_gshared_inline)(__this, method);
}
// UnityEngine.GameObject UnityEngine.EventSystems.RaycastResult::get_gameObject()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* RaycastResult_get_gameObject_m77014B442B9E2D10F2CC3AEEDC07AA95CDE1E2F1_inline (RaycastResult_tEC6A7B7CABA99C386F054F01E498AEC426CF8023* __this, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::MoveNext()
inline bool Enumerator_MoveNext_m22E22E0108683303C9E8F060E9C970D6AECF313C (Enumerator_tFEA671794CD7ED9545DB6E9B1D2E744410E5EB1C* __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tFEA671794CD7ED9545DB6E9B1D2E744410E5EB1C*, const RuntimeMethod*))Enumerator_MoveNext_m22E22E0108683303C9E8F060E9C970D6AECF313C_gshared)(__this, method);
}
// System.Void Lean.Common.LeanScreenQuery::DoRaycastUI(UnityEngine.Vector2,UnityEngine.Component&,System.Single&,UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanScreenQuery_DoRaycastUI_m82C6B1269710BCB6377C4F1D25B4B4351DB89B99 (LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___0_screenPosition, Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3** ___1_bestResult, float* ___2_bestDistance, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* ___3_bestPosition, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Int32)
inline void List_1__ctor_mB89D13B8B739042E3841C5DFAF30C3C51A797EA7 (List_1_t8292C421BBB00D7661DC07462822936152BAB446* __this, int32_t ___0_capacity, const RuntimeMethod* method)
{
	((  void (*) (List_1_t8292C421BBB00D7661DC07462822936152BAB446*, int32_t, const RuntimeMethod*))List_1__ctor_mB89D13B8B739042E3841C5DFAF30C3C51A797EA7_gshared)(__this, ___0_capacity, method);
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.GameObject,System.Int32>>::.ctor()
inline void List_1__ctor_m50607A2028B005421BBF8F137494E6877D6CAE5A (List_1_tC2267FBE21298A38669F48AD07AE8540B7AA80AF* __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tC2267FBE21298A38669F48AD07AE8540B7AA80AF*, const RuntimeMethod*))List_1__ctor_m28EFF96ED442DBB1D7387E7591CC371F0087B937_gshared)(__this, method);
}
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* __this, float ___0_x, float ___1_y, float ___2_z, const RuntimeMethod* method) ;
// System.Single UnityEngine.Vector3::Magnitude(UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector3_Magnitude_m21652D951393A3D7CE92CE40049A0E7F76544D1B_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_vector, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Division(UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_Division_mCC6BB24E372AB96B8380D1678446EF6A8BAE13BB_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_a, float ___1_d, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_get_zero_m0C1249C3F25B1C70EAD3CC8B31259975A457AE39_inline (const RuntimeMethod* method) ;
// System.Single UnityEngine.Mathf::Max(System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Mathf_Max_mF5379E63D2BBAC76D090748695D833934F8AD051_inline (float ___0_a, float ___1_b, const RuntimeMethod* method) ;
// System.Single UnityEngine.Mathf::Repeat(System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Mathf_Repeat_m6F1560A163481BB311D685294E1B463C3E4EB3BA_inline (float ___0_t, float ___1_length, const RuntimeMethod* method) ;
// UnityEngine.Quaternion UnityEngine.Quaternion::Internal_FromEulerRad(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 Quaternion_Internal_FromEulerRad_m66D4475341F53949471E6870FB5C5E4A5E9BA93E (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_euler, const RuntimeMethod* method) ;
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F* __this, float ___0_r, float ___1_g, float ___2_b, float ___3_a, const RuntimeMethod* method) ;
// System.Void System.Array::Clear(System.Array,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Array_Clear_m50BAA3751899858B097D3FF2ED31F284703FE5CB (RuntimeArray* ___0_array, int32_t ___1_index, int32_t ___2_length, const RuntimeMethod* method) ;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Common.LeanDestroy::set_Execute(Lean.Common.LeanDestroy/ExecuteType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanDestroy_set_Execute_m5C29E31056B8139967F89C45B8D447836062DAE9 (LeanDestroy_tB9D1797F553C4D87243711A8C9777654CE71E9B6* __this, int32_t ___0_value, const RuntimeMethod* method) 
{
	{
		// public ExecuteType Execute { set { execute = value; } get { return execute; } } [SerializeField] private ExecuteType execute = ExecuteType.Manually;
		int32_t L_0 = ___0_value;
		__this->___execute_4 = L_0;
		// public ExecuteType Execute { set { execute = value; } get { return execute; } } [SerializeField] private ExecuteType execute = ExecuteType.Manually;
		return;
	}
}
// Lean.Common.LeanDestroy/ExecuteType Lean.Common.LeanDestroy::get_Execute()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t LeanDestroy_get_Execute_m481B0CBE35A667EAA83678C5498882D1AEDE0546 (LeanDestroy_tB9D1797F553C4D87243711A8C9777654CE71E9B6* __this, const RuntimeMethod* method) 
{
	{
		// public ExecuteType Execute { set { execute = value; } get { return execute; } } [SerializeField] private ExecuteType execute = ExecuteType.Manually;
		int32_t L_0 = __this->___execute_4;
		return L_0;
	}
}
// System.Void Lean.Common.LeanDestroy::set_Target(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanDestroy_set_Target_mD718EFA46FEE09131B8B915D389A95D56D46FF65 (LeanDestroy_tB9D1797F553C4D87243711A8C9777654CE71E9B6* __this, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___0_value, const RuntimeMethod* method) 
{
	{
		// public GameObject Target { set { target = value; } get { return target; } } [SerializeField] private GameObject target;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = ___0_value;
		__this->___target_5 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___target_5), (void*)L_0);
		// public GameObject Target { set { target = value; } get { return target; } } [SerializeField] private GameObject target;
		return;
	}
}
// UnityEngine.GameObject Lean.Common.LeanDestroy::get_Target()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* LeanDestroy_get_Target_m30B946B4D8587451136B9AAB2308D8D5422292F9 (LeanDestroy_tB9D1797F553C4D87243711A8C9777654CE71E9B6* __this, const RuntimeMethod* method) 
{
	{
		// public GameObject Target { set { target = value; } get { return target; } } [SerializeField] private GameObject target;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___target_5;
		return L_0;
	}
}
// System.Void Lean.Common.LeanDestroy::set_Seconds(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanDestroy_set_Seconds_mDAD6221A4E324B314AB58B92956DE68F126ECF45 (LeanDestroy_tB9D1797F553C4D87243711A8C9777654CE71E9B6* __this, float ___0_value, const RuntimeMethod* method) 
{
	{
		// public float Seconds { set { seconds = value; } get { return seconds; } } [SerializeField] private float seconds = -1.0f;
		float L_0 = ___0_value;
		__this->___seconds_6 = L_0;
		// public float Seconds { set { seconds = value; } get { return seconds; } } [SerializeField] private float seconds = -1.0f;
		return;
	}
}
// System.Single Lean.Common.LeanDestroy::get_Seconds()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float LeanDestroy_get_Seconds_mE1EDED00363E72029C4154C84D8953DC56B0389D (LeanDestroy_tB9D1797F553C4D87243711A8C9777654CE71E9B6* __this, const RuntimeMethod* method) 
{
	{
		// public float Seconds { set { seconds = value; } get { return seconds; } } [SerializeField] private float seconds = -1.0f;
		float L_0 = __this->___seconds_6;
		return L_0;
	}
}
// System.Void Lean.Common.LeanDestroy::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanDestroy_Update_m4788151349CB14FC1919255CC9FBD0E1D8B056CB (LeanDestroy_tB9D1797F553C4D87243711A8C9777654CE71E9B6* __this, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	{
		// switch (execute)
		int32_t L_0 = __this->___execute_4;
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_001a;
			}
			case 1:
			{
				goto IL_0021;
			}
			case 2:
			{
				goto IL_0047;
			}
		}
	}
	{
		return;
	}

IL_001a:
	{
		// DestroyNow();
		LeanDestroy_DestroyNow_m2F3B8BCB8ED431FCC7EE6E7317A1FD84FF92BC9E(__this, NULL);
		// break;
		return;
	}

IL_0021:
	{
		// seconds -= Time.deltaTime;
		float L_2 = __this->___seconds_6;
		float L_3;
		L_3 = Time_get_deltaTime_mC3195000401F0FD167DD2F948FD2BC58330D0865(NULL);
		__this->___seconds_6 = ((float)il2cpp_codegen_subtract(L_2, L_3));
		// if (seconds <= 0.0f)
		float L_4 = __this->___seconds_6;
		if ((!(((float)L_4) <= ((float)(0.0f)))))
		{
			goto IL_006c;
		}
	}
	{
		// DestroyNow();
		LeanDestroy_DestroyNow_m2F3B8BCB8ED431FCC7EE6E7317A1FD84FF92BC9E(__this, NULL);
		// break;
		return;
	}

IL_0047:
	{
		// seconds -= Time.unscaledDeltaTime;
		float L_5 = __this->___seconds_6;
		float L_6;
		L_6 = Time_get_unscaledDeltaTime_mF057EECA857E5C0F90A3F910D26D3EE59F27C4B5(NULL);
		__this->___seconds_6 = ((float)il2cpp_codegen_subtract(L_5, L_6));
		// if (seconds <= 0.0f)
		float L_7 = __this->___seconds_6;
		if ((!(((float)L_7) <= ((float)(0.0f)))))
		{
			goto IL_006c;
		}
	}
	{
		// DestroyNow();
		LeanDestroy_DestroyNow_m2F3B8BCB8ED431FCC7EE6E7317A1FD84FF92BC9E(__this, NULL);
	}

IL_006c:
	{
		// }
		return;
	}
}
// System.Void Lean.Common.LeanDestroy::DestroyNow()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanDestroy_DestroyNow_m2F3B8BCB8ED431FCC7EE6E7317A1FD84FF92BC9E (LeanDestroy_tB9D1797F553C4D87243711A8C9777654CE71E9B6* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* G_B3_0 = NULL;
	{
		// execute = ExecuteType.Manually;
		__this->___execute_4 = 3;
		// Destroy(target != null ? target : gameObject);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___target_5;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_mD0BE578448EAA61948F25C32F8DD55AB1F778602(L_0, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (L_1)
		{
			goto IL_001d;
		}
	}
	{
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2;
		L_2 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(__this, NULL);
		G_B3_0 = L_2;
		goto IL_0023;
	}

IL_001d:
	{
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3 = __this->___target_5;
		G_B3_0 = L_3;
	}

IL_0023:
	{
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		Object_Destroy_mE97D0A766419A81296E8D4E5C23D01D3FE91ACBB(G_B3_0, NULL);
		// }
		return;
	}
}
// System.Void Lean.Common.LeanDestroy::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanDestroy__ctor_m870FFF17B12BC66ECC0A1C46355DEF4C8A842DDC (LeanDestroy_tB9D1797F553C4D87243711A8C9777654CE71E9B6* __this, const RuntimeMethod* method) 
{
	{
		// public ExecuteType Execute { set { execute = value; } get { return execute; } } [SerializeField] private ExecuteType execute = ExecuteType.Manually;
		__this->___execute_4 = 3;
		// public float Seconds { set { seconds = value; } get { return seconds; } } [SerializeField] private float seconds = -1.0f;
		__this->___seconds_6 = (-1.0f);
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Common.LeanFormatString::set_Format(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanFormatString_set_Format_m44BC70AF63CB152D7CCEA963C201F071257EDF56 (LeanFormatString_tA7B453B58D2C7E96D7ECF908A884299F3F6563EE* __this, String_t* ___0_value, const RuntimeMethod* method) 
{
	{
		// public string Format { set { format = value; } get { return format; } } [SerializeField] [Multiline] private string format = "Current Value = {0}";
		String_t* L_0 = ___0_value;
		__this->___format_4 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___format_4), (void*)L_0);
		// public string Format { set { format = value; } get { return format; } } [SerializeField] [Multiline] private string format = "Current Value = {0}";
		return;
	}
}
// System.String Lean.Common.LeanFormatString::get_Format()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* LeanFormatString_get_Format_mC8915C244025C812AD23E7DC225B9BAC09C678B4 (LeanFormatString_tA7B453B58D2C7E96D7ECF908A884299F3F6563EE* __this, const RuntimeMethod* method) 
{
	{
		// public string Format { set { format = value; } get { return format; } } [SerializeField] [Multiline] private string format = "Current Value = {0}";
		String_t* L_0 = __this->___format_4;
		return L_0;
	}
}
// Lean.Common.LeanFormatString/StringEvent Lean.Common.LeanFormatString::get_OnString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringEvent_t12A6CB6ADC4D0C58126DC29CA2AB35D8FD93E161* LeanFormatString_get_OnString_mAEA8872283296CDB0137A782BD7CE49770A5BC94 (LeanFormatString_tA7B453B58D2C7E96D7ECF908A884299F3F6563EE* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringEvent_t12A6CB6ADC4D0C58126DC29CA2AB35D8FD93E161_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public StringEvent OnString { get { if (onString == null) onString = new StringEvent(); return onString;  } } [SerializeField] private StringEvent onString;
		StringEvent_t12A6CB6ADC4D0C58126DC29CA2AB35D8FD93E161* L_0 = __this->___onString_5;
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		// public StringEvent OnString { get { if (onString == null) onString = new StringEvent(); return onString;  } } [SerializeField] private StringEvent onString;
		StringEvent_t12A6CB6ADC4D0C58126DC29CA2AB35D8FD93E161* L_1 = (StringEvent_t12A6CB6ADC4D0C58126DC29CA2AB35D8FD93E161*)il2cpp_codegen_object_new(StringEvent_t12A6CB6ADC4D0C58126DC29CA2AB35D8FD93E161_il2cpp_TypeInfo_var);
		NullCheck(L_1);
		StringEvent__ctor_m0EFECAE65155F17A84EA7E5F220261C6E03ABA0B(L_1, NULL);
		__this->___onString_5 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___onString_5), (void*)L_1);
	}

IL_0013:
	{
		// public StringEvent OnString { get { if (onString == null) onString = new StringEvent(); return onString;  } } [SerializeField] private StringEvent onString;
		StringEvent_t12A6CB6ADC4D0C58126DC29CA2AB35D8FD93E161* L_2 = __this->___onString_5;
		return L_2;
	}
}
// System.Void Lean.Common.LeanFormatString::SetString(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanFormatString_SetString_m8D34E974327EAB919F519DEB1783452FB3A9AB1E (LeanFormatString_tA7B453B58D2C7E96D7ECF908A884299F3F6563EE* __this, String_t* ___0_a, const RuntimeMethod* method) 
{
	{
		// SendString(a);
		String_t* L_0 = ___0_a;
		LeanFormatString_SendString_m9851DD3FFE8B07C743265B961F78D4F8065B9A52(__this, L_0, NULL);
		// }
		return;
	}
}
// System.Void Lean.Common.LeanFormatString::SetString(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanFormatString_SetString_m9F8B437E60F2E07935E34A100D515D4D73723C52 (LeanFormatString_tA7B453B58D2C7E96D7ECF908A884299F3F6563EE* __this, String_t* ___0_a, String_t* ___1_b, const RuntimeMethod* method) 
{
	{
		// SendString(a, b);
		String_t* L_0 = ___0_a;
		String_t* L_1 = ___1_b;
		LeanFormatString_SendString_m07D565F1CE9F6A76EE31FFC46C3B344AD653C419(__this, L_0, L_1, NULL);
		// }
		return;
	}
}
// System.Void Lean.Common.LeanFormatString::SetString(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanFormatString_SetString_m8F493F752F5512E72CAE6A441A07583E33C0CC25 (LeanFormatString_tA7B453B58D2C7E96D7ECF908A884299F3F6563EE* __this, int32_t ___0_a, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SendString(a);
		int32_t L_0 = ___0_a;
		int32_t L_1 = L_0;
		RuntimeObject* L_2 = Box(Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var, &L_1);
		LeanFormatString_SendString_m9851DD3FFE8B07C743265B961F78D4F8065B9A52(__this, L_2, NULL);
		// }
		return;
	}
}
// System.Void Lean.Common.LeanFormatString::SetString(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanFormatString_SetString_mB2BE0D52A16DDD83004E38DE7FFC4494768885C2 (LeanFormatString_tA7B453B58D2C7E96D7ECF908A884299F3F6563EE* __this, int32_t ___0_a, int32_t ___1_b, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SendString(a, b);
		int32_t L_0 = ___0_a;
		int32_t L_1 = L_0;
		RuntimeObject* L_2 = Box(Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var, &L_1);
		int32_t L_3 = ___1_b;
		int32_t L_4 = L_3;
		RuntimeObject* L_5 = Box(Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var, &L_4);
		LeanFormatString_SendString_m07D565F1CE9F6A76EE31FFC46C3B344AD653C419(__this, L_2, L_5, NULL);
		// }
		return;
	}
}
// System.Void Lean.Common.LeanFormatString::SetString(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanFormatString_SetString_m2437E031907355F53C319AA4BFD23C657BF1977C (LeanFormatString_tA7B453B58D2C7E96D7ECF908A884299F3F6563EE* __this, float ___0_a, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SendString(a);
		float L_0 = ___0_a;
		float L_1 = L_0;
		RuntimeObject* L_2 = Box(Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var, &L_1);
		LeanFormatString_SendString_m9851DD3FFE8B07C743265B961F78D4F8065B9A52(__this, L_2, NULL);
		// }
		return;
	}
}
// System.Void Lean.Common.LeanFormatString::SetString(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanFormatString_SetString_m58FA40B9691AD4AC08AC7AF928D472A388C9A731 (LeanFormatString_tA7B453B58D2C7E96D7ECF908A884299F3F6563EE* __this, float ___0_a, float ___1_b, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SendString(a, b);
		float L_0 = ___0_a;
		float L_1 = L_0;
		RuntimeObject* L_2 = Box(Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var, &L_1);
		float L_3 = ___1_b;
		float L_4 = L_3;
		RuntimeObject* L_5 = Box(Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var, &L_4);
		LeanFormatString_SendString_m07D565F1CE9F6A76EE31FFC46C3B344AD653C419(__this, L_2, L_5, NULL);
		// }
		return;
	}
}
// System.Void Lean.Common.LeanFormatString::SetString(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanFormatString_SetString_mFC595BB40EDB35B7C11FE607055D5CF7DDFDA262 (LeanFormatString_tA7B453B58D2C7E96D7ECF908A884299F3F6563EE* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___0_a, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SendString(a);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ___0_a;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_1 = L_0;
		RuntimeObject* L_2 = Box(Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_il2cpp_TypeInfo_var, &L_1);
		LeanFormatString_SendString_m9851DD3FFE8B07C743265B961F78D4F8065B9A52(__this, L_2, NULL);
		// }
		return;
	}
}
// System.Void Lean.Common.LeanFormatString::SetString(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanFormatString_SetString_m876CE712B0011822BF9161405056100B243AA3A8 (LeanFormatString_tA7B453B58D2C7E96D7ECF908A884299F3F6563EE* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___0_a, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___1_b, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SendString(a, b);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ___0_a;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_1 = L_0;
		RuntimeObject* L_2 = Box(Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_il2cpp_TypeInfo_var, &L_1);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_3 = ___1_b;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_4 = L_3;
		RuntimeObject* L_5 = Box(Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_il2cpp_TypeInfo_var, &L_4);
		LeanFormatString_SendString_m07D565F1CE9F6A76EE31FFC46C3B344AD653C419(__this, L_2, L_5, NULL);
		// }
		return;
	}
}
// System.Void Lean.Common.LeanFormatString::SetString(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanFormatString_SetString_m34E176B98EBBCD00635EED9AFD9444F6C759CA24 (LeanFormatString_tA7B453B58D2C7E96D7ECF908A884299F3F6563EE* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_a, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SendString(a);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___0_a;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1 = L_0;
		RuntimeObject* L_2 = Box(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var, &L_1);
		LeanFormatString_SendString_m9851DD3FFE8B07C743265B961F78D4F8065B9A52(__this, L_2, NULL);
		// }
		return;
	}
}
// System.Void Lean.Common.LeanFormatString::SetString(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanFormatString_SetString_m9C6E82EE5FE63366EAFD04D22746790647A5AEAD (LeanFormatString_tA7B453B58D2C7E96D7ECF908A884299F3F6563EE* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_a, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___1_b, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SendString(a, b);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___0_a;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1 = L_0;
		RuntimeObject* L_2 = Box(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var, &L_1);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_3 = ___1_b;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4 = L_3;
		RuntimeObject* L_5 = Box(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var, &L_4);
		LeanFormatString_SendString_m07D565F1CE9F6A76EE31FFC46C3B344AD653C419(__this, L_2, L_5, NULL);
		// }
		return;
	}
}
// System.Void Lean.Common.LeanFormatString::SetString(UnityEngine.Vector4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanFormatString_SetString_m8031C96BBB16DD6368CA6927A7E40FB79B1524DF (LeanFormatString_tA7B453B58D2C7E96D7ECF908A884299F3F6563EE* __this, Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___0_a, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SendString(a);
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_0 = ___0_a;
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_1 = L_0;
		RuntimeObject* L_2 = Box(Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3_il2cpp_TypeInfo_var, &L_1);
		LeanFormatString_SendString_m9851DD3FFE8B07C743265B961F78D4F8065B9A52(__this, L_2, NULL);
		// }
		return;
	}
}
// System.Void Lean.Common.LeanFormatString::SetString(UnityEngine.Vector4,UnityEngine.Vector4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanFormatString_SetString_mE42B62823E25A93A7E657F720F6ECCE6E1B34EE9 (LeanFormatString_tA7B453B58D2C7E96D7ECF908A884299F3F6563EE* __this, Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___0_a, Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___1_b, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SendString(a, b);
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_0 = ___0_a;
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_1 = L_0;
		RuntimeObject* L_2 = Box(Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3_il2cpp_TypeInfo_var, &L_1);
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_3 = ___1_b;
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_4 = L_3;
		RuntimeObject* L_5 = Box(Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3_il2cpp_TypeInfo_var, &L_4);
		LeanFormatString_SendString_m07D565F1CE9F6A76EE31FFC46C3B344AD653C419(__this, L_2, L_5, NULL);
		// }
		return;
	}
}
// System.Void Lean.Common.LeanFormatString::SetString(System.Single,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanFormatString_SetString_mD779B6F5DD7178CD857B422242EFA01C1DE25C99 (LeanFormatString_tA7B453B58D2C7E96D7ECF908A884299F3F6563EE* __this, float ___0_a, int32_t ___1_b, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SendString(a, b);
		float L_0 = ___0_a;
		float L_1 = L_0;
		RuntimeObject* L_2 = Box(Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var, &L_1);
		int32_t L_3 = ___1_b;
		int32_t L_4 = L_3;
		RuntimeObject* L_5 = Box(Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var, &L_4);
		LeanFormatString_SendString_m07D565F1CE9F6A76EE31FFC46C3B344AD653C419(__this, L_2, L_5, NULL);
		// }
		return;
	}
}
// System.Void Lean.Common.LeanFormatString::SetString(System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanFormatString_SetString_mC9A49868C45EC0A6253D0B321D8DAECEA8BBCF8D (LeanFormatString_tA7B453B58D2C7E96D7ECF908A884299F3F6563EE* __this, int32_t ___0_a, float ___1_b, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SendString(a, b);
		int32_t L_0 = ___0_a;
		int32_t L_1 = L_0;
		RuntimeObject* L_2 = Box(Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var, &L_1);
		float L_3 = ___1_b;
		float L_4 = L_3;
		RuntimeObject* L_5 = Box(Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var, &L_4);
		LeanFormatString_SendString_m07D565F1CE9F6A76EE31FFC46C3B344AD653C419(__this, L_2, L_5, NULL);
		// }
		return;
	}
}
// System.Void Lean.Common.LeanFormatString::SendString(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanFormatString_SendString_m9851DD3FFE8B07C743265B961F78D4F8065B9A52 (LeanFormatString_tA7B453B58D2C7E96D7ECF908A884299F3F6563EE* __this, RuntimeObject* ___0_a, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1_Invoke_mA633B48B5D287DA856FB954AC3E4012487E63C15_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (onString != null)
		StringEvent_t12A6CB6ADC4D0C58126DC29CA2AB35D8FD93E161* L_0 = __this->___onString_5;
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		// onString.Invoke(string.Format(format, a));
		StringEvent_t12A6CB6ADC4D0C58126DC29CA2AB35D8FD93E161* L_1 = __this->___onString_5;
		String_t* L_2 = __this->___format_4;
		RuntimeObject* L_3 = ___0_a;
		String_t* L_4;
		L_4 = String_Format_mA8DBB4C2516B9723C5A41E6CB1E2FAF4BBE96DD8(L_2, L_3, NULL);
		NullCheck(L_1);
		UnityEvent_1_Invoke_mA633B48B5D287DA856FB954AC3E4012487E63C15(L_1, L_4, UnityEvent_1_Invoke_mA633B48B5D287DA856FB954AC3E4012487E63C15_RuntimeMethod_var);
	}

IL_001f:
	{
		// }
		return;
	}
}
// System.Void Lean.Common.LeanFormatString::SendString(System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanFormatString_SendString_m07D565F1CE9F6A76EE31FFC46C3B344AD653C419 (LeanFormatString_tA7B453B58D2C7E96D7ECF908A884299F3F6563EE* __this, RuntimeObject* ___0_a, RuntimeObject* ___1_b, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1_Invoke_mA633B48B5D287DA856FB954AC3E4012487E63C15_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (onString != null)
		StringEvent_t12A6CB6ADC4D0C58126DC29CA2AB35D8FD93E161* L_0 = __this->___onString_5;
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		// onString.Invoke(string.Format(format, a, b));
		StringEvent_t12A6CB6ADC4D0C58126DC29CA2AB35D8FD93E161* L_1 = __this->___onString_5;
		String_t* L_2 = __this->___format_4;
		RuntimeObject* L_3 = ___0_a;
		RuntimeObject* L_4 = ___1_b;
		String_t* L_5;
		L_5 = String_Format_mFB7DA489BD99F4670881FF50EC017BFB0A5C0987(L_2, L_3, L_4, NULL);
		NullCheck(L_1);
		UnityEvent_1_Invoke_mA633B48B5D287DA856FB954AC3E4012487E63C15(L_1, L_5, UnityEvent_1_Invoke_mA633B48B5D287DA856FB954AC3E4012487E63C15_RuntimeMethod_var);
	}

IL_0020:
	{
		// }
		return;
	}
}
// System.Void Lean.Common.LeanFormatString::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanFormatString__ctor_mE9E92C890B48F40081B87F48C3F3752EE92F8DD1 (LeanFormatString_tA7B453B58D2C7E96D7ECF908A884299F3F6563EE* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5D1DD7CD3FED08FC2AEEF290CBF04FF642A87F37);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public string Format { set { format = value; } get { return format; } } [SerializeField] [Multiline] private string format = "Current Value = {0}";
		__this->___format_4 = _stringLiteral5D1DD7CD3FED08FC2AEEF290CBF04FF642A87F37;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___format_4), (void*)_stringLiteral5D1DD7CD3FED08FC2AEEF290CBF04FF642A87F37);
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Common.LeanFormatString/StringEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StringEvent__ctor_m0EFECAE65155F17A84EA7E5F220261C6E03ABA0B (StringEvent_t12A6CB6ADC4D0C58126DC29CA2AB35D8FD93E161* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1__ctor_m0F7D790DACD6F3D18E865D01FE4427603C1B0CC6_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_m0F7D790DACD6F3D18E865D01FE4427603C1B0CC6(__this, UnityEvent_1__ctor_m0F7D790DACD6F3D18E865D01FE4427603C1B0CC6_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Int32 Lean.Common.LeanPath::get_PointCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t LeanPath_get_PointCount_mF5F7F552E76CE7E52AA8B9FA38760AF801136B88 (LeanPath_t52FC47E81D073093417CE48BADFF80E5182D132F* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m46EEFFA770BE665EA0CB3A5332E941DA4B3C1D37_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// if (Points != null)
		List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B* L_0 = __this->___Points_4;
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		// var count = Points.Count;
		List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B* L_1 = __this->___Points_4;
		NullCheck(L_1);
		int32_t L_2;
		L_2 = List_1_get_Count_m46EEFFA770BE665EA0CB3A5332E941DA4B3C1D37_inline(L_1, List_1_get_Count_m46EEFFA770BE665EA0CB3A5332E941DA4B3C1D37_RuntimeMethod_var);
		V_0 = L_2;
		// if (count >= 2)
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) < ((int32_t)2)))
		{
			goto IL_0026;
		}
	}
	{
		// if (Loop == true)
		bool L_4 = __this->___Loop_5;
		if (!L_4)
		{
			goto IL_0024;
		}
	}
	{
		// return count + 1;
		int32_t L_5 = V_0;
		return ((int32_t)il2cpp_codegen_add(L_5, 1));
	}

IL_0024:
	{
		// return count;
		int32_t L_6 = V_0;
		return L_6;
	}

IL_0026:
	{
		// return 0;
		return 0;
	}
}
// System.Int32 Lean.Common.LeanPath::GetPointCount(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t LeanPath_GetPointCount_m904AC0FF40B2AE9750C9541C035CF128F9FB6094 (LeanPath_t52FC47E81D073093417CE48BADFF80E5182D132F* __this, int32_t ___0_smoothing, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m46EEFFA770BE665EA0CB3A5332E941DA4B3C1D37_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// if (Points != null)
		List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B* L_0 = __this->___Points_4;
		if (!L_0)
		{
			goto IL_003e;
		}
	}
	{
		// if (smoothing < 0)
		int32_t L_1 = ___0_smoothing;
		if ((((int32_t)L_1) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		// smoothing = Smoothing;
		int32_t L_2 = __this->___Smoothing_7;
		___0_smoothing = L_2;
	}

IL_0014:
	{
		// var count = Points.Count;
		List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B* L_3 = __this->___Points_4;
		NullCheck(L_3);
		int32_t L_4;
		L_4 = List_1_get_Count_m46EEFFA770BE665EA0CB3A5332E941DA4B3C1D37_inline(L_3, List_1_get_Count_m46EEFFA770BE665EA0CB3A5332E941DA4B3C1D37_RuntimeMethod_var);
		V_0 = L_4;
		// if (count >= 2 && smoothing >= 1)
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) < ((int32_t)2)))
		{
			goto IL_003e;
		}
	}
	{
		int32_t L_6 = ___0_smoothing;
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_003e;
		}
	}
	{
		// if (Loop == true)
		bool L_7 = __this->___Loop_5;
		if (!L_7)
		{
			goto IL_0036;
		}
	}
	{
		// return count * smoothing + 1;
		int32_t L_8 = V_0;
		int32_t L_9 = ___0_smoothing;
		return ((int32_t)il2cpp_codegen_add(((int32_t)il2cpp_codegen_multiply(L_8, L_9)), 1));
	}

IL_0036:
	{
		// return (count - 1) * smoothing + 1;
		int32_t L_10 = V_0;
		int32_t L_11 = ___0_smoothing;
		return ((int32_t)il2cpp_codegen_add(((int32_t)il2cpp_codegen_multiply(((int32_t)il2cpp_codegen_subtract(L_10, 1)), L_11)), 1));
	}

IL_003e:
	{
		// return 0;
		return 0;
	}
}
// UnityEngine.Vector3 Lean.Common.LeanPath::GetSmoothedPoint(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 LeanPath_GetSmoothedPoint_m7284BFFDC1350CD161F27992008413A28EB6A309 (LeanPath_t52FC47E81D073093417CE48BADFF80E5182D132F* __this, float ___0_index, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m46EEFFA770BE665EA0CB3A5332E941DA4B3C1D37_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_3;
	memset((&V_3), 0, sizeof(V_3));
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_4;
	memset((&V_4), 0, sizeof(V_4));
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_5;
	memset((&V_5), 0, sizeof(V_5));
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_6;
	memset((&V_6), 0, sizeof(V_6));
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_7;
	memset((&V_7), 0, sizeof(V_7));
	{
		// if (Points == null)
		List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B* L_0 = __this->___Points_4;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		// throw new System.IndexOutOfRangeException();
		IndexOutOfRangeException_t7ECB35264FB6CA8FAA516BD958F4B2ADC78E8A82* L_1 = (IndexOutOfRangeException_t7ECB35264FB6CA8FAA516BD958F4B2ADC78E8A82*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&IndexOutOfRangeException_t7ECB35264FB6CA8FAA516BD958F4B2ADC78E8A82_il2cpp_TypeInfo_var)));
		NullCheck(L_1);
		IndexOutOfRangeException__ctor_m270ED9671475CE680EEA8C62A7A43308AE4188EF(L_1, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&LeanPath_GetSmoothedPoint_m7284BFFDC1350CD161F27992008413A28EB6A309_RuntimeMethod_var)));
	}

IL_000e:
	{
		// var count = Points.Count;
		List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B* L_2 = __this->___Points_4;
		NullCheck(L_2);
		int32_t L_3;
		L_3 = List_1_get_Count_m46EEFFA770BE665EA0CB3A5332E941DA4B3C1D37_inline(L_2, List_1_get_Count_m46EEFFA770BE665EA0CB3A5332E941DA4B3C1D37_RuntimeMethod_var);
		V_0 = L_3;
		// if (count < 2)
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) >= ((int32_t)2)))
		{
			goto IL_0024;
		}
	}
	{
		// throw new System.Exception();
		Exception_t* L_5 = (Exception_t*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)));
		NullCheck(L_5);
		Exception__ctor_m203319D1EA1274689B380A947B4ADC8445662B8F(L_5, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&LeanPath_GetSmoothedPoint_m7284BFFDC1350CD161F27992008413A28EB6A309_RuntimeMethod_var)));
	}

IL_0024:
	{
		// var i = (int)index;
		float L_6 = ___0_index;
		V_1 = il2cpp_codegen_cast_double_to_int<int32_t>(L_6);
		// var t = Mathf.Abs(index - i);
		float L_7 = ___0_index;
		int32_t L_8 = V_1;
		float L_9;
		L_9 = fabsf(((float)il2cpp_codegen_subtract(L_7, ((float)L_8))));
		V_2 = L_9;
		// var a = GetPointRaw(i - 1, count);
		int32_t L_10 = V_1;
		int32_t L_11 = V_0;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_12;
		L_12 = LeanPath_GetPointRaw_mEFEC1A35269B12C264B32A53883F27E347327D19(__this, ((int32_t)il2cpp_codegen_subtract(L_10, 1)), L_11, NULL);
		V_3 = L_12;
		// var b = GetPointRaw(i    , count);
		int32_t L_13 = V_1;
		int32_t L_14 = V_0;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_15;
		L_15 = LeanPath_GetPointRaw_mEFEC1A35269B12C264B32A53883F27E347327D19(__this, L_13, L_14, NULL);
		V_4 = L_15;
		// var c = GetPointRaw(i + 1, count);
		int32_t L_16 = V_1;
		int32_t L_17 = V_0;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_18;
		L_18 = LeanPath_GetPointRaw_mEFEC1A35269B12C264B32A53883F27E347327D19(__this, ((int32_t)il2cpp_codegen_add(L_16, 1)), L_17, NULL);
		V_5 = L_18;
		// var d = GetPointRaw(i + 2, count);
		int32_t L_19 = V_1;
		int32_t L_20 = V_0;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_21;
		L_21 = LeanPath_GetPointRaw_mEFEC1A35269B12C264B32A53883F27E347327D19(__this, ((int32_t)il2cpp_codegen_add(L_19, 2)), L_20, NULL);
		V_6 = L_21;
		// var p = default(Vector3);
		il2cpp_codegen_initobj((&V_7), sizeof(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2));
		// p.x = CubicInterpolate(a.x, b.x, c.x, d.x, t);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_22 = V_3;
		float L_23 = L_22.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_24 = V_4;
		float L_25 = L_24.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_26 = V_5;
		float L_27 = L_26.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_28 = V_6;
		float L_29 = L_28.___x_2;
		float L_30 = V_2;
		float L_31;
		L_31 = LeanPath_CubicInterpolate_m1AA1DCBB91FEE95D41E5082FD32446ED979B4D84(__this, L_23, L_25, L_27, L_29, L_30, NULL);
		(&V_7)->___x_2 = L_31;
		// p.y = CubicInterpolate(a.y, b.y, c.y, d.y, t);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_32 = V_3;
		float L_33 = L_32.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_34 = V_4;
		float L_35 = L_34.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_36 = V_5;
		float L_37 = L_36.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_38 = V_6;
		float L_39 = L_38.___y_3;
		float L_40 = V_2;
		float L_41;
		L_41 = LeanPath_CubicInterpolate_m1AA1DCBB91FEE95D41E5082FD32446ED979B4D84(__this, L_33, L_35, L_37, L_39, L_40, NULL);
		(&V_7)->___y_3 = L_41;
		// p.z = CubicInterpolate(a.z, b.z, c.z, d.z, t);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_42 = V_3;
		float L_43 = L_42.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_44 = V_4;
		float L_45 = L_44.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_46 = V_5;
		float L_47 = L_46.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_48 = V_6;
		float L_49 = L_48.___z_4;
		float L_50 = V_2;
		float L_51;
		L_51 = LeanPath_CubicInterpolate_m1AA1DCBB91FEE95D41E5082FD32446ED979B4D84(__this, L_43, L_45, L_47, L_49, L_50, NULL);
		(&V_7)->___z_4 = L_51;
		// return p;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_52 = V_7;
		return L_52;
	}
}
// UnityEngine.Vector3 Lean.Common.LeanPath::GetPoint(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 LeanPath_GetPoint_m809DDF4B617B43CB30CA6EF2C33AA8AD3A4D6659 (LeanPath_t52FC47E81D073093417CE48BADFF80E5182D132F* __this, int32_t ___0_index, int32_t ___1_smoothing, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m46EEFFA770BE665EA0CB3A5332E941DA4B3C1D37_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// if (Points == null)
		List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B* L_0 = __this->___Points_4;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		// throw new System.IndexOutOfRangeException();
		IndexOutOfRangeException_t7ECB35264FB6CA8FAA516BD958F4B2ADC78E8A82* L_1 = (IndexOutOfRangeException_t7ECB35264FB6CA8FAA516BD958F4B2ADC78E8A82*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&IndexOutOfRangeException_t7ECB35264FB6CA8FAA516BD958F4B2ADC78E8A82_il2cpp_TypeInfo_var)));
		NullCheck(L_1);
		IndexOutOfRangeException__ctor_m270ED9671475CE680EEA8C62A7A43308AE4188EF(L_1, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&LeanPath_GetPoint_m809DDF4B617B43CB30CA6EF2C33AA8AD3A4D6659_RuntimeMethod_var)));
	}

IL_000e:
	{
		// if (smoothing < 0)
		int32_t L_2 = ___1_smoothing;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_001a;
		}
	}
	{
		// smoothing = Smoothing;
		int32_t L_3 = __this->___Smoothing_7;
		___1_smoothing = L_3;
	}

IL_001a:
	{
		// if (smoothing < 1)
		int32_t L_4 = ___1_smoothing;
		if ((((int32_t)L_4) >= ((int32_t)1)))
		{
			goto IL_0024;
		}
	}
	{
		// throw new System.ArgumentOutOfRangeException();
		ArgumentOutOfRangeException_tEA2822DAF62B10EEED00E0E3A341D4BAF78CF85F* L_5 = (ArgumentOutOfRangeException_tEA2822DAF62B10EEED00E0E3A341D4BAF78CF85F*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentOutOfRangeException_tEA2822DAF62B10EEED00E0E3A341D4BAF78CF85F_il2cpp_TypeInfo_var)));
		NullCheck(L_5);
		ArgumentOutOfRangeException__ctor_mB596C51BFA864B65C2CED275458FAE90F7CD29C9(L_5, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&LeanPath_GetPoint_m809DDF4B617B43CB30CA6EF2C33AA8AD3A4D6659_RuntimeMethod_var)));
	}

IL_0024:
	{
		// var count = Points.Count;
		List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B* L_6 = __this->___Points_4;
		NullCheck(L_6);
		int32_t L_7;
		L_7 = List_1_get_Count_m46EEFFA770BE665EA0CB3A5332E941DA4B3C1D37_inline(L_6, List_1_get_Count_m46EEFFA770BE665EA0CB3A5332E941DA4B3C1D37_RuntimeMethod_var);
		V_0 = L_7;
		// if (count < 2)
		int32_t L_8 = V_0;
		if ((((int32_t)L_8) >= ((int32_t)2)))
		{
			goto IL_003a;
		}
	}
	{
		// throw new System.Exception();
		Exception_t* L_9 = (Exception_t*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)));
		NullCheck(L_9);
		Exception__ctor_m203319D1EA1274689B380A947B4ADC8445662B8F(L_9, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&LeanPath_GetPoint_m809DDF4B617B43CB30CA6EF2C33AA8AD3A4D6659_RuntimeMethod_var)));
	}

IL_003a:
	{
		// if (smoothing > 0)
		int32_t L_10 = ___1_smoothing;
		if ((((int32_t)L_10) <= ((int32_t)0)))
		{
			goto IL_004a;
		}
	}
	{
		// return GetSmoothedPoint(index / (float)smoothing);
		int32_t L_11 = ___0_index;
		int32_t L_12 = ___1_smoothing;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_13;
		L_13 = LeanPath_GetSmoothedPoint_m7284BFFDC1350CD161F27992008413A28EB6A309(__this, ((float)(((float)L_11)/((float)L_12))), NULL);
		return L_13;
	}

IL_004a:
	{
		// return GetPointRaw(index, count);
		int32_t L_14 = ___0_index;
		int32_t L_15 = V_0;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_16;
		L_16 = LeanPath_GetPointRaw_mEFEC1A35269B12C264B32A53883F27E347327D19(__this, L_14, L_15, NULL);
		return L_16;
	}
}
// UnityEngine.Vector3 Lean.Common.LeanPath::GetPointRaw(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 LeanPath_GetPointRaw_mEFEC1A35269B12C264B32A53883F27E347327D19 (LeanPath_t52FC47E81D073093417CE48BADFF80E5182D132F* __this, int32_t ___0_index, int32_t ___1_count, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m8F2E15FC96DA75186C51228128A0660709E4E810_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// if (Loop == true)
		bool L_0 = __this->___Loop_5;
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		// index = Mod(index, count);
		int32_t L_1 = ___0_index;
		int32_t L_2 = ___1_count;
		int32_t L_3;
		L_3 = LeanPath_Mod_mA52316BA69AE1DC6A8B5F15D76589B4055DCFA66(__this, L_1, L_2, NULL);
		___0_index = L_3;
		goto IL_0020;
	}

IL_0014:
	{
		// index = Mathf.Clamp(index, 0, count - 1);
		int32_t L_4 = ___0_index;
		int32_t L_5 = ___1_count;
		int32_t L_6;
		L_6 = Mathf_Clamp_m4DC36EEFDBE5F07C16249DA568023C5ECCFF0E7B_inline(L_4, 0, ((int32_t)il2cpp_codegen_subtract(L_5, 1)), NULL);
		___0_index = L_6;
	}

IL_0020:
	{
		// var point = Points[index];
		List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B* L_7 = __this->___Points_4;
		int32_t L_8 = ___0_index;
		NullCheck(L_7);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_9;
		L_9 = List_1_get_Item_m8F2E15FC96DA75186C51228128A0660709E4E810(L_7, L_8, List_1_get_Item_m8F2E15FC96DA75186C51228128A0660709E4E810_RuntimeMethod_var);
		V_0 = L_9;
		// if (Space == Space.Self)
		int32_t L_10 = __this->___Space_6;
		if ((!(((uint32_t)L_10) == ((uint32_t)1))))
		{
			goto IL_0043;
		}
	}
	{
		// point = transform.TransformPoint(point);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_11;
		L_11 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_12 = V_0;
		NullCheck(L_11);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_13;
		L_13 = Transform_TransformPoint_m05BFF013DB830D7BFE44A007703694AE1062EE44(L_11, L_12, NULL);
		V_0 = L_13;
	}

IL_0043:
	{
		// return point;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_14 = V_0;
		return L_14;
	}
}
// System.Void Lean.Common.LeanPath::SetLine(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanPath_SetLine_m5221734FC5F5B4482B46A24C5CC2C8355B16BB34 (LeanPath_t52FC47E81D073093417CE48BADFF80E5182D132F* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_a, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___1_b, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m79E50C4F592B1703F4B76A8BE7B4855515460CA1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Clear_m455780C5A45049F9BDC25EAD3BA10A681D16385D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mC54E2BCBE43279A96FC082F5CDE2D76388BD8F9C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Points == null)
		List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B* L_0 = __this->___Points_4;
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		// Points = new List<Vector3>();
		List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B* L_1 = (List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B*)il2cpp_codegen_object_new(List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B_il2cpp_TypeInfo_var);
		NullCheck(L_1);
		List_1__ctor_mC54E2BCBE43279A96FC082F5CDE2D76388BD8F9C(L_1, List_1__ctor_mC54E2BCBE43279A96FC082F5CDE2D76388BD8F9C_RuntimeMethod_var);
		__this->___Points_4 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___Points_4), (void*)L_1);
		goto IL_0020;
	}

IL_0015:
	{
		// Points.Clear();
		List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B* L_2 = __this->___Points_4;
		NullCheck(L_2);
		List_1_Clear_m455780C5A45049F9BDC25EAD3BA10A681D16385D_inline(L_2, List_1_Clear_m455780C5A45049F9BDC25EAD3BA10A681D16385D_RuntimeMethod_var);
	}

IL_0020:
	{
		// Points.Add(a);
		List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B* L_3 = __this->___Points_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4 = ___0_a;
		NullCheck(L_3);
		List_1_Add_m79E50C4F592B1703F4B76A8BE7B4855515460CA1_inline(L_3, L_4, List_1_Add_m79E50C4F592B1703F4B76A8BE7B4855515460CA1_RuntimeMethod_var);
		// Points.Add(b);
		List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B* L_5 = __this->___Points_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6 = ___1_b;
		NullCheck(L_5);
		List_1_Add_m79E50C4F592B1703F4B76A8BE7B4855515460CA1_inline(L_5, L_6, List_1_Add_m79E50C4F592B1703F4B76A8BE7B4855515460CA1_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Boolean Lean.Common.LeanPath::TryGetClosest(UnityEngine.Vector3,UnityEngine.Vector3&,System.Int32&,System.Int32&,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool LeanPath_TryGetClosest_m961C2E4329FDA0D8F6B54898725ACBD69317C659 (LeanPath_t52FC47E81D073093417CE48BADFF80E5182D132F* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_position, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* ___1_closestPoint, int32_t* ___2_closestIndexA, int32_t* ___3_closestIndexB, int32_t ___4_smoothing, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeanPath_t52FC47E81D073093417CE48BADFF80E5182D132F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_2;
	memset((&V_2), 0, sizeof(V_2));
	float V_3 = 0.0f;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_6;
	memset((&V_6), 0, sizeof(V_6));
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_7;
	memset((&V_7), 0, sizeof(V_7));
	float V_8 = 0.0f;
	{
		// var count = GetPointCount(smoothing);
		int32_t L_0 = ___4_smoothing;
		int32_t L_1;
		L_1 = LeanPath_GetPointCount_m904AC0FF40B2AE9750C9541C035CF128F9FB6094(__this, L_0, NULL);
		V_0 = L_1;
		// if (count >= 2)
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) < ((int32_t)2)))
		{
			goto IL_0091;
		}
	}
	{
		// var indexA          = 0;
		V_1 = 0;
		// var pointA          = GetPoint(indexA, smoothing);
		int32_t L_3 = V_1;
		int32_t L_4 = ___4_smoothing;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_5;
		L_5 = LeanPath_GetPoint_m809DDF4B617B43CB30CA6EF2C33AA8AD3A4D6659(__this, L_3, L_4, NULL);
		V_2 = L_5;
		// var closestDistance = float.PositiveInfinity;
		V_3 = (std::numeric_limits<float>::infinity());
		// for (var i = 1; i < count; i++)
		V_4 = 1;
		goto IL_008a;
	}

IL_0027:
	{
		// var indexB   = i;
		int32_t L_6 = V_4;
		V_5 = L_6;
		// var pointB   = GetPoint(indexB, smoothing);
		int32_t L_7 = V_5;
		int32_t L_8 = ___4_smoothing;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_9;
		L_9 = LeanPath_GetPoint_m809DDF4B617B43CB30CA6EF2C33AA8AD3A4D6659(__this, L_7, L_8, NULL);
		V_6 = L_9;
		// var point    = GetClosestPoint(position, pointA, pointB - pointA);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = ___0_position;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_11 = V_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_12 = V_6;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_13 = V_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_14;
		L_14 = Vector3_op_Subtraction_mE42023FF80067CB44A1D4A27EB7CF2B24CABB828_inline(L_12, L_13, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_15;
		L_15 = LeanPath_GetClosestPoint_m35BCB88CADA5172B750A9C1DC015CF57704CDA95(__this, L_10, L_11, L_14, NULL);
		V_7 = L_15;
		// var distance = Vector3.Distance(position, point);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_16 = ___0_position;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_17 = V_7;
		float L_18;
		L_18 = Vector3_Distance_m2314DB9B8BD01157E013DF87BEA557375C7F9FF9_inline(L_16, L_17, NULL);
		V_8 = L_18;
		// if (distance < closestDistance)
		float L_19 = V_8;
		float L_20 = V_3;
		if ((!(((float)L_19) < ((float)L_20))))
		{
			goto IL_007e;
		}
	}
	{
		// closestIndexA   = indexA;
		int32_t* L_21 = ___2_closestIndexA;
		int32_t L_22 = V_1;
		*((int32_t*)L_21) = (int32_t)L_22;
		// closestIndexB   = i;
		int32_t* L_23 = ___3_closestIndexB;
		int32_t L_24 = V_4;
		*((int32_t*)L_23) = (int32_t)L_24;
		// closestPoint    = point;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_25 = ___1_closestPoint;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_26 = V_7;
		*(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*)L_25 = L_26;
		// closestDistance = distance;
		float L_27 = V_8;
		V_3 = L_27;
		// LastWorldNormal = Vector3.Normalize(point - pointB);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_28 = V_7;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_29 = V_6;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_30;
		L_30 = Vector3_op_Subtraction_mE42023FF80067CB44A1D4A27EB7CF2B24CABB828_inline(L_28, L_29, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_31;
		L_31 = Vector3_Normalize_mEF8349CC39674236CFC694189AFD36E31F89AC8F_inline(L_30, NULL);
		il2cpp_codegen_runtime_class_init_inline(LeanPath_t52FC47E81D073093417CE48BADFF80E5182D132F_il2cpp_TypeInfo_var);
		((LeanPath_t52FC47E81D073093417CE48BADFF80E5182D132F_StaticFields*)il2cpp_codegen_static_fields_for(LeanPath_t52FC47E81D073093417CE48BADFF80E5182D132F_il2cpp_TypeInfo_var))->___LastWorldNormal_9 = L_31;
	}

IL_007e:
	{
		// pointA = pointB;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_32 = V_6;
		V_2 = L_32;
		// indexA = indexB;
		int32_t L_33 = V_5;
		V_1 = L_33;
		// for (var i = 1; i < count; i++)
		int32_t L_34 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add(L_34, 1));
	}

IL_008a:
	{
		// for (var i = 1; i < count; i++)
		int32_t L_35 = V_4;
		int32_t L_36 = V_0;
		if ((((int32_t)L_35) < ((int32_t)L_36)))
		{
			goto IL_0027;
		}
	}
	{
		// return true;
		return (bool)1;
	}

IL_0091:
	{
		// return false;
		return (bool)0;
	}
}
// System.Boolean Lean.Common.LeanPath::TryGetClosest(UnityEngine.Vector3,UnityEngine.Vector3&,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool LeanPath_TryGetClosest_m445515693D3D0C0908FB0533F01DC6E59238C2B9 (LeanPath_t52FC47E81D073093417CE48BADFF80E5182D132F* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_position, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* ___1_closestPoint, int32_t ___2_smoothing, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		// var closestIndexA = default(int);
		V_0 = 0;
		// var closestIndexB = default(int);
		V_1 = 0;
		// return TryGetClosest(position, ref closestPoint, ref closestIndexA, ref closestIndexB, smoothing);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___0_position;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_1 = ___1_closestPoint;
		int32_t L_2 = ___2_smoothing;
		bool L_3;
		L_3 = LeanPath_TryGetClosest_m961C2E4329FDA0D8F6B54898725ACBD69317C659(__this, L_0, L_1, (&V_0), (&V_1), L_2, NULL);
		return L_3;
	}
}
// System.Boolean Lean.Common.LeanPath::TryGetClosest(UnityEngine.Ray,UnityEngine.Vector3&,System.Int32&,System.Int32&,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool LeanPath_TryGetClosest_mC5CDF1FF80E51741388D1DE012E07E52B722485B (LeanPath_t52FC47E81D073093417CE48BADFF80E5182D132F* __this, Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 ___0_ray, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* ___1_closestPoint, int32_t* ___2_closestIndexA, int32_t* ___3_closestIndexB, int32_t ___4_smoothing, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeanPath_t52FC47E81D073093417CE48BADFF80E5182D132F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_2;
	memset((&V_2), 0, sizeof(V_2));
	float V_3 = 0.0f;
	int32_t V_4 = 0;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_5;
	memset((&V_5), 0, sizeof(V_5));
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_6;
	memset((&V_6), 0, sizeof(V_6));
	float V_7 = 0.0f;
	{
		// var count = GetPointCount(smoothing);
		int32_t L_0 = ___4_smoothing;
		int32_t L_1;
		L_1 = LeanPath_GetPointCount_m904AC0FF40B2AE9750C9541C035CF128F9FB6094(__this, L_0, NULL);
		V_0 = L_1;
		// if (count >= 2)
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) < ((int32_t)2)))
		{
			goto IL_008b;
		}
	}
	{
		// var indexA          = 0;
		V_1 = 0;
		// var pointA          = GetPoint(0, smoothing);
		int32_t L_3 = ___4_smoothing;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4;
		L_4 = LeanPath_GetPoint_m809DDF4B617B43CB30CA6EF2C33AA8AD3A4D6659(__this, 0, L_3, NULL);
		V_2 = L_4;
		// var closestDistance = float.PositiveInfinity;
		V_3 = (std::numeric_limits<float>::infinity());
		// for (var i = 1; i < count; i++)
		V_4 = 1;
		goto IL_0084;
	}

IL_0024:
	{
		// var pointB   = GetPoint(i, smoothing);
		int32_t L_5 = V_4;
		int32_t L_6 = ___4_smoothing;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_7;
		L_7 = LeanPath_GetPoint_m809DDF4B617B43CB30CA6EF2C33AA8AD3A4D6659(__this, L_5, L_6, NULL);
		V_5 = L_7;
		// var point    = GetClosestPoint(ray, pointA, pointB - pointA);
		Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 L_8 = ___0_ray;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_9 = V_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = V_5;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_11 = V_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_12;
		L_12 = Vector3_op_Subtraction_mE42023FF80067CB44A1D4A27EB7CF2B24CABB828_inline(L_10, L_11, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_13;
		L_13 = LeanPath_GetClosestPoint_m8C1E2BD9CAB8AD2A3D281FADAA705D2A9D3224C4(__this, L_8, L_9, L_12, NULL);
		V_6 = L_13;
		// var distance = GetClosestDistance(ray, point);
		Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 L_14 = ___0_ray;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_15 = V_6;
		float L_16;
		L_16 = LeanPath_GetClosestDistance_m25EDF5A03B96A0615B55298C1458859FC6253AC8(__this, L_14, L_15, NULL);
		V_7 = L_16;
		// if (distance < closestDistance)
		float L_17 = V_7;
		float L_18 = V_3;
		if ((!(((float)L_17) < ((float)L_18))))
		{
			goto IL_0078;
		}
	}
	{
		// closestIndexA   = indexA;
		int32_t* L_19 = ___2_closestIndexA;
		int32_t L_20 = V_1;
		*((int32_t*)L_19) = (int32_t)L_20;
		// closestIndexB   = i;
		int32_t* L_21 = ___3_closestIndexB;
		int32_t L_22 = V_4;
		*((int32_t*)L_21) = (int32_t)L_22;
		// closestPoint    = point;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_23 = ___1_closestPoint;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_24 = V_6;
		*(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*)L_23 = L_24;
		// closestDistance = distance;
		float L_25 = V_7;
		V_3 = L_25;
		// LastWorldNormal = Vector3.Normalize(point - pointB);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_26 = V_6;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_27 = V_5;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_28;
		L_28 = Vector3_op_Subtraction_mE42023FF80067CB44A1D4A27EB7CF2B24CABB828_inline(L_26, L_27, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_29;
		L_29 = Vector3_Normalize_mEF8349CC39674236CFC694189AFD36E31F89AC8F_inline(L_28, NULL);
		il2cpp_codegen_runtime_class_init_inline(LeanPath_t52FC47E81D073093417CE48BADFF80E5182D132F_il2cpp_TypeInfo_var);
		((LeanPath_t52FC47E81D073093417CE48BADFF80E5182D132F_StaticFields*)il2cpp_codegen_static_fields_for(LeanPath_t52FC47E81D073093417CE48BADFF80E5182D132F_il2cpp_TypeInfo_var))->___LastWorldNormal_9 = L_29;
	}

IL_0078:
	{
		// pointA = pointB;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_30 = V_5;
		V_2 = L_30;
		// indexA = i;
		int32_t L_31 = V_4;
		V_1 = L_31;
		// for (var i = 1; i < count; i++)
		int32_t L_32 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add(L_32, 1));
	}

IL_0084:
	{
		// for (var i = 1; i < count; i++)
		int32_t L_33 = V_4;
		int32_t L_34 = V_0;
		if ((((int32_t)L_33) < ((int32_t)L_34)))
		{
			goto IL_0024;
		}
	}
	{
		// return true;
		return (bool)1;
	}

IL_008b:
	{
		// return false;
		return (bool)0;
	}
}
// System.Boolean Lean.Common.LeanPath::TryGetClosest(UnityEngine.Ray,UnityEngine.Vector3&,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool LeanPath_TryGetClosest_m150E1C0790AF8C436ED99E02AD665914ED0E54D8 (LeanPath_t52FC47E81D073093417CE48BADFF80E5182D132F* __this, Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 ___0_ray, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* ___1_currentPoint, int32_t ___2_smoothing, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		// var closestIndexA = default(int);
		V_0 = 0;
		// var closestIndexB = default(int);
		V_1 = 0;
		// return TryGetClosest(ray, ref currentPoint, ref closestIndexA, ref closestIndexB, smoothing);
		Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 L_0 = ___0_ray;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_1 = ___1_currentPoint;
		int32_t L_2 = ___2_smoothing;
		bool L_3;
		L_3 = LeanPath_TryGetClosest_mC5CDF1FF80E51741388D1DE012E07E52B722485B(__this, L_0, L_1, (&V_0), (&V_1), L_2, NULL);
		return L_3;
	}
}
// System.Boolean Lean.Common.LeanPath::TryGetClosest(UnityEngine.Ray,UnityEngine.Vector3&,System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool LeanPath_TryGetClosest_m9EDCC556842AB6C4487B06DB5D37FA9C454DCD8C (LeanPath_t52FC47E81D073093417CE48BADFF80E5182D132F* __this, Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 ___0_ray, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* ___1_currentPoint, int32_t ___2_smoothing, float ___3_maximumDelta, const RuntimeMethod* method) 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// if (maximumDelta > 0.0f)
		float L_0 = ___3_maximumDelta;
		if ((!(((float)L_0) > ((float)(0.0f)))))
		{
			goto IL_0037;
		}
	}
	{
		// var closestPoint = currentPoint;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_1 = ___1_currentPoint;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2 = (*(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*)L_1);
		V_0 = L_2;
		// if (TryGetClosest(ray, ref closestPoint, smoothing) == true)
		Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 L_3 = ___0_ray;
		int32_t L_4 = ___2_smoothing;
		bool L_5;
		L_5 = LeanPath_TryGetClosest_m150E1C0790AF8C436ED99E02AD665914ED0E54D8(__this, L_3, (&V_0), L_4, NULL);
		if (!L_5)
		{
			goto IL_0035;
		}
	}
	{
		// var targetPoint = Vector3.MoveTowards(currentPoint, closestPoint, maximumDelta);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_6 = ___1_currentPoint;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_7 = (*(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*)L_6);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8 = V_0;
		float L_9 = ___3_maximumDelta;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10;
		L_10 = Vector3_MoveTowards_m0363264647799F3173AC37F8E819F98298249B08_inline(L_7, L_8, L_9, NULL);
		V_1 = L_10;
		// return TryGetClosest(targetPoint, ref currentPoint, smoothing);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_11 = V_1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_12 = ___1_currentPoint;
		int32_t L_13 = ___2_smoothing;
		bool L_14;
		L_14 = LeanPath_TryGetClosest_m445515693D3D0C0908FB0533F01DC6E59238C2B9(__this, L_11, L_12, L_13, NULL);
		return L_14;
	}

IL_0035:
	{
		// return false;
		return (bool)0;
	}

IL_0037:
	{
		// return TryGetClosest(ray, ref currentPoint, smoothing);
		Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 L_15 = ___0_ray;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_16 = ___1_currentPoint;
		int32_t L_17 = ___2_smoothing;
		bool L_18;
		L_18 = LeanPath_TryGetClosest_m150E1C0790AF8C436ED99E02AD665914ED0E54D8(__this, L_15, L_16, L_17, NULL);
		return L_18;
	}
}
// UnityEngine.Vector3 Lean.Common.LeanPath::GetClosestPoint(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 LeanPath_GetClosestPoint_m35BCB88CADA5172B750A9C1DC015CF57704CDA95 (LeanPath_t52FC47E81D073093417CE48BADFF80E5182D132F* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_position, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___1_origin, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___2_direction, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		// var denom = Vector3.Dot(direction, direction);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___2_direction;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1 = ___2_direction;
		float L_2;
		L_2 = Vector3_Dot_mBB86BB940AA0A32FA7D3C02AC42E5BC7095A5D52_inline(L_0, L_1, NULL);
		V_0 = L_2;
		// if (denom == 0.0f)
		float L_3 = V_0;
		if ((!(((float)L_3) == ((float)(0.0f)))))
		{
			goto IL_0012;
		}
	}
	{
		// return origin;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4 = ___1_origin;
		return L_4;
	}

IL_0012:
	{
		// var dist01 = Vector3.Dot(position - origin, direction) / denom;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_5 = ___0_position;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6 = ___1_origin;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_7;
		L_7 = Vector3_op_Subtraction_mE42023FF80067CB44A1D4A27EB7CF2B24CABB828_inline(L_5, L_6, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8 = ___2_direction;
		float L_9;
		L_9 = Vector3_Dot_mBB86BB940AA0A32FA7D3C02AC42E5BC7095A5D52_inline(L_7, L_8, NULL);
		float L_10 = V_0;
		V_1 = ((float)(L_9/L_10));
		// return origin + direction * Mathf.Clamp01(dist01);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_11 = ___1_origin;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_12 = ___2_direction;
		float L_13 = V_1;
		float L_14;
		L_14 = Mathf_Clamp01_mA7E048DBDA832D399A581BE4D6DED9FA44CE0F14_inline(L_13, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_15;
		L_15 = Vector3_op_Multiply_m87BA7C578F96C8E49BB07088DAAC4649F83B0353_inline(L_12, L_14, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_16;
		L_16 = Vector3_op_Addition_m78C0EC70CB66E8DCAC225743D82B268DAEE92067_inline(L_11, L_15, NULL);
		return L_16;
	}
}
// UnityEngine.Vector3 Lean.Common.LeanPath::GetClosestPoint(UnityEngine.Ray,UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 LeanPath_GetClosestPoint_m8C1E2BD9CAB8AD2A3D281FADAA705D2A9D3224C4 (LeanPath_t52FC47E81D073093417CE48BADFF80E5182D132F* __this, Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 ___0_ray, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___1_origin, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___2_direction, const RuntimeMethod* method) 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	float V_1 = 0.0f;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_2;
	memset((&V_2), 0, sizeof(V_2));
	float V_3 = 0.0f;
	{
		// var crossA = Vector3.Cross(ray.direction, direction);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0;
		L_0 = Ray_get_direction_m21C2D22D3BD4A683BD4DC191AB22DD05F5EC2086((&___0_ray), NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1 = ___2_direction;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2;
		L_2 = Vector3_Cross_mF93A280558BCE756D13B6CC5DCD7DE8A43148987_inline(L_0, L_1, NULL);
		V_0 = L_2;
		// var denom  = Vector3.Dot(crossA, crossA);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_3 = V_0;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4 = V_0;
		float L_5;
		L_5 = Vector3_Dot_mBB86BB940AA0A32FA7D3C02AC42E5BC7095A5D52_inline(L_3, L_4, NULL);
		V_1 = L_5;
		// if (denom == 0.0f)
		float L_6 = V_1;
		if ((!(((float)L_6) == ((float)(0.0f)))))
		{
			goto IL_0020;
		}
	}
	{
		// return origin;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_7 = ___1_origin;
		return L_7;
	}

IL_0020:
	{
		// var crossB = Vector3.Cross(ray.direction, ray.origin - origin);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8;
		L_8 = Ray_get_direction_m21C2D22D3BD4A683BD4DC191AB22DD05F5EC2086((&___0_ray), NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_9;
		L_9 = Ray_get_origin_m97604A8F180316A410DCD77B7D74D04522FA1BA6((&___0_ray), NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = ___1_origin;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_11;
		L_11 = Vector3_op_Subtraction_mE42023FF80067CB44A1D4A27EB7CF2B24CABB828_inline(L_9, L_10, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_12;
		L_12 = Vector3_Cross_mF93A280558BCE756D13B6CC5DCD7DE8A43148987_inline(L_8, L_11, NULL);
		V_2 = L_12;
		// var dist01 = Vector3.Dot(crossA, crossB) / denom;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_13 = V_0;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_14 = V_2;
		float L_15;
		L_15 = Vector3_Dot_mBB86BB940AA0A32FA7D3C02AC42E5BC7095A5D52_inline(L_13, L_14, NULL);
		float L_16 = V_1;
		V_3 = ((float)(L_15/L_16));
		// return origin + direction * Mathf.Clamp01(dist01);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_17 = ___1_origin;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_18 = ___2_direction;
		float L_19 = V_3;
		float L_20;
		L_20 = Mathf_Clamp01_mA7E048DBDA832D399A581BE4D6DED9FA44CE0F14_inline(L_19, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_21;
		L_21 = Vector3_op_Multiply_m87BA7C578F96C8E49BB07088DAAC4649F83B0353_inline(L_18, L_20, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_22;
		L_22 = Vector3_op_Addition_m78C0EC70CB66E8DCAC225743D82B268DAEE92067_inline(L_17, L_21, NULL);
		return L_22;
	}
}
// System.Single Lean.Common.LeanPath::GetClosestDistance(UnityEngine.Ray,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float LeanPath_GetClosestDistance_m25EDF5A03B96A0615B55298C1458859FC6253AC8 (LeanPath_t52FC47E81D073093417CE48BADFF80E5182D132F* __this, Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 ___0_ray, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___1_point, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		// var denom = Vector3.Dot(ray.direction, ray.direction);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0;
		L_0 = Ray_get_direction_m21C2D22D3BD4A683BD4DC191AB22DD05F5EC2086((&___0_ray), NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1;
		L_1 = Ray_get_direction_m21C2D22D3BD4A683BD4DC191AB22DD05F5EC2086((&___0_ray), NULL);
		float L_2;
		L_2 = Vector3_Dot_mBB86BB940AA0A32FA7D3C02AC42E5BC7095A5D52_inline(L_0, L_1, NULL);
		V_0 = L_2;
		// if (denom == 0.0f)
		float L_3 = V_0;
		if ((!(((float)L_3) == ((float)(0.0f)))))
		{
			goto IL_002a;
		}
	}
	{
		// return Vector3.Distance(ray.origin, point);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4;
		L_4 = Ray_get_origin_m97604A8F180316A410DCD77B7D74D04522FA1BA6((&___0_ray), NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_5 = ___1_point;
		float L_6;
		L_6 = Vector3_Distance_m2314DB9B8BD01157E013DF87BEA557375C7F9FF9_inline(L_4, L_5, NULL);
		return L_6;
	}

IL_002a:
	{
		// var dist01 = Vector3.Dot(point - ray.origin, ray.direction) / denom;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_7 = ___1_point;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8;
		L_8 = Ray_get_origin_m97604A8F180316A410DCD77B7D74D04522FA1BA6((&___0_ray), NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_9;
		L_9 = Vector3_op_Subtraction_mE42023FF80067CB44A1D4A27EB7CF2B24CABB828_inline(L_7, L_8, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10;
		L_10 = Ray_get_direction_m21C2D22D3BD4A683BD4DC191AB22DD05F5EC2086((&___0_ray), NULL);
		float L_11;
		L_11 = Vector3_Dot_mBB86BB940AA0A32FA7D3C02AC42E5BC7095A5D52_inline(L_9, L_10, NULL);
		float L_12 = V_0;
		V_1 = ((float)(L_11/L_12));
		// return Vector3.Distance(point, ray.GetPoint(dist01));
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_13 = ___1_point;
		float L_14 = V_1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_15;
		L_15 = Ray_GetPoint_mAF4E1D38026156E6434EF2BED2420ED5236392AF((&___0_ray), L_14, NULL);
		float L_16;
		L_16 = Vector3_Distance_m2314DB9B8BD01157E013DF87BEA557375C7F9FF9_inline(L_13, L_15, NULL);
		return L_16;
	}
}
// System.Int32 Lean.Common.LeanPath::Mod(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t LeanPath_Mod_mA52316BA69AE1DC6A8B5F15D76589B4055DCFA66 (LeanPath_t52FC47E81D073093417CE48BADFF80E5182D132F* __this, int32_t ___0_a, int32_t ___1_b, const RuntimeMethod* method) 
{
	{
		// a %= b; return a < 0 ? a + b : a;
		int32_t L_0 = ___0_a;
		int32_t L_1 = ___1_b;
		___0_a = ((int32_t)(L_0%L_1));
		// a %= b; return a < 0 ? a + b : a;
		int32_t L_2 = ___0_a;
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_000b;
		}
	}
	{
		int32_t L_3 = ___0_a;
		return L_3;
	}

IL_000b:
	{
		int32_t L_4 = ___0_a;
		int32_t L_5 = ___1_b;
		return ((int32_t)il2cpp_codegen_add(L_4, L_5));
	}
}
// System.Single Lean.Common.LeanPath::CubicInterpolate(System.Single,System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float LeanPath_CubicInterpolate_m1AA1DCBB91FEE95D41E5082FD32446ED979B4D84 (LeanPath_t52FC47E81D073093417CE48BADFF80E5182D132F* __this, float ___0_a, float ___1_b, float ___2_c, float ___3_d, float ___4_t, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	{
		// var tt  = t * t;
		float L_0 = ___4_t;
		float L_1 = ___4_t;
		V_0 = ((float)il2cpp_codegen_multiply(L_0, L_1));
		// var ttt = tt * t;
		float L_2 = V_0;
		float L_3 = ___4_t;
		V_1 = ((float)il2cpp_codegen_multiply(L_2, L_3));
		// var e = a - b;
		float L_4 = ___0_a;
		float L_5 = ___1_b;
		V_2 = ((float)il2cpp_codegen_subtract(L_4, L_5));
		// var f = d - c;
		float L_6 = ___3_d;
		float L_7 = ___2_c;
		// var g = f - e;
		float L_8 = V_2;
		V_3 = ((float)il2cpp_codegen_subtract(((float)il2cpp_codegen_subtract(L_6, L_7)), L_8));
		// var h = e - g;
		float L_9 = V_2;
		float L_10 = V_3;
		V_4 = ((float)il2cpp_codegen_subtract(L_9, L_10));
		// var i = c - a;
		float L_11 = ___2_c;
		float L_12 = ___0_a;
		V_5 = ((float)il2cpp_codegen_subtract(L_11, L_12));
		// return g * ttt + h * tt + i * t + b;
		float L_13 = V_3;
		float L_14 = V_1;
		float L_15 = V_4;
		float L_16 = V_0;
		float L_17 = V_5;
		float L_18 = ___4_t;
		float L_19 = ___1_b;
		return ((float)il2cpp_codegen_add(((float)il2cpp_codegen_add(((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(L_13, L_14)), ((float)il2cpp_codegen_multiply(L_15, L_16)))), ((float)il2cpp_codegen_multiply(L_17, L_18)))), L_19));
	}
}
// System.Void Lean.Common.LeanPath::UpdateVisual()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanPath_UpdateVisual_mC4B1C0DD129E0D88C1ED43B4F29B4A1B5DEE8202 (LeanPath_t52FC47E81D073093417CE48BADFF80E5182D132F* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		// if (Visual != null)
		LineRenderer_tEFEF960672DB69CB14B6D181FAE6292F0CF8B63D* L_0 = __this->___Visual_8;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_mD0BE578448EAA61948F25C32F8DD55AB1F778602(L_0, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_1)
		{
			goto IL_0042;
		}
	}
	{
		// var count = GetPointCount();
		int32_t L_2;
		L_2 = LeanPath_GetPointCount_m904AC0FF40B2AE9750C9541C035CF128F9FB6094(__this, (-1), NULL);
		V_0 = L_2;
		// Visual.positionCount = count;
		LineRenderer_tEFEF960672DB69CB14B6D181FAE6292F0CF8B63D* L_3 = __this->___Visual_8;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		LineRenderer_set_positionCount_m2001FB4044053895ECBE897AB833284F3300B205(L_3, L_4, NULL);
		// for (var i = 0; i < count; i++)
		V_1 = 0;
		goto IL_003e;
	}

IL_0026:
	{
		// Visual.SetPosition(i, GetPoint(i));
		LineRenderer_tEFEF960672DB69CB14B6D181FAE6292F0CF8B63D* L_5 = __this->___Visual_8;
		int32_t L_6 = V_1;
		int32_t L_7 = V_1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8;
		L_8 = LeanPath_GetPoint_m809DDF4B617B43CB30CA6EF2C33AA8AD3A4D6659(__this, L_7, (-1), NULL);
		NullCheck(L_5);
		LineRenderer_SetPosition_m84C4AD9ADC6AC62B33DB4D7E4C9F066DFF8440C1(L_5, L_6, L_8, NULL);
		// for (var i = 0; i < count; i++)
		int32_t L_9 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add(L_9, 1));
	}

IL_003e:
	{
		// for (var i = 0; i < count; i++)
		int32_t L_10 = V_1;
		int32_t L_11 = V_0;
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0026;
		}
	}

IL_0042:
	{
		// }
		return;
	}
}
// System.Void Lean.Common.LeanPath::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanPath_Update_m87AF4F8022BC7ECB2057DA94C1932B3E2392D673 (LeanPath_t52FC47E81D073093417CE48BADFF80E5182D132F* __this, const RuntimeMethod* method) 
{
	{
		// UpdateVisual();
		LeanPath_UpdateVisual_mC4B1C0DD129E0D88C1ED43B4F29B4A1B5DEE8202(__this, NULL);
		// }
		return;
	}
}
// System.Void Lean.Common.LeanPath::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanPath__ctor_m50BA53BF46D63335083336D43580FF76737CB380 (LeanPath_t52FC47E81D073093417CE48BADFF80E5182D132F* __this, const RuntimeMethod* method) 
{
	{
		// public Space Space = Space.Self;
		__this->___Space_6 = 1;
		// public int Smoothing = 1;
		__this->___Smoothing_7 = 1;
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
// System.Void Lean.Common.LeanPath::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanPath__cctor_m17D84FAF9DA3FD05C14B36E46B09BB4928292D36 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeanPath_t52FC47E81D073093417CE48BADFF80E5182D132F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static Vector3 LastWorldNormal = Vector3.forward;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0;
		L_0 = Vector3_get_forward_mAA55A7034304DF8B2152EAD49AE779FC4CA2EB4A_inline(NULL);
		((LeanPath_t52FC47E81D073093417CE48BADFF80E5182D132F_StaticFields*)il2cpp_codegen_static_fields_for(LeanPath_t52FC47E81D073093417CE48BADFF80E5182D132F_il2cpp_TypeInfo_var))->___LastWorldNormal_9 = L_0;
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Vector3 Lean.Common.LeanPlane::GetClosest(UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 LeanPlane_GetClosest_m31305D784A9F385A4030A64D6145AB7E9E7B1848 (LeanPlane_tDB7C59EE99114AFD2E4745F6ADE3D448FCC76E31* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_position, float ___1_offset, const RuntimeMethod* method) 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// var point = transform.InverseTransformPoint(position);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_0;
		L_0 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1 = ___0_position;
		NullCheck(L_0);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2;
		L_2 = Transform_InverseTransformPoint_m18CD395144D9C78F30E15A5B82B6670E792DBA5D(L_0, L_1, NULL);
		V_0 = L_2;
		// if (ClampX == true)
		bool L_3 = __this->___ClampX_4;
		if (!L_3)
		{
			goto IL_0033;
		}
	}
	{
		// point.x = Mathf.Clamp(point.x, MinX, MaxX);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4 = V_0;
		float L_5 = L_4.___x_2;
		float L_6 = __this->___MinX_5;
		float L_7 = __this->___MaxX_6;
		float L_8;
		L_8 = Mathf_Clamp_mEB9AEA827D27D20FCC787F7375156AF46BB12BBF_inline(L_5, L_6, L_7, NULL);
		(&V_0)->___x_2 = L_8;
	}

IL_0033:
	{
		// if (ClampY == true)
		bool L_9 = __this->___ClampY_7;
		if (!L_9)
		{
			goto IL_0059;
		}
	}
	{
		// point.y = Mathf.Clamp(point.y, MinY, MaxY);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = V_0;
		float L_11 = L_10.___y_3;
		float L_12 = __this->___MinY_8;
		float L_13 = __this->___MaxY_9;
		float L_14;
		L_14 = Mathf_Clamp_mEB9AEA827D27D20FCC787F7375156AF46BB12BBF_inline(L_11, L_12, L_13, NULL);
		(&V_0)->___y_3 = L_14;
	}

IL_0059:
	{
		// if (SnapX != 0.0f)
		float L_15 = __this->___SnapX_10;
		if ((((float)L_15) == ((float)(0.0f))))
		{
			goto IL_0086;
		}
	}
	{
		// point.x = Mathf.Round(point.x / SnapX) * SnapX;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_16 = V_0;
		float L_17 = L_16.___x_2;
		float L_18 = __this->___SnapX_10;
		float L_19;
		L_19 = bankers_roundf(((float)(L_17/L_18)));
		float L_20 = __this->___SnapX_10;
		(&V_0)->___x_2 = ((float)il2cpp_codegen_multiply(L_19, L_20));
	}

IL_0086:
	{
		// if (SnapY != 0.0f)
		float L_21 = __this->___SnapY_11;
		if ((((float)L_21) == ((float)(0.0f))))
		{
			goto IL_00b3;
		}
	}
	{
		// point.y = Mathf.Round(point.y / SnapY) * SnapY;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_22 = V_0;
		float L_23 = L_22.___y_3;
		float L_24 = __this->___SnapY_11;
		float L_25;
		L_25 = bankers_roundf(((float)(L_23/L_24)));
		float L_26 = __this->___SnapY_11;
		(&V_0)->___y_3 = ((float)il2cpp_codegen_multiply(L_25, L_26));
	}

IL_00b3:
	{
		// point.z = 0.0f;
		(&V_0)->___z_4 = (0.0f);
		// return transform.TransformPoint(point) + transform.forward * offset;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_27;
		L_27 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_28 = V_0;
		NullCheck(L_27);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_29;
		L_29 = Transform_TransformPoint_m05BFF013DB830D7BFE44A007703694AE1062EE44(L_27, L_28, NULL);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_30;
		L_30 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		NullCheck(L_30);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_31;
		L_31 = Transform_get_forward_mFCFACF7165FDAB21E80E384C494DF278386CEE2F(L_30, NULL);
		float L_32 = ___1_offset;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_33;
		L_33 = Vector3_op_Multiply_m87BA7C578F96C8E49BB07088DAAC4649F83B0353_inline(L_31, L_32, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_34;
		L_34 = Vector3_op_Addition_m78C0EC70CB66E8DCAC225743D82B268DAEE92067_inline(L_29, L_33, NULL);
		return L_34;
	}
}
// System.Boolean Lean.Common.LeanPlane::TryRaycast(UnityEngine.Ray,UnityEngine.Vector3&,System.Single,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool LeanPlane_TryRaycast_m082ED056209FFFD090AE7F6331C2341D56EEC608 (LeanPlane_tDB7C59EE99114AFD2E4745F6ADE3D448FCC76E31* __this, Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 ___0_ray, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* ___1_hit, float ___2_offset, bool ___3_getClosest, const RuntimeMethod* method) 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	float V_1 = 0.0f;
	{
		// var normal   = transform.forward;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_0;
		L_0 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		NullCheck(L_0);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1;
		L_1 = Transform_get_forward_mFCFACF7165FDAB21E80E384C494DF278386CEE2F(L_0, NULL);
		V_0 = L_1;
		// var point    = transform.position + normal * offset;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_2;
		L_2 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		NullCheck(L_2);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_3;
		L_3 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_2, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4 = V_0;
		float L_5 = ___2_offset;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6;
		L_6 = Vector3_op_Multiply_m87BA7C578F96C8E49BB07088DAAC4649F83B0353_inline(L_4, L_5, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_7;
		L_7 = Vector3_op_Addition_m78C0EC70CB66E8DCAC225743D82B268DAEE92067_inline(L_3, L_6, NULL);
		// var distance = default(float);
		V_1 = (0.0f);
		// if (RayToPlane(point, normal, ray, ref distance) == true)
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8 = V_0;
		Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 L_9 = ___0_ray;
		bool L_10;
		L_10 = LeanPlane_RayToPlane_mBD9C38BE30C7D51157BEAE1270C63709A453D8BB(L_7, L_8, L_9, (&V_1), NULL);
		if (!L_10)
		{
			goto IL_005b;
		}
	}
	{
		// hit = ray.GetPoint(distance);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_11 = ___1_hit;
		float L_12 = V_1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_13;
		L_13 = Ray_GetPoint_mAF4E1D38026156E6434EF2BED2420ED5236392AF((&___0_ray), L_12, NULL);
		*(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*)L_11 = L_13;
		// if (getClosest == true)
		bool L_14 = ___3_getClosest;
		if (!L_14)
		{
			goto IL_0059;
		}
	}
	{
		// hit = GetClosest(hit, offset);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_15 = ___1_hit;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_16 = ___1_hit;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_17 = (*(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*)L_16);
		float L_18 = ___2_offset;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_19;
		L_19 = LeanPlane_GetClosest_m31305D784A9F385A4030A64D6145AB7E9E7B1848(__this, L_17, L_18, NULL);
		*(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*)L_15 = L_19;
	}

IL_0059:
	{
		// return true;
		return (bool)1;
	}

IL_005b:
	{
		// return false;
		return (bool)0;
	}
}
// UnityEngine.Vector3 Lean.Common.LeanPlane::GetClosest(UnityEngine.Ray,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 LeanPlane_GetClosest_mF04997CFE9B18AEEA1DF3C31E19EFFB4DDB278F2 (LeanPlane_tDB7C59EE99114AFD2E4745F6ADE3D448FCC76E31* __this, Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 ___0_ray, float ___1_offset, const RuntimeMethod* method) 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_1;
	memset((&V_1), 0, sizeof(V_1));
	float V_2 = 0.0f;
	{
		// var normal   = transform.forward;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_0;
		L_0 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		NullCheck(L_0);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1;
		L_1 = Transform_get_forward_mFCFACF7165FDAB21E80E384C494DF278386CEE2F(L_0, NULL);
		V_0 = L_1;
		// var point    = transform.position + normal * offset;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_2;
		L_2 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		NullCheck(L_2);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_3;
		L_3 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_2, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4 = V_0;
		float L_5 = ___1_offset;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6;
		L_6 = Vector3_op_Multiply_m87BA7C578F96C8E49BB07088DAAC4649F83B0353_inline(L_4, L_5, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_7;
		L_7 = Vector3_op_Addition_m78C0EC70CB66E8DCAC225743D82B268DAEE92067_inline(L_3, L_6, NULL);
		V_1 = L_7;
		// var distance = default(float);
		V_2 = (0.0f);
		// if (RayToPlane(point, normal, ray, ref distance) == true)
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8 = V_1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_9 = V_0;
		Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 L_10 = ___0_ray;
		bool L_11;
		L_11 = LeanPlane_RayToPlane_mBD9C38BE30C7D51157BEAE1270C63709A453D8BB(L_8, L_9, L_10, (&V_2), NULL);
		if (!L_11)
		{
			goto IL_0046;
		}
	}
	{
		// return GetClosest(ray.GetPoint(distance), offset);
		float L_12 = V_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_13;
		L_13 = Ray_GetPoint_mAF4E1D38026156E6434EF2BED2420ED5236392AF((&___0_ray), L_12, NULL);
		float L_14 = ___1_offset;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_15;
		L_15 = LeanPlane_GetClosest_m31305D784A9F385A4030A64D6145AB7E9E7B1848(__this, L_13, L_14, NULL);
		return L_15;
	}

IL_0046:
	{
		// return point;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_16 = V_1;
		return L_16;
	}
}
// System.Boolean Lean.Common.LeanPlane::RayToPlane(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Ray,System.Single&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool LeanPlane_RayToPlane_mBD9C38BE30C7D51157BEAE1270C63709A453D8BB (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_point, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___1_normal, Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 ___2_ray, float* ___3_distance, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		// var b = Vector3.Dot(ray.direction, normal);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0;
		L_0 = Ray_get_direction_m21C2D22D3BD4A683BD4DC191AB22DD05F5EC2086((&___2_ray), NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1 = ___1_normal;
		float L_2;
		L_2 = Vector3_Dot_mBB86BB940AA0A32FA7D3C02AC42E5BC7095A5D52_inline(L_0, L_1, NULL);
		V_0 = L_2;
		// if (Mathf.Approximately(b, 0.0f) == true)
		float L_3 = V_0;
		bool L_4;
		L_4 = Mathf_Approximately_m1DADD012A8FC82E11FB282501AE2EBBF9A77150B_inline(L_3, (0.0f), NULL);
		if (!L_4)
		{
			goto IL_001d;
		}
	}
	{
		// return false;
		return (bool)0;
	}

IL_001d:
	{
		// var d = -Vector3.Dot(normal, point);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_5 = ___1_normal;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6 = ___0_point;
		float L_7;
		L_7 = Vector3_Dot_mBB86BB940AA0A32FA7D3C02AC42E5BC7095A5D52_inline(L_5, L_6, NULL);
		V_1 = ((-L_7));
		// var a = -Vector3.Dot(ray.origin, normal) - d;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8;
		L_8 = Ray_get_origin_m97604A8F180316A410DCD77B7D74D04522FA1BA6((&___2_ray), NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_9 = ___1_normal;
		float L_10;
		L_10 = Vector3_Dot_mBB86BB940AA0A32FA7D3C02AC42E5BC7095A5D52_inline(L_8, L_9, NULL);
		float L_11 = V_1;
		V_2 = ((float)il2cpp_codegen_subtract(((-L_10)), L_11));
		// distance = a / b;
		float* L_12 = ___3_distance;
		float L_13 = V_2;
		float L_14 = V_0;
		*((float*)L_12) = (float)((float)(L_13/L_14));
		// return distance > 0.0f;
		float* L_15 = ___3_distance;
		float L_16 = *((float*)L_15);
		return (bool)((((float)L_16) > ((float)(0.0f)))? 1 : 0);
	}
}
// System.Void Lean.Common.LeanPlane::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanPlane__ctor_m6136ABF83F1F703CF82F2CA908BD8962CE681CEA (LeanPlane_tDB7C59EE99114AFD2E4745F6ADE3D448FCC76E31* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Common.LeanRoll::set_Angle(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanRoll_set_Angle_mE887D61B151551F0514A8AE3CA243E87A8910B8C (LeanRoll_t205306FB01D404E1DBC7D81AC3D66A5796097934* __this, float ___0_value, const RuntimeMethod* method) 
{
	{
		// public float Angle { set { angle = value; } get { return angle; } } [SerializeField] private float angle;
		float L_0 = ___0_value;
		__this->___angle_4 = L_0;
		// public float Angle { set { angle = value; } get { return angle; } } [SerializeField] private float angle;
		return;
	}
}
// System.Single Lean.Common.LeanRoll::get_Angle()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float LeanRoll_get_Angle_mEAE28B2DBAAB93691705B724E3B560E3C2C4BDE2 (LeanRoll_t205306FB01D404E1DBC7D81AC3D66A5796097934* __this, const RuntimeMethod* method) 
{
	{
		// public float Angle { set { angle = value; } get { return angle; } } [SerializeField] private float angle;
		float L_0 = __this->___angle_4;
		return L_0;
	}
}
// System.Void Lean.Common.LeanRoll::set_Clamp(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanRoll_set_Clamp_m0907C0FC8FA63A2DF12B97E2125C24B70CBB6E18 (LeanRoll_t205306FB01D404E1DBC7D81AC3D66A5796097934* __this, bool ___0_value, const RuntimeMethod* method) 
{
	{
		// public bool Clamp { set { clamp = value; } get { return clamp; } } [SerializeField] private bool clamp;
		bool L_0 = ___0_value;
		__this->___clamp_5 = L_0;
		// public bool Clamp { set { clamp = value; } get { return clamp; } } [SerializeField] private bool clamp;
		return;
	}
}
// System.Boolean Lean.Common.LeanRoll::get_Clamp()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool LeanRoll_get_Clamp_m88E823DC225A2981FC017909BCAA471849F5FB31 (LeanRoll_t205306FB01D404E1DBC7D81AC3D66A5796097934* __this, const RuntimeMethod* method) 
{
	{
		// public bool Clamp { set { clamp = value; } get { return clamp; } } [SerializeField] private bool clamp;
		bool L_0 = __this->___clamp_5;
		return L_0;
	}
}
// System.Void Lean.Common.LeanRoll::set_ClampMin(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanRoll_set_ClampMin_mACCED40B2FC690691CF51DE4E658DF62845D5D46 (LeanRoll_t205306FB01D404E1DBC7D81AC3D66A5796097934* __this, float ___0_value, const RuntimeMethod* method) 
{
	{
		// public float ClampMin { set { clampMin = value; } get { return clampMin; } } [SerializeField] private float clampMin;
		float L_0 = ___0_value;
		__this->___clampMin_6 = L_0;
		// public float ClampMin { set { clampMin = value; } get { return clampMin; } } [SerializeField] private float clampMin;
		return;
	}
}
// System.Single Lean.Common.LeanRoll::get_ClampMin()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float LeanRoll_get_ClampMin_m6EE98C23D42A1A116B549C27E9A9CDAAFED17169 (LeanRoll_t205306FB01D404E1DBC7D81AC3D66A5796097934* __this, const RuntimeMethod* method) 
{
	{
		// public float ClampMin { set { clampMin = value; } get { return clampMin; } } [SerializeField] private float clampMin;
		float L_0 = __this->___clampMin_6;
		return L_0;
	}
}
// System.Void Lean.Common.LeanRoll::set_ClampMax(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanRoll_set_ClampMax_m9087288BFB7F016D5AE8C16D77CAD2FE92E3D9D1 (LeanRoll_t205306FB01D404E1DBC7D81AC3D66A5796097934* __this, float ___0_value, const RuntimeMethod* method) 
{
	{
		// public float ClampMax { set { clampMax = value; } get { return clampMax; } } [SerializeField] private float clampMax;
		float L_0 = ___0_value;
		__this->___clampMax_7 = L_0;
		// public float ClampMax { set { clampMax = value; } get { return clampMax; } } [SerializeField] private float clampMax;
		return;
	}
}
// System.Single Lean.Common.LeanRoll::get_ClampMax()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float LeanRoll_get_ClampMax_mE4BDF0B55CF54A9DD139E23EA5E376A113C6CE45 (LeanRoll_t205306FB01D404E1DBC7D81AC3D66A5796097934* __this, const RuntimeMethod* method) 
{
	{
		// public float ClampMax { set { clampMax = value; } get { return clampMax; } } [SerializeField] private float clampMax;
		float L_0 = __this->___clampMax_7;
		return L_0;
	}
}
// System.Void Lean.Common.LeanRoll::set_Damping(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanRoll_set_Damping_m18C84B8122542D69247EB83C97C3D0A7FDA57CCA (LeanRoll_t205306FB01D404E1DBC7D81AC3D66A5796097934* __this, float ___0_value, const RuntimeMethod* method) 
{
	{
		// public float Damping { set { damping = value; } get { return damping; } } [SerializeField] private float damping = -1.0f;
		float L_0 = ___0_value;
		__this->___damping_8 = L_0;
		// public float Damping { set { damping = value; } get { return damping; } } [SerializeField] private float damping = -1.0f;
		return;
	}
}
// System.Single Lean.Common.LeanRoll::get_Damping()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float LeanRoll_get_Damping_mC449B549B4AF7D89E4456883D6FCB5BAA2CA4C84 (LeanRoll_t205306FB01D404E1DBC7D81AC3D66A5796097934* __this, const RuntimeMethod* method) 
{
	{
		// public float Damping { set { damping = value; } get { return damping; } } [SerializeField] private float damping = -1.0f;
		float L_0 = __this->___damping_8;
		return L_0;
	}
}
// System.Void Lean.Common.LeanRoll::IncrementAngle(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanRoll_IncrementAngle_m3F6D6F82CDF3857CC56A3B3BB08EFBEA7BAC6375 (LeanRoll_t205306FB01D404E1DBC7D81AC3D66A5796097934* __this, float ___0_delta, const RuntimeMethod* method) 
{
	{
		// angle += delta;
		float L_0 = __this->___angle_4;
		float L_1 = ___0_delta;
		__this->___angle_4 = ((float)il2cpp_codegen_add(L_0, L_1));
		// }
		return;
	}
}
// System.Void Lean.Common.LeanRoll::DecrementAngle(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanRoll_DecrementAngle_mF980FD369DC11AF3E44E8FC7176A74C776C01AA7 (LeanRoll_t205306FB01D404E1DBC7D81AC3D66A5796097934* __this, float ___0_delta, const RuntimeMethod* method) 
{
	{
		// angle -= delta;
		float L_0 = __this->___angle_4;
		float L_1 = ___0_delta;
		__this->___angle_4 = ((float)il2cpp_codegen_subtract(L_0, L_1));
		// }
		return;
	}
}
// System.Void Lean.Common.LeanRoll::RotateToDelta(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanRoll_RotateToDelta_mDB7CFA88C6866D016C8867A84F9EDBD7663A4062 (LeanRoll_t205306FB01D404E1DBC7D81AC3D66A5796097934* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___0_delta, const RuntimeMethod* method) 
{
	{
		// if (delta.sqrMagnitude > 0.0f)
		float L_0;
		L_0 = Vector2_get_sqrMagnitude_mA16336720C14EEF8BA9B55AE33B98C9EE2082BDC_inline((&___0_delta), NULL);
		if ((!(((float)L_0) > ((float)(0.0f)))))
		{
			goto IL_002b;
		}
	}
	{
		// angle = Mathf.Atan2(delta.x, delta.y) * Mathf.Rad2Deg;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_1 = ___0_delta;
		float L_2 = L_1.___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_3 = ___0_delta;
		float L_4 = L_3.___y_1;
		float L_5;
		L_5 = atan2f(L_2, L_4);
		__this->___angle_4 = ((float)il2cpp_codegen_multiply(L_5, (57.2957802f)));
	}

IL_002b:
	{
		// }
		return;
	}
}
// System.Void Lean.Common.LeanRoll::SnapToTarget()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanRoll_SnapToTarget_m38DE0B392D53013FAF766B9975F04E3BCA9226F1 (LeanRoll_t205306FB01D404E1DBC7D81AC3D66A5796097934* __this, const RuntimeMethod* method) 
{
	{
		// currentAngle = angle;
		float L_0 = __this->___angle_4;
		__this->___currentAngle_9 = L_0;
		// }
		return;
	}
}
// System.Void Lean.Common.LeanRoll::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanRoll_Start_m9AC4D7BB13467134AF2E8C78F60FBB04966DF729 (LeanRoll_t205306FB01D404E1DBC7D81AC3D66A5796097934* __this, const RuntimeMethod* method) 
{
	{
		// currentAngle = angle;
		float L_0 = __this->___angle_4;
		__this->___currentAngle_9 = L_0;
		// }
		return;
	}
}
// System.Void Lean.Common.LeanRoll::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanRoll_Update_m05A1404797C5EE0BB5F3A6A3AD72A3520E5CD354 (LeanRoll_t205306FB01D404E1DBC7D81AC3D66A5796097934* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CwHelper_tCEAA370BDC6A41945D0FEDD0D8D490440F1C18F7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		// var factor = CwHelper.DampenFactor(damping, Time.deltaTime);
		float L_0 = __this->___damping_8;
		float L_1;
		L_1 = Time_get_deltaTime_mC3195000401F0FD167DD2F948FD2BC58330D0865(NULL);
		il2cpp_codegen_runtime_class_init_inline(CwHelper_tCEAA370BDC6A41945D0FEDD0D8D490440F1C18F7_il2cpp_TypeInfo_var);
		float L_2;
		L_2 = CwHelper_DampenFactor_m730A8463F0CBE628CD66EF69E1B5969091015C9A(L_0, L_1, NULL);
		V_0 = L_2;
		// if (clamp == true)
		bool L_3 = __this->___clamp_5;
		if (!L_3)
		{
			goto IL_0036;
		}
	}
	{
		// angle = Mathf.Clamp(angle, clampMin, clampMax);
		float L_4 = __this->___angle_4;
		float L_5 = __this->___clampMin_6;
		float L_6 = __this->___clampMax_7;
		float L_7;
		L_7 = Mathf_Clamp_mEB9AEA827D27D20FCC787F7375156AF46BB12BBF_inline(L_4, L_5, L_6, NULL);
		__this->___angle_4 = L_7;
	}

IL_0036:
	{
		// currentAngle = Mathf.LerpAngle(currentAngle, angle, factor);
		float L_8 = __this->___currentAngle_9;
		float L_9 = __this->___angle_4;
		float L_10 = V_0;
		float L_11;
		L_11 = Mathf_LerpAngle_m0653422E15193C2E4A4E5AF05236B6315C789C23_inline(L_8, L_9, L_10, NULL);
		__this->___currentAngle_9 = L_11;
		// transform.rotation = Quaternion.Euler(0.0f, 0.0f, -currentAngle);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_12;
		L_12 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		float L_13 = __this->___currentAngle_9;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_14;
		L_14 = Quaternion_Euler_m9262AB29E3E9CE94EF71051F38A28E82AEC73F90_inline((0.0f), (0.0f), ((-L_13)), NULL);
		NullCheck(L_12);
		Transform_set_rotation_m61340DE74726CF0F9946743A727C4D444397331D(L_12, L_14, NULL);
		// }
		return;
	}
}
// System.Void Lean.Common.LeanRoll::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanRoll__ctor_m241DAF9B4D59659AEF203451CDF3B63E0BA24977 (LeanRoll_t205306FB01D404E1DBC7D81AC3D66A5796097934* __this, const RuntimeMethod* method) 
{
	{
		// public float Damping { set { damping = value; } get { return damping; } } [SerializeField] private float damping = -1.0f;
		__this->___damping_8 = (-1.0f);
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Common.LeanSelect::set_DeselectWithNothing(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelect_set_DeselectWithNothing_m96D5EAE0EEB615F298F2644201276366A760536C (LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* __this, bool ___0_value, const RuntimeMethod* method) 
{
	{
		// public bool DeselectWithNothing { set { deselectWithNothing = value; } get { return deselectWithNothing; } } [SerializeField] private bool deselectWithNothing;
		bool L_0 = ___0_value;
		__this->___deselectWithNothing_6 = L_0;
		// public bool DeselectWithNothing { set { deselectWithNothing = value; } get { return deselectWithNothing; } } [SerializeField] private bool deselectWithNothing;
		return;
	}
}
// System.Boolean Lean.Common.LeanSelect::get_DeselectWithNothing()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool LeanSelect_get_DeselectWithNothing_m6E5FC30F06BAE82F950E1A6705665BCC659082D5 (LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* __this, const RuntimeMethod* method) 
{
	{
		// public bool DeselectWithNothing { set { deselectWithNothing = value; } get { return deselectWithNothing; } } [SerializeField] private bool deselectWithNothing;
		bool L_0 = __this->___deselectWithNothing_6;
		return L_0;
	}
}
// System.Void Lean.Common.LeanSelect::set_Limit(Lean.Common.LeanSelect/LimitType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelect_set_Limit_m1DA527E7B5FF03948AD4B561A0F5FB5031C04964 (LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* __this, int32_t ___0_value, const RuntimeMethod* method) 
{
	{
		// public LimitType Limit { set { limit = value; } get { return limit; } } [SerializeField] private LimitType limit;
		int32_t L_0 = ___0_value;
		__this->___limit_7 = L_0;
		// public LimitType Limit { set { limit = value; } get { return limit; } } [SerializeField] private LimitType limit;
		return;
	}
}
// Lean.Common.LeanSelect/LimitType Lean.Common.LeanSelect::get_Limit()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t LeanSelect_get_Limit_m47EA6BE60E037C5C994F01A81C728C0D0D01FECB (LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* __this, const RuntimeMethod* method) 
{
	{
		// public LimitType Limit { set { limit = value; } get { return limit; } } [SerializeField] private LimitType limit;
		int32_t L_0 = __this->___limit_7;
		return L_0;
	}
}
// System.Void Lean.Common.LeanSelect::set_MaxSelectables(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelect_set_MaxSelectables_m2E43A08FDDE829DAC1E44349ED6809ECDB2DF329 (LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* __this, int32_t ___0_value, const RuntimeMethod* method) 
{
	{
		// public int MaxSelectables { set { maxSelectables = value; } get { return maxSelectables; } } [SerializeField] private int maxSelectables = 5;
		int32_t L_0 = ___0_value;
		__this->___maxSelectables_8 = L_0;
		// public int MaxSelectables { set { maxSelectables = value; } get { return maxSelectables; } } [SerializeField] private int maxSelectables = 5;
		return;
	}
}
// System.Int32 Lean.Common.LeanSelect::get_MaxSelectables()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t LeanSelect_get_MaxSelectables_m1A6011E15712E78F14DC334A12E411EEFC7FFFC8 (LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* __this, const RuntimeMethod* method) 
{
	{
		// public int MaxSelectables { set { maxSelectables = value; } get { return maxSelectables; } } [SerializeField] private int maxSelectables = 5;
		int32_t L_0 = __this->___maxSelectables_8;
		return L_0;
	}
}
// System.Void Lean.Common.LeanSelect::set_Reselect(Lean.Common.LeanSelect/ReselectType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelect_set_Reselect_mA5CAFC19DB17E24C7BBD30039066CE18ABBE22C8 (LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* __this, int32_t ___0_value, const RuntimeMethod* method) 
{
	{
		// public ReselectType Reselect { set { reselect = value; } get { return reselect; } } [SerializeField] private ReselectType reselect = ReselectType.SelectAgain;
		int32_t L_0 = ___0_value;
		__this->___reselect_9 = L_0;
		// public ReselectType Reselect { set { reselect = value; } get { return reselect; } } [SerializeField] private ReselectType reselect = ReselectType.SelectAgain;
		return;
	}
}
// Lean.Common.LeanSelect/ReselectType Lean.Common.LeanSelect::get_Reselect()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t LeanSelect_get_Reselect_m21BB1AFD8981E47B83CEF1170EEB8032AFE562BB (LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* __this, const RuntimeMethod* method) 
{
	{
		// public ReselectType Reselect { set { reselect = value; } get { return reselect; } } [SerializeField] private ReselectType reselect = ReselectType.SelectAgain;
		int32_t L_0 = __this->___reselect_9;
		return L_0;
	}
}
// System.Collections.Generic.List`1<Lean.Common.LeanSelectable> Lean.Common.LeanSelect::get_Selectables()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3* LeanSelect_get_Selectables_m0541739B28A2D6DE8DDB291B67F36C3D4BB7255C (LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m4444F647B6C0C03F0F6CD67236CF2D7F76202E7A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public List<LeanSelectable> Selectables { get { if (selectables == null) selectables = new List<LeanSelectable>(); return selectables; } } [SerializeField] protected List<LeanSelectable> selectables;
		List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3* L_0 = __this->___selectables_10;
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		// public List<LeanSelectable> Selectables { get { if (selectables == null) selectables = new List<LeanSelectable>(); return selectables; } } [SerializeField] protected List<LeanSelectable> selectables;
		List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3* L_1 = (List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3*)il2cpp_codegen_object_new(List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3_il2cpp_TypeInfo_var);
		NullCheck(L_1);
		List_1__ctor_m4444F647B6C0C03F0F6CD67236CF2D7F76202E7A(L_1, List_1__ctor_m4444F647B6C0C03F0F6CD67236CF2D7F76202E7A_RuntimeMethod_var);
		__this->___selectables_10 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___selectables_10), (void*)L_1);
	}

IL_0013:
	{
		// public List<LeanSelectable> Selectables { get { if (selectables == null) selectables = new List<LeanSelectable>(); return selectables; } } [SerializeField] protected List<LeanSelectable> selectables;
		List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3* L_2 = __this->___selectables_10;
		return L_2;
	}
}
// Lean.Common.LeanSelect/LeanSelectableEvent Lean.Common.LeanSelect::get_OnSelected()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR LeanSelectableEvent_tE1BC7E60B77ABC37DF10F63761C513AB4EBEC09D* LeanSelect_get_OnSelected_m3C80CDF2BB1C018C1FD67A55D148EDA9E9FFC907 (LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeanSelectableEvent_tE1BC7E60B77ABC37DF10F63761C513AB4EBEC09D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public LeanSelectableEvent OnSelected { get { if (onSelected == null) onSelected = new LeanSelectableEvent(); return onSelected; } } [SerializeField] private LeanSelectableEvent onSelected;
		LeanSelectableEvent_tE1BC7E60B77ABC37DF10F63761C513AB4EBEC09D* L_0 = __this->___onSelected_11;
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		// public LeanSelectableEvent OnSelected { get { if (onSelected == null) onSelected = new LeanSelectableEvent(); return onSelected; } } [SerializeField] private LeanSelectableEvent onSelected;
		LeanSelectableEvent_tE1BC7E60B77ABC37DF10F63761C513AB4EBEC09D* L_1 = (LeanSelectableEvent_tE1BC7E60B77ABC37DF10F63761C513AB4EBEC09D*)il2cpp_codegen_object_new(LeanSelectableEvent_tE1BC7E60B77ABC37DF10F63761C513AB4EBEC09D_il2cpp_TypeInfo_var);
		NullCheck(L_1);
		LeanSelectableEvent__ctor_mF85F656B71FB189CE41973B9F90133305E4889DF(L_1, NULL);
		__this->___onSelected_11 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___onSelected_11), (void*)L_1);
	}

IL_0013:
	{
		// public LeanSelectableEvent OnSelected { get { if (onSelected == null) onSelected = new LeanSelectableEvent(); return onSelected; } } [SerializeField] private LeanSelectableEvent onSelected;
		LeanSelectableEvent_tE1BC7E60B77ABC37DF10F63761C513AB4EBEC09D* L_2 = __this->___onSelected_11;
		return L_2;
	}
}
// Lean.Common.LeanSelect/LeanSelectableEvent Lean.Common.LeanSelect::get_OnDeselected()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR LeanSelectableEvent_tE1BC7E60B77ABC37DF10F63761C513AB4EBEC09D* LeanSelect_get_OnDeselected_m533685C0474A82E4171383017FCB2EF3F6FB3EC8 (LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeanSelectableEvent_tE1BC7E60B77ABC37DF10F63761C513AB4EBEC09D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public LeanSelectableEvent OnDeselected { get { if (onDeselected == null) onDeselected = new LeanSelectableEvent(); return onDeselected; } } [SerializeField] private LeanSelectableEvent onDeselected;
		LeanSelectableEvent_tE1BC7E60B77ABC37DF10F63761C513AB4EBEC09D* L_0 = __this->___onDeselected_12;
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		// public LeanSelectableEvent OnDeselected { get { if (onDeselected == null) onDeselected = new LeanSelectableEvent(); return onDeselected; } } [SerializeField] private LeanSelectableEvent onDeselected;
		LeanSelectableEvent_tE1BC7E60B77ABC37DF10F63761C513AB4EBEC09D* L_1 = (LeanSelectableEvent_tE1BC7E60B77ABC37DF10F63761C513AB4EBEC09D*)il2cpp_codegen_object_new(LeanSelectableEvent_tE1BC7E60B77ABC37DF10F63761C513AB4EBEC09D_il2cpp_TypeInfo_var);
		NullCheck(L_1);
		LeanSelectableEvent__ctor_mF85F656B71FB189CE41973B9F90133305E4889DF(L_1, NULL);
		__this->___onDeselected_12 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___onDeselected_12), (void*)L_1);
	}

IL_0013:
	{
		// public LeanSelectableEvent OnDeselected { get { if (onDeselected == null) onDeselected = new LeanSelectableEvent(); return onDeselected; } } [SerializeField] private LeanSelectableEvent onDeselected;
		LeanSelectableEvent_tE1BC7E60B77ABC37DF10F63761C513AB4EBEC09D* L_2 = __this->___onDeselected_12;
		return L_2;
	}
}
// UnityEngine.Events.UnityEvent Lean.Common.LeanSelect::get_OnNothing()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UnityEvent_tDC2C3548799DBC91D1E3F3DE60083A66F4751977* LeanSelect_get_OnNothing_m3A87EBDF988FCC05BAC09023CF229676FD836D88 (LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_tDC2C3548799DBC91D1E3F3DE60083A66F4751977_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public UnityEvent OnNothing { get { if (onNothing == null) onNothing = new UnityEvent(); return onNothing; } } [SerializeField] private UnityEvent onNothing;
		UnityEvent_tDC2C3548799DBC91D1E3F3DE60083A66F4751977* L_0 = __this->___onNothing_13;
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		// public UnityEvent OnNothing { get { if (onNothing == null) onNothing = new UnityEvent(); return onNothing; } } [SerializeField] private UnityEvent onNothing;
		UnityEvent_tDC2C3548799DBC91D1E3F3DE60083A66F4751977* L_1 = (UnityEvent_tDC2C3548799DBC91D1E3F3DE60083A66F4751977*)il2cpp_codegen_object_new(UnityEvent_tDC2C3548799DBC91D1E3F3DE60083A66F4751977_il2cpp_TypeInfo_var);
		NullCheck(L_1);
		UnityEvent__ctor_m03D3E5121B9A6100351984D0CE3050B909CD3235(L_1, NULL);
		__this->___onNothing_13 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___onNothing_13), (void*)L_1);
	}

IL_0013:
	{
		// public UnityEvent OnNothing { get { if (onNothing == null) onNothing = new UnityEvent(); return onNothing; } } [SerializeField] private UnityEvent onNothing;
		UnityEvent_tDC2C3548799DBC91D1E3F3DE60083A66F4751977* L_2 = __this->___onNothing_13;
		return L_2;
	}
}
// System.Void Lean.Common.LeanSelect::add_OnAnySelected(System.Action`2<Lean.Common.LeanSelect,Lean.Common.LeanSelectable>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelect_add_OnAnySelected_mCBC3335FADE43EE74713529033E1862D9A4FA7CB (Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* ___0_value, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* V_0 = NULL;
	Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* V_1 = NULL;
	Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* V_2 = NULL;
	{
		il2cpp_codegen_runtime_class_init_inline(LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var);
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_0 = ((LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var))->___OnAnySelected_14;
		V_0 = L_0;
	}

IL_0006:
	{
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_1 = V_0;
		V_1 = L_1;
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_2 = V_1;
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_3 = ___0_value;
		Delegate_t* L_4;
		L_4 = Delegate_Combine_m1F725AEF318BE6F0426863490691A6F4606E7D00(L_2, L_3, NULL);
		V_2 = ((Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190*)Castclass((RuntimeObject*)L_4, Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190_il2cpp_TypeInfo_var));
		il2cpp_codegen_runtime_class_init_inline(LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var);
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_5 = V_2;
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_6 = V_1;
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_7;
		L_7 = InterlockedCompareExchangeImpl<Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190*>((&((LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var))->___OnAnySelected_14), L_5, L_6);
		V_0 = L_7;
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_8 = V_0;
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_9 = V_1;
		if ((!(((RuntimeObject*)(Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190*)L_8) == ((RuntimeObject*)(Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190*)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void Lean.Common.LeanSelect::remove_OnAnySelected(System.Action`2<Lean.Common.LeanSelect,Lean.Common.LeanSelectable>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelect_remove_OnAnySelected_mE81C8107A509B588D2844CFB9D9DAE50930405EE (Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* ___0_value, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* V_0 = NULL;
	Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* V_1 = NULL;
	Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* V_2 = NULL;
	{
		il2cpp_codegen_runtime_class_init_inline(LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var);
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_0 = ((LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var))->___OnAnySelected_14;
		V_0 = L_0;
	}

IL_0006:
	{
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_1 = V_0;
		V_1 = L_1;
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_2 = V_1;
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_3 = ___0_value;
		Delegate_t* L_4;
		L_4 = Delegate_Remove_m8B7DD5661308FA972E23CA1CC3FC9CEB355504E3(L_2, L_3, NULL);
		V_2 = ((Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190*)Castclass((RuntimeObject*)L_4, Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190_il2cpp_TypeInfo_var));
		il2cpp_codegen_runtime_class_init_inline(LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var);
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_5 = V_2;
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_6 = V_1;
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_7;
		L_7 = InterlockedCompareExchangeImpl<Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190*>((&((LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var))->___OnAnySelected_14), L_5, L_6);
		V_0 = L_7;
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_8 = V_0;
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_9 = V_1;
		if ((!(((RuntimeObject*)(Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190*)L_8) == ((RuntimeObject*)(Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190*)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void Lean.Common.LeanSelect::add_OnAnyDeselected(System.Action`2<Lean.Common.LeanSelect,Lean.Common.LeanSelectable>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelect_add_OnAnyDeselected_m8D59CD367791FED7F05E552087DEAAA38EAE5D9B (Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* ___0_value, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* V_0 = NULL;
	Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* V_1 = NULL;
	Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* V_2 = NULL;
	{
		il2cpp_codegen_runtime_class_init_inline(LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var);
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_0 = ((LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var))->___OnAnyDeselected_15;
		V_0 = L_0;
	}

IL_0006:
	{
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_1 = V_0;
		V_1 = L_1;
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_2 = V_1;
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_3 = ___0_value;
		Delegate_t* L_4;
		L_4 = Delegate_Combine_m1F725AEF318BE6F0426863490691A6F4606E7D00(L_2, L_3, NULL);
		V_2 = ((Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190*)Castclass((RuntimeObject*)L_4, Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190_il2cpp_TypeInfo_var));
		il2cpp_codegen_runtime_class_init_inline(LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var);
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_5 = V_2;
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_6 = V_1;
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_7;
		L_7 = InterlockedCompareExchangeImpl<Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190*>((&((LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var))->___OnAnyDeselected_15), L_5, L_6);
		V_0 = L_7;
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_8 = V_0;
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_9 = V_1;
		if ((!(((RuntimeObject*)(Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190*)L_8) == ((RuntimeObject*)(Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190*)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void Lean.Common.LeanSelect::remove_OnAnyDeselected(System.Action`2<Lean.Common.LeanSelect,Lean.Common.LeanSelectable>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelect_remove_OnAnyDeselected_m92803FDD673B11B87F9C54B31A277648D8A08C83 (Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* ___0_value, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* V_0 = NULL;
	Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* V_1 = NULL;
	Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* V_2 = NULL;
	{
		il2cpp_codegen_runtime_class_init_inline(LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var);
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_0 = ((LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var))->___OnAnyDeselected_15;
		V_0 = L_0;
	}

IL_0006:
	{
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_1 = V_0;
		V_1 = L_1;
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_2 = V_1;
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_3 = ___0_value;
		Delegate_t* L_4;
		L_4 = Delegate_Remove_m8B7DD5661308FA972E23CA1CC3FC9CEB355504E3(L_2, L_3, NULL);
		V_2 = ((Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190*)Castclass((RuntimeObject*)L_4, Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190_il2cpp_TypeInfo_var));
		il2cpp_codegen_runtime_class_init_inline(LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var);
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_5 = V_2;
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_6 = V_1;
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_7;
		L_7 = InterlockedCompareExchangeImpl<Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190*>((&((LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var))->___OnAnyDeselected_15), L_5, L_6);
		V_0 = L_7;
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_8 = V_0;
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_9 = V_1;
		if ((!(((RuntimeObject*)(Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190*)L_8) == ((RuntimeObject*)(Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190*)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Boolean Lean.Common.LeanSelect::IsSelected(Lean.Common.LeanSelectable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool LeanSelect_IsSelected_m882CF8026A3B01FE1F1A5374536481252A04FC16 (LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* __this, LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* ___0_selectable, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Contains_mCB5517DFCB5671A1504FF59C499DA2B27EE1B8AE_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return selectables != null && selectables.Contains(selectable);
		List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3* L_0 = __this->___selectables_10;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3* L_1 = __this->___selectables_10;
		LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* L_2 = ___0_selectable;
		NullCheck(L_1);
		bool L_3;
		L_3 = List_1_Contains_mCB5517DFCB5671A1504FF59C499DA2B27EE1B8AE(L_1, L_2, List_1_Contains_mCB5517DFCB5671A1504FF59C499DA2B27EE1B8AE_RuntimeMethod_var);
		return L_3;
	}

IL_0015:
	{
		return (bool)0;
	}
}
// System.Void Lean.Common.LeanSelect::Select(Lean.Common.LeanSelectable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelect_Select_mB2CE2E0E829BA535BBDB80F7BC38054655C5D150 (LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* __this, LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* ___0_selectable, const RuntimeMethod* method) 
{
	{
		// TrySelect(selectable);
		LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* L_0 = ___0_selectable;
		bool L_1;
		L_1 = LeanSelect_TrySelect_mAA0CABFECC04FE41D594CFF3B1D944E738AE35E9(__this, L_0, NULL);
		// }
		return;
	}
}
// System.Void Lean.Common.LeanSelect::Deselect(Lean.Common.LeanSelectable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelect_Deselect_mD29FDB8833FDBCE9CD91833CBE489E52D001C628 (LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* __this, LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* ___0_selectable, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (selectable != null && selectables != null)
		LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* L_0 = ___0_selectable;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_mD0BE578448EAA61948F25C32F8DD55AB1F778602(L_0, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3* L_2 = __this->___selectables_10;
		if (!L_2)
		{
			goto IL_0019;
		}
	}
	{
		// TryDeselect(selectable);
		LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* L_3 = ___0_selectable;
		bool L_4;
		L_4 = LeanSelect_TryDeselect_m908988308BE386DB0EE1E3FC7D7C465CDBBEBB5F(__this, L_3, NULL);
	}

IL_0019:
	{
		// }
		return;
	}
}
// System.Boolean Lean.Common.LeanSelect::TrySelect(Lean.Common.LeanSelectable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool LeanSelect_TrySelect_mAA0CABFECC04FE41D594CFF3B1D944E738AE35E9 (LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* __this, LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* ___0_selectable, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CwHelper_tCEAA370BDC6A41945D0FEDD0D8D490440F1C18F7_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m879794CD85E22D7F3E18EB8223A87D702B9E74C8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Contains_mCB5517DFCB5671A1504FF59C499DA2B27EE1B8AE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m45E012A15BC6389D71A4A103287549989EDC4DF2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m0712D8C4200A8A2294457480069FDD039D529E1E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1_Invoke_mE535439F54E8EA413F15E7C4EF538598058BBD9A_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// if (CwHelper.Enabled(selectable) == true)
		LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* L_0 = ___0_selectable;
		il2cpp_codegen_runtime_class_init_inline(CwHelper_tCEAA370BDC6A41945D0FEDD0D8D490440F1C18F7_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = CwHelper_Enabled_mD9E9A832413D64688E133F17F26B9F071F1C5272(L_0, NULL);
		if (!L_1)
		{
			goto IL_00c5;
		}
	}
	{
		// if (TryReselect(selectable) == true)
		LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* L_2 = ___0_selectable;
		bool L_3;
		L_3 = LeanSelect_TryReselect_m2F88C4B33A2F49E72EAC9FF4321E905D10A06D2D(__this, L_2, NULL);
		if (!L_3)
		{
			goto IL_00e6;
		}
	}
	{
		// if (Selectables.Contains(selectable) == false) // NOTE: Property
		List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3* L_4;
		L_4 = LeanSelect_get_Selectables_m0541739B28A2D6DE8DDB291B67F36C3D4BB7255C(__this, NULL);
		LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* L_5 = ___0_selectable;
		NullCheck(L_4);
		bool L_6;
		L_6 = List_1_Contains_mCB5517DFCB5671A1504FF59C499DA2B27EE1B8AE(L_4, L_5, List_1_Contains_mCB5517DFCB5671A1504FF59C499DA2B27EE1B8AE_RuntimeMethod_var);
		if (L_6)
		{
			goto IL_0089;
		}
	}
	{
		// switch (limit)
		int32_t L_7 = __this->___limit_7;
		V_0 = L_7;
		int32_t L_8 = V_0;
		switch (L_8)
		{
			case 0:
			{
				goto IL_0089;
			}
			case 1:
			{
				goto IL_0040;
			}
			case 2:
			{
				goto IL_0055;
			}
		}
	}
	{
		goto IL_0089;
	}

IL_0040:
	{
		// if (selectables.Count >= maxSelectables)
		List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3* L_9 = __this->___selectables_10;
		NullCheck(L_9);
		int32_t L_10;
		L_10 = List_1_get_Count_m45E012A15BC6389D71A4A103287549989EDC4DF2_inline(L_9, List_1_get_Count_m45E012A15BC6389D71A4A103287549989EDC4DF2_RuntimeMethod_var);
		int32_t L_11 = __this->___maxSelectables_8;
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0089;
		}
	}
	{
		// return false;
		return (bool)0;
	}

IL_0055:
	{
		// if (selectables.Count > 0 && selectables.Count >= maxSelectables)
		List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3* L_12 = __this->___selectables_10;
		NullCheck(L_12);
		int32_t L_13;
		L_13 = List_1_get_Count_m45E012A15BC6389D71A4A103287549989EDC4DF2_inline(L_12, List_1_get_Count_m45E012A15BC6389D71A4A103287549989EDC4DF2_RuntimeMethod_var);
		if ((((int32_t)L_13) <= ((int32_t)0)))
		{
			goto IL_0089;
		}
	}
	{
		List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3* L_14 = __this->___selectables_10;
		NullCheck(L_14);
		int32_t L_15;
		L_15 = List_1_get_Count_m45E012A15BC6389D71A4A103287549989EDC4DF2_inline(L_14, List_1_get_Count_m45E012A15BC6389D71A4A103287549989EDC4DF2_RuntimeMethod_var);
		int32_t L_16 = __this->___maxSelectables_8;
		if ((((int32_t)L_15) < ((int32_t)L_16)))
		{
			goto IL_0089;
		}
	}
	{
		// TryDeselect(selectables[0]);
		List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3* L_17 = __this->___selectables_10;
		NullCheck(L_17);
		LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* L_18;
		L_18 = List_1_get_Item_m0712D8C4200A8A2294457480069FDD039D529E1E(L_17, 0, List_1_get_Item_m0712D8C4200A8A2294457480069FDD039D529E1E_RuntimeMethod_var);
		bool L_19;
		L_19 = LeanSelect_TryDeselect_m908988308BE386DB0EE1E3FC7D7C465CDBBEBB5F(__this, L_18, NULL);
	}

IL_0089:
	{
		// selectables.Add(selectable);
		List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3* L_20 = __this->___selectables_10;
		LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* L_21 = ___0_selectable;
		NullCheck(L_20);
		List_1_Add_m879794CD85E22D7F3E18EB8223A87D702B9E74C8_inline(L_20, L_21, List_1_Add_m879794CD85E22D7F3E18EB8223A87D702B9E74C8_RuntimeMethod_var);
		// if (onSelected != null) onSelected.Invoke(selectable);
		LeanSelectableEvent_tE1BC7E60B77ABC37DF10F63761C513AB4EBEC09D* L_22 = __this->___onSelected_11;
		if (!L_22)
		{
			goto IL_00a9;
		}
	}
	{
		// if (onSelected != null) onSelected.Invoke(selectable);
		LeanSelectableEvent_tE1BC7E60B77ABC37DF10F63761C513AB4EBEC09D* L_23 = __this->___onSelected_11;
		LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* L_24 = ___0_selectable;
		NullCheck(L_23);
		UnityEvent_1_Invoke_mE535439F54E8EA413F15E7C4EF538598058BBD9A(L_23, L_24, UnityEvent_1_Invoke_mE535439F54E8EA413F15E7C4EF538598058BBD9A_RuntimeMethod_var);
	}

IL_00a9:
	{
		// if (OnAnySelected != null) OnAnySelected.Invoke(this, selectable);
		il2cpp_codegen_runtime_class_init_inline(LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var);
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_25 = ((LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var))->___OnAnySelected_14;
		if (!L_25)
		{
			goto IL_00bc;
		}
	}
	{
		// if (OnAnySelected != null) OnAnySelected.Invoke(this, selectable);
		il2cpp_codegen_runtime_class_init_inline(LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var);
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_26 = ((LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var))->___OnAnySelected_14;
		LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* L_27 = ___0_selectable;
		NullCheck(L_26);
		Action_2_Invoke_m92A2047F7B6E70B622B357B63B29E4FB7BD96336_inline(L_26, __this, L_27, NULL);
	}

IL_00bc:
	{
		// selectable.InvokeOnSelected(this);
		LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* L_28 = ___0_selectable;
		NullCheck(L_28);
		LeanSelectable_InvokeOnSelected_m3C81335F17D41C2F14CEBF38F31B275E59D6FAE9(L_28, __this, NULL);
		// return true;
		return (bool)1;
	}

IL_00c5:
	{
		// if (onNothing != null) onNothing.Invoke();
		UnityEvent_tDC2C3548799DBC91D1E3F3DE60083A66F4751977* L_29 = __this->___onNothing_13;
		if (!L_29)
		{
			goto IL_00d8;
		}
	}
	{
		// if (onNothing != null) onNothing.Invoke();
		UnityEvent_tDC2C3548799DBC91D1E3F3DE60083A66F4751977* L_30 = __this->___onNothing_13;
		NullCheck(L_30);
		UnityEvent_Invoke_mFBF80D59B03C30C5FE6A06F897D954ACADE061D2(L_30, NULL);
	}

IL_00d8:
	{
		// if (deselectWithNothing == true)
		bool L_31 = __this->___deselectWithNothing_6;
		if (!L_31)
		{
			goto IL_00e6;
		}
	}
	{
		// DeselectAll();
		LeanSelect_DeselectAll_mFE5E913DADC65729C87BE27A69D04658F3785E18(__this, NULL);
	}

IL_00e6:
	{
		// return false;
		return (bool)0;
	}
}
// System.Boolean Lean.Common.LeanSelect::TryReselect(Lean.Common.LeanSelectable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool LeanSelect_TryReselect_m2F88C4B33A2F49E72EAC9FF4321E905D10A06D2D (LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* __this, LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* ___0_selectable, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Contains_mCB5517DFCB5671A1504FF59C499DA2B27EE1B8AE_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// switch (reselect)
		int32_t L_0 = __this->___reselect_9;
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_001f;
			}
			case 1:
			{
				goto IL_002f;
			}
			case 2:
			{
				goto IL_0048;
			}
			case 3:
			{
				goto IL_005f;
			}
		}
	}
	{
		goto IL_0061;
	}

IL_001f:
	{
		// if (Selectables.Contains(selectable) == false) // NOTE: Property
		List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3* L_2;
		L_2 = LeanSelect_get_Selectables_m0541739B28A2D6DE8DDB291B67F36C3D4BB7255C(__this, NULL);
		LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* L_3 = ___0_selectable;
		NullCheck(L_2);
		bool L_4;
		L_4 = List_1_Contains_mCB5517DFCB5671A1504FF59C499DA2B27EE1B8AE(L_2, L_3, List_1_Contains_mCB5517DFCB5671A1504FF59C499DA2B27EE1B8AE_RuntimeMethod_var);
		if (L_4)
		{
			goto IL_0061;
		}
	}
	{
		// return true;
		return (bool)1;
	}

IL_002f:
	{
		// if (Selectables.Contains(selectable) == false) // NOTE: Property
		List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3* L_5;
		L_5 = LeanSelect_get_Selectables_m0541739B28A2D6DE8DDB291B67F36C3D4BB7255C(__this, NULL);
		LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* L_6 = ___0_selectable;
		NullCheck(L_5);
		bool L_7;
		L_7 = List_1_Contains_mCB5517DFCB5671A1504FF59C499DA2B27EE1B8AE(L_5, L_6, List_1_Contains_mCB5517DFCB5671A1504FF59C499DA2B27EE1B8AE_RuntimeMethod_var);
		if (L_7)
		{
			goto IL_003f;
		}
	}
	{
		// return true;
		return (bool)1;
	}

IL_003f:
	{
		// Deselect(selectable);
		LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* L_8 = ___0_selectable;
		LeanSelect_Deselect_mD29FDB8833FDBCE9CD91833CBE489E52D001C628(__this, L_8, NULL);
		// break;
		goto IL_0061;
	}

IL_0048:
	{
		// if (Selectables.Contains(selectable) == true) // NOTE: Property
		List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3* L_9;
		L_9 = LeanSelect_get_Selectables_m0541739B28A2D6DE8DDB291B67F36C3D4BB7255C(__this, NULL);
		LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* L_10 = ___0_selectable;
		NullCheck(L_9);
		bool L_11;
		L_11 = List_1_Contains_mCB5517DFCB5671A1504FF59C499DA2B27EE1B8AE(L_9, L_10, List_1_Contains_mCB5517DFCB5671A1504FF59C499DA2B27EE1B8AE_RuntimeMethod_var);
		if (!L_11)
		{
			goto IL_005d;
		}
	}
	{
		// Deselect(selectable);
		LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* L_12 = ___0_selectable;
		LeanSelect_Deselect_mD29FDB8833FDBCE9CD91833CBE489E52D001C628(__this, L_12, NULL);
	}

IL_005d:
	{
		// return true;
		return (bool)1;
	}

IL_005f:
	{
		// return true;
		return (bool)1;
	}

IL_0061:
	{
		// return false;
		return (bool)0;
	}
}
// System.Boolean Lean.Common.LeanSelect::TryDeselect(Lean.Common.LeanSelectable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool LeanSelect_TryDeselect_m908988308BE386DB0EE1E3FC7D7C465CDBBEBB5F (LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* __this, LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* ___0_selectable, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_IndexOf_mCDCF06F86563E158C15328FB5D36286E68D653B6_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// if (selectables != null)
		List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3* L_0 = __this->___selectables_10;
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		// var index = selectables.IndexOf(selectable);
		List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3* L_1 = __this->___selectables_10;
		LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* L_2 = ___0_selectable;
		NullCheck(L_1);
		int32_t L_3;
		L_3 = List_1_IndexOf_mCDCF06F86563E158C15328FB5D36286E68D653B6(L_1, L_2, List_1_IndexOf_mCDCF06F86563E158C15328FB5D36286E68D653B6_RuntimeMethod_var);
		V_0 = L_3;
		// if (index >= 0)
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) < ((int32_t)0)))
		{
			goto IL_0021;
		}
	}
	{
		// return TryDeselect(index);
		int32_t L_5 = V_0;
		bool L_6;
		L_6 = LeanSelect_TryDeselect_m1F043BCFE0AF219FE3D252F4F5E63F7BD25E455D(__this, L_5, NULL);
		return L_6;
	}

IL_0021:
	{
		// return false;
		return (bool)0;
	}
}
// System.Boolean Lean.Common.LeanSelect::TryDeselect(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool LeanSelect_TryDeselect_m1F043BCFE0AF219FE3D252F4F5E63F7BD25E455D (LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* __this, int32_t ___0_index, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_RemoveAt_m3D273B83DA1DA3CA02F13C3D3ECB2DDE052CFD2C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m45E012A15BC6389D71A4A103287549989EDC4DF2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m0712D8C4200A8A2294457480069FDD039D529E1E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1_Invoke_mE535439F54E8EA413F15E7C4EF538598058BBD9A_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* V_1 = NULL;
	{
		// var success = false;
		V_0 = (bool)0;
		// if (selectables != null && index >= 0 && index < selectables.Count)
		List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3* L_0 = __this->___selectables_10;
		if (!L_0)
		{
			goto IL_006e;
		}
	}
	{
		int32_t L_1 = ___0_index;
		if ((((int32_t)L_1) < ((int32_t)0)))
		{
			goto IL_006e;
		}
	}
	{
		int32_t L_2 = ___0_index;
		List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3* L_3 = __this->___selectables_10;
		NullCheck(L_3);
		int32_t L_4;
		L_4 = List_1_get_Count_m45E012A15BC6389D71A4A103287549989EDC4DF2_inline(L_3, List_1_get_Count_m45E012A15BC6389D71A4A103287549989EDC4DF2_RuntimeMethod_var);
		if ((((int32_t)L_2) >= ((int32_t)L_4)))
		{
			goto IL_006e;
		}
	}
	{
		// var selectable = selectables[index];
		List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3* L_5 = __this->___selectables_10;
		int32_t L_6 = ___0_index;
		NullCheck(L_5);
		LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* L_7;
		L_7 = List_1_get_Item_m0712D8C4200A8A2294457480069FDD039D529E1E(L_5, L_6, List_1_get_Item_m0712D8C4200A8A2294457480069FDD039D529E1E_RuntimeMethod_var);
		V_1 = L_7;
		// selectables.RemoveAt(index);
		List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3* L_8 = __this->___selectables_10;
		int32_t L_9 = ___0_index;
		NullCheck(L_8);
		List_1_RemoveAt_m3D273B83DA1DA3CA02F13C3D3ECB2DDE052CFD2C(L_8, L_9, List_1_RemoveAt_m3D273B83DA1DA3CA02F13C3D3ECB2DDE052CFD2C_RuntimeMethod_var);
		// if (selectable != null)
		LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* L_10 = V_1;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_11;
		L_11 = Object_op_Inequality_mD0BE578448EAA61948F25C32F8DD55AB1F778602(L_10, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_11)
		{
			goto IL_006c;
		}
	}
	{
		// selectable.InvokeOnDeslected(this);
		LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* L_12 = V_1;
		NullCheck(L_12);
		LeanSelectable_InvokeOnDeslected_m6B165447EB743395D368821C3E598822DFDEA7DF(L_12, __this, NULL);
		// if (onDeselected != null)
		LeanSelectableEvent_tE1BC7E60B77ABC37DF10F63761C513AB4EBEC09D* L_13 = __this->___onDeselected_12;
		if (!L_13)
		{
			goto IL_0059;
		}
	}
	{
		// onDeselected.Invoke(selectable);
		LeanSelectableEvent_tE1BC7E60B77ABC37DF10F63761C513AB4EBEC09D* L_14 = __this->___onDeselected_12;
		LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* L_15 = V_1;
		NullCheck(L_14);
		UnityEvent_1_Invoke_mE535439F54E8EA413F15E7C4EF538598058BBD9A(L_14, L_15, UnityEvent_1_Invoke_mE535439F54E8EA413F15E7C4EF538598058BBD9A_RuntimeMethod_var);
	}

IL_0059:
	{
		// if (OnAnyDeselected != null)
		il2cpp_codegen_runtime_class_init_inline(LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var);
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_16 = ((LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var))->___OnAnyDeselected_15;
		if (!L_16)
		{
			goto IL_006c;
		}
	}
	{
		// OnAnyDeselected.Invoke(this, selectable);
		il2cpp_codegen_runtime_class_init_inline(LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var);
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_17 = ((LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var))->___OnAnyDeselected_15;
		LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* L_18 = V_1;
		NullCheck(L_17);
		Action_2_Invoke_m92A2047F7B6E70B622B357B63B29E4FB7BD96336_inline(L_17, __this, L_18, NULL);
	}

IL_006c:
	{
		// success = true;
		V_0 = (bool)1;
	}

IL_006e:
	{
		// return success;
		bool L_19 = V_0;
		return L_19;
	}
}
// System.Void Lean.Common.LeanSelect::DeselectAll()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelect_DeselectAll_mFE5E913DADC65729C87BE27A69D04658F3785E18 (LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_RemoveAt_m3D273B83DA1DA3CA02F13C3D3ECB2DDE052CFD2C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m45E012A15BC6389D71A4A103287549989EDC4DF2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m0712D8C4200A8A2294457480069FDD039D529E1E_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// if (selectables != null)
		List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3* L_0 = __this->___selectables_10;
		if (!L_0)
		{
			goto IL_0044;
		}
	}
	{
		goto IL_0036;
	}

IL_000a:
	{
		// var index      = selectables.Count - 1;
		List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3* L_1 = __this->___selectables_10;
		NullCheck(L_1);
		int32_t L_2;
		L_2 = List_1_get_Count_m45E012A15BC6389D71A4A103287549989EDC4DF2_inline(L_1, List_1_get_Count_m45E012A15BC6389D71A4A103287549989EDC4DF2_RuntimeMethod_var);
		V_0 = ((int32_t)il2cpp_codegen_subtract(L_2, 1));
		// var selectable = selectables[index];
		List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3* L_3 = __this->___selectables_10;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* L_5;
		L_5 = List_1_get_Item_m0712D8C4200A8A2294457480069FDD039D529E1E(L_3, L_4, List_1_get_Item_m0712D8C4200A8A2294457480069FDD039D529E1E_RuntimeMethod_var);
		// selectables.RemoveAt(index);
		List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3* L_6 = __this->___selectables_10;
		int32_t L_7 = V_0;
		NullCheck(L_6);
		List_1_RemoveAt_m3D273B83DA1DA3CA02F13C3D3ECB2DDE052CFD2C(L_6, L_7, List_1_RemoveAt_m3D273B83DA1DA3CA02F13C3D3ECB2DDE052CFD2C_RuntimeMethod_var);
		// selectable.InvokeOnDeslected(this);
		NullCheck(L_5);
		LeanSelectable_InvokeOnDeslected_m6B165447EB743395D368821C3E598822DFDEA7DF(L_5, __this, NULL);
	}

IL_0036:
	{
		// while (selectables.Count > 0)
		List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3* L_8 = __this->___selectables_10;
		NullCheck(L_8);
		int32_t L_9;
		L_9 = List_1_get_Count_m45E012A15BC6389D71A4A103287549989EDC4DF2_inline(L_8, List_1_get_Count_m45E012A15BC6389D71A4A103287549989EDC4DF2_RuntimeMethod_var);
		if ((((int32_t)L_9) > ((int32_t)0)))
		{
			goto IL_000a;
		}
	}

IL_0044:
	{
		// }
		return;
	}
}
// System.Void Lean.Common.LeanSelect::Cull(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelect_Cull_m74A83CB52DDCA433C22E4F9830F22A8E20A078F2 (LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* __this, int32_t ___0_maxCount, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_RemoveAt_m3D273B83DA1DA3CA02F13C3D3ECB2DDE052CFD2C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m45E012A15BC6389D71A4A103287549989EDC4DF2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m0712D8C4200A8A2294457480069FDD039D529E1E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* V_0 = NULL;
	{
		// if (selectables != null)
		List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3* L_0 = __this->___selectables_10;
		if (!L_0)
		{
			goto IL_0058;
		}
	}
	{
		goto IL_003c;
	}

IL_000a:
	{
		// var selectable = selectables[0];
		List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3* L_1 = __this->___selectables_10;
		NullCheck(L_1);
		LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* L_2;
		L_2 = List_1_get_Item_m0712D8C4200A8A2294457480069FDD039D529E1E(L_1, 0, List_1_get_Item_m0712D8C4200A8A2294457480069FDD039D529E1E_RuntimeMethod_var);
		V_0 = L_2;
		// selectables.RemoveAt(0);
		List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3* L_3 = __this->___selectables_10;
		NullCheck(L_3);
		List_1_RemoveAt_m3D273B83DA1DA3CA02F13C3D3ECB2DDE052CFD2C(L_3, 0, List_1_RemoveAt_m3D273B83DA1DA3CA02F13C3D3ECB2DDE052CFD2C_RuntimeMethod_var);
		// if (selectable != null)
		LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* L_4 = V_0;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Inequality_mD0BE578448EAA61948F25C32F8DD55AB1F778602(L_4, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_5)
		{
			goto IL_003c;
		}
	}
	{
		// if (selectable != null)
		LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* L_6 = V_0;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_7;
		L_7 = Object_op_Inequality_mD0BE578448EAA61948F25C32F8DD55AB1F778602(L_6, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_7)
		{
			goto IL_003c;
		}
	}
	{
		// Deselect(selectable);
		LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* L_8 = V_0;
		LeanSelect_Deselect_mD29FDB8833FDBCE9CD91833CBE489E52D001C628(__this, L_8, NULL);
	}

IL_003c:
	{
		// while (selectables.Count > 0 && selectables.Count > maxCount)
		List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3* L_9 = __this->___selectables_10;
		NullCheck(L_9);
		int32_t L_10;
		L_10 = List_1_get_Count_m45E012A15BC6389D71A4A103287549989EDC4DF2_inline(L_9, List_1_get_Count_m45E012A15BC6389D71A4A103287549989EDC4DF2_RuntimeMethod_var);
		if ((((int32_t)L_10) <= ((int32_t)0)))
		{
			goto IL_0058;
		}
	}
	{
		List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3* L_11 = __this->___selectables_10;
		NullCheck(L_11);
		int32_t L_12;
		L_12 = List_1_get_Count_m45E012A15BC6389D71A4A103287549989EDC4DF2_inline(L_11, List_1_get_Count_m45E012A15BC6389D71A4A103287549989EDC4DF2_RuntimeMethod_var);
		int32_t L_13 = ___0_maxCount;
		if ((((int32_t)L_12) > ((int32_t)L_13)))
		{
			goto IL_000a;
		}
	}

IL_0058:
	{
		// }
		return;
	}
}
// System.Void Lean.Common.LeanSelect::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelect_OnEnable_m3C1CA4EABCE9D1BEE4D216FF5421E07A393E46D2 (LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LinkedList_1_AddLast_m22A0173682AA7EC7831D043A8AD267DFEA1446B9_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// instancesNode = Instances.AddLast(this);
		il2cpp_codegen_runtime_class_init_inline(LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var);
		LinkedList_1_t74DD3E2689E35FC0DF8DF1971B5CD529EEE8CAA1* L_0 = ((LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var))->___Instances_4;
		NullCheck(L_0);
		LinkedListNode_1_t7D0DA7355A32A333D2AB6F41BC1E57C6EF2AE9F7* L_1;
		L_1 = LinkedList_1_AddLast_m22A0173682AA7EC7831D043A8AD267DFEA1446B9(L_0, __this, LinkedList_1_AddLast_m22A0173682AA7EC7831D043A8AD267DFEA1446B9_RuntimeMethod_var);
		__this->___instancesNode_5 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___instancesNode_5), (void*)L_1);
		// }
		return;
	}
}
// System.Void Lean.Common.LeanSelect::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelect_OnDisable_m89F60E7601E09C57440F0411CEEE9EDD563D9334 (LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LinkedList_1_Remove_mC8D384262E80C141DEA34CEC4DC755F5BCB953FC_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Instances.Remove(instancesNode); instancesNode = null;
		il2cpp_codegen_runtime_class_init_inline(LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var);
		LinkedList_1_t74DD3E2689E35FC0DF8DF1971B5CD529EEE8CAA1* L_0 = ((LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var))->___Instances_4;
		LinkedListNode_1_t7D0DA7355A32A333D2AB6F41BC1E57C6EF2AE9F7* L_1 = __this->___instancesNode_5;
		NullCheck(L_0);
		LinkedList_1_Remove_mC8D384262E80C141DEA34CEC4DC755F5BCB953FC(L_0, L_1, LinkedList_1_Remove_mC8D384262E80C141DEA34CEC4DC755F5BCB953FC_RuntimeMethod_var);
		// Instances.Remove(instancesNode); instancesNode = null;
		__this->___instancesNode_5 = (LinkedListNode_1_t7D0DA7355A32A333D2AB6F41BC1E57C6EF2AE9F7*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___instancesNode_5), (void*)(LinkedListNode_1_t7D0DA7355A32A333D2AB6F41BC1E57C6EF2AE9F7*)NULL);
		// }
		return;
	}
}
// System.Void Lean.Common.LeanSelect::OnDestroy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelect_OnDestroy_mAA479854F63C0D809850EC48D625DBCC75CDC04E (LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* __this, const RuntimeMethod* method) 
{
	{
		// DeselectAll();
		LeanSelect_DeselectAll_mFE5E913DADC65729C87BE27A69D04658F3785E18(__this, NULL);
		// }
		return;
	}
}
// System.Void Lean.Common.LeanSelect::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelect__ctor_m1D98D8A3CDA79B09257B6715C3D6A127D0FB09B9 (LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* __this, const RuntimeMethod* method) 
{
	{
		// public int MaxSelectables { set { maxSelectables = value; } get { return maxSelectables; } } [SerializeField] private int maxSelectables = 5;
		__this->___maxSelectables_8 = 5;
		// public ReselectType Reselect { set { reselect = value; } get { return reselect; } } [SerializeField] private ReselectType reselect = ReselectType.SelectAgain;
		__this->___reselect_9 = 3;
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
// System.Void Lean.Common.LeanSelect::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelect__cctor_m16B8DE2064E0D0191A004953151CAFD300A627A4 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LinkedList_1__ctor_mC7E25FA9B50FCAED34E6C53704CBCDAE4BE0BB46_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LinkedList_1_t74DD3E2689E35FC0DF8DF1971B5CD529EEE8CAA1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static LinkedList<LeanSelect> Instances = new LinkedList<LeanSelect>(); [System.NonSerialized] private LinkedListNode<LeanSelect> instancesNode;
		LinkedList_1_t74DD3E2689E35FC0DF8DF1971B5CD529EEE8CAA1* L_0 = (LinkedList_1_t74DD3E2689E35FC0DF8DF1971B5CD529EEE8CAA1*)il2cpp_codegen_object_new(LinkedList_1_t74DD3E2689E35FC0DF8DF1971B5CD529EEE8CAA1_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		LinkedList_1__ctor_mC7E25FA9B50FCAED34E6C53704CBCDAE4BE0BB46(L_0, LinkedList_1__ctor_mC7E25FA9B50FCAED34E6C53704CBCDAE4BE0BB46_RuntimeMethod_var);
		((LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var))->___Instances_4 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&((LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var))->___Instances_4), (void*)L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Common.LeanSelect/LeanSelectableEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectableEvent__ctor_mF85F656B71FB189CE41973B9F90133305E4889DF (LeanSelectableEvent_tE1BC7E60B77ABC37DF10F63761C513AB4EBEC09D* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1__ctor_mA2FAFACB7630DDA1C8722C0684B8E14E2517CD57_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_mA2FAFACB7630DDA1C8722C0684B8E14E2517CD57(__this, UnityEvent_1__ctor_mA2FAFACB7630DDA1C8722C0684B8E14E2517CD57_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Common.LeanSelectable::set_SelfSelected(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectable_set_SelfSelected_m3B119374E382EB78103F159BFC08C1DDB8220186 (LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* __this, bool ___0_value, const RuntimeMethod* method) 
{
	{
		// public bool SelfSelected { set { if (selfSelected != value) { selfSelected = value; if (value == true) InvokeOnSelected(null); else InvokeOnDeslected(null); } } get { return selfSelected; } } [SerializeField] private bool selfSelected;
		bool L_0 = __this->___selfSelected_6;
		bool L_1 = ___0_value;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0022;
		}
	}
	{
		// public bool SelfSelected { set { if (selfSelected != value) { selfSelected = value; if (value == true) InvokeOnSelected(null); else InvokeOnDeslected(null); } } get { return selfSelected; } } [SerializeField] private bool selfSelected;
		bool L_2 = ___0_value;
		__this->___selfSelected_6 = L_2;
		// public bool SelfSelected { set { if (selfSelected != value) { selfSelected = value; if (value == true) InvokeOnSelected(null); else InvokeOnDeslected(null); } } get { return selfSelected; } } [SerializeField] private bool selfSelected;
		bool L_3 = ___0_value;
		if (!L_3)
		{
			goto IL_001b;
		}
	}
	{
		// public bool SelfSelected { set { if (selfSelected != value) { selfSelected = value; if (value == true) InvokeOnSelected(null); else InvokeOnDeslected(null); } } get { return selfSelected; } } [SerializeField] private bool selfSelected;
		LeanSelectable_InvokeOnSelected_m3C81335F17D41C2F14CEBF38F31B275E59D6FAE9(__this, (LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F*)NULL, NULL);
		return;
	}

IL_001b:
	{
		// public bool SelfSelected { set { if (selfSelected != value) { selfSelected = value; if (value == true) InvokeOnSelected(null); else InvokeOnDeslected(null); } } get { return selfSelected; } } [SerializeField] private bool selfSelected;
		LeanSelectable_InvokeOnDeslected_m6B165447EB743395D368821C3E598822DFDEA7DF(__this, (LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F*)NULL, NULL);
	}

IL_0022:
	{
		// public bool SelfSelected { set { if (selfSelected != value) { selfSelected = value; if (value == true) InvokeOnSelected(null); else InvokeOnDeslected(null); } } get { return selfSelected; } } [SerializeField] private bool selfSelected;
		return;
	}
}
// System.Boolean Lean.Common.LeanSelectable::get_SelfSelected()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool LeanSelectable_get_SelfSelected_m092BB429EE8A9983CEB84B287CAFCE436DE4EF75 (LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* __this, const RuntimeMethod* method) 
{
	{
		// public bool SelfSelected { set { if (selfSelected != value) { selfSelected = value; if (value == true) InvokeOnSelected(null); else InvokeOnDeslected(null); } } get { return selfSelected; } } [SerializeField] private bool selfSelected;
		bool L_0 = __this->___selfSelected_6;
		return L_0;
	}
}
// Lean.Common.LeanSelectable/LeanSelectEvent Lean.Common.LeanSelectable::get_OnSelected()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR LeanSelectEvent_tE1B7F7125473C6F40463EC6C88B8842A0404C34F* LeanSelectable_get_OnSelected_mEE84613146ACD47032B93CB4E8988F526A15988B (LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeanSelectEvent_tE1B7F7125473C6F40463EC6C88B8842A0404C34F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public LeanSelectEvent OnSelected { get { if (onSelected == null) onSelected = new LeanSelectEvent(); return onSelected; } } [SerializeField] private LeanSelectEvent onSelected;
		LeanSelectEvent_tE1B7F7125473C6F40463EC6C88B8842A0404C34F* L_0 = __this->___onSelected_7;
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		// public LeanSelectEvent OnSelected { get { if (onSelected == null) onSelected = new LeanSelectEvent(); return onSelected; } } [SerializeField] private LeanSelectEvent onSelected;
		LeanSelectEvent_tE1B7F7125473C6F40463EC6C88B8842A0404C34F* L_1 = (LeanSelectEvent_tE1B7F7125473C6F40463EC6C88B8842A0404C34F*)il2cpp_codegen_object_new(LeanSelectEvent_tE1B7F7125473C6F40463EC6C88B8842A0404C34F_il2cpp_TypeInfo_var);
		NullCheck(L_1);
		LeanSelectEvent__ctor_m298B432186C5138B38B93C71D379689F44C0B204(L_1, NULL);
		__this->___onSelected_7 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___onSelected_7), (void*)L_1);
	}

IL_0013:
	{
		// public LeanSelectEvent OnSelected { get { if (onSelected == null) onSelected = new LeanSelectEvent(); return onSelected; } } [SerializeField] private LeanSelectEvent onSelected;
		LeanSelectEvent_tE1B7F7125473C6F40463EC6C88B8842A0404C34F* L_2 = __this->___onSelected_7;
		return L_2;
	}
}
// Lean.Common.LeanSelectable/LeanSelectEvent Lean.Common.LeanSelectable::get_OnDeselected()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR LeanSelectEvent_tE1B7F7125473C6F40463EC6C88B8842A0404C34F* LeanSelectable_get_OnDeselected_m055BD9B88E71CAB5E6C4D429CA271308F56EBC29 (LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeanSelectEvent_tE1B7F7125473C6F40463EC6C88B8842A0404C34F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public LeanSelectEvent OnDeselected { get { if (onDeselected == null) onDeselected = new LeanSelectEvent(); return onDeselected; } } [SerializeField] private LeanSelectEvent onDeselected;
		LeanSelectEvent_tE1B7F7125473C6F40463EC6C88B8842A0404C34F* L_0 = __this->___onDeselected_8;
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		// public LeanSelectEvent OnDeselected { get { if (onDeselected == null) onDeselected = new LeanSelectEvent(); return onDeselected; } } [SerializeField] private LeanSelectEvent onDeselected;
		LeanSelectEvent_tE1B7F7125473C6F40463EC6C88B8842A0404C34F* L_1 = (LeanSelectEvent_tE1B7F7125473C6F40463EC6C88B8842A0404C34F*)il2cpp_codegen_object_new(LeanSelectEvent_tE1B7F7125473C6F40463EC6C88B8842A0404C34F_il2cpp_TypeInfo_var);
		NullCheck(L_1);
		LeanSelectEvent__ctor_m298B432186C5138B38B93C71D379689F44C0B204(L_1, NULL);
		__this->___onDeselected_8 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___onDeselected_8), (void*)L_1);
	}

IL_0013:
	{
		// public LeanSelectEvent OnDeselected { get { if (onDeselected == null) onDeselected = new LeanSelectEvent(); return onDeselected; } } [SerializeField] private LeanSelectEvent onDeselected;
		LeanSelectEvent_tE1B7F7125473C6F40463EC6C88B8842A0404C34F* L_2 = __this->___onDeselected_8;
		return L_2;
	}
}
// System.Void Lean.Common.LeanSelectable::add_OnAnyEnabled(System.Action`1<Lean.Common.LeanSelectable>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectable_add_OnAnyEnabled_mE8C99B3F6FEDF172E77B2BDAB39FD1FE08D91BF8 (Action_1_tAD366A485E434448836F3A4108059A1618C980AF* ___0_value, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_tAD366A485E434448836F3A4108059A1618C980AF_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_tAD366A485E434448836F3A4108059A1618C980AF* V_0 = NULL;
	Action_1_tAD366A485E434448836F3A4108059A1618C980AF* V_1 = NULL;
	Action_1_tAD366A485E434448836F3A4108059A1618C980AF* V_2 = NULL;
	{
		il2cpp_codegen_runtime_class_init_inline(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var);
		Action_1_tAD366A485E434448836F3A4108059A1618C980AF* L_0 = ((LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var))->___OnAnyEnabled_9;
		V_0 = L_0;
	}

IL_0006:
	{
		Action_1_tAD366A485E434448836F3A4108059A1618C980AF* L_1 = V_0;
		V_1 = L_1;
		Action_1_tAD366A485E434448836F3A4108059A1618C980AF* L_2 = V_1;
		Action_1_tAD366A485E434448836F3A4108059A1618C980AF* L_3 = ___0_value;
		Delegate_t* L_4;
		L_4 = Delegate_Combine_m1F725AEF318BE6F0426863490691A6F4606E7D00(L_2, L_3, NULL);
		V_2 = ((Action_1_tAD366A485E434448836F3A4108059A1618C980AF*)Castclass((RuntimeObject*)L_4, Action_1_tAD366A485E434448836F3A4108059A1618C980AF_il2cpp_TypeInfo_var));
		il2cpp_codegen_runtime_class_init_inline(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var);
		Action_1_tAD366A485E434448836F3A4108059A1618C980AF* L_5 = V_2;
		Action_1_tAD366A485E434448836F3A4108059A1618C980AF* L_6 = V_1;
		Action_1_tAD366A485E434448836F3A4108059A1618C980AF* L_7;
		L_7 = InterlockedCompareExchangeImpl<Action_1_tAD366A485E434448836F3A4108059A1618C980AF*>((&((LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var))->___OnAnyEnabled_9), L_5, L_6);
		V_0 = L_7;
		Action_1_tAD366A485E434448836F3A4108059A1618C980AF* L_8 = V_0;
		Action_1_tAD366A485E434448836F3A4108059A1618C980AF* L_9 = V_1;
		if ((!(((RuntimeObject*)(Action_1_tAD366A485E434448836F3A4108059A1618C980AF*)L_8) == ((RuntimeObject*)(Action_1_tAD366A485E434448836F3A4108059A1618C980AF*)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void Lean.Common.LeanSelectable::remove_OnAnyEnabled(System.Action`1<Lean.Common.LeanSelectable>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectable_remove_OnAnyEnabled_mD48231C5388141EEEA0A98289D82D46759507577 (Action_1_tAD366A485E434448836F3A4108059A1618C980AF* ___0_value, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_tAD366A485E434448836F3A4108059A1618C980AF_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_tAD366A485E434448836F3A4108059A1618C980AF* V_0 = NULL;
	Action_1_tAD366A485E434448836F3A4108059A1618C980AF* V_1 = NULL;
	Action_1_tAD366A485E434448836F3A4108059A1618C980AF* V_2 = NULL;
	{
		il2cpp_codegen_runtime_class_init_inline(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var);
		Action_1_tAD366A485E434448836F3A4108059A1618C980AF* L_0 = ((LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var))->___OnAnyEnabled_9;
		V_0 = L_0;
	}

IL_0006:
	{
		Action_1_tAD366A485E434448836F3A4108059A1618C980AF* L_1 = V_0;
		V_1 = L_1;
		Action_1_tAD366A485E434448836F3A4108059A1618C980AF* L_2 = V_1;
		Action_1_tAD366A485E434448836F3A4108059A1618C980AF* L_3 = ___0_value;
		Delegate_t* L_4;
		L_4 = Delegate_Remove_m8B7DD5661308FA972E23CA1CC3FC9CEB355504E3(L_2, L_3, NULL);
		V_2 = ((Action_1_tAD366A485E434448836F3A4108059A1618C980AF*)Castclass((RuntimeObject*)L_4, Action_1_tAD366A485E434448836F3A4108059A1618C980AF_il2cpp_TypeInfo_var));
		il2cpp_codegen_runtime_class_init_inline(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var);
		Action_1_tAD366A485E434448836F3A4108059A1618C980AF* L_5 = V_2;
		Action_1_tAD366A485E434448836F3A4108059A1618C980AF* L_6 = V_1;
		Action_1_tAD366A485E434448836F3A4108059A1618C980AF* L_7;
		L_7 = InterlockedCompareExchangeImpl<Action_1_tAD366A485E434448836F3A4108059A1618C980AF*>((&((LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var))->___OnAnyEnabled_9), L_5, L_6);
		V_0 = L_7;
		Action_1_tAD366A485E434448836F3A4108059A1618C980AF* L_8 = V_0;
		Action_1_tAD366A485E434448836F3A4108059A1618C980AF* L_9 = V_1;
		if ((!(((RuntimeObject*)(Action_1_tAD366A485E434448836F3A4108059A1618C980AF*)L_8) == ((RuntimeObject*)(Action_1_tAD366A485E434448836F3A4108059A1618C980AF*)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void Lean.Common.LeanSelectable::add_OnAnyDisabled(System.Action`1<Lean.Common.LeanSelectable>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectable_add_OnAnyDisabled_m7D9AF4A62DC7189BEA8AD97B3B0E09AEF07224B8 (Action_1_tAD366A485E434448836F3A4108059A1618C980AF* ___0_value, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_tAD366A485E434448836F3A4108059A1618C980AF_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_tAD366A485E434448836F3A4108059A1618C980AF* V_0 = NULL;
	Action_1_tAD366A485E434448836F3A4108059A1618C980AF* V_1 = NULL;
	Action_1_tAD366A485E434448836F3A4108059A1618C980AF* V_2 = NULL;
	{
		il2cpp_codegen_runtime_class_init_inline(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var);
		Action_1_tAD366A485E434448836F3A4108059A1618C980AF* L_0 = ((LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var))->___OnAnyDisabled_10;
		V_0 = L_0;
	}

IL_0006:
	{
		Action_1_tAD366A485E434448836F3A4108059A1618C980AF* L_1 = V_0;
		V_1 = L_1;
		Action_1_tAD366A485E434448836F3A4108059A1618C980AF* L_2 = V_1;
		Action_1_tAD366A485E434448836F3A4108059A1618C980AF* L_3 = ___0_value;
		Delegate_t* L_4;
		L_4 = Delegate_Combine_m1F725AEF318BE6F0426863490691A6F4606E7D00(L_2, L_3, NULL);
		V_2 = ((Action_1_tAD366A485E434448836F3A4108059A1618C980AF*)Castclass((RuntimeObject*)L_4, Action_1_tAD366A485E434448836F3A4108059A1618C980AF_il2cpp_TypeInfo_var));
		il2cpp_codegen_runtime_class_init_inline(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var);
		Action_1_tAD366A485E434448836F3A4108059A1618C980AF* L_5 = V_2;
		Action_1_tAD366A485E434448836F3A4108059A1618C980AF* L_6 = V_1;
		Action_1_tAD366A485E434448836F3A4108059A1618C980AF* L_7;
		L_7 = InterlockedCompareExchangeImpl<Action_1_tAD366A485E434448836F3A4108059A1618C980AF*>((&((LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var))->___OnAnyDisabled_10), L_5, L_6);
		V_0 = L_7;
		Action_1_tAD366A485E434448836F3A4108059A1618C980AF* L_8 = V_0;
		Action_1_tAD366A485E434448836F3A4108059A1618C980AF* L_9 = V_1;
		if ((!(((RuntimeObject*)(Action_1_tAD366A485E434448836F3A4108059A1618C980AF*)L_8) == ((RuntimeObject*)(Action_1_tAD366A485E434448836F3A4108059A1618C980AF*)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void Lean.Common.LeanSelectable::remove_OnAnyDisabled(System.Action`1<Lean.Common.LeanSelectable>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectable_remove_OnAnyDisabled_m978AF768489C7EE61BC56590D1E359146315D87B (Action_1_tAD366A485E434448836F3A4108059A1618C980AF* ___0_value, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_tAD366A485E434448836F3A4108059A1618C980AF_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_tAD366A485E434448836F3A4108059A1618C980AF* V_0 = NULL;
	Action_1_tAD366A485E434448836F3A4108059A1618C980AF* V_1 = NULL;
	Action_1_tAD366A485E434448836F3A4108059A1618C980AF* V_2 = NULL;
	{
		il2cpp_codegen_runtime_class_init_inline(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var);
		Action_1_tAD366A485E434448836F3A4108059A1618C980AF* L_0 = ((LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var))->___OnAnyDisabled_10;
		V_0 = L_0;
	}

IL_0006:
	{
		Action_1_tAD366A485E434448836F3A4108059A1618C980AF* L_1 = V_0;
		V_1 = L_1;
		Action_1_tAD366A485E434448836F3A4108059A1618C980AF* L_2 = V_1;
		Action_1_tAD366A485E434448836F3A4108059A1618C980AF* L_3 = ___0_value;
		Delegate_t* L_4;
		L_4 = Delegate_Remove_m8B7DD5661308FA972E23CA1CC3FC9CEB355504E3(L_2, L_3, NULL);
		V_2 = ((Action_1_tAD366A485E434448836F3A4108059A1618C980AF*)Castclass((RuntimeObject*)L_4, Action_1_tAD366A485E434448836F3A4108059A1618C980AF_il2cpp_TypeInfo_var));
		il2cpp_codegen_runtime_class_init_inline(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var);
		Action_1_tAD366A485E434448836F3A4108059A1618C980AF* L_5 = V_2;
		Action_1_tAD366A485E434448836F3A4108059A1618C980AF* L_6 = V_1;
		Action_1_tAD366A485E434448836F3A4108059A1618C980AF* L_7;
		L_7 = InterlockedCompareExchangeImpl<Action_1_tAD366A485E434448836F3A4108059A1618C980AF*>((&((LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var))->___OnAnyDisabled_10), L_5, L_6);
		V_0 = L_7;
		Action_1_tAD366A485E434448836F3A4108059A1618C980AF* L_8 = V_0;
		Action_1_tAD366A485E434448836F3A4108059A1618C980AF* L_9 = V_1;
		if ((!(((RuntimeObject*)(Action_1_tAD366A485E434448836F3A4108059A1618C980AF*)L_8) == ((RuntimeObject*)(Action_1_tAD366A485E434448836F3A4108059A1618C980AF*)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void Lean.Common.LeanSelectable::add_OnAnySelected(System.Action`2<Lean.Common.LeanSelect,Lean.Common.LeanSelectable>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectable_add_OnAnySelected_m06916055558D3230F3988C01A8097171E7A39248 (Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* ___0_value, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* V_0 = NULL;
	Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* V_1 = NULL;
	Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* V_2 = NULL;
	{
		il2cpp_codegen_runtime_class_init_inline(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var);
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_0 = ((LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var))->___OnAnySelected_11;
		V_0 = L_0;
	}

IL_0006:
	{
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_1 = V_0;
		V_1 = L_1;
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_2 = V_1;
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_3 = ___0_value;
		Delegate_t* L_4;
		L_4 = Delegate_Combine_m1F725AEF318BE6F0426863490691A6F4606E7D00(L_2, L_3, NULL);
		V_2 = ((Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190*)Castclass((RuntimeObject*)L_4, Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190_il2cpp_TypeInfo_var));
		il2cpp_codegen_runtime_class_init_inline(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var);
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_5 = V_2;
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_6 = V_1;
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_7;
		L_7 = InterlockedCompareExchangeImpl<Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190*>((&((LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var))->___OnAnySelected_11), L_5, L_6);
		V_0 = L_7;
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_8 = V_0;
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_9 = V_1;
		if ((!(((RuntimeObject*)(Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190*)L_8) == ((RuntimeObject*)(Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190*)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void Lean.Common.LeanSelectable::remove_OnAnySelected(System.Action`2<Lean.Common.LeanSelect,Lean.Common.LeanSelectable>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectable_remove_OnAnySelected_m9F438F7C15015AEC827AB0C6BCD00A208FAB71E3 (Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* ___0_value, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* V_0 = NULL;
	Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* V_1 = NULL;
	Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* V_2 = NULL;
	{
		il2cpp_codegen_runtime_class_init_inline(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var);
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_0 = ((LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var))->___OnAnySelected_11;
		V_0 = L_0;
	}

IL_0006:
	{
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_1 = V_0;
		V_1 = L_1;
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_2 = V_1;
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_3 = ___0_value;
		Delegate_t* L_4;
		L_4 = Delegate_Remove_m8B7DD5661308FA972E23CA1CC3FC9CEB355504E3(L_2, L_3, NULL);
		V_2 = ((Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190*)Castclass((RuntimeObject*)L_4, Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190_il2cpp_TypeInfo_var));
		il2cpp_codegen_runtime_class_init_inline(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var);
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_5 = V_2;
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_6 = V_1;
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_7;
		L_7 = InterlockedCompareExchangeImpl<Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190*>((&((LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var))->___OnAnySelected_11), L_5, L_6);
		V_0 = L_7;
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_8 = V_0;
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_9 = V_1;
		if ((!(((RuntimeObject*)(Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190*)L_8) == ((RuntimeObject*)(Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190*)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void Lean.Common.LeanSelectable::add_OnAnyDeselected(System.Action`2<Lean.Common.LeanSelect,Lean.Common.LeanSelectable>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectable_add_OnAnyDeselected_mFA9DE7F64AB71F46D2AAA77E58486D57EDB507B2 (Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* ___0_value, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* V_0 = NULL;
	Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* V_1 = NULL;
	Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* V_2 = NULL;
	{
		il2cpp_codegen_runtime_class_init_inline(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var);
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_0 = ((LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var))->___OnAnyDeselected_12;
		V_0 = L_0;
	}

IL_0006:
	{
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_1 = V_0;
		V_1 = L_1;
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_2 = V_1;
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_3 = ___0_value;
		Delegate_t* L_4;
		L_4 = Delegate_Combine_m1F725AEF318BE6F0426863490691A6F4606E7D00(L_2, L_3, NULL);
		V_2 = ((Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190*)Castclass((RuntimeObject*)L_4, Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190_il2cpp_TypeInfo_var));
		il2cpp_codegen_runtime_class_init_inline(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var);
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_5 = V_2;
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_6 = V_1;
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_7;
		L_7 = InterlockedCompareExchangeImpl<Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190*>((&((LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var))->___OnAnyDeselected_12), L_5, L_6);
		V_0 = L_7;
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_8 = V_0;
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_9 = V_1;
		if ((!(((RuntimeObject*)(Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190*)L_8) == ((RuntimeObject*)(Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190*)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void Lean.Common.LeanSelectable::remove_OnAnyDeselected(System.Action`2<Lean.Common.LeanSelect,Lean.Common.LeanSelectable>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectable_remove_OnAnyDeselected_m5B2A4C22F2850FDCED2C7A808D26CBE3BDD8C35D (Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* ___0_value, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* V_0 = NULL;
	Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* V_1 = NULL;
	Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* V_2 = NULL;
	{
		il2cpp_codegen_runtime_class_init_inline(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var);
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_0 = ((LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var))->___OnAnyDeselected_12;
		V_0 = L_0;
	}

IL_0006:
	{
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_1 = V_0;
		V_1 = L_1;
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_2 = V_1;
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_3 = ___0_value;
		Delegate_t* L_4;
		L_4 = Delegate_Remove_m8B7DD5661308FA972E23CA1CC3FC9CEB355504E3(L_2, L_3, NULL);
		V_2 = ((Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190*)Castclass((RuntimeObject*)L_4, Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190_il2cpp_TypeInfo_var));
		il2cpp_codegen_runtime_class_init_inline(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var);
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_5 = V_2;
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_6 = V_1;
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_7;
		L_7 = InterlockedCompareExchangeImpl<Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190*>((&((LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var))->___OnAnyDeselected_12), L_5, L_6);
		V_0 = L_7;
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_8 = V_0;
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_9 = V_1;
		if ((!(((RuntimeObject*)(Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190*)L_8) == ((RuntimeObject*)(Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190*)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Int32 Lean.Common.LeanSelectable::get_SelectedCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t LeanSelectable_get_SelectedCount_m68F30EB52466B6A0AB9F60449D8E9713457B3940 (LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m15AE326ED9AE5BBF6AD71F57A8B15B6D10B75F1A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mAC2A0346E0E1D3D9AC42A6CDFB77A42E2EF081CF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m5BCF5CFF717F6739116402C6E75944686E15AD0D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LinkedList_1_GetEnumerator_mB93F43BCCD2C6A1C3A285DFD6BBCCA71529E58D1_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Enumerator_t3F4E85BE6F8338474FC5FB1BD081F64E1ECC5A1E V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// var count = 0;
		V_0 = 0;
		// if (selfSelected == true)
		bool L_0 = __this->___selfSelected_6;
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		// count += 1;
		int32_t L_1 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add(L_1, 1));
	}

IL_000e:
	{
		// foreach (var select in LeanSelect.Instances)
		il2cpp_codegen_runtime_class_init_inline(LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var);
		LinkedList_1_t74DD3E2689E35FC0DF8DF1971B5CD529EEE8CAA1* L_2 = ((LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var))->___Instances_4;
		NullCheck(L_2);
		Enumerator_t3F4E85BE6F8338474FC5FB1BD081F64E1ECC5A1E L_3;
		L_3 = LinkedList_1_GetEnumerator_mB93F43BCCD2C6A1C3A285DFD6BBCCA71529E58D1(L_2, LinkedList_1_GetEnumerator_mB93F43BCCD2C6A1C3A285DFD6BBCCA71529E58D1_RuntimeMethod_var);
		V_1 = L_3;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0039:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m15AE326ED9AE5BBF6AD71F57A8B15B6D10B75F1A((&V_1), Enumerator_Dispose_m15AE326ED9AE5BBF6AD71F57A8B15B6D10B75F1A_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_002e_1;
			}

IL_001b_1:
			{
				// foreach (var select in LeanSelect.Instances)
				LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* L_4;
				L_4 = Enumerator_get_Current_m5BCF5CFF717F6739116402C6E75944686E15AD0D_inline((&V_1), Enumerator_get_Current_m5BCF5CFF717F6739116402C6E75944686E15AD0D_RuntimeMethod_var);
				// if (select.IsSelected(this) == true)
				NullCheck(L_4);
				bool L_5;
				L_5 = LeanSelect_IsSelected_m882CF8026A3B01FE1F1A5374536481252A04FC16(L_4, __this, NULL);
				if (!L_5)
				{
					goto IL_002e_1;
				}
			}
			{
				// count += 1;
				int32_t L_6 = V_0;
				V_0 = ((int32_t)il2cpp_codegen_add(L_6, 1));
			}

IL_002e_1:
			{
				// foreach (var select in LeanSelect.Instances)
				bool L_7;
				L_7 = Enumerator_MoveNext_mAC2A0346E0E1D3D9AC42A6CDFB77A42E2EF081CF((&V_1), Enumerator_MoveNext_mAC2A0346E0E1D3D9AC42A6CDFB77A42E2EF081CF_RuntimeMethod_var);
				if (L_7)
				{
					goto IL_001b_1;
				}
			}
			{
				goto IL_0047;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_0047:
	{
		// return count;
		int32_t L_8 = V_0;
		return L_8;
	}
}
// System.Boolean Lean.Common.LeanSelectable::get_IsSelected()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool LeanSelectable_get_IsSelected_m55508746D046549000EB2085D5EA859B5ED855CB (LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m15AE326ED9AE5BBF6AD71F57A8B15B6D10B75F1A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mAC2A0346E0E1D3D9AC42A6CDFB77A42E2EF081CF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m5BCF5CFF717F6739116402C6E75944686E15AD0D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LinkedList_1_GetEnumerator_mB93F43BCCD2C6A1C3A285DFD6BBCCA71529E58D1_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Enumerator_t3F4E85BE6F8338474FC5FB1BD081F64E1ECC5A1E V_0;
	memset((&V_0), 0, sizeof(V_0));
	bool V_1 = false;
	{
		// if (selfSelected == true)
		bool L_0 = __this->___selfSelected_6;
		if (!L_0)
		{
			goto IL_000a;
		}
	}
	{
		// return true;
		return (bool)1;
	}

IL_000a:
	{
		// foreach (var select in LeanSelect.Instances)
		il2cpp_codegen_runtime_class_init_inline(LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var);
		LinkedList_1_t74DD3E2689E35FC0DF8DF1971B5CD529EEE8CAA1* L_1 = ((LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var))->___Instances_4;
		NullCheck(L_1);
		Enumerator_t3F4E85BE6F8338474FC5FB1BD081F64E1ECC5A1E L_2;
		L_2 = LinkedList_1_GetEnumerator_mB93F43BCCD2C6A1C3A285DFD6BBCCA71529E58D1(L_1, LinkedList_1_GetEnumerator_mB93F43BCCD2C6A1C3A285DFD6BBCCA71529E58D1_RuntimeMethod_var);
		V_0 = L_2;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0035:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m15AE326ED9AE5BBF6AD71F57A8B15B6D10B75F1A((&V_0), Enumerator_Dispose_m15AE326ED9AE5BBF6AD71F57A8B15B6D10B75F1A_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_002a_1;
			}

IL_0017_1:
			{
				// foreach (var select in LeanSelect.Instances)
				LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* L_3;
				L_3 = Enumerator_get_Current_m5BCF5CFF717F6739116402C6E75944686E15AD0D_inline((&V_0), Enumerator_get_Current_m5BCF5CFF717F6739116402C6E75944686E15AD0D_RuntimeMethod_var);
				// if (select.IsSelected(this) == true)
				NullCheck(L_3);
				bool L_4;
				L_4 = LeanSelect_IsSelected_m882CF8026A3B01FE1F1A5374536481252A04FC16(L_3, __this, NULL);
				if (!L_4)
				{
					goto IL_002a_1;
				}
			}
			{
				// return true;
				V_1 = (bool)1;
				goto IL_0045;
			}

IL_002a_1:
			{
				// foreach (var select in LeanSelect.Instances)
				bool L_5;
				L_5 = Enumerator_MoveNext_mAC2A0346E0E1D3D9AC42A6CDFB77A42E2EF081CF((&V_0), Enumerator_MoveNext_mAC2A0346E0E1D3D9AC42A6CDFB77A42E2EF081CF_RuntimeMethod_var);
				if (L_5)
				{
					goto IL_0017_1;
				}
			}
			{
				goto IL_0043;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_0043:
	{
		// return false;
		return (bool)0;
	}

IL_0045:
	{
		// }
		bool L_6 = V_1;
		return L_6;
	}
}
// System.Int32 Lean.Common.LeanSelectable::get_IsSelectedCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t LeanSelectable_get_IsSelectedCount_m91D6757A7652F1D8AA7CCC847DD6021915783E77 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m3CC6549F7A60846A9D156C06EDCF724D21D0763C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_m89C7C4FA562ABDDA990D8F12AFAB5714BC74927C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m357A4F354FD535F202C1DAECEB1427B848D781D7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LinkedList_1_GetEnumerator_mDB5737AFFB871DAB61A205446EC59B28F4A810D3_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Enumerator_t2DDE01BDFB37E13974A8807CCD46D71DEA3E8F33 V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// var count = 0;
		V_0 = 0;
		// foreach (var selectable in Instances)
		il2cpp_codegen_runtime_class_init_inline(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var);
		LinkedList_1_t25474EE5265E341D73136D6D26BA9381A1E5361C* L_0 = ((LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var))->___Instances_4;
		NullCheck(L_0);
		Enumerator_t2DDE01BDFB37E13974A8807CCD46D71DEA3E8F33 L_1;
		L_1 = LinkedList_1_GetEnumerator_mDB5737AFFB871DAB61A205446EC59B28F4A810D3(L_0, LinkedList_1_GetEnumerator_mDB5737AFFB871DAB61A205446EC59B28F4A810D3_RuntimeMethod_var);
		V_1 = L_1;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_002c:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m3CC6549F7A60846A9D156C06EDCF724D21D0763C((&V_1), Enumerator_Dispose_m3CC6549F7A60846A9D156C06EDCF724D21D0763C_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_0021_1;
			}

IL_000f_1:
			{
				// foreach (var selectable in Instances)
				LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* L_2;
				L_2 = Enumerator_get_Current_m357A4F354FD535F202C1DAECEB1427B848D781D7_inline((&V_1), Enumerator_get_Current_m357A4F354FD535F202C1DAECEB1427B848D781D7_RuntimeMethod_var);
				// if (selectable.IsSelected == true)
				NullCheck(L_2);
				bool L_3;
				L_3 = LeanSelectable_get_IsSelected_m55508746D046549000EB2085D5EA859B5ED855CB(L_2, NULL);
				if (!L_3)
				{
					goto IL_0021_1;
				}
			}
			{
				// count += 1;
				int32_t L_4 = V_0;
				V_0 = ((int32_t)il2cpp_codegen_add(L_4, 1));
			}

IL_0021_1:
			{
				// foreach (var selectable in Instances)
				bool L_5;
				L_5 = Enumerator_MoveNext_m89C7C4FA562ABDDA990D8F12AFAB5714BC74927C((&V_1), Enumerator_MoveNext_m89C7C4FA562ABDDA990D8F12AFAB5714BC74927C_RuntimeMethod_var);
				if (L_5)
				{
					goto IL_000f_1;
				}
			}
			{
				goto IL_003a;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_003a:
	{
		// return count;
		int32_t L_6 = V_0;
		return L_6;
	}
}
// System.Void Lean.Common.LeanSelectable::Deselect()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectable_Deselect_mCB84D48D0F71C8345F70321595197109042D2A27 (LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m15AE326ED9AE5BBF6AD71F57A8B15B6D10B75F1A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mAC2A0346E0E1D3D9AC42A6CDFB77A42E2EF081CF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m5BCF5CFF717F6739116402C6E75944686E15AD0D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LinkedList_1_GetEnumerator_mB93F43BCCD2C6A1C3A285DFD6BBCCA71529E58D1_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Enumerator_t3F4E85BE6F8338474FC5FB1BD081F64E1ECC5A1E V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// SelfSelected = false;
		LeanSelectable_set_SelfSelected_m3B119374E382EB78103F159BFC08C1DDB8220186(__this, (bool)0, NULL);
		// foreach (var select in LeanSelect.Instances)
		il2cpp_codegen_runtime_class_init_inline(LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var);
		LinkedList_1_t74DD3E2689E35FC0DF8DF1971B5CD529EEE8CAA1* L_0 = ((LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var))->___Instances_4;
		NullCheck(L_0);
		Enumerator_t3F4E85BE6F8338474FC5FB1BD081F64E1ECC5A1E L_1;
		L_1 = LinkedList_1_GetEnumerator_mB93F43BCCD2C6A1C3A285DFD6BBCCA71529E58D1(L_0, LinkedList_1_GetEnumerator_mB93F43BCCD2C6A1C3A285DFD6BBCCA71529E58D1_RuntimeMethod_var);
		V_0 = L_1;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_002c:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m15AE326ED9AE5BBF6AD71F57A8B15B6D10B75F1A((&V_0), Enumerator_Dispose_m15AE326ED9AE5BBF6AD71F57A8B15B6D10B75F1A_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_0021_1;
			}

IL_0014_1:
			{
				// foreach (var select in LeanSelect.Instances)
				LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* L_2;
				L_2 = Enumerator_get_Current_m5BCF5CFF717F6739116402C6E75944686E15AD0D_inline((&V_0), Enumerator_get_Current_m5BCF5CFF717F6739116402C6E75944686E15AD0D_RuntimeMethod_var);
				// select.Deselect(this);
				NullCheck(L_2);
				LeanSelect_Deselect_mD29FDB8833FDBCE9CD91833CBE489E52D001C628(L_2, __this, NULL);
			}

IL_0021_1:
			{
				// foreach (var select in LeanSelect.Instances)
				bool L_3;
				L_3 = Enumerator_MoveNext_mAC2A0346E0E1D3D9AC42A6CDFB77A42E2EF081CF((&V_0), Enumerator_MoveNext_mAC2A0346E0E1D3D9AC42A6CDFB77A42E2EF081CF_RuntimeMethod_var);
				if (L_3)
				{
					goto IL_0014_1;
				}
			}
			{
				goto IL_003a;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_003a:
	{
		// }
		return;
	}
}
// System.Void Lean.Common.LeanSelectable::DeselectAll()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectable_DeselectAll_m4D43396ED0D131513830F2A49E7742F009F4BE08 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m15AE326ED9AE5BBF6AD71F57A8B15B6D10B75F1A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m3CC6549F7A60846A9D156C06EDCF724D21D0763C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_m89C7C4FA562ABDDA990D8F12AFAB5714BC74927C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mAC2A0346E0E1D3D9AC42A6CDFB77A42E2EF081CF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m357A4F354FD535F202C1DAECEB1427B848D781D7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m5BCF5CFF717F6739116402C6E75944686E15AD0D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LinkedList_1_GetEnumerator_mB93F43BCCD2C6A1C3A285DFD6BBCCA71529E58D1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LinkedList_1_GetEnumerator_mDB5737AFFB871DAB61A205446EC59B28F4A810D3_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Enumerator_t3F4E85BE6F8338474FC5FB1BD081F64E1ECC5A1E V_0;
	memset((&V_0), 0, sizeof(V_0));
	Enumerator_t2DDE01BDFB37E13974A8807CCD46D71DEA3E8F33 V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// foreach (var select in LeanSelect.Instances)
		il2cpp_codegen_runtime_class_init_inline(LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var);
		LinkedList_1_t74DD3E2689E35FC0DF8DF1971B5CD529EEE8CAA1* L_0 = ((LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F_il2cpp_TypeInfo_var))->___Instances_4;
		NullCheck(L_0);
		Enumerator_t3F4E85BE6F8338474FC5FB1BD081F64E1ECC5A1E L_1;
		L_1 = LinkedList_1_GetEnumerator_mB93F43BCCD2C6A1C3A285DFD6BBCCA71529E58D1(L_0, LinkedList_1_GetEnumerator_mB93F43BCCD2C6A1C3A285DFD6BBCCA71529E58D1_RuntimeMethod_var);
		V_0 = L_1;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0024:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m15AE326ED9AE5BBF6AD71F57A8B15B6D10B75F1A((&V_0), Enumerator_Dispose_m15AE326ED9AE5BBF6AD71F57A8B15B6D10B75F1A_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_0019_1;
			}

IL_000d_1:
			{
				// foreach (var select in LeanSelect.Instances)
				LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* L_2;
				L_2 = Enumerator_get_Current_m5BCF5CFF717F6739116402C6E75944686E15AD0D_inline((&V_0), Enumerator_get_Current_m5BCF5CFF717F6739116402C6E75944686E15AD0D_RuntimeMethod_var);
				// select.DeselectAll();
				NullCheck(L_2);
				LeanSelect_DeselectAll_mFE5E913DADC65729C87BE27A69D04658F3785E18(L_2, NULL);
			}

IL_0019_1:
			{
				// foreach (var select in LeanSelect.Instances)
				bool L_3;
				L_3 = Enumerator_MoveNext_mAC2A0346E0E1D3D9AC42A6CDFB77A42E2EF081CF((&V_0), Enumerator_MoveNext_mAC2A0346E0E1D3D9AC42A6CDFB77A42E2EF081CF_RuntimeMethod_var);
				if (L_3)
				{
					goto IL_000d_1;
				}
			}
			{
				goto IL_0032;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_0032:
	{
		// foreach (var selectable in LeanSelectable.Instances)
		il2cpp_codegen_runtime_class_init_inline(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var);
		LinkedList_1_t25474EE5265E341D73136D6D26BA9381A1E5361C* L_4 = ((LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var))->___Instances_4;
		NullCheck(L_4);
		Enumerator_t2DDE01BDFB37E13974A8807CCD46D71DEA3E8F33 L_5;
		L_5 = LinkedList_1_GetEnumerator_mDB5737AFFB871DAB61A205446EC59B28F4A810D3(L_4, LinkedList_1_GetEnumerator_mDB5737AFFB871DAB61A205446EC59B28F4A810D3_RuntimeMethod_var);
		V_1 = L_5;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0057:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m3CC6549F7A60846A9D156C06EDCF724D21D0763C((&V_1), Enumerator_Dispose_m3CC6549F7A60846A9D156C06EDCF724D21D0763C_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_004c_1;
			}

IL_003f_1:
			{
				// foreach (var selectable in LeanSelectable.Instances)
				LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* L_6;
				L_6 = Enumerator_get_Current_m357A4F354FD535F202C1DAECEB1427B848D781D7_inline((&V_1), Enumerator_get_Current_m357A4F354FD535F202C1DAECEB1427B848D781D7_RuntimeMethod_var);
				// selectable.SelfSelected = false;
				NullCheck(L_6);
				LeanSelectable_set_SelfSelected_m3B119374E382EB78103F159BFC08C1DDB8220186(L_6, (bool)0, NULL);
			}

IL_004c_1:
			{
				// foreach (var selectable in LeanSelectable.Instances)
				bool L_7;
				L_7 = Enumerator_MoveNext_m89C7C4FA562ABDDA990D8F12AFAB5714BC74927C((&V_1), Enumerator_MoveNext_m89C7C4FA562ABDDA990D8F12AFAB5714BC74927C_RuntimeMethod_var);
				if (L_7)
				{
					goto IL_003f_1;
				}
			}
			{
				goto IL_0065;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_0065:
	{
		// }
		return;
	}
}
// System.Void Lean.Common.LeanSelectable::InvokeOnSelected(Lean.Common.LeanSelect)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectable_InvokeOnSelected_m3C81335F17D41C2F14CEBF38F31B275E59D6FAE9 (LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* __this, LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* ___0_select, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1_Invoke_m14D167DBD029D5C80FE4F7F6DFEDDA46937311C4_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (onSelected != null)
		LeanSelectEvent_tE1B7F7125473C6F40463EC6C88B8842A0404C34F* L_0 = __this->___onSelected_7;
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		// onSelected.Invoke(select);
		LeanSelectEvent_tE1B7F7125473C6F40463EC6C88B8842A0404C34F* L_1 = __this->___onSelected_7;
		LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* L_2 = ___0_select;
		NullCheck(L_1);
		UnityEvent_1_Invoke_m14D167DBD029D5C80FE4F7F6DFEDDA46937311C4(L_1, L_2, UnityEvent_1_Invoke_m14D167DBD029D5C80FE4F7F6DFEDDA46937311C4_RuntimeMethod_var);
	}

IL_0014:
	{
		// if (OnAnySelected != null)
		il2cpp_codegen_runtime_class_init_inline(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var);
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_3 = ((LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var))->___OnAnySelected_11;
		if (!L_3)
		{
			goto IL_0027;
		}
	}
	{
		// OnAnySelected.Invoke(select, this);
		il2cpp_codegen_runtime_class_init_inline(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var);
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_4 = ((LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var))->___OnAnySelected_11;
		LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* L_5 = ___0_select;
		NullCheck(L_4);
		Action_2_Invoke_m92A2047F7B6E70B622B357B63B29E4FB7BD96336_inline(L_4, L_5, __this, NULL);
	}

IL_0027:
	{
		// }
		return;
	}
}
// System.Void Lean.Common.LeanSelectable::InvokeOnDeslected(Lean.Common.LeanSelect)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectable_InvokeOnDeslected_m6B165447EB743395D368821C3E598822DFDEA7DF (LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* __this, LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* ___0_select, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1_Invoke_m14D167DBD029D5C80FE4F7F6DFEDDA46937311C4_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (onDeselected != null)
		LeanSelectEvent_tE1B7F7125473C6F40463EC6C88B8842A0404C34F* L_0 = __this->___onDeselected_8;
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		// onDeselected.Invoke(select);
		LeanSelectEvent_tE1B7F7125473C6F40463EC6C88B8842A0404C34F* L_1 = __this->___onDeselected_8;
		LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* L_2 = ___0_select;
		NullCheck(L_1);
		UnityEvent_1_Invoke_m14D167DBD029D5C80FE4F7F6DFEDDA46937311C4(L_1, L_2, UnityEvent_1_Invoke_m14D167DBD029D5C80FE4F7F6DFEDDA46937311C4_RuntimeMethod_var);
	}

IL_0014:
	{
		// if (OnAnyDeselected != null)
		il2cpp_codegen_runtime_class_init_inline(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var);
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_3 = ((LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var))->___OnAnyDeselected_12;
		if (!L_3)
		{
			goto IL_0027;
		}
	}
	{
		// OnAnyDeselected.Invoke(select, this);
		il2cpp_codegen_runtime_class_init_inline(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var);
		Action_2_tCF448FD296B1704AB37DB53CBF48E98B5C0DF190* L_4 = ((LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var))->___OnAnyDeselected_12;
		LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* L_5 = ___0_select;
		NullCheck(L_4);
		Action_2_Invoke_m92A2047F7B6E70B622B357B63B29E4FB7BD96336_inline(L_4, L_5, __this, NULL);
	}

IL_0027:
	{
		// }
		return;
	}
}
// System.Void Lean.Common.LeanSelectable::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectable_OnEnable_m2BECBBA774A3DA623EDB2C6482D65882947FA6FD (LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LinkedList_1_AddLast_mDC5C4929E156AC04B13A0D54368063664D3A861A_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// instancesNode = Instances.AddLast(this);
		il2cpp_codegen_runtime_class_init_inline(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var);
		LinkedList_1_t25474EE5265E341D73136D6D26BA9381A1E5361C* L_0 = ((LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var))->___Instances_4;
		NullCheck(L_0);
		LinkedListNode_1_tC4B0FFBEE56621B13FC8502EB49ADCA6A7E69B72* L_1;
		L_1 = LinkedList_1_AddLast_mDC5C4929E156AC04B13A0D54368063664D3A861A(L_0, __this, LinkedList_1_AddLast_mDC5C4929E156AC04B13A0D54368063664D3A861A_RuntimeMethod_var);
		__this->___instancesNode_5 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___instancesNode_5), (void*)L_1);
		// if (OnAnyEnabled != null)
		Action_1_tAD366A485E434448836F3A4108059A1618C980AF* L_2 = ((LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var))->___OnAnyEnabled_9;
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		// OnAnyEnabled.Invoke(this);
		il2cpp_codegen_runtime_class_init_inline(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var);
		Action_1_tAD366A485E434448836F3A4108059A1618C980AF* L_3 = ((LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var))->___OnAnyEnabled_9;
		NullCheck(L_3);
		Action_1_Invoke_m155488EBEB85B08AEF82D89E9EB046BFB10AA612_inline(L_3, __this, NULL);
	}

IL_0023:
	{
		// }
		return;
	}
}
// System.Void Lean.Common.LeanSelectable::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectable_OnDisable_m59F3853E5EFDA75B2E35DA0E37F18E2C4E736680 (LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LinkedList_1_Remove_mC7399940C9CFA33D74D4C0DAC876404129DF8562_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Instances.Remove(instancesNode); instancesNode = null;
		il2cpp_codegen_runtime_class_init_inline(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var);
		LinkedList_1_t25474EE5265E341D73136D6D26BA9381A1E5361C* L_0 = ((LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var))->___Instances_4;
		LinkedListNode_1_tC4B0FFBEE56621B13FC8502EB49ADCA6A7E69B72* L_1 = __this->___instancesNode_5;
		NullCheck(L_0);
		LinkedList_1_Remove_mC7399940C9CFA33D74D4C0DAC876404129DF8562(L_0, L_1, LinkedList_1_Remove_mC7399940C9CFA33D74D4C0DAC876404129DF8562_RuntimeMethod_var);
		// Instances.Remove(instancesNode); instancesNode = null;
		__this->___instancesNode_5 = (LinkedListNode_1_tC4B0FFBEE56621B13FC8502EB49ADCA6A7E69B72*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___instancesNode_5), (void*)(LinkedListNode_1_tC4B0FFBEE56621B13FC8502EB49ADCA6A7E69B72*)NULL);
		// if (OnAnyDisabled != null)
		Action_1_tAD366A485E434448836F3A4108059A1618C980AF* L_2 = ((LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var))->___OnAnyDisabled_10;
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		// OnAnyDisabled.Invoke(this);
		il2cpp_codegen_runtime_class_init_inline(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var);
		Action_1_tAD366A485E434448836F3A4108059A1618C980AF* L_3 = ((LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var))->___OnAnyDisabled_10;
		NullCheck(L_3);
		Action_1_Invoke_m155488EBEB85B08AEF82D89E9EB046BFB10AA612_inline(L_3, __this, NULL);
	}

IL_0029:
	{
		// }
		return;
	}
}
// System.Void Lean.Common.LeanSelectable::OnDestroy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectable_OnDestroy_mFE969691889D2B97EC1CE32A980EEFAA9E244CCD (LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* __this, const RuntimeMethod* method) 
{
	{
		// Deselect();
		LeanSelectable_Deselect_mCB84D48D0F71C8345F70321595197109042D2A27(__this, NULL);
		// }
		return;
	}
}
// System.Void Lean.Common.LeanSelectable::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectable__ctor_m64F3EFA449DC2206C65BFD65AFB8A5C0B2B0E738 (LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
// System.Void Lean.Common.LeanSelectable::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectable__cctor_m0DB2F5F487EE2D3579644575756DA358869D5286 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LinkedList_1__ctor_m7E3FF3E1520AA5DE2DBE33B6A1E7657EC4FE65BF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LinkedList_1_t25474EE5265E341D73136D6D26BA9381A1E5361C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m4444F647B6C0C03F0F6CD67236CF2D7F76202E7A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static LinkedList<LeanSelectable> Instances = new LinkedList<LeanSelectable>(); [System.NonSerialized] private LinkedListNode<LeanSelectable> instancesNode;
		LinkedList_1_t25474EE5265E341D73136D6D26BA9381A1E5361C* L_0 = (LinkedList_1_t25474EE5265E341D73136D6D26BA9381A1E5361C*)il2cpp_codegen_object_new(LinkedList_1_t25474EE5265E341D73136D6D26BA9381A1E5361C_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		LinkedList_1__ctor_m7E3FF3E1520AA5DE2DBE33B6A1E7657EC4FE65BF(L_0, LinkedList_1__ctor_m7E3FF3E1520AA5DE2DBE33B6A1E7657EC4FE65BF_RuntimeMethod_var);
		((LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var))->___Instances_4 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&((LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var))->___Instances_4), (void*)L_0);
		// protected static List<LeanSelectable> tempSelectables = new List<LeanSelectable>();
		List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3* L_1 = (List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3*)il2cpp_codegen_object_new(List_1_t22CAA6D6CB9D4F69A48D40A9D84DF710CC48CBB3_il2cpp_TypeInfo_var);
		NullCheck(L_1);
		List_1__ctor_m4444F647B6C0C03F0F6CD67236CF2D7F76202E7A(L_1, List_1__ctor_m4444F647B6C0C03F0F6CD67236CF2D7F76202E7A_RuntimeMethod_var);
		((LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var))->___tempSelectables_13 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&((LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_StaticFields*)il2cpp_codegen_static_fields_for(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_il2cpp_TypeInfo_var))->___tempSelectables_13), (void*)L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Common.LeanSelectable/LeanSelectEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectEvent__ctor_m298B432186C5138B38B93C71D379689F44C0B204 (LeanSelectEvent_tE1B7F7125473C6F40463EC6C88B8842A0404C34F* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1__ctor_mA971530EC4BACA70B45C39F76AD5A0BE118CF63C_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_mA971530EC4BACA70B45C39F76AD5A0BE118CF63C(__this, UnityEvent_1__ctor_mA971530EC4BACA70B45C39F76AD5A0BE118CF63C_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Lean.Common.LeanSelectable Lean.Common.LeanSelectableBehaviour::get_Selectable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* LeanSelectableBehaviour_get_Selectable_mDA4E928E7FE887A93CF29BBC0EDD11483D52B97E (LeanSelectableBehaviour_t07650DA7B5AA5AEACF3FA60433E5C36E1A679CD9* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (selectable == null)
		LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* L_0 = __this->___selectable_4;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mB6120F782D83091EF56A198FCEBCF066DB4A9605(L_0, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		// Register();
		LeanSelectableBehaviour_Register_m59BCA4108A01A335DA517DC30A64C29ADB64DB80(__this, NULL);
	}

IL_0014:
	{
		// return selectable;
		LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* L_2 = __this->___selectable_4;
		return L_2;
	}
}
// System.Void Lean.Common.LeanSelectableBehaviour::Register()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectableBehaviour_Register_m59BCA4108A01A335DA517DC30A64C29ADB64DB80 (LeanSelectableBehaviour_t07650DA7B5AA5AEACF3FA60433E5C36E1A679CD9* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponentInParent_TisLeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_mCB62694EE43FA4026B1B4D8D8C9B372BDEE1DD51_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Register(GetComponentInParent<LeanSelectable>());
		LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* L_0;
		L_0 = Component_GetComponentInParent_TisLeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_mCB62694EE43FA4026B1B4D8D8C9B372BDEE1DD51(__this, Component_GetComponentInParent_TisLeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E_mCB62694EE43FA4026B1B4D8D8C9B372BDEE1DD51_RuntimeMethod_var);
		LeanSelectableBehaviour_Register_mDADF1B6384CBA28AC26D2161AA5831FC1D7B9747(__this, L_0, NULL);
		// }
		return;
	}
}
// System.Void Lean.Common.LeanSelectableBehaviour::Register(Lean.Common.LeanSelectable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectableBehaviour_Register_mDADF1B6384CBA28AC26D2161AA5831FC1D7B9747 (LeanSelectableBehaviour_t07650DA7B5AA5AEACF3FA60433E5C36E1A679CD9* __this, LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* ___0_newSelectable, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1_tA601885BAAC49BD9D535DC344F67685863BD4573_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1_AddListener_m41D8372E4AB1C156FF3CA895F0B66CBD484E6C57_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (newSelectable != selectable)
		LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* L_0 = ___0_newSelectable;
		LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* L_1 = __this->___selectable_4;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Inequality_mD0BE578448EAA61948F25C32F8DD55AB1F778602(L_0, L_1, NULL);
		if (!L_2)
		{
			goto IL_005e;
		}
	}
	{
		// Unregister();
		LeanSelectableBehaviour_Unregister_m21AA701DA1065421693A87AA7CD10DB32AD9318C(__this, NULL);
		// if (newSelectable != null)
		LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* L_3 = ___0_newSelectable;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_4;
		L_4 = Object_op_Inequality_mD0BE578448EAA61948F25C32F8DD55AB1F778602(L_3, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_4)
		{
			goto IL_005e;
		}
	}
	{
		// selectable = newSelectable;
		LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* L_5 = ___0_newSelectable;
		__this->___selectable_4 = L_5;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___selectable_4), (void*)L_5);
		// selectable.OnSelected.AddListener(OnSelected);
		LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* L_6 = __this->___selectable_4;
		NullCheck(L_6);
		LeanSelectEvent_tE1B7F7125473C6F40463EC6C88B8842A0404C34F* L_7;
		L_7 = LeanSelectable_get_OnSelected_mEE84613146ACD47032B93CB4E8988F526A15988B(L_6, NULL);
		UnityAction_1_tA601885BAAC49BD9D535DC344F67685863BD4573* L_8 = (UnityAction_1_tA601885BAAC49BD9D535DC344F67685863BD4573*)il2cpp_codegen_object_new(UnityAction_1_tA601885BAAC49BD9D535DC344F67685863BD4573_il2cpp_TypeInfo_var);
		NullCheck(L_8);
		UnityAction_1__ctor_mA72F946150482FECDC3C04BED2CBF062038EE470(L_8, __this, (intptr_t)((void*)GetVirtualMethodInfo(__this, 7)), NULL);
		NullCheck(L_7);
		UnityEvent_1_AddListener_m41D8372E4AB1C156FF3CA895F0B66CBD484E6C57(L_7, L_8, UnityEvent_1_AddListener_m41D8372E4AB1C156FF3CA895F0B66CBD484E6C57_RuntimeMethod_var);
		// selectable.OnDeselected.AddListener(OnDeselected);
		LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* L_9 = __this->___selectable_4;
		NullCheck(L_9);
		LeanSelectEvent_tE1B7F7125473C6F40463EC6C88B8842A0404C34F* L_10;
		L_10 = LeanSelectable_get_OnDeselected_m055BD9B88E71CAB5E6C4D429CA271308F56EBC29(L_9, NULL);
		UnityAction_1_tA601885BAAC49BD9D535DC344F67685863BD4573* L_11 = (UnityAction_1_tA601885BAAC49BD9D535DC344F67685863BD4573*)il2cpp_codegen_object_new(UnityAction_1_tA601885BAAC49BD9D535DC344F67685863BD4573_il2cpp_TypeInfo_var);
		NullCheck(L_11);
		UnityAction_1__ctor_mA72F946150482FECDC3C04BED2CBF062038EE470(L_11, __this, (intptr_t)((void*)GetVirtualMethodInfo(__this, 8)), NULL);
		NullCheck(L_10);
		UnityEvent_1_AddListener_m41D8372E4AB1C156FF3CA895F0B66CBD484E6C57(L_10, L_11, UnityEvent_1_AddListener_m41D8372E4AB1C156FF3CA895F0B66CBD484E6C57_RuntimeMethod_var);
	}

IL_005e:
	{
		// }
		return;
	}
}
// System.Void Lean.Common.LeanSelectableBehaviour::Unregister()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectableBehaviour_Unregister_m21AA701DA1065421693A87AA7CD10DB32AD9318C (LeanSelectableBehaviour_t07650DA7B5AA5AEACF3FA60433E5C36E1A679CD9* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1_tA601885BAAC49BD9D535DC344F67685863BD4573_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1_RemoveListener_m93A6005ABE25250DBD0E2497D329D839BC2C92D8_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (selectable != null)
		LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* L_0 = __this->___selectable_4;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_mD0BE578448EAA61948F25C32F8DD55AB1F778602(L_0, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_1)
		{
			goto IL_004f;
		}
	}
	{
		// selectable.OnSelected.RemoveListener(OnSelected);
		LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* L_2 = __this->___selectable_4;
		NullCheck(L_2);
		LeanSelectEvent_tE1B7F7125473C6F40463EC6C88B8842A0404C34F* L_3;
		L_3 = LeanSelectable_get_OnSelected_mEE84613146ACD47032B93CB4E8988F526A15988B(L_2, NULL);
		UnityAction_1_tA601885BAAC49BD9D535DC344F67685863BD4573* L_4 = (UnityAction_1_tA601885BAAC49BD9D535DC344F67685863BD4573*)il2cpp_codegen_object_new(UnityAction_1_tA601885BAAC49BD9D535DC344F67685863BD4573_il2cpp_TypeInfo_var);
		NullCheck(L_4);
		UnityAction_1__ctor_mA72F946150482FECDC3C04BED2CBF062038EE470(L_4, __this, (intptr_t)((void*)GetVirtualMethodInfo(__this, 7)), NULL);
		NullCheck(L_3);
		UnityEvent_1_RemoveListener_m93A6005ABE25250DBD0E2497D329D839BC2C92D8(L_3, L_4, UnityEvent_1_RemoveListener_m93A6005ABE25250DBD0E2497D329D839BC2C92D8_RuntimeMethod_var);
		// selectable.OnDeselected.RemoveListener(OnDeselected);
		LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* L_5 = __this->___selectable_4;
		NullCheck(L_5);
		LeanSelectEvent_tE1B7F7125473C6F40463EC6C88B8842A0404C34F* L_6;
		L_6 = LeanSelectable_get_OnDeselected_m055BD9B88E71CAB5E6C4D429CA271308F56EBC29(L_5, NULL);
		UnityAction_1_tA601885BAAC49BD9D535DC344F67685863BD4573* L_7 = (UnityAction_1_tA601885BAAC49BD9D535DC344F67685863BD4573*)il2cpp_codegen_object_new(UnityAction_1_tA601885BAAC49BD9D535DC344F67685863BD4573_il2cpp_TypeInfo_var);
		NullCheck(L_7);
		UnityAction_1__ctor_mA72F946150482FECDC3C04BED2CBF062038EE470(L_7, __this, (intptr_t)((void*)GetVirtualMethodInfo(__this, 8)), NULL);
		NullCheck(L_6);
		UnityEvent_1_RemoveListener_m93A6005ABE25250DBD0E2497D329D839BC2C92D8(L_6, L_7, UnityEvent_1_RemoveListener_m93A6005ABE25250DBD0E2497D329D839BC2C92D8_RuntimeMethod_var);
		// selectable = null;
		__this->___selectable_4 = (LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___selectable_4), (void*)(LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E*)NULL);
	}

IL_004f:
	{
		// }
		return;
	}
}
// System.Void Lean.Common.LeanSelectableBehaviour::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectableBehaviour_OnEnable_m11C1074ED7800B1AE7FF3355ED6F6B564EF91D9D (LeanSelectableBehaviour_t07650DA7B5AA5AEACF3FA60433E5C36E1A679CD9* __this, const RuntimeMethod* method) 
{
	{
		// Register();
		LeanSelectableBehaviour_Register_m59BCA4108A01A335DA517DC30A64C29ADB64DB80(__this, NULL);
		// }
		return;
	}
}
// System.Void Lean.Common.LeanSelectableBehaviour::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectableBehaviour_Start_mCC6D17C6D1520EA1C4BAF1C4F3BB0801ECE7402B (LeanSelectableBehaviour_t07650DA7B5AA5AEACF3FA60433E5C36E1A679CD9* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (selectable == null)
		LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* L_0 = __this->___selectable_4;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mB6120F782D83091EF56A198FCEBCF066DB4A9605(L_0, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		// Register();
		LeanSelectableBehaviour_Register_m59BCA4108A01A335DA517DC30A64C29ADB64DB80(__this, NULL);
	}

IL_0014:
	{
		// }
		return;
	}
}
// System.Void Lean.Common.LeanSelectableBehaviour::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectableBehaviour_OnDisable_mFDF5E11937776F863480234CA5DB2103F0CEDCE5 (LeanSelectableBehaviour_t07650DA7B5AA5AEACF3FA60433E5C36E1A679CD9* __this, const RuntimeMethod* method) 
{
	{
		// Unregister();
		LeanSelectableBehaviour_Unregister_m21AA701DA1065421693A87AA7CD10DB32AD9318C(__this, NULL);
		// }
		return;
	}
}
// System.Void Lean.Common.LeanSelectableBehaviour::OnSelected(Lean.Common.LeanSelect)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectableBehaviour_OnSelected_m4037909C4E9F254A345795C9351B21884FF52248 (LeanSelectableBehaviour_t07650DA7B5AA5AEACF3FA60433E5C36E1A679CD9* __this, LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* ___0_select, const RuntimeMethod* method) 
{
	{
		// }
		return;
	}
}
// System.Void Lean.Common.LeanSelectableBehaviour::OnDeselected(Lean.Common.LeanSelect)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectableBehaviour_OnDeselected_m6D0284D8BDCE6F3596579DB616EB621060A10039 (LeanSelectableBehaviour_t07650DA7B5AA5AEACF3FA60433E5C36E1A679CD9* __this, LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* ___0_select, const RuntimeMethod* method) 
{
	{
		// }
		return;
	}
}
// System.Void Lean.Common.LeanSelectableBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectableBehaviour__ctor_m24001ACBC3695134BE8306864960F29F6A4573B9 (LeanSelectableBehaviour_t07650DA7B5AA5AEACF3FA60433E5C36E1A679CD9* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Common.LeanSelectableGraphicColor::set_DefaultColor(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectableGraphicColor_set_DefaultColor_m04E4A634432FEA2A06EA9D4A206A923C7235C8A8 (LeanSelectableGraphicColor_tFDDBE08F9483FC2CB8E1511BEEE1AF8F5601345A* __this, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___0_value, const RuntimeMethod* method) 
{
	{
		// public Color DefaultColor { set { defaultColor = value; UpdateColor(); } get { return defaultColor; } } [SerializeField] private Color defaultColor = Color.white;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0 = ___0_value;
		__this->___defaultColor_5 = L_0;
		// public Color DefaultColor { set { defaultColor = value; UpdateColor(); } get { return defaultColor; } } [SerializeField] private Color defaultColor = Color.white;
		LeanSelectableGraphicColor_UpdateColor_m8C4B31A043489DD4DA3BFC8DAA3CC0F985E3FE41(__this, NULL);
		// public Color DefaultColor { set { defaultColor = value; UpdateColor(); } get { return defaultColor; } } [SerializeField] private Color defaultColor = Color.white;
		return;
	}
}
// UnityEngine.Color Lean.Common.LeanSelectableGraphicColor::get_DefaultColor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F LeanSelectableGraphicColor_get_DefaultColor_m471842FCD7B965642321981E8443ECE91D3A92B9 (LeanSelectableGraphicColor_tFDDBE08F9483FC2CB8E1511BEEE1AF8F5601345A* __this, const RuntimeMethod* method) 
{
	{
		// public Color DefaultColor { set { defaultColor = value; UpdateColor(); } get { return defaultColor; } } [SerializeField] private Color defaultColor = Color.white;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0 = __this->___defaultColor_5;
		return L_0;
	}
}
// System.Void Lean.Common.LeanSelectableGraphicColor::set_SelectedColor(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectableGraphicColor_set_SelectedColor_mF266529928AD6E74DD5AAC127B8B66B4C853F084 (LeanSelectableGraphicColor_tFDDBE08F9483FC2CB8E1511BEEE1AF8F5601345A* __this, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___0_value, const RuntimeMethod* method) 
{
	{
		// public Color SelectedColor { set { selectedColor = value; UpdateColor(); } get { return selectedColor; } } [SerializeField] private Color selectedColor = Color.green;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0 = ___0_value;
		__this->___selectedColor_6 = L_0;
		// public Color SelectedColor { set { selectedColor = value; UpdateColor(); } get { return selectedColor; } } [SerializeField] private Color selectedColor = Color.green;
		LeanSelectableGraphicColor_UpdateColor_m8C4B31A043489DD4DA3BFC8DAA3CC0F985E3FE41(__this, NULL);
		// public Color SelectedColor { set { selectedColor = value; UpdateColor(); } get { return selectedColor; } } [SerializeField] private Color selectedColor = Color.green;
		return;
	}
}
// UnityEngine.Color Lean.Common.LeanSelectableGraphicColor::get_SelectedColor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F LeanSelectableGraphicColor_get_SelectedColor_m1FC1925B3DEBA9C0DDADF948E09E70AF2CDBAE7B (LeanSelectableGraphicColor_tFDDBE08F9483FC2CB8E1511BEEE1AF8F5601345A* __this, const RuntimeMethod* method) 
{
	{
		// public Color SelectedColor { set { selectedColor = value; UpdateColor(); } get { return selectedColor; } } [SerializeField] private Color selectedColor = Color.green;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0 = __this->___selectedColor_6;
		return L_0;
	}
}
// System.Void Lean.Common.LeanSelectableGraphicColor::OnSelected(Lean.Common.LeanSelect)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectableGraphicColor_OnSelected_m105017121C7C20B4B3A407FA3E8EC0BD99F2A696 (LeanSelectableGraphicColor_tFDDBE08F9483FC2CB8E1511BEEE1AF8F5601345A* __this, LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* ___0_select, const RuntimeMethod* method) 
{
	{
		// UpdateColor();
		LeanSelectableGraphicColor_UpdateColor_m8C4B31A043489DD4DA3BFC8DAA3CC0F985E3FE41(__this, NULL);
		// }
		return;
	}
}
// System.Void Lean.Common.LeanSelectableGraphicColor::OnDeselected(Lean.Common.LeanSelect)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectableGraphicColor_OnDeselected_mBBEF2C2E5D4B9CF53858B4723E8D466B92379BFF (LeanSelectableGraphicColor_tFDDBE08F9483FC2CB8E1511BEEE1AF8F5601345A* __this, LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* ___0_select, const RuntimeMethod* method) 
{
	{
		// UpdateColor();
		LeanSelectableGraphicColor_UpdateColor_m8C4B31A043489DD4DA3BFC8DAA3CC0F985E3FE41(__this, NULL);
		// }
		return;
	}
}
// System.Void Lean.Common.LeanSelectableGraphicColor::UpdateColor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectableGraphicColor_UpdateColor_m8C4B31A043489DD4DA3BFC8DAA3CC0F985E3FE41 (LeanSelectableGraphicColor_tFDDBE08F9483FC2CB8E1511BEEE1AF8F5601345A* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisGraphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931_mFE18E20FC92395F90E776DBC4CD214A4F2D97D90_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F V_0;
	memset((&V_0), 0, sizeof(V_0));
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F G_B6_0;
	memset((&G_B6_0), 0, sizeof(G_B6_0));
	{
		// if (cachedGraphic == null) cachedGraphic = GetComponent<Graphic>();
		Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931* L_0 = __this->___cachedGraphic_7;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mB6120F782D83091EF56A198FCEBCF066DB4A9605(L_0, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		// if (cachedGraphic == null) cachedGraphic = GetComponent<Graphic>();
		Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931* L_2;
		L_2 = Component_GetComponent_TisGraphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931_mFE18E20FC92395F90E776DBC4CD214A4F2D97D90(__this, Component_GetComponent_TisGraphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931_mFE18E20FC92395F90E776DBC4CD214A4F2D97D90_RuntimeMethod_var);
		__this->___cachedGraphic_7 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___cachedGraphic_7), (void*)L_2);
	}

IL_001a:
	{
		// var color = Selectable != null && Selectable.IsSelected == true ? selectedColor : defaultColor;
		LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* L_3;
		L_3 = LeanSelectableBehaviour_get_Selectable_mDA4E928E7FE887A93CF29BBC0EDD11483D52B97E(__this, NULL);
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_4;
		L_4 = Object_op_Inequality_mD0BE578448EAA61948F25C32F8DD55AB1F778602(L_3, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_4)
		{
			goto IL_0035;
		}
	}
	{
		LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* L_5;
		L_5 = LeanSelectableBehaviour_get_Selectable_mDA4E928E7FE887A93CF29BBC0EDD11483D52B97E(__this, NULL);
		NullCheck(L_5);
		bool L_6;
		L_6 = LeanSelectable_get_IsSelected_m55508746D046549000EB2085D5EA859B5ED855CB(L_5, NULL);
		if (L_6)
		{
			goto IL_003d;
		}
	}

IL_0035:
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_7 = __this->___defaultColor_5;
		G_B6_0 = L_7;
		goto IL_0043;
	}

IL_003d:
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_8 = __this->___selectedColor_6;
		G_B6_0 = L_8;
	}

IL_0043:
	{
		V_0 = G_B6_0;
		// cachedGraphic.color = color;
		Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931* L_9 = __this->___cachedGraphic_7;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_10 = V_0;
		NullCheck(L_9);
		VirtualActionInvoker1< Color_tD001788D726C3A7F1379BEED0260B9591F440C1F >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_9, L_10);
		// }
		return;
	}
}
// System.Void Lean.Common.LeanSelectableGraphicColor::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectableGraphicColor__ctor_m3B890E4A02B1C0E7EC5969CA311D4CC7F03434B1 (LeanSelectableGraphicColor_tFDDBE08F9483FC2CB8E1511BEEE1AF8F5601345A* __this, const RuntimeMethod* method) 
{
	{
		// public Color DefaultColor { set { defaultColor = value; UpdateColor(); } get { return defaultColor; } } [SerializeField] private Color defaultColor = Color.white;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0;
		L_0 = Color_get_white_m068F5AF879B0FCA584E3693F762EA41BB65532C6_inline(NULL);
		__this->___defaultColor_5 = L_0;
		// public Color SelectedColor { set { selectedColor = value; UpdateColor(); } get { return selectedColor; } } [SerializeField] private Color selectedColor = Color.green;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1;
		L_1 = Color_get_green_mEB001F2CD8C68C6BBAEF9101990B779D3AA2A6EF_inline(NULL);
		__this->___selectedColor_6 = L_1;
		LeanSelectableBehaviour__ctor_m24001ACBC3695134BE8306864960F29F6A4573B9(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Common.LeanSelectableRendererColor::set_DefaultColor(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectableRendererColor_set_DefaultColor_m1CCCA7BD2A7E89B727F6EBEF14240BFB0553DAB7 (LeanSelectableRendererColor_tDD4EBBF7DAB508DD4FC536B35A7F8DB48142433E* __this, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___0_value, const RuntimeMethod* method) 
{
	{
		// public Color DefaultColor { set { defaultColor = value; UpdateColor(); } get { return defaultColor; } } [SerializeField] private Color defaultColor = Color.white;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0 = ___0_value;
		__this->___defaultColor_5 = L_0;
		// public Color DefaultColor { set { defaultColor = value; UpdateColor(); } get { return defaultColor; } } [SerializeField] private Color defaultColor = Color.white;
		LeanSelectableRendererColor_UpdateColor_mBB4BFB9173F24C7C758D291DC6AAFA77F91F484C(__this, NULL);
		// public Color DefaultColor { set { defaultColor = value; UpdateColor(); } get { return defaultColor; } } [SerializeField] private Color defaultColor = Color.white;
		return;
	}
}
// UnityEngine.Color Lean.Common.LeanSelectableRendererColor::get_DefaultColor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F LeanSelectableRendererColor_get_DefaultColor_mBBA833F9D97DD57E02CA62F5CB3E06ACCD6292EF (LeanSelectableRendererColor_tDD4EBBF7DAB508DD4FC536B35A7F8DB48142433E* __this, const RuntimeMethod* method) 
{
	{
		// public Color DefaultColor { set { defaultColor = value; UpdateColor(); } get { return defaultColor; } } [SerializeField] private Color defaultColor = Color.white;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0 = __this->___defaultColor_5;
		return L_0;
	}
}
// System.Void Lean.Common.LeanSelectableRendererColor::set_SelectedColor(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectableRendererColor_set_SelectedColor_mDED68B9DAD22DF8C010784056B6CB8EE59489E0C (LeanSelectableRendererColor_tDD4EBBF7DAB508DD4FC536B35A7F8DB48142433E* __this, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___0_value, const RuntimeMethod* method) 
{
	{
		// public Color SelectedColor { set { selectedColor = value; UpdateColor(); } get { return selectedColor; } } [SerializeField] private Color selectedColor = Color.green;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0 = ___0_value;
		__this->___selectedColor_6 = L_0;
		// public Color SelectedColor { set { selectedColor = value; UpdateColor(); } get { return selectedColor; } } [SerializeField] private Color selectedColor = Color.green;
		LeanSelectableRendererColor_UpdateColor_mBB4BFB9173F24C7C758D291DC6AAFA77F91F484C(__this, NULL);
		// public Color SelectedColor { set { selectedColor = value; UpdateColor(); } get { return selectedColor; } } [SerializeField] private Color selectedColor = Color.green;
		return;
	}
}
// UnityEngine.Color Lean.Common.LeanSelectableRendererColor::get_SelectedColor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F LeanSelectableRendererColor_get_SelectedColor_m1789A269D339ADEE19BC55F124E9CA04FF33C81B (LeanSelectableRendererColor_tDD4EBBF7DAB508DD4FC536B35A7F8DB48142433E* __this, const RuntimeMethod* method) 
{
	{
		// public Color SelectedColor { set { selectedColor = value; UpdateColor(); } get { return selectedColor; } } [SerializeField] private Color selectedColor = Color.green;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0 = __this->___selectedColor_6;
		return L_0;
	}
}
// System.Void Lean.Common.LeanSelectableRendererColor::OnSelected(Lean.Common.LeanSelect)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectableRendererColor_OnSelected_mBDD16192639BDC6D919A7124D386F89C4B9218F5 (LeanSelectableRendererColor_tDD4EBBF7DAB508DD4FC536B35A7F8DB48142433E* __this, LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* ___0_select, const RuntimeMethod* method) 
{
	{
		// UpdateColor();
		LeanSelectableRendererColor_UpdateColor_mBB4BFB9173F24C7C758D291DC6AAFA77F91F484C(__this, NULL);
		// }
		return;
	}
}
// System.Void Lean.Common.LeanSelectableRendererColor::OnDeselected(Lean.Common.LeanSelect)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectableRendererColor_OnDeselected_mADD2E795E00EA66A0CBA84232DB469B563BB71F4 (LeanSelectableRendererColor_tDD4EBBF7DAB508DD4FC536B35A7F8DB48142433E* __this, LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* ___0_select, const RuntimeMethod* method) 
{
	{
		// UpdateColor();
		LeanSelectableRendererColor_UpdateColor_mBB4BFB9173F24C7C758D291DC6AAFA77F91F484C(__this, NULL);
		// }
		return;
	}
}
// System.Void Lean.Common.LeanSelectableRendererColor::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectableRendererColor_Start_mB4C768F2029D6F8D4EB2B2424CD86B1D51095818 (LeanSelectableRendererColor_tDD4EBBF7DAB508DD4FC536B35A7F8DB48142433E* __this, const RuntimeMethod* method) 
{
	{
		// base.Start();
		LeanSelectableBehaviour_Start_mCC6D17C6D1520EA1C4BAF1C4F3BB0801ECE7402B(__this, NULL);
		// UpdateColor();
		LeanSelectableRendererColor_UpdateColor_mBB4BFB9173F24C7C758D291DC6AAFA77F91F484C(__this, NULL);
		// }
		return;
	}
}
// System.Void Lean.Common.LeanSelectableRendererColor::UpdateColor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectableRendererColor_UpdateColor_mBB4BFB9173F24C7C758D291DC6AAFA77F91F484C (LeanSelectableRendererColor_tDD4EBBF7DAB508DD4FC536B35A7F8DB48142433E* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_mC91ACC92AD57CA6CA00991DAF1DB3830BCE07AF8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE);
		s_Il2CppMethodInitialized = true;
	}
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F V_0;
	memset((&V_0), 0, sizeof(V_0));
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F G_B6_0;
	memset((&G_B6_0), 0, sizeof(G_B6_0));
	{
		// if (cachedRenderer == null) cachedRenderer = GetComponent<Renderer>();
		Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_0 = __this->___cachedRenderer_7;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mB6120F782D83091EF56A198FCEBCF066DB4A9605(L_0, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		// if (cachedRenderer == null) cachedRenderer = GetComponent<Renderer>();
		Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_2;
		L_2 = Component_GetComponent_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_mC91ACC92AD57CA6CA00991DAF1DB3830BCE07AF8(__this, Component_GetComponent_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_mC91ACC92AD57CA6CA00991DAF1DB3830BCE07AF8_RuntimeMethod_var);
		__this->___cachedRenderer_7 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___cachedRenderer_7), (void*)L_2);
	}

IL_001a:
	{
		// var color = Selectable != null && Selectable.IsSelected == true ? selectedColor : defaultColor;
		LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* L_3;
		L_3 = LeanSelectableBehaviour_get_Selectable_mDA4E928E7FE887A93CF29BBC0EDD11483D52B97E(__this, NULL);
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_4;
		L_4 = Object_op_Inequality_mD0BE578448EAA61948F25C32F8DD55AB1F778602(L_3, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_4)
		{
			goto IL_0035;
		}
	}
	{
		LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* L_5;
		L_5 = LeanSelectableBehaviour_get_Selectable_mDA4E928E7FE887A93CF29BBC0EDD11483D52B97E(__this, NULL);
		NullCheck(L_5);
		bool L_6;
		L_6 = LeanSelectable_get_IsSelected_m55508746D046549000EB2085D5EA859B5ED855CB(L_5, NULL);
		if (L_6)
		{
			goto IL_003d;
		}
	}

IL_0035:
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_7 = __this->___defaultColor_5;
		G_B6_0 = L_7;
		goto IL_0043;
	}

IL_003d:
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_8 = __this->___selectedColor_6;
		G_B6_0 = L_8;
	}

IL_0043:
	{
		V_0 = G_B6_0;
		// if (properties == null)
		MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D* L_9 = __this->___properties_8;
		if (L_9)
		{
			goto IL_0057;
		}
	}
	{
		// properties = new MaterialPropertyBlock();
		MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D* L_10 = (MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D*)il2cpp_codegen_object_new(MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D_il2cpp_TypeInfo_var);
		NullCheck(L_10);
		MaterialPropertyBlock__ctor_m14C3432585F7BB65028BCD64A0FD6607A1B490FB(L_10, NULL);
		__this->___properties_8 = L_10;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___properties_8), (void*)L_10);
	}

IL_0057:
	{
		// cachedRenderer.GetPropertyBlock(properties);
		Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_11 = __this->___cachedRenderer_7;
		MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D* L_12 = __this->___properties_8;
		NullCheck(L_11);
		Renderer_GetPropertyBlock_mD062F90343D70151CA060AE7EBEF2E85146A9FBA(L_11, L_12, NULL);
		// properties.SetColor("_Color", color);
		MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D* L_13 = __this->___properties_8;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_14 = V_0;
		NullCheck(L_13);
		MaterialPropertyBlock_SetColor_m5B4E910B5E42518BBD0088055EB68E4A3A609DDE(L_13, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_14, NULL);
		// cachedRenderer.SetPropertyBlock(properties);
		Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_15 = __this->___cachedRenderer_7;
		MaterialPropertyBlock_t2308669579033A857EFE6E4831909F638B27411D* L_16 = __this->___properties_8;
		NullCheck(L_15);
		Renderer_SetPropertyBlock_mF565698782FE54580B17CC0BFF9B0C4F0D68DF50(L_15, L_16, NULL);
		// }
		return;
	}
}
// System.Void Lean.Common.LeanSelectableRendererColor::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectableRendererColor__ctor_m514753D2E70569C0B05EE8674786B76B4CA8E846 (LeanSelectableRendererColor_tDD4EBBF7DAB508DD4FC536B35A7F8DB48142433E* __this, const RuntimeMethod* method) 
{
	{
		// public Color DefaultColor { set { defaultColor = value; UpdateColor(); } get { return defaultColor; } } [SerializeField] private Color defaultColor = Color.white;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0;
		L_0 = Color_get_white_m068F5AF879B0FCA584E3693F762EA41BB65532C6_inline(NULL);
		__this->___defaultColor_5 = L_0;
		// public Color SelectedColor { set { selectedColor = value; UpdateColor(); } get { return selectedColor; } } [SerializeField] private Color selectedColor = Color.green;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1;
		L_1 = Color_get_green_mEB001F2CD8C68C6BBAEF9101990B779D3AA2A6EF_inline(NULL);
		__this->___selectedColor_6 = L_1;
		LeanSelectableBehaviour__ctor_m24001ACBC3695134BE8306864960F29F6A4573B9(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Common.LeanSelectableSpriteRendererColor::set_DefaultColor(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectableSpriteRendererColor_set_DefaultColor_m276192DD1E9B19CD0799180F6BB9247572F4232B (LeanSelectableSpriteRendererColor_tAD5AA41024B15A0A25E574475377A2669143886E* __this, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___0_value, const RuntimeMethod* method) 
{
	{
		// public Color DefaultColor { set { defaultColor = value; UpdateColor(); } get { return defaultColor; } } [SerializeField] private Color defaultColor = Color.white;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0 = ___0_value;
		__this->___defaultColor_5 = L_0;
		// public Color DefaultColor { set { defaultColor = value; UpdateColor(); } get { return defaultColor; } } [SerializeField] private Color defaultColor = Color.white;
		LeanSelectableSpriteRendererColor_UpdateColor_mB959E44683EB3D85A96956DB84A4AEB79A124D2B(__this, NULL);
		// public Color DefaultColor { set { defaultColor = value; UpdateColor(); } get { return defaultColor; } } [SerializeField] private Color defaultColor = Color.white;
		return;
	}
}
// UnityEngine.Color Lean.Common.LeanSelectableSpriteRendererColor::get_DefaultColor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F LeanSelectableSpriteRendererColor_get_DefaultColor_m8D2CBDCC7577986F67D9C04DD9F11D40EF76BD0D (LeanSelectableSpriteRendererColor_tAD5AA41024B15A0A25E574475377A2669143886E* __this, const RuntimeMethod* method) 
{
	{
		// public Color DefaultColor { set { defaultColor = value; UpdateColor(); } get { return defaultColor; } } [SerializeField] private Color defaultColor = Color.white;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0 = __this->___defaultColor_5;
		return L_0;
	}
}
// System.Void Lean.Common.LeanSelectableSpriteRendererColor::set_SelectedColor(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectableSpriteRendererColor_set_SelectedColor_mD61014AF5613A92CDE8668CAD70DB68C02A3ADF1 (LeanSelectableSpriteRendererColor_tAD5AA41024B15A0A25E574475377A2669143886E* __this, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___0_value, const RuntimeMethod* method) 
{
	{
		// public Color SelectedColor { set { selectedColor = value; UpdateColor(); } get { return selectedColor; } } [SerializeField] private Color selectedColor = Color.green;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0 = ___0_value;
		__this->___selectedColor_6 = L_0;
		// public Color SelectedColor { set { selectedColor = value; UpdateColor(); } get { return selectedColor; } } [SerializeField] private Color selectedColor = Color.green;
		LeanSelectableSpriteRendererColor_UpdateColor_mB959E44683EB3D85A96956DB84A4AEB79A124D2B(__this, NULL);
		// public Color SelectedColor { set { selectedColor = value; UpdateColor(); } get { return selectedColor; } } [SerializeField] private Color selectedColor = Color.green;
		return;
	}
}
// UnityEngine.Color Lean.Common.LeanSelectableSpriteRendererColor::get_SelectedColor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F LeanSelectableSpriteRendererColor_get_SelectedColor_m49805D8A311F4B734CA86528C2BAE5437452E806 (LeanSelectableSpriteRendererColor_tAD5AA41024B15A0A25E574475377A2669143886E* __this, const RuntimeMethod* method) 
{
	{
		// public Color SelectedColor { set { selectedColor = value; UpdateColor(); } get { return selectedColor; } } [SerializeField] private Color selectedColor = Color.green;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0 = __this->___selectedColor_6;
		return L_0;
	}
}
// System.Void Lean.Common.LeanSelectableSpriteRendererColor::OnSelected(Lean.Common.LeanSelect)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectableSpriteRendererColor_OnSelected_mA0482CB6294A989841715178F2BC601F49C77477 (LeanSelectableSpriteRendererColor_tAD5AA41024B15A0A25E574475377A2669143886E* __this, LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* ___0_select, const RuntimeMethod* method) 
{
	{
		// UpdateColor();
		LeanSelectableSpriteRendererColor_UpdateColor_mB959E44683EB3D85A96956DB84A4AEB79A124D2B(__this, NULL);
		// }
		return;
	}
}
// System.Void Lean.Common.LeanSelectableSpriteRendererColor::OnDeselected(Lean.Common.LeanSelect)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectableSpriteRendererColor_OnDeselected_m7C8E1081624ABE0358F0CB6B357E7694A431D98A (LeanSelectableSpriteRendererColor_tAD5AA41024B15A0A25E574475377A2669143886E* __this, LeanSelect_t6AF3CA4DEC0EE5CB9BC859A9D77529B2CA42E94F* ___0_select, const RuntimeMethod* method) 
{
	{
		// UpdateColor();
		LeanSelectableSpriteRendererColor_UpdateColor_mB959E44683EB3D85A96956DB84A4AEB79A124D2B(__this, NULL);
		// }
		return;
	}
}
// System.Void Lean.Common.LeanSelectableSpriteRendererColor::UpdateColor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectableSpriteRendererColor_UpdateColor_mB959E44683EB3D85A96956DB84A4AEB79A124D2B (LeanSelectableSpriteRendererColor_tAD5AA41024B15A0A25E574475377A2669143886E* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisSpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B_m6181F10C09FC1650DAE0EF2308D344A2F170AA45_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F V_0;
	memset((&V_0), 0, sizeof(V_0));
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F G_B6_0;
	memset((&G_B6_0), 0, sizeof(G_B6_0));
	{
		// if (cachedSpriteRenderer == null) cachedSpriteRenderer = GetComponent<SpriteRenderer>();
		SpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B* L_0 = __this->___cachedSpriteRenderer_7;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mB6120F782D83091EF56A198FCEBCF066DB4A9605(L_0, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		// if (cachedSpriteRenderer == null) cachedSpriteRenderer = GetComponent<SpriteRenderer>();
		SpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B* L_2;
		L_2 = Component_GetComponent_TisSpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B_m6181F10C09FC1650DAE0EF2308D344A2F170AA45(__this, Component_GetComponent_TisSpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B_m6181F10C09FC1650DAE0EF2308D344A2F170AA45_RuntimeMethod_var);
		__this->___cachedSpriteRenderer_7 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___cachedSpriteRenderer_7), (void*)L_2);
	}

IL_001a:
	{
		// var color = Selectable != null && Selectable.IsSelected == true ? selectedColor : defaultColor;
		LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* L_3;
		L_3 = LeanSelectableBehaviour_get_Selectable_mDA4E928E7FE887A93CF29BBC0EDD11483D52B97E(__this, NULL);
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_4;
		L_4 = Object_op_Inequality_mD0BE578448EAA61948F25C32F8DD55AB1F778602(L_3, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_4)
		{
			goto IL_0035;
		}
	}
	{
		LeanSelectable_t575BE3CCAC73886324954CFCAA22C5C5FC9A6A6E* L_5;
		L_5 = LeanSelectableBehaviour_get_Selectable_mDA4E928E7FE887A93CF29BBC0EDD11483D52B97E(__this, NULL);
		NullCheck(L_5);
		bool L_6;
		L_6 = LeanSelectable_get_IsSelected_m55508746D046549000EB2085D5EA859B5ED855CB(L_5, NULL);
		if (L_6)
		{
			goto IL_003d;
		}
	}

IL_0035:
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_7 = __this->___defaultColor_5;
		G_B6_0 = L_7;
		goto IL_0043;
	}

IL_003d:
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_8 = __this->___selectedColor_6;
		G_B6_0 = L_8;
	}

IL_0043:
	{
		V_0 = G_B6_0;
		// cachedSpriteRenderer.color = color;
		SpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B* L_9 = __this->___cachedSpriteRenderer_7;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_10 = V_0;
		NullCheck(L_9);
		SpriteRenderer_set_color_mB0EEC2845A0347E296C01C831F967731D2804546(L_9, L_10, NULL);
		// }
		return;
	}
}
// System.Void Lean.Common.LeanSelectableSpriteRendererColor::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSelectableSpriteRendererColor__ctor_mD0A401E22773FF1503E454D73F1E7A2B9E57BC2E (LeanSelectableSpriteRendererColor_tAD5AA41024B15A0A25E574475377A2669143886E* __this, const RuntimeMethod* method) 
{
	{
		// public Color DefaultColor { set { defaultColor = value; UpdateColor(); } get { return defaultColor; } } [SerializeField] private Color defaultColor = Color.white;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0;
		L_0 = Color_get_white_m068F5AF879B0FCA584E3693F762EA41BB65532C6_inline(NULL);
		__this->___defaultColor_5 = L_0;
		// public Color SelectedColor { set { selectedColor = value; UpdateColor(); } get { return selectedColor; } } [SerializeField] private Color selectedColor = Color.green;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1;
		L_1 = Color_get_green_mEB001F2CD8C68C6BBAEF9101990B779D3AA2A6EF_inline(NULL);
		__this->___selectedColor_6 = L_1;
		LeanSelectableBehaviour__ctor_m24001ACBC3695134BE8306864960F29F6A4573B9(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lean.Common.LeanSpawn::set_Prefab(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSpawn_set_Prefab_m9EF621030E54E351A4BA12F6A3F02B3CBA712EDC (LeanSpawn_t9B3DF6A3500CC180061ACF23149F40C2F2439801* __this, Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___0_value, const RuntimeMethod* method) 
{
	{
		// public Transform Prefab { set { prefab = value; } get { return prefab; } } [SerializeField] private Transform prefab;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_0 = ___0_value;
		__this->___prefab_4 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___prefab_4), (void*)L_0);
		// public Transform Prefab { set { prefab = value; } get { return prefab; } } [SerializeField] private Transform prefab;
		return;
	}
}
// UnityEngine.Transform Lean.Common.LeanSpawn::get_Prefab()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* LeanSpawn_get_Prefab_mB50202E0832F14C4C9A0599EE005812CA96B7424 (LeanSpawn_t9B3DF6A3500CC180061ACF23149F40C2F2439801* __this, const RuntimeMethod* method) 
{
	{
		// public Transform Prefab { set { prefab = value; } get { return prefab; } } [SerializeField] private Transform prefab;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_0 = __this->___prefab_4;
		return L_0;
	}
}
// System.Void Lean.Common.LeanSpawn::set_DefaultPosition(Lean.Common.LeanSpawn/SourceType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSpawn_set_DefaultPosition_mC45A0DC850B31A783E27971EE2539D7EC218BD3C (LeanSpawn_t9B3DF6A3500CC180061ACF23149F40C2F2439801* __this, int32_t ___0_value, const RuntimeMethod* method) 
{
	{
		// public SourceType DefaultPosition { set { defaultPosition = value; } get { return defaultPosition; } } [SerializeField] private SourceType defaultPosition;
		int32_t L_0 = ___0_value;
		__this->___defaultPosition_5 = L_0;
		// public SourceType DefaultPosition { set { defaultPosition = value; } get { return defaultPosition; } } [SerializeField] private SourceType defaultPosition;
		return;
	}
}
// Lean.Common.LeanSpawn/SourceType Lean.Common.LeanSpawn::get_DefaultPosition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t LeanSpawn_get_DefaultPosition_m977F92325E8630DF382313785A9565D373C1E887 (LeanSpawn_t9B3DF6A3500CC180061ACF23149F40C2F2439801* __this, const RuntimeMethod* method) 
{
	{
		// public SourceType DefaultPosition { set { defaultPosition = value; } get { return defaultPosition; } } [SerializeField] private SourceType defaultPosition;
		int32_t L_0 = __this->___defaultPosition_5;
		return L_0;
	}
}
// System.Void Lean.Common.LeanSpawn::set_DefaultRotation(Lean.Common.LeanSpawn/SourceType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSpawn_set_DefaultRotation_m3332F942D2F460104B36D1F039571557661982C1 (LeanSpawn_t9B3DF6A3500CC180061ACF23149F40C2F2439801* __this, int32_t ___0_value, const RuntimeMethod* method) 
{
	{
		// public SourceType DefaultRotation { set { defaultRotation = value; } get { return defaultRotation; } } [SerializeField] private SourceType defaultRotation;
		int32_t L_0 = ___0_value;
		__this->___defaultRotation_6 = L_0;
		// public SourceType DefaultRotation { set { defaultRotation = value; } get { return defaultRotation; } } [SerializeField] private SourceType defaultRotation;
		return;
	}
}
// Lean.Common.LeanSpawn/SourceType Lean.Common.LeanSpawn::get_DefaultRotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t LeanSpawn_get_DefaultRotation_m71C99E2B15F8D895EE8055F9E3D87A2B8D76732F (LeanSpawn_t9B3DF6A3500CC180061ACF23149F40C2F2439801* __this, const RuntimeMethod* method) 
{
	{
		// public SourceType DefaultRotation { set { defaultRotation = value; } get { return defaultRotation; } } [SerializeField] private SourceType defaultRotation;
		int32_t L_0 = __this->___defaultRotation_6;
		return L_0;
	}
}
// System.Void Lean.Common.LeanSpawn::Spawn()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSpawn_Spawn_m4B2658D62A3E17E4D8EC41B5606010E05445CE16 (LeanSpawn_t9B3DF6A3500CC180061ACF23149F40C2F2439801* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_Instantiate_TisTransform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1_mD6EFBE8E5CFC309C748E284B0798BF1C184F49ED_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 V_1;
	memset((&V_1), 0, sizeof(V_1));
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 G_B4_0;
	memset((&G_B4_0), 0, sizeof(G_B4_0));
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 G_B7_0;
	memset((&G_B7_0), 0, sizeof(G_B7_0));
	{
		// if (prefab != null)
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_0 = __this->___prefab_4;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_mD0BE578448EAA61948F25C32F8DD55AB1F778602(L_0, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_1)
		{
			goto IL_006a;
		}
	}
	{
		// var position = defaultPosition == SourceType.Prefab ? prefab.position : transform.position;
		int32_t L_2 = __this->___defaultPosition_5;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0024;
		}
	}
	{
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_3;
		L_3 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		NullCheck(L_3);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4;
		L_4 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_3, NULL);
		G_B4_0 = L_4;
		goto IL_002f;
	}

IL_0024:
	{
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_5 = __this->___prefab_4;
		NullCheck(L_5);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6;
		L_6 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_5, NULL);
		G_B4_0 = L_6;
	}

IL_002f:
	{
		V_0 = G_B4_0;
		// var rotation = defaultRotation == SourceType.Prefab ? prefab.rotation : transform.rotation;
		int32_t L_7 = __this->___defaultRotation_6;
		if ((((int32_t)L_7) == ((int32_t)1)))
		{
			goto IL_0046;
		}
	}
	{
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_8;
		L_8 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		NullCheck(L_8);
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_9;
		L_9 = Transform_get_rotation_m32AF40CA0D50C797DA639A696F8EAEC7524C179C(L_8, NULL);
		G_B7_0 = L_9;
		goto IL_0051;
	}

IL_0046:
	{
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_10 = __this->___prefab_4;
		NullCheck(L_10);
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_11;
		L_11 = Transform_get_rotation_m32AF40CA0D50C797DA639A696F8EAEC7524C179C(L_10, NULL);
		G_B7_0 = L_11;
	}

IL_0051:
	{
		V_1 = G_B7_0;
		// var clone    = Instantiate(prefab, position, rotation);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_12 = __this->___prefab_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_13 = V_0;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_14 = V_1;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_15;
		L_15 = Object_Instantiate_TisTransform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1_mD6EFBE8E5CFC309C748E284B0798BF1C184F49ED(L_12, L_13, L_14, Object_Instantiate_TisTransform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1_mD6EFBE8E5CFC309C748E284B0798BF1C184F49ED_RuntimeMethod_var);
		// clone.gameObject.SetActive(true);
		NullCheck(L_15);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_16;
		L_16 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_15, NULL);
		NullCheck(L_16);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_16, (bool)1, NULL);
	}

IL_006a:
	{
		// }
		return;
	}
}
// System.Void Lean.Common.LeanSpawn::Spawn(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSpawn_Spawn_mB6149967EADA16BBA46AD9B5580384B79ED8E5FD (LeanSpawn_t9B3DF6A3500CC180061ACF23149F40C2F2439801* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_position, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_Instantiate_TisTransform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1_mD6EFBE8E5CFC309C748E284B0798BF1C184F49ED_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 V_0;
	memset((&V_0), 0, sizeof(V_0));
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 G_B4_0;
	memset((&G_B4_0), 0, sizeof(G_B4_0));
	{
		// if (prefab != null)
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_0 = __this->___prefab_4;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_mD0BE578448EAA61948F25C32F8DD55AB1F778602(L_0, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_1)
		{
			goto IL_0048;
		}
	}
	{
		// var rotation = defaultRotation == SourceType.Prefab ? prefab.rotation : transform.rotation;
		int32_t L_2 = __this->___defaultRotation_6;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0024;
		}
	}
	{
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_3;
		L_3 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		NullCheck(L_3);
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_4;
		L_4 = Transform_get_rotation_m32AF40CA0D50C797DA639A696F8EAEC7524C179C(L_3, NULL);
		G_B4_0 = L_4;
		goto IL_002f;
	}

IL_0024:
	{
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_5 = __this->___prefab_4;
		NullCheck(L_5);
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_6;
		L_6 = Transform_get_rotation_m32AF40CA0D50C797DA639A696F8EAEC7524C179C(L_5, NULL);
		G_B4_0 = L_6;
	}

IL_002f:
	{
		V_0 = G_B4_0;
		// var clone    = Instantiate(prefab, position, rotation);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_7 = __this->___prefab_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8 = ___0_position;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_9 = V_0;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_10;
		L_10 = Object_Instantiate_TisTransform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1_mD6EFBE8E5CFC309C748E284B0798BF1C184F49ED(L_7, L_8, L_9, Object_Instantiate_TisTransform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1_mD6EFBE8E5CFC309C748E284B0798BF1C184F49ED_RuntimeMethod_var);
		// clone.gameObject.SetActive(true);
		NullCheck(L_10);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_11;
		L_11 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_10, NULL);
		NullCheck(L_11);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_11, (bool)1, NULL);
	}

IL_0048:
	{
		// }
		return;
	}
}
// System.Void Lean.Common.LeanSpawn::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanSpawn__ctor_mAD61599D9F51F60C2A90BB4EDA5813A6CB7BEF17 (LeanSpawn_t9B3DF6A3500CC180061ACF23149F40C2F2439801* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Vector2 Lean.Common.LeanCommon::Hermite(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 LeanCommon_Hermite_m933D6F37B112788EA496FEEC7663E6553B0CC3F3 (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___0_a, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___1_b, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___2_c, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___3_d, float ___4_t, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		// var mu2 = t * t;
		float L_0 = ___4_t;
		float L_1 = ___4_t;
		V_0 = ((float)il2cpp_codegen_multiply(L_0, L_1));
		// var mu3 = mu2 * t;
		float L_2 = V_0;
		float L_3 = ___4_t;
		V_1 = ((float)il2cpp_codegen_multiply(L_2, L_3));
		// var x   = HermiteInterpolate(a.x, b.x, c.x, d.x, t, mu2, mu3);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_4 = ___0_a;
		float L_5 = L_4.___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_6 = ___1_b;
		float L_7 = L_6.___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_8 = ___2_c;
		float L_9 = L_8.___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_10 = ___3_d;
		float L_11 = L_10.___x_0;
		float L_12 = ___4_t;
		float L_13 = V_0;
		float L_14 = V_1;
		float L_15;
		L_15 = LeanCommon_HermiteInterpolate_m18965FC0DCD5D7A0F11486C2497BCDCD4CC205B2(L_5, L_7, L_9, L_11, L_12, L_13, L_14, NULL);
		// var y   = HermiteInterpolate(a.y, b.y, c.y, d.y, t, mu2, mu3);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_16 = ___0_a;
		float L_17 = L_16.___y_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_18 = ___1_b;
		float L_19 = L_18.___y_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_20 = ___2_c;
		float L_21 = L_20.___y_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_22 = ___3_d;
		float L_23 = L_22.___y_1;
		float L_24 = ___4_t;
		float L_25 = V_0;
		float L_26 = V_1;
		float L_27;
		L_27 = LeanCommon_HermiteInterpolate_m18965FC0DCD5D7A0F11486C2497BCDCD4CC205B2(L_17, L_19, L_21, L_23, L_24, L_25, L_26, NULL);
		V_2 = L_27;
		// return new Vector2(x, y);
		float L_28 = V_2;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_29;
		memset((&L_29), 0, sizeof(L_29));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_29), L_15, L_28, /*hidden argument*/NULL);
		return L_29;
	}
}
// System.Single Lean.Common.LeanCommon::HermiteInterpolate(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float LeanCommon_HermiteInterpolate_m18965FC0DCD5D7A0F11486C2497BCDCD4CC205B2 (float ___0_y0, float ___1_y1, float ___2_y2, float ___3_y3, float ___4_mu, float ___5_mu2, float ___6_mu3, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	{
		// var m0 = (y1 - y0) * 0.5f + (y2 - y1) * 0.5f;
		float L_0 = ___1_y1;
		float L_1 = ___0_y0;
		float L_2 = ___2_y2;
		float L_3 = ___1_y1;
		V_0 = ((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(((float)il2cpp_codegen_subtract(L_0, L_1)), (0.5f))), ((float)il2cpp_codegen_multiply(((float)il2cpp_codegen_subtract(L_2, L_3)), (0.5f)))));
		// var m1 = (y2 - y1) * 0.5f + (y3 - y2) * 0.5f;
		float L_4 = ___2_y2;
		float L_5 = ___1_y1;
		float L_6 = ___3_y3;
		float L_7 = ___2_y2;
		V_1 = ((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(((float)il2cpp_codegen_subtract(L_4, L_5)), (0.5f))), ((float)il2cpp_codegen_multiply(((float)il2cpp_codegen_subtract(L_6, L_7)), (0.5f)))));
		// var a0 =  2.0f * mu3 - 3.0f * mu2 + 1.0f;
		float L_8 = ___6_mu3;
		float L_9 = ___5_mu2;
		// var a1 =         mu3 - 2.0f * mu2 + mu;
		float L_10 = ___6_mu3;
		float L_11 = ___5_mu2;
		float L_12 = ___4_mu;
		V_2 = ((float)il2cpp_codegen_add(((float)il2cpp_codegen_subtract(L_10, ((float)il2cpp_codegen_multiply((2.0f), L_11)))), L_12));
		// var a2 =         mu3 -        mu2;
		float L_13 = ___6_mu3;
		float L_14 = ___5_mu2;
		V_3 = ((float)il2cpp_codegen_subtract(L_13, L_14));
		// var a3 = -2.0f * mu3 + 3.0f * mu2;
		float L_15 = ___6_mu3;
		float L_16 = ___5_mu2;
		V_4 = ((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply((-2.0f), L_15)), ((float)il2cpp_codegen_multiply((3.0f), L_16))));
		// return a0 * y1 + a1 * m0 + a2 * m1 + a3 * y2;
		float L_17 = ___1_y1;
		float L_18 = V_2;
		float L_19 = V_0;
		float L_20 = V_3;
		float L_21 = V_1;
		float L_22 = V_4;
		float L_23 = ___2_y2;
		return ((float)il2cpp_codegen_add(((float)il2cpp_codegen_add(((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(((float)il2cpp_codegen_add(((float)il2cpp_codegen_subtract(((float)il2cpp_codegen_multiply((2.0f), L_8)), ((float)il2cpp_codegen_multiply((3.0f), L_9)))), (1.0f))), L_17)), ((float)il2cpp_codegen_multiply(L_18, L_19)))), ((float)il2cpp_codegen_multiply(L_20, L_21)))), ((float)il2cpp_codegen_multiply(L_22, L_23))));
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: Lean.Common.LeanScreenQuery
IL2CPP_EXTERN_C void LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_marshal_pinvoke(const LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535& unmarshaled, LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_marshaled_pinvoke& marshaled)
{
	Exception_t* ___Camera_4Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'Camera' of type 'LeanScreenQuery': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___Camera_4Exception, NULL);
}
IL2CPP_EXTERN_C void LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_marshal_pinvoke_back(const LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_marshaled_pinvoke& marshaled, LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535& unmarshaled)
{
	Exception_t* ___Camera_4Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'Camera' of type 'LeanScreenQuery': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___Camera_4Exception, NULL);
}
// Conversion method for clean up from marshalling of: Lean.Common.LeanScreenQuery
IL2CPP_EXTERN_C void LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_marshal_pinvoke_cleanup(LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: Lean.Common.LeanScreenQuery
IL2CPP_EXTERN_C void LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_marshal_com(const LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535& unmarshaled, LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_marshaled_com& marshaled)
{
	Exception_t* ___Camera_4Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'Camera' of type 'LeanScreenQuery': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___Camera_4Exception, NULL);
}
IL2CPP_EXTERN_C void LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_marshal_com_back(const LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_marshaled_com& marshaled, LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535& unmarshaled)
{
	Exception_t* ___Camera_4Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'Camera' of type 'LeanScreenQuery': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___Camera_4Exception, NULL);
}
// Conversion method for clean up from marshalling of: Lean.Common.LeanScreenQuery
IL2CPP_EXTERN_C void LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_marshal_com_cleanup(LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_marshaled_com& marshaled)
{
}
// System.Void Lean.Common.LeanScreenQuery::.ctor(Lean.Common.LeanScreenQuery/MethodType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanScreenQuery__ctor_m59065C77ACB27AC2072C31D92B4D418EC9C60A85 (LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535* __this, int32_t ___0_newMethod, const RuntimeMethod* method) 
{
	{
		// public LeanScreenQuery(MethodType newMethod) : this(newMethod, Physics.DefaultRaycastLayers)
		int32_t L_0 = ___0_newMethod;
		LayerMask_t97CB6BDADEDC3D6423C7BCFEA7F86DA2EC6241DB L_1;
		L_1 = LayerMask_op_Implicit_m01C8996A2CB2085328B9C33539C43139660D8222(((int32_t)-5), NULL);
		LeanScreenQuery__ctor_m96FE813B432186035779FED836415F1E1E16B23A(__this, L_0, L_1, NULL);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void LeanScreenQuery__ctor_m59065C77ACB27AC2072C31D92B4D418EC9C60A85_AdjustorThunk (RuntimeObject* __this, int32_t ___0_newMethod, const RuntimeMethod* method)
{
	LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535*>(__this + _offset);
	LeanScreenQuery__ctor_m59065C77ACB27AC2072C31D92B4D418EC9C60A85(_thisAdjusted, ___0_newMethod, method);
}
// System.Void Lean.Common.LeanScreenQuery::.ctor(Lean.Common.LeanScreenQuery/MethodType,UnityEngine.LayerMask)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanScreenQuery__ctor_m96FE813B432186035779FED836415F1E1E16B23A (LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535* __this, int32_t ___0_newMethod, LayerMask_t97CB6BDADEDC3D6423C7BCFEA7F86DA2EC6241DB ___1_layers, const RuntimeMethod* method) 
{
	{
		// Method      = newMethod;
		int32_t L_0 = ___0_newMethod;
		__this->___Method_0 = L_0;
		// Search      = SearchType.GetComponentInParent;
		__this->___Search_2 = 1;
		// RequiredTag = null;
		__this->___RequiredTag_3 = (String_t*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___RequiredTag_3), (void*)(String_t*)NULL);
		// Camera      = null;
		__this->___Camera_4 = (Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___Camera_4), (void*)(Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184*)NULL);
		// Layers      = layers;
		LayerMask_t97CB6BDADEDC3D6423C7BCFEA7F86DA2EC6241DB L_1 = ___1_layers;
		__this->___Layers_1 = L_1;
		// Distance    = 50.0f;
		__this->___Distance_5 = (50.0f);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void LeanScreenQuery__ctor_m96FE813B432186035779FED836415F1E1E16B23A_AdjustorThunk (RuntimeObject* __this, int32_t ___0_newMethod, LayerMask_t97CB6BDADEDC3D6423C7BCFEA7F86DA2EC6241DB ___1_layers, const RuntimeMethod* method)
{
	LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535*>(__this + _offset);
	LeanScreenQuery__ctor_m96FE813B432186035779FED836415F1E1E16B23A(_thisAdjusted, ___0_newMethod, ___1_layers, method);
}
// System.Void Lean.Common.LeanScreenQuery::ChangeLayers(UnityEngine.GameObject,System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanScreenQuery_ChangeLayers_m0FE445BF645A2E5B913D4ABE847CCE70C1F87D25 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___0_root, bool ___1_ancestors, bool ___2_children, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KeyValuePair_2__ctor_m882B36949BE2FB86948D3198EA9BC21D5AED8330_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mB5F4B5AC39AACE323FD5679252895AE6CF289E08_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// tempLayers.Add(new KeyValuePair<GameObject, int>(root, root.layer));
		il2cpp_codegen_runtime_class_init_inline(LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var);
		List_1_tC2267FBE21298A38669F48AD07AE8540B7AA80AF* L_0 = ((LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_StaticFields*)il2cpp_codegen_static_fields_for(LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var))->___tempLayers_11;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1 = ___0_root;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = ___0_root;
		NullCheck(L_2);
		int32_t L_3;
		L_3 = GameObject_get_layer_m108902B9C89E9F837CE06B9942AA42307450FEAF(L_2, NULL);
		KeyValuePair_2_t9BD14587548B372830B82C49C122A2710251FD58 L_4;
		memset((&L_4), 0, sizeof(L_4));
		KeyValuePair_2__ctor_m882B36949BE2FB86948D3198EA9BC21D5AED8330((&L_4), L_1, L_3, /*hidden argument*/KeyValuePair_2__ctor_m882B36949BE2FB86948D3198EA9BC21D5AED8330_RuntimeMethod_var);
		NullCheck(L_0);
		List_1_Add_mB5F4B5AC39AACE323FD5679252895AE6CF289E08_inline(L_0, L_4, List_1_Add_mB5F4B5AC39AACE323FD5679252895AE6CF289E08_RuntimeMethod_var);
		// root.layer = 2; // Ignore raycast
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5 = ___0_root;
		NullCheck(L_5);
		GameObject_set_layer_m6E1AF478A2CC86BD222B96317BEB78B7D89B18D0(L_5, 2, NULL);
		// if (ancestors == true && root.transform.parent != null)
		bool L_6 = ___1_ancestors;
		if (!L_6)
		{
			goto IL_004a;
		}
	}
	{
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7 = ___0_root;
		NullCheck(L_7);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_8;
		L_8 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_7, NULL);
		NullCheck(L_8);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_9;
		L_9 = Transform_get_parent_m65354E28A4C94EC00EBCF03532F7B0718380791E(L_8, NULL);
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_10;
		L_10 = Object_op_Inequality_mD0BE578448EAA61948F25C32F8DD55AB1F778602(L_9, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_10)
		{
			goto IL_004a;
		}
	}
	{
		// ChangeLayers(root.transform.parent.gameObject, true, false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_11 = ___0_root;
		NullCheck(L_11);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_12;
		L_12 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_11, NULL);
		NullCheck(L_12);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_13;
		L_13 = Transform_get_parent_m65354E28A4C94EC00EBCF03532F7B0718380791E(L_12, NULL);
		NullCheck(L_13);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_14;
		L_14 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_13, NULL);
		il2cpp_codegen_runtime_class_init_inline(LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var);
		LeanScreenQuery_ChangeLayers_m0FE445BF645A2E5B913D4ABE847CCE70C1F87D25(L_14, (bool)1, (bool)0, NULL);
	}

IL_004a:
	{
		// if (children == true)
		bool L_15 = ___2_children;
		if (!L_15)
		{
			goto IL_007d;
		}
	}
	{
		// for (var i = root.transform.childCount - 1; i >= 0; i--)
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_16 = ___0_root;
		NullCheck(L_16);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_17;
		L_17 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_16, NULL);
		NullCheck(L_17);
		int32_t L_18;
		L_18 = Transform_get_childCount_mE9C29C702AB662CC540CA053EDE48BDAFA35B4B0(L_17, NULL);
		V_0 = ((int32_t)il2cpp_codegen_subtract(L_18, 1));
		goto IL_0079;
	}

IL_005d:
	{
		// ChangeLayers(root.transform.GetChild(i).gameObject, false, true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_19 = ___0_root;
		NullCheck(L_19);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_20;
		L_20 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_19, NULL);
		int32_t L_21 = V_0;
		NullCheck(L_20);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_22;
		L_22 = Transform_GetChild_mE686DF0C7AAC1F7AEF356967B1C04D8B8E240EAF(L_20, L_21, NULL);
		NullCheck(L_22);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_23;
		L_23 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_22, NULL);
		il2cpp_codegen_runtime_class_init_inline(LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var);
		LeanScreenQuery_ChangeLayers_m0FE445BF645A2E5B913D4ABE847CCE70C1F87D25(L_23, (bool)0, (bool)1, NULL);
		// for (var i = root.transform.childCount - 1; i >= 0; i--)
		int32_t L_24 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_subtract(L_24, 1));
	}

IL_0079:
	{
		// for (var i = root.transform.childCount - 1; i >= 0; i--)
		int32_t L_25 = V_0;
		if ((((int32_t)L_25) >= ((int32_t)0)))
		{
			goto IL_005d;
		}
	}

IL_007d:
	{
		// }
		return;
	}
}
// System.Void Lean.Common.LeanScreenQuery::RevertLayers()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanScreenQuery_RevertLayers_mC14E59AC2206E7FA05F7BC48F400DCC67858FA48 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m78D479DDD8F3214C93E9BE0B062995D6E182E773_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_m3C9B7035E15808178030F1D80430BAFA582C5C32_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m97354C43156F3E8FF03CC8D54C4151C9992C605C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KeyValuePair_2_get_Key_m3AB1FFB220CFA62C8913F62DF7F79F0D2F39A314_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KeyValuePair_2_get_Value_mB01CEBC5D69BFC0A878DB1ACBF3DE05B8EC1B964_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Clear_m6D7EC60334D1F90BC2538200034022903CF7DE50_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m552F956B853F080F1BB08A652725EAA51DE82680_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Enumerator_tE49F457C3EA7DD2E164F160F3A3D19CC62191D80 V_0;
	memset((&V_0), 0, sizeof(V_0));
	KeyValuePair_2_t9BD14587548B372830B82C49C122A2710251FD58 V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// foreach (var tempLayer in tempLayers)
		il2cpp_codegen_runtime_class_init_inline(LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var);
		List_1_tC2267FBE21298A38669F48AD07AE8540B7AA80AF* L_0 = ((LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_StaticFields*)il2cpp_codegen_static_fields_for(LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var))->___tempLayers_11;
		NullCheck(L_0);
		Enumerator_tE49F457C3EA7DD2E164F160F3A3D19CC62191D80 L_1;
		L_1 = List_1_GetEnumerator_m552F956B853F080F1BB08A652725EAA51DE82680(L_0, List_1_GetEnumerator_m552F956B853F080F1BB08A652725EAA51DE82680_RuntimeMethod_var);
		V_0 = L_1;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0042:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m78D479DDD8F3214C93E9BE0B062995D6E182E773((&V_0), Enumerator_Dispose_m78D479DDD8F3214C93E9BE0B062995D6E182E773_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_0037_1;
			}

IL_000d_1:
			{
				// foreach (var tempLayer in tempLayers)
				KeyValuePair_2_t9BD14587548B372830B82C49C122A2710251FD58 L_2;
				L_2 = Enumerator_get_Current_m97354C43156F3E8FF03CC8D54C4151C9992C605C_inline((&V_0), Enumerator_get_Current_m97354C43156F3E8FF03CC8D54C4151C9992C605C_RuntimeMethod_var);
				V_1 = L_2;
				// if (tempLayer.Key != null)
				GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
				L_3 = KeyValuePair_2_get_Key_m3AB1FFB220CFA62C8913F62DF7F79F0D2F39A314_inline((&V_1), KeyValuePair_2_get_Key_m3AB1FFB220CFA62C8913F62DF7F79F0D2F39A314_RuntimeMethod_var);
				il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
				bool L_4;
				L_4 = Object_op_Inequality_mD0BE578448EAA61948F25C32F8DD55AB1F778602(L_3, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
				if (!L_4)
				{
					goto IL_0037_1;
				}
			}
			{
				// tempLayer.Key.layer = tempLayer.Value;
				GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
				L_5 = KeyValuePair_2_get_Key_m3AB1FFB220CFA62C8913F62DF7F79F0D2F39A314_inline((&V_1), KeyValuePair_2_get_Key_m3AB1FFB220CFA62C8913F62DF7F79F0D2F39A314_RuntimeMethod_var);
				int32_t L_6;
				L_6 = KeyValuePair_2_get_Value_mB01CEBC5D69BFC0A878DB1ACBF3DE05B8EC1B964_inline((&V_1), KeyValuePair_2_get_Value_mB01CEBC5D69BFC0A878DB1ACBF3DE05B8EC1B964_RuntimeMethod_var);
				NullCheck(L_5);
				GameObject_set_layer_m6E1AF478A2CC86BD222B96317BEB78B7D89B18D0(L_5, L_6, NULL);
			}

IL_0037_1:
			{
				// foreach (var tempLayer in tempLayers)
				bool L_7;
				L_7 = Enumerator_MoveNext_m3C9B7035E15808178030F1D80430BAFA582C5C32((&V_0), Enumerator_MoveNext_m3C9B7035E15808178030F1D80430BAFA582C5C32_RuntimeMethod_var);
				if (L_7)
				{
					goto IL_000d_1;
				}
			}
			{
				goto IL_0050;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_0050:
	{
		// tempLayers.Clear();
		il2cpp_codegen_runtime_class_init_inline(LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var);
		List_1_tC2267FBE21298A38669F48AD07AE8540B7AA80AF* L_8 = ((LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_StaticFields*)il2cpp_codegen_static_fields_for(LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var))->___tempLayers_11;
		NullCheck(L_8);
		List_1_Clear_m6D7EC60334D1F90BC2538200034022903CF7DE50_inline(L_8, List_1_Clear_m6D7EC60334D1F90BC2538200034022903CF7DE50_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Int32 Lean.Common.LeanScreenQuery::GetClosestRaycastHitsIndex(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t LeanScreenQuery_GetClosestRaycastHitsIndex_mCA37AA7178583AABC833765E4D9434EBA5BC91E5 (int32_t ___0_count, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	float V_3 = 0.0f;
	{
		// var closestIndex    = -1;
		V_0 = (-1);
		// var closestDistance = float.PositiveInfinity;
		V_1 = (std::numeric_limits<float>::infinity());
		// for (var i = 0; i < count; i++)
		V_2 = 0;
		goto IL_0029;
	}

IL_000c:
	{
		// var distance = raycastHits[i].distance;
		il2cpp_codegen_runtime_class_init_inline(LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var);
		RaycastHitU5BU5D_t008B8309DE422FE7567068D743D68054D5EBF1A8* L_0 = ((LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_StaticFields*)il2cpp_codegen_static_fields_for(LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var))->___raycastHits_6;
		int32_t L_1 = V_2;
		NullCheck(L_0);
		float L_2;
		L_2 = RaycastHit_get_distance_m035194B0E9BB6229259CFC43B095A9C8E5011C78(((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1))), NULL);
		V_3 = L_2;
		// if (distance < closestDistance)
		float L_3 = V_3;
		float L_4 = V_1;
		if ((!(((float)L_3) < ((float)L_4))))
		{
			goto IL_0025;
		}
	}
	{
		// closestIndex    = i;
		int32_t L_5 = V_2;
		V_0 = L_5;
		// closestDistance = distance;
		float L_6 = V_3;
		V_1 = L_6;
	}

IL_0025:
	{
		// for (var i = 0; i < count; i++)
		int32_t L_7 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add(L_7, 1));
	}

IL_0029:
	{
		// for (var i = 0; i < count; i++)
		int32_t L_8 = V_2;
		int32_t L_9 = ___0_count;
		if ((((int32_t)L_8) < ((int32_t)L_9)))
		{
			goto IL_000c;
		}
	}
	{
		// return closestIndex;
		int32_t L_10 = V_0;
		return L_10;
	}
}
// System.Void Lean.Common.LeanScreenQuery::DoRaycast3D(UnityEngine.Camera,UnityEngine.Vector2,UnityEngine.Component&,System.Single&,UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanScreenQuery_DoRaycast3D_m79EEB1F96BF3BB4CC4BCB70E9F1667E4CA7D993E (LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535* __this, Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* ___0_camera, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___1_screenPosition, Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3** ___2_bestResult, float* ___3_bestDistance, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* ___4_bestPosition, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RaycastHit_t6F30BD0B38B56401CA833A1B87BD74F2ACD2F2B5 V_1;
	memset((&V_1), 0, sizeof(V_1));
	float V_2 = 0.0f;
	{
		// var ray   = camera.ScreenPointToRay(screenPosition);
		Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* L_0 = ___0_camera;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_1 = ___1_screenPosition;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2;
		L_2 = Vector2_op_Implicit_m6D9CABB2C791A192867D7A4559D132BE86DD3EB7_inline(L_1, NULL);
		NullCheck(L_0);
		Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 L_3;
		L_3 = Camera_ScreenPointToRay_m2887B9A49880B7AB670C57D66B67D6A6689FE315(L_0, L_2, NULL);
		// var count = Physics.RaycastNonAlloc(ray, raycastHits, float.PositiveInfinity, Layers);
		il2cpp_codegen_runtime_class_init_inline(LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var);
		RaycastHitU5BU5D_t008B8309DE422FE7567068D743D68054D5EBF1A8* L_4 = ((LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_StaticFields*)il2cpp_codegen_static_fields_for(LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var))->___raycastHits_6;
		LayerMask_t97CB6BDADEDC3D6423C7BCFEA7F86DA2EC6241DB L_5 = __this->___Layers_1;
		int32_t L_6;
		L_6 = LayerMask_op_Implicit_m7F5A5B9D079281AC445ED39DEE1FCFA9D795810D(L_5, NULL);
		int32_t L_7;
		L_7 = Physics_RaycastNonAlloc_m2BFEE9072E390ED6ACD500FD0AE4E714DE9549BC(L_3, L_4, (std::numeric_limits<float>::infinity()), L_6, NULL);
		V_0 = L_7;
		// if (count > 0)
		int32_t L_8 = V_0;
		if ((((int32_t)L_8) <= ((int32_t)0)))
		{
			goto IL_0065;
		}
	}
	{
		// var closestHit = raycastHits[GetClosestRaycastHitsIndex(count)];
		il2cpp_codegen_runtime_class_init_inline(LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var);
		RaycastHitU5BU5D_t008B8309DE422FE7567068D743D68054D5EBF1A8* L_9 = ((LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_StaticFields*)il2cpp_codegen_static_fields_for(LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var))->___raycastHits_6;
		int32_t L_10 = V_0;
		int32_t L_11;
		L_11 = LeanScreenQuery_GetClosestRaycastHitsIndex_mCA37AA7178583AABC833765E4D9434EBA5BC91E5(L_10, NULL);
		NullCheck(L_9);
		int32_t L_12 = L_11;
		RaycastHit_t6F30BD0B38B56401CA833A1B87BD74F2ACD2F2B5 L_13 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		V_1 = L_13;
		// var distance   = closestHit.distance;
		float L_14;
		L_14 = RaycastHit_get_distance_m035194B0E9BB6229259CFC43B095A9C8E5011C78((&V_1), NULL);
		V_2 = L_14;
		// if (distance < bestDistance)
		float L_15 = V_2;
		float* L_16 = ___3_bestDistance;
		float L_17 = *((float*)L_16);
		if ((!(((float)L_15) < ((float)L_17))))
		{
			goto IL_0065;
		}
	}
	{
		// bestDistance = distance;
		float* L_18 = ___3_bestDistance;
		float L_19 = V_2;
		*((float*)L_18) = (float)L_19;
		// bestResult   = closestHit.collider;
		Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3** L_20 = ___2_bestResult;
		Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* L_21;
		L_21 = RaycastHit_get_collider_m84B160439BBEAB6D9E94B799F720E25C9E2D444D((&V_1), NULL);
		*((RuntimeObject**)L_20) = (RuntimeObject*)L_21;
		Il2CppCodeGenWriteBarrier((void**)(RuntimeObject**)L_20, (void*)(RuntimeObject*)L_21);
		// bestPosition = closestHit.point;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_22 = ___4_bestPosition;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_23;
		L_23 = RaycastHit_get_point_m02B764612562AFE0F998CC7CFB2EEDE41BA47F39((&V_1), NULL);
		*(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*)L_22 = L_23;
	}

IL_0065:
	{
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void LeanScreenQuery_DoRaycast3D_m79EEB1F96BF3BB4CC4BCB70E9F1667E4CA7D993E_AdjustorThunk (RuntimeObject* __this, Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* ___0_camera, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___1_screenPosition, Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3** ___2_bestResult, float* ___3_bestDistance, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* ___4_bestPosition, const RuntimeMethod* method)
{
	LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535*>(__this + _offset);
	LeanScreenQuery_DoRaycast3D_m79EEB1F96BF3BB4CC4BCB70E9F1667E4CA7D993E(_thisAdjusted, ___0_camera, ___1_screenPosition, ___2_bestResult, ___3_bestDistance, ___4_bestPosition, method);
}
// System.Void Lean.Common.LeanScreenQuery::DoRaycast2D(UnityEngine.Camera,UnityEngine.Vector2,UnityEngine.Component&,System.Single&,UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanScreenQuery_DoRaycast2D_m57CB1F6A5464D8D88EB1E5E83DA7BA15BAB854B7 (LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535* __this, Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* ___0_camera, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___1_screenPosition, Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3** ___2_bestResult, float* ___3_bestDistance, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* ___4_bestPosition, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Physics2D_t64C0DB5246067DAC2E83A52558A0AC68AF3BE94D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA V_0;
	memset((&V_0), 0, sizeof(V_0));
	float V_1 = 0.0f;
	{
		// var ray   = camera.ScreenPointToRay(screenPosition);
		Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* L_0 = ___0_camera;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_1 = ___1_screenPosition;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2;
		L_2 = Vector2_op_Implicit_m6D9CABB2C791A192867D7A4559D132BE86DD3EB7_inline(L_1, NULL);
		NullCheck(L_0);
		Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 L_3;
		L_3 = Camera_ScreenPointToRay_m2887B9A49880B7AB670C57D66B67D6A6689FE315(L_0, L_2, NULL);
		// var count = Physics2D.GetRayIntersectionNonAlloc(ray, raycastHit2Ds, float.PositiveInfinity, Layers);
		il2cpp_codegen_runtime_class_init_inline(LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var);
		RaycastHit2DU5BU5D_t28739C686586993113318B63C84927FD43063FC7* L_4 = ((LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_StaticFields*)il2cpp_codegen_static_fields_for(LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var))->___raycastHit2Ds_7;
		LayerMask_t97CB6BDADEDC3D6423C7BCFEA7F86DA2EC6241DB L_5 = __this->___Layers_1;
		int32_t L_6;
		L_6 = LayerMask_op_Implicit_m7F5A5B9D079281AC445ED39DEE1FCFA9D795810D(L_5, NULL);
		il2cpp_codegen_runtime_class_init_inline(Physics2D_t64C0DB5246067DAC2E83A52558A0AC68AF3BE94D_il2cpp_TypeInfo_var);
		int32_t L_7;
		L_7 = Physics2D_GetRayIntersectionNonAlloc_mB7942B73C8B86F369262FC3B87F080132E7A369C(L_3, L_4, (std::numeric_limits<float>::infinity()), L_6, NULL);
		// if (count > 0)
		if ((((int32_t)L_7) <= ((int32_t)0)))
		{
			goto IL_0063;
		}
	}
	{
		// var closestHit = raycastHit2Ds[0];
		il2cpp_codegen_runtime_class_init_inline(LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var);
		RaycastHit2DU5BU5D_t28739C686586993113318B63C84927FD43063FC7* L_8 = ((LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_StaticFields*)il2cpp_codegen_static_fields_for(LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var))->___raycastHit2Ds_7;
		NullCheck(L_8);
		int32_t L_9 = 0;
		RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		V_0 = L_10;
		// var distance   = closestHit.distance;
		float L_11;
		L_11 = RaycastHit2D_get_distance_mD0FE1482E2768CF587AFB65488459697EAB64613((&V_0), NULL);
		V_1 = L_11;
		// if (distance < bestDistance)
		float L_12 = V_1;
		float* L_13 = ___3_bestDistance;
		float L_14 = *((float*)L_13);
		if ((!(((float)L_12) < ((float)L_14))))
		{
			goto IL_0063;
		}
	}
	{
		// bestDistance = distance;
		float* L_15 = ___3_bestDistance;
		float L_16 = V_1;
		*((float*)L_15) = (float)L_16;
		// bestResult   = closestHit.transform;
		Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3** L_17 = ___2_bestResult;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_18;
		L_18 = RaycastHit2D_get_transform_mA5E3F8DC9914E79D3C9F6F3F2515B49EEBB4564A((&V_0), NULL);
		*((RuntimeObject**)L_17) = (RuntimeObject*)L_18;
		Il2CppCodeGenWriteBarrier((void**)(RuntimeObject**)L_17, (void*)(RuntimeObject*)L_18);
		// bestPosition = closestHit.point;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_19 = ___4_bestPosition;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_20;
		L_20 = RaycastHit2D_get_point_mB35E988E9E04328EFE926228A18334326721A36B((&V_0), NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_21;
		L_21 = Vector2_op_Implicit_m6D9CABB2C791A192867D7A4559D132BE86DD3EB7_inline(L_20, NULL);
		*(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*)L_19 = L_21;
	}

IL_0063:
	{
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void LeanScreenQuery_DoRaycast2D_m57CB1F6A5464D8D88EB1E5E83DA7BA15BAB854B7_AdjustorThunk (RuntimeObject* __this, Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* ___0_camera, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___1_screenPosition, Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3** ___2_bestResult, float* ___3_bestDistance, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* ___4_bestPosition, const RuntimeMethod* method)
{
	LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535*>(__this + _offset);
	LeanScreenQuery_DoRaycast2D_m57CB1F6A5464D8D88EB1E5E83DA7BA15BAB854B7(_thisAdjusted, ___0_camera, ___1_screenPosition, ___2_bestResult, ___3_bestDistance, ___4_bestPosition, method);
}
// System.Void Lean.Common.LeanScreenQuery::DoRaycastUI(UnityEngine.Vector2,UnityEngine.Component&,System.Single&,UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanScreenQuery_DoRaycastUI_m82C6B1269710BCB6377C4F1D25B4B4351DB89B99 (LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___0_screenPosition, Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3** ___1_bestResult, float* ___2_bestDistance, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* ___3_bestPosition, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_mE3FB1E26BA0B10EAB4C06CC56F1C78002726865C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_m22E22E0108683303C9E8F060E9C970D6AECF313C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m53EF02F206B7F773103FC51D50AC1B974AE692F4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventSystem_t61C51380B105BE9D2C39C4F15B7E655659957707_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m5032C7535D90065B4FA0A5942E7F92F4D72B8D41_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_FindObjectOfType_TisEventSystem_t61C51380B105BE9D2C39C4F15B7E655659957707_m35D4A88CE80EF52117B3256977C521D1E9F2E7E4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PointerEventData_t9670F3C7D823CCB738A1604C72A1EB90292396FB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EventSystem_t61C51380B105BE9D2C39C4F15B7E655659957707* V_0 = NULL;
	Enumerator_tFEA671794CD7ED9545DB6E9B1D2E744410E5EB1C V_1;
	memset((&V_1), 0, sizeof(V_1));
	RaycastResult_tEC6A7B7CABA99C386F054F01E498AEC426CF8023 V_2;
	memset((&V_2), 0, sizeof(V_2));
	float V_3 = 0.0f;
	{
		// var currentEventSystem = EventSystem.current;
		il2cpp_codegen_runtime_class_init_inline(EventSystem_t61C51380B105BE9D2C39C4F15B7E655659957707_il2cpp_TypeInfo_var);
		EventSystem_t61C51380B105BE9D2C39C4F15B7E655659957707* L_0;
		L_0 = EventSystem_get_current_mC87C69FB418563DC2A571A10E2F9DB59A6785016(NULL);
		V_0 = L_0;
		// if (currentEventSystem == null)
		EventSystem_t61C51380B105BE9D2C39C4F15B7E655659957707* L_1 = V_0;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Equality_mB6120F782D83091EF56A198FCEBCF066DB4A9605(L_1, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		// currentEventSystem = Object.FindObjectOfType<EventSystem>();
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		EventSystem_t61C51380B105BE9D2C39C4F15B7E655659957707* L_3;
		L_3 = Object_FindObjectOfType_TisEventSystem_t61C51380B105BE9D2C39C4F15B7E655659957707_m35D4A88CE80EF52117B3256977C521D1E9F2E7E4(Object_FindObjectOfType_TisEventSystem_t61C51380B105BE9D2C39C4F15B7E655659957707_m35D4A88CE80EF52117B3256977C521D1E9F2E7E4_RuntimeMethod_var);
		V_0 = L_3;
	}

IL_0015:
	{
		// if (currentEventSystem != null)
		EventSystem_t61C51380B105BE9D2C39C4F15B7E655659957707* L_4 = V_0;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Inequality_mD0BE578448EAA61948F25C32F8DD55AB1F778602(L_4, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_5)
		{
			goto IL_00ea;
		}
	}
	{
		// if (currentEventSystem != tempEventSystem)
		EventSystem_t61C51380B105BE9D2C39C4F15B7E655659957707* L_6 = V_0;
		il2cpp_codegen_runtime_class_init_inline(LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var);
		EventSystem_t61C51380B105BE9D2C39C4F15B7E655659957707* L_7 = ((LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_StaticFields*)il2cpp_codegen_static_fields_for(LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var))->___tempEventSystem_10;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_8;
		L_8 = Object_op_Inequality_mD0BE578448EAA61948F25C32F8DD55AB1F778602(L_6, L_7, NULL);
		if (!L_8)
		{
			goto IL_0056;
		}
	}
	{
		// tempEventSystem = currentEventSystem;
		EventSystem_t61C51380B105BE9D2C39C4F15B7E655659957707* L_9 = V_0;
		il2cpp_codegen_runtime_class_init_inline(LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var);
		((LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_StaticFields*)il2cpp_codegen_static_fields_for(LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var))->___tempEventSystem_10 = L_9;
		Il2CppCodeGenWriteBarrier((void**)(&((LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_StaticFields*)il2cpp_codegen_static_fields_for(LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var))->___tempEventSystem_10), (void*)L_9);
		// if (tempPointerEventData == null)
		PointerEventData_t9670F3C7D823CCB738A1604C72A1EB90292396FB* L_10 = ((LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_StaticFields*)il2cpp_codegen_static_fields_for(LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var))->___tempPointerEventData_9;
		if (L_10)
		{
			goto IL_004c;
		}
	}
	{
		// tempPointerEventData = new PointerEventData(tempEventSystem);
		il2cpp_codegen_runtime_class_init_inline(LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var);
		EventSystem_t61C51380B105BE9D2C39C4F15B7E655659957707* L_11 = ((LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_StaticFields*)il2cpp_codegen_static_fields_for(LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var))->___tempEventSystem_10;
		PointerEventData_t9670F3C7D823CCB738A1604C72A1EB90292396FB* L_12 = (PointerEventData_t9670F3C7D823CCB738A1604C72A1EB90292396FB*)il2cpp_codegen_object_new(PointerEventData_t9670F3C7D823CCB738A1604C72A1EB90292396FB_il2cpp_TypeInfo_var);
		NullCheck(L_12);
		PointerEventData__ctor_m63837790B68893F0022CCEFEF26ADD55A975F71C(L_12, L_11, NULL);
		((LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_StaticFields*)il2cpp_codegen_static_fields_for(LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var))->___tempPointerEventData_9 = L_12;
		Il2CppCodeGenWriteBarrier((void**)(&((LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_StaticFields*)il2cpp_codegen_static_fields_for(LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var))->___tempPointerEventData_9), (void*)L_12);
		goto IL_0056;
	}

IL_004c:
	{
		// tempPointerEventData.Reset();
		il2cpp_codegen_runtime_class_init_inline(LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var);
		PointerEventData_t9670F3C7D823CCB738A1604C72A1EB90292396FB* L_13 = ((LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_StaticFields*)il2cpp_codegen_static_fields_for(LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var))->___tempPointerEventData_9;
		NullCheck(L_13);
		VirtualActionInvoker0::Invoke(4 /* System.Void UnityEngine.EventSystems.AbstractEventData::Reset() */, L_13);
	}

IL_0056:
	{
		// tempPointerEventData.position = screenPosition;
		il2cpp_codegen_runtime_class_init_inline(LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var);
		PointerEventData_t9670F3C7D823CCB738A1604C72A1EB90292396FB* L_14 = ((LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_StaticFields*)il2cpp_codegen_static_fields_for(LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var))->___tempPointerEventData_9;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_15 = ___0_screenPosition;
		NullCheck(L_14);
		PointerEventData_set_position_m66E8DFE693F550372E6B085C6E2F887FDB092FAA_inline(L_14, L_15, NULL);
		// currentEventSystem.RaycastAll(tempPointerEventData, tempRaycastResults);
		EventSystem_t61C51380B105BE9D2C39C4F15B7E655659957707* L_16 = V_0;
		PointerEventData_t9670F3C7D823CCB738A1604C72A1EB90292396FB* L_17 = ((LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_StaticFields*)il2cpp_codegen_static_fields_for(LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var))->___tempPointerEventData_9;
		List_1_t8292C421BBB00D7661DC07462822936152BAB446* L_18 = ((LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_StaticFields*)il2cpp_codegen_static_fields_for(LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var))->___tempRaycastResults_8;
		NullCheck(L_16);
		EventSystem_RaycastAll_mE93CC75909438D20D17A0EF98348A064FBFEA528(L_16, L_17, L_18, NULL);
		// foreach (var result in tempRaycastResults)
		List_1_t8292C421BBB00D7661DC07462822936152BAB446* L_19 = ((LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_StaticFields*)il2cpp_codegen_static_fields_for(LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var))->___tempRaycastResults_8;
		NullCheck(L_19);
		Enumerator_tFEA671794CD7ED9545DB6E9B1D2E744410E5EB1C L_20;
		L_20 = List_1_GetEnumerator_m5032C7535D90065B4FA0A5942E7F92F4D72B8D41(L_19, List_1_GetEnumerator_m5032C7535D90065B4FA0A5942E7F92F4D72B8D41_RuntimeMethod_var);
		V_1 = L_20;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_00dc:
			{// begin finally (depth: 1)
				Enumerator_Dispose_mE3FB1E26BA0B10EAB4C06CC56F1C78002726865C((&V_1), Enumerator_Dispose_mE3FB1E26BA0B10EAB4C06CC56F1C78002726865C_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_00d1_1;
			}

IL_007e_1:
			{
				// foreach (var result in tempRaycastResults)
				RaycastResult_tEC6A7B7CABA99C386F054F01E498AEC426CF8023 L_21;
				L_21 = Enumerator_get_Current_m53EF02F206B7F773103FC51D50AC1B974AE692F4_inline((&V_1), Enumerator_get_Current_m53EF02F206B7F773103FC51D50AC1B974AE692F4_RuntimeMethod_var);
				V_2 = L_21;
				// var resultLayer = 1 << result.gameObject.layer;
				GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_22;
				L_22 = RaycastResult_get_gameObject_m77014B442B9E2D10F2CC3AEEDC07AA95CDE1E2F1_inline((&V_2), NULL);
				NullCheck(L_22);
				int32_t L_23;
				L_23 = GameObject_get_layer_m108902B9C89E9F837CE06B9942AA42307450FEAF(L_22, NULL);
				// if ((resultLayer & Layers) != 0)
				LayerMask_t97CB6BDADEDC3D6423C7BCFEA7F86DA2EC6241DB L_24 = __this->___Layers_1;
				int32_t L_25;
				L_25 = LayerMask_op_Implicit_m7F5A5B9D079281AC445ED39DEE1FCFA9D795810D(L_24, NULL);
				if (!((int32_t)(((int32_t)(1<<((int32_t)(L_23&((int32_t)31)))))&L_25)))
				{
					goto IL_00d1_1;
				}
			}
			{
				// var distance = result.distance;
				RaycastResult_tEC6A7B7CABA99C386F054F01E498AEC426CF8023 L_26 = V_2;
				float L_27 = L_26.___distance_2;
				V_3 = L_27;
				// if (distance < bestDistance)
				float L_28 = V_3;
				float* L_29 = ___2_bestDistance;
				float L_30 = *((float*)L_29);
				if ((!(((float)L_28) < ((float)L_30))))
				{
					goto IL_00da_1;
				}
			}
			{
				// bestDistance = distance;
				float* L_31 = ___2_bestDistance;
				float L_32 = V_3;
				*((float*)L_31) = (float)L_32;
				// bestResult   = result.gameObject.transform;
				Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3** L_33 = ___1_bestResult;
				GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_34;
				L_34 = RaycastResult_get_gameObject_m77014B442B9E2D10F2CC3AEEDC07AA95CDE1E2F1_inline((&V_2), NULL);
				NullCheck(L_34);
				Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_35;
				L_35 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_34, NULL);
				*((RuntimeObject**)L_33) = (RuntimeObject*)L_35;
				Il2CppCodeGenWriteBarrier((void**)(RuntimeObject**)L_33, (void*)(RuntimeObject*)L_35);
				// bestPosition = result.worldPosition;
				Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_36 = ___3_bestPosition;
				RaycastResult_tEC6A7B7CABA99C386F054F01E498AEC426CF8023 L_37 = V_2;
				Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_38 = L_37.___worldPosition_7;
				*(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*)L_36 = L_38;
				// break;
				goto IL_00ea;
			}

IL_00d1_1:
			{
				// foreach (var result in tempRaycastResults)
				bool L_39;
				L_39 = Enumerator_MoveNext_m22E22E0108683303C9E8F060E9C970D6AECF313C((&V_1), Enumerator_MoveNext_m22E22E0108683303C9E8F060E9C970D6AECF313C_RuntimeMethod_var);
				if (L_39)
				{
					goto IL_007e_1;
				}
			}

IL_00da_1:
			{
				goto IL_00ea;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_00ea:
	{
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void LeanScreenQuery_DoRaycastUI_m82C6B1269710BCB6377C4F1D25B4B4351DB89B99_AdjustorThunk (RuntimeObject* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___0_screenPosition, Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3** ___1_bestResult, float* ___2_bestDistance, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* ___3_bestPosition, const RuntimeMethod* method)
{
	LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535* _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535*>(__this + _offset);
	LeanScreenQuery_DoRaycastUI_m82C6B1269710BCB6377C4F1D25B4B4351DB89B99(_thisAdjusted, ___0_screenPosition, ___1_bestResult, ___2_bestDistance, ___3_bestPosition, method);
}
// System.Void Lean.Common.LeanScreenQuery::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeanScreenQuery__cctor_m14B930C96207D6A19AB70FBC4871869A6005184D (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m50607A2028B005421BBF8F137494E6877D6CAE5A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mB89D13B8B739042E3841C5DFAF30C3C51A797EA7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t8292C421BBB00D7661DC07462822936152BAB446_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_tC2267FBE21298A38669F48AD07AE8540B7AA80AF_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RaycastHit2DU5BU5D_t28739C686586993113318B63C84927FD43063FC7_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RaycastHitU5BU5D_t008B8309DE422FE7567068D743D68054D5EBF1A8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private static RaycastHit[] raycastHits = new RaycastHit[1024];
		RaycastHitU5BU5D_t008B8309DE422FE7567068D743D68054D5EBF1A8* L_0 = (RaycastHitU5BU5D_t008B8309DE422FE7567068D743D68054D5EBF1A8*)(RaycastHitU5BU5D_t008B8309DE422FE7567068D743D68054D5EBF1A8*)SZArrayNew(RaycastHitU5BU5D_t008B8309DE422FE7567068D743D68054D5EBF1A8_il2cpp_TypeInfo_var, (uint32_t)((int32_t)1024));
		((LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_StaticFields*)il2cpp_codegen_static_fields_for(LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var))->___raycastHits_6 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&((LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_StaticFields*)il2cpp_codegen_static_fields_for(LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var))->___raycastHits_6), (void*)L_0);
		// private static RaycastHit2D[] raycastHit2Ds = new RaycastHit2D[1024];
		RaycastHit2DU5BU5D_t28739C686586993113318B63C84927FD43063FC7* L_1 = (RaycastHit2DU5BU5D_t28739C686586993113318B63C84927FD43063FC7*)(RaycastHit2DU5BU5D_t28739C686586993113318B63C84927FD43063FC7*)SZArrayNew(RaycastHit2DU5BU5D_t28739C686586993113318B63C84927FD43063FC7_il2cpp_TypeInfo_var, (uint32_t)((int32_t)1024));
		((LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_StaticFields*)il2cpp_codegen_static_fields_for(LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var))->___raycastHit2Ds_7 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&((LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_StaticFields*)il2cpp_codegen_static_fields_for(LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var))->___raycastHit2Ds_7), (void*)L_1);
		// private static List<RaycastResult> tempRaycastResults = new List<RaycastResult>(10);
		List_1_t8292C421BBB00D7661DC07462822936152BAB446* L_2 = (List_1_t8292C421BBB00D7661DC07462822936152BAB446*)il2cpp_codegen_object_new(List_1_t8292C421BBB00D7661DC07462822936152BAB446_il2cpp_TypeInfo_var);
		NullCheck(L_2);
		List_1__ctor_mB89D13B8B739042E3841C5DFAF30C3C51A797EA7(L_2, ((int32_t)10), List_1__ctor_mB89D13B8B739042E3841C5DFAF30C3C51A797EA7_RuntimeMethod_var);
		((LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_StaticFields*)il2cpp_codegen_static_fields_for(LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var))->___tempRaycastResults_8 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&((LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_StaticFields*)il2cpp_codegen_static_fields_for(LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var))->___tempRaycastResults_8), (void*)L_2);
		// private static List<KeyValuePair<GameObject, int>> tempLayers = new List<KeyValuePair<GameObject, int>>();
		List_1_tC2267FBE21298A38669F48AD07AE8540B7AA80AF* L_3 = (List_1_tC2267FBE21298A38669F48AD07AE8540B7AA80AF*)il2cpp_codegen_object_new(List_1_tC2267FBE21298A38669F48AD07AE8540B7AA80AF_il2cpp_TypeInfo_var);
		NullCheck(L_3);
		List_1__ctor_m50607A2028B005421BBF8F137494E6877D6CAE5A(L_3, List_1__ctor_m50607A2028B005421BBF8F137494E6877D6CAE5A_RuntimeMethod_var);
		((LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_StaticFields*)il2cpp_codegen_static_fields_for(LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var))->___tempLayers_11 = L_3;
		Il2CppCodeGenWriteBarrier((void**)(&((LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_StaticFields*)il2cpp_codegen_static_fields_for(LeanScreenQuery_tA3D7FF2B5B7C1C648410E1B098DCF83F7BF0F535_il2cpp_TypeInfo_var))->___tempLayers_11), (void*)L_3);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Mathf_Clamp_m4DC36EEFDBE5F07C16249DA568023C5ECCFF0E7B_inline (int32_t ___0_value, int32_t ___1_min, int32_t ___2_max, const RuntimeMethod* method) 
{
	bool V_0 = false;
	bool V_1 = false;
	int32_t V_2 = 0;
	{
		int32_t L_0 = ___0_value;
		int32_t L_1 = ___1_min;
		V_0 = (bool)((((int32_t)L_0) < ((int32_t)L_1))? 1 : 0);
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_3 = ___1_min;
		___0_value = L_3;
		goto IL_0019;
	}

IL_000e:
	{
		int32_t L_4 = ___0_value;
		int32_t L_5 = ___2_max;
		V_1 = (bool)((((int32_t)L_4) > ((int32_t)L_5))? 1 : 0);
		bool L_6 = V_1;
		if (!L_6)
		{
			goto IL_0019;
		}
	}
	{
		int32_t L_7 = ___2_max;
		___0_value = L_7;
	}

IL_0019:
	{
		int32_t L_8 = ___0_value;
		V_2 = L_8;
		goto IL_001d;
	}

IL_001d:
	{
		int32_t L_9 = V_2;
		return L_9;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_Subtraction_mE42023FF80067CB44A1D4A27EB7CF2B24CABB828_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_a, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___1_b, const RuntimeMethod* method) 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___0_a;
		float L_1 = L_0.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2 = ___1_b;
		float L_3 = L_2.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4 = ___0_a;
		float L_5 = L_4.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6 = ___1_b;
		float L_7 = L_6.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8 = ___0_a;
		float L_9 = L_8.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = ___1_b;
		float L_11 = L_10.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_12), ((float)il2cpp_codegen_subtract(L_1, L_3)), ((float)il2cpp_codegen_subtract(L_5, L_7)), ((float)il2cpp_codegen_subtract(L_9, L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector3_Distance_m2314DB9B8BD01157E013DF87BEA557375C7F9FF9_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_a, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___1_b, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___0_a;
		float L_1 = L_0.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2 = ___1_b;
		float L_3 = L_2.___x_2;
		V_0 = ((float)il2cpp_codegen_subtract(L_1, L_3));
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4 = ___0_a;
		float L_5 = L_4.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6 = ___1_b;
		float L_7 = L_6.___y_3;
		V_1 = ((float)il2cpp_codegen_subtract(L_5, L_7));
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8 = ___0_a;
		float L_9 = L_8.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = ___1_b;
		float L_11 = L_10.___z_4;
		V_2 = ((float)il2cpp_codegen_subtract(L_9, L_11));
		float L_12 = V_0;
		float L_13 = V_0;
		float L_14 = V_1;
		float L_15 = V_1;
		float L_16 = V_2;
		float L_17 = V_2;
		il2cpp_codegen_runtime_class_init_inline(Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		double L_18;
		L_18 = sqrt(((double)((float)il2cpp_codegen_add(((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(L_12, L_13)), ((float)il2cpp_codegen_multiply(L_14, L_15)))), ((float)il2cpp_codegen_multiply(L_16, L_17))))));
		V_3 = ((float)L_18);
		goto IL_0040;
	}

IL_0040:
	{
		float L_19 = V_3;
		return L_19;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_Normalize_mEF8349CC39674236CFC694189AFD36E31F89AC8F_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_value, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	bool V_1 = false;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___0_value;
		float L_1;
		L_1 = Vector3_Magnitude_m21652D951393A3D7CE92CE40049A0E7F76544D1B_inline(L_0, NULL);
		V_0 = L_1;
		float L_2 = V_0;
		V_1 = (bool)((((float)L_2) > ((float)(9.99999975E-06f)))? 1 : 0);
		bool L_3 = V_1;
		if (!L_3)
		{
			goto IL_001e;
		}
	}
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4 = ___0_value;
		float L_5 = V_0;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6;
		L_6 = Vector3_op_Division_mCC6BB24E372AB96B8380D1678446EF6A8BAE13BB_inline(L_4, L_5, NULL);
		V_2 = L_6;
		goto IL_0026;
	}

IL_001e:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_7;
		L_7 = Vector3_get_zero_m0C1249C3F25B1C70EAD3CC8B31259975A457AE39_inline(NULL);
		V_2 = L_7;
		goto IL_0026;
	}

IL_0026:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8 = V_2;
		return L_8;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_MoveTowards_m0363264647799F3173AC37F8E819F98298249B08_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_current, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___1_target, float ___2_maxDistanceDelta, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	bool V_5 = false;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_6;
	memset((&V_6), 0, sizeof(V_6));
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___1_target;
		float L_1 = L_0.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2 = ___0_current;
		float L_3 = L_2.___x_2;
		V_0 = ((float)il2cpp_codegen_subtract(L_1, L_3));
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4 = ___1_target;
		float L_5 = L_4.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6 = ___0_current;
		float L_7 = L_6.___y_3;
		V_1 = ((float)il2cpp_codegen_subtract(L_5, L_7));
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8 = ___1_target;
		float L_9 = L_8.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = ___0_current;
		float L_11 = L_10.___z_4;
		V_2 = ((float)il2cpp_codegen_subtract(L_9, L_11));
		float L_12 = V_0;
		float L_13 = V_0;
		float L_14 = V_1;
		float L_15 = V_1;
		float L_16 = V_2;
		float L_17 = V_2;
		V_3 = ((float)il2cpp_codegen_add(((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(L_12, L_13)), ((float)il2cpp_codegen_multiply(L_14, L_15)))), ((float)il2cpp_codegen_multiply(L_16, L_17))));
		float L_18 = V_3;
		if ((((float)L_18) == ((float)(0.0f))))
		{
			goto IL_0055;
		}
	}
	{
		float L_19 = ___2_maxDistanceDelta;
		if ((!(((float)L_19) >= ((float)(0.0f)))))
		{
			goto IL_0052;
		}
	}
	{
		float L_20 = V_3;
		float L_21 = ___2_maxDistanceDelta;
		float L_22 = ___2_maxDistanceDelta;
		G_B4_0 = ((((int32_t)((!(((float)L_20) <= ((float)((float)il2cpp_codegen_multiply(L_21, L_22)))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0053;
	}

IL_0052:
	{
		G_B4_0 = 0;
	}

IL_0053:
	{
		G_B6_0 = G_B4_0;
		goto IL_0056;
	}

IL_0055:
	{
		G_B6_0 = 1;
	}

IL_0056:
	{
		V_5 = (bool)G_B6_0;
		bool L_23 = V_5;
		if (!L_23)
		{
			goto IL_0061;
		}
	}
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_24 = ___1_target;
		V_6 = L_24;
		goto IL_009b;
	}

IL_0061:
	{
		float L_25 = V_3;
		il2cpp_codegen_runtime_class_init_inline(Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		double L_26;
		L_26 = sqrt(((double)L_25));
		V_4 = ((float)L_26);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_27 = ___0_current;
		float L_28 = L_27.___x_2;
		float L_29 = V_0;
		float L_30 = V_4;
		float L_31 = ___2_maxDistanceDelta;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_32 = ___0_current;
		float L_33 = L_32.___y_3;
		float L_34 = V_1;
		float L_35 = V_4;
		float L_36 = ___2_maxDistanceDelta;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_37 = ___0_current;
		float L_38 = L_37.___z_4;
		float L_39 = V_2;
		float L_40 = V_4;
		float L_41 = ___2_maxDistanceDelta;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_42;
		memset((&L_42), 0, sizeof(L_42));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_42), ((float)il2cpp_codegen_add(L_28, ((float)il2cpp_codegen_multiply(((float)(L_29/L_30)), L_31)))), ((float)il2cpp_codegen_add(L_33, ((float)il2cpp_codegen_multiply(((float)(L_34/L_35)), L_36)))), ((float)il2cpp_codegen_add(L_38, ((float)il2cpp_codegen_multiply(((float)(L_39/L_40)), L_41)))), /*hidden argument*/NULL);
		V_6 = L_42;
		goto IL_009b;
	}

IL_009b:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_43 = V_6;
		return L_43;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector3_Dot_mBB86BB940AA0A32FA7D3C02AC42E5BC7095A5D52_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_lhs, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___1_rhs, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___0_lhs;
		float L_1 = L_0.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2 = ___1_rhs;
		float L_3 = L_2.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4 = ___0_lhs;
		float L_5 = L_4.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6 = ___1_rhs;
		float L_7 = L_6.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8 = ___0_lhs;
		float L_9 = L_8.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = ___1_rhs;
		float L_11 = L_10.___z_4;
		V_0 = ((float)il2cpp_codegen_add(((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(L_1, L_3)), ((float)il2cpp_codegen_multiply(L_5, L_7)))), ((float)il2cpp_codegen_multiply(L_9, L_11))));
		goto IL_002d;
	}

IL_002d:
	{
		float L_12 = V_0;
		return L_12;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Mathf_Clamp01_mA7E048DBDA832D399A581BE4D6DED9FA44CE0F14_inline (float ___0_value, const RuntimeMethod* method) 
{
	bool V_0 = false;
	float V_1 = 0.0f;
	bool V_2 = false;
	{
		float L_0 = ___0_value;
		V_0 = (bool)((((float)L_0) < ((float)(0.0f)))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0015;
		}
	}
	{
		V_1 = (0.0f);
		goto IL_002d;
	}

IL_0015:
	{
		float L_2 = ___0_value;
		V_2 = (bool)((((float)L_2) > ((float)(1.0f)))? 1 : 0);
		bool L_3 = V_2;
		if (!L_3)
		{
			goto IL_0029;
		}
	}
	{
		V_1 = (1.0f);
		goto IL_002d;
	}

IL_0029:
	{
		float L_4 = ___0_value;
		V_1 = L_4;
		goto IL_002d;
	}

IL_002d:
	{
		float L_5 = V_1;
		return L_5;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_Multiply_m87BA7C578F96C8E49BB07088DAAC4649F83B0353_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_a, float ___1_d, const RuntimeMethod* method) 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___0_a;
		float L_1 = L_0.___x_2;
		float L_2 = ___1_d;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_3 = ___0_a;
		float L_4 = L_3.___y_3;
		float L_5 = ___1_d;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6 = ___0_a;
		float L_7 = L_6.___z_4;
		float L_8 = ___1_d;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_9;
		memset((&L_9), 0, sizeof(L_9));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_9), ((float)il2cpp_codegen_multiply(L_1, L_2)), ((float)il2cpp_codegen_multiply(L_4, L_5)), ((float)il2cpp_codegen_multiply(L_7, L_8)), /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = V_0;
		return L_10;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_Addition_m78C0EC70CB66E8DCAC225743D82B268DAEE92067_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_a, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___1_b, const RuntimeMethod* method) 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___0_a;
		float L_1 = L_0.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2 = ___1_b;
		float L_3 = L_2.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4 = ___0_a;
		float L_5 = L_4.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6 = ___1_b;
		float L_7 = L_6.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8 = ___0_a;
		float L_9 = L_8.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = ___1_b;
		float L_11 = L_10.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_12), ((float)il2cpp_codegen_add(L_1, L_3)), ((float)il2cpp_codegen_add(L_5, L_7)), ((float)il2cpp_codegen_add(L_9, L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_Cross_mF93A280558BCE756D13B6CC5DCD7DE8A43148987_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_lhs, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___1_rhs, const RuntimeMethod* method) 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___0_lhs;
		float L_1 = L_0.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2 = ___1_rhs;
		float L_3 = L_2.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4 = ___0_lhs;
		float L_5 = L_4.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6 = ___1_rhs;
		float L_7 = L_6.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8 = ___0_lhs;
		float L_9 = L_8.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = ___1_rhs;
		float L_11 = L_10.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_12 = ___0_lhs;
		float L_13 = L_12.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_14 = ___1_rhs;
		float L_15 = L_14.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_16 = ___0_lhs;
		float L_17 = L_16.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_18 = ___1_rhs;
		float L_19 = L_18.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_20 = ___0_lhs;
		float L_21 = L_20.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_22 = ___1_rhs;
		float L_23 = L_22.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_24;
		memset((&L_24), 0, sizeof(L_24));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_24), ((float)il2cpp_codegen_subtract(((float)il2cpp_codegen_multiply(L_1, L_3)), ((float)il2cpp_codegen_multiply(L_5, L_7)))), ((float)il2cpp_codegen_subtract(((float)il2cpp_codegen_multiply(L_9, L_11)), ((float)il2cpp_codegen_multiply(L_13, L_15)))), ((float)il2cpp_codegen_subtract(((float)il2cpp_codegen_multiply(L_17, L_19)), ((float)il2cpp_codegen_multiply(L_21, L_23)))), /*hidden argument*/NULL);
		V_0 = L_24;
		goto IL_005a;
	}

IL_005a:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_25 = V_0;
		return L_25;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_get_forward_mAA55A7034304DF8B2152EAD49AE779FC4CA2EB4A_inline (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ((Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var))->___forwardVector_11;
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Mathf_Clamp_mEB9AEA827D27D20FCC787F7375156AF46BB12BBF_inline (float ___0_value, float ___1_min, float ___2_max, const RuntimeMethod* method) 
{
	bool V_0 = false;
	bool V_1 = false;
	float V_2 = 0.0f;
	{
		float L_0 = ___0_value;
		float L_1 = ___1_min;
		V_0 = (bool)((((float)L_0) < ((float)L_1))? 1 : 0);
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_000e;
		}
	}
	{
		float L_3 = ___1_min;
		___0_value = L_3;
		goto IL_0019;
	}

IL_000e:
	{
		float L_4 = ___0_value;
		float L_5 = ___2_max;
		V_1 = (bool)((((float)L_4) > ((float)L_5))? 1 : 0);
		bool L_6 = V_1;
		if (!L_6)
		{
			goto IL_0019;
		}
	}
	{
		float L_7 = ___2_max;
		___0_value = L_7;
	}

IL_0019:
	{
		float L_8 = ___0_value;
		V_2 = L_8;
		goto IL_001d;
	}

IL_001d:
	{
		float L_9 = V_2;
		return L_9;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Mathf_Approximately_m1DADD012A8FC82E11FB282501AE2EBBF9A77150B_inline (float ___0_a, float ___1_b, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Mathf_tE284D016E3B297B72311AAD9EB8F0E643F6A4682_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		float L_0 = ___1_b;
		float L_1 = ___0_a;
		float L_2;
		L_2 = fabsf(((float)il2cpp_codegen_subtract(L_0, L_1)));
		float L_3 = ___0_a;
		float L_4;
		L_4 = fabsf(L_3);
		float L_5 = ___1_b;
		float L_6;
		L_6 = fabsf(L_5);
		float L_7;
		L_7 = Mathf_Max_mF5379E63D2BBAC76D090748695D833934F8AD051_inline(L_4, L_6, NULL);
		float L_8 = ((Mathf_tE284D016E3B297B72311AAD9EB8F0E643F6A4682_StaticFields*)il2cpp_codegen_static_fields_for(Mathf_tE284D016E3B297B72311AAD9EB8F0E643F6A4682_il2cpp_TypeInfo_var))->___Epsilon_0;
		float L_9;
		L_9 = Mathf_Max_mF5379E63D2BBAC76D090748695D833934F8AD051_inline(((float)il2cpp_codegen_multiply((9.99999997E-07f), L_7)), ((float)il2cpp_codegen_multiply(L_8, (8.0f))), NULL);
		V_0 = (bool)((((float)L_2) < ((float)L_9))? 1 : 0);
		goto IL_0035;
	}

IL_0035:
	{
		bool L_10 = V_0;
		return L_10;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector2_get_sqrMagnitude_mA16336720C14EEF8BA9B55AE33B98C9EE2082BDC_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* __this, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->___x_0;
		float L_1 = __this->___x_0;
		float L_2 = __this->___y_1;
		float L_3 = __this->___y_1;
		V_0 = ((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(L_0, L_1)), ((float)il2cpp_codegen_multiply(L_2, L_3))));
		goto IL_001f;
	}

IL_001f:
	{
		float L_4 = V_0;
		return L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Mathf_LerpAngle_m0653422E15193C2E4A4E5AF05236B6315C789C23_inline (float ___0_a, float ___1_b, float ___2_t, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	bool V_1 = false;
	float V_2 = 0.0f;
	{
		float L_0 = ___1_b;
		float L_1 = ___0_a;
		float L_2;
		L_2 = Mathf_Repeat_m6F1560A163481BB311D685294E1B463C3E4EB3BA_inline(((float)il2cpp_codegen_subtract(L_0, L_1)), (360.0f), NULL);
		V_0 = L_2;
		float L_3 = V_0;
		V_1 = (bool)((((float)L_3) > ((float)(180.0f)))? 1 : 0);
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_0023;
		}
	}
	{
		float L_5 = V_0;
		V_0 = ((float)il2cpp_codegen_subtract(L_5, (360.0f)));
	}

IL_0023:
	{
		float L_6 = ___0_a;
		float L_7 = V_0;
		float L_8 = ___2_t;
		float L_9;
		L_9 = Mathf_Clamp01_mA7E048DBDA832D399A581BE4D6DED9FA44CE0F14_inline(L_8, NULL);
		V_2 = ((float)il2cpp_codegen_add(L_6, ((float)il2cpp_codegen_multiply(L_7, L_9))));
		goto IL_0030;
	}

IL_0030:
	{
		float L_10 = V_2;
		return L_10;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 Quaternion_Euler_m9262AB29E3E9CE94EF71051F38A28E82AEC73F90_inline (float ___0_x, float ___1_y, float ___2_z, const RuntimeMethod* method) 
{
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		float L_0 = ___0_x;
		float L_1 = ___1_y;
		float L_2 = ___2_z;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_3;
		memset((&L_3), 0, sizeof(L_3));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_3), L_0, L_1, L_2, /*hidden argument*/NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4;
		L_4 = Vector3_op_Multiply_m87BA7C578F96C8E49BB07088DAAC4649F83B0353_inline(L_3, (0.0174532924f), NULL);
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_5;
		L_5 = Quaternion_Internal_FromEulerRad_m66D4475341F53949471E6870FB5C5E4A5E9BA93E(L_4, NULL);
		V_0 = L_5;
		goto IL_001b;
	}

IL_001b:
	{
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_6 = V_0;
		return L_6;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_white_m068F5AF879B0FCA584E3693F762EA41BB65532C6_inline (const RuntimeMethod* method) 
{
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0;
		memset((&L_0), 0, sizeof(L_0));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_0), (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_001d;
	}

IL_001d:
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_green_mEB001F2CD8C68C6BBAEF9101990B779D3AA2A6EF_inline (const RuntimeMethod* method) 
{
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0;
		memset((&L_0), 0, sizeof(L_0));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_0), (0.0f), (1.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_001d;
	}

IL_001d:
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* __this, float ___0_x, float ___1_y, const RuntimeMethod* method) 
{
	{
		float L_0 = ___0_x;
		__this->___x_0 = L_0;
		float L_1 = ___1_y;
		__this->___y_1 = L_1;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector2_op_Implicit_m6D9CABB2C791A192867D7A4559D132BE86DD3EB7_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___0_v, const RuntimeMethod* method) 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ___0_v;
		float L_1 = L_0.___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_2 = ___0_v;
		float L_3 = L_2.___y_1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4;
		memset((&L_4), 0, sizeof(L_4));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_4), L_1, L_3, (0.0f), /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_001a;
	}

IL_001a:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_5 = V_0;
		return L_5;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PointerEventData_set_position_m66E8DFE693F550372E6B085C6E2F887FDB092FAA_inline (PointerEventData_t9670F3C7D823CCB738A1604C72A1EB90292396FB* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___0_value, const RuntimeMethod* method) 
{
	{
		// public Vector2 position { get; set; }
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ___0_value;
		__this->___U3CpositionU3Ek__BackingField_13 = L_0;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* RaycastResult_get_gameObject_m77014B442B9E2D10F2CC3AEEDC07AA95CDE1E2F1_inline (RaycastResult_tEC6A7B7CABA99C386F054F01E498AEC426CF8023* __this, const RuntimeMethod* method) 
{
	{
		// get { return m_GameObject; }
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___m_GameObject_0;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m46EEFFA770BE665EA0CB3A5332E941DA4B3C1D37_gshared_inline (List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = (int32_t)__this->____size_2;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Clear_m455780C5A45049F9BDC25EAD3BA10A681D16385D_gshared_inline (List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B* __this, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->____version_3;
		__this->____version_3 = ((int32_t)il2cpp_codegen_add(L_0, 1));
		if (!false)
		{
			goto IL_0035;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->____size_2;
		V_0 = L_1;
		__this->____size_2 = 0;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_003c;
		}
	}
	{
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_3 = (Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C*)__this->____items_1;
		int32_t L_4 = V_0;
		Array_Clear_m50BAA3751899858B097D3FF2ED31F284703FE5CB((RuntimeArray*)L_3, 0, L_4, NULL);
		return;
	}

IL_0035:
	{
		__this->____size_2 = 0;
	}

IL_003c:
	{
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Add_m79E50C4F592B1703F4B76A8BE7B4855515460CA1_gshared_inline (List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_item, const RuntimeMethod* method) 
{
	Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)__this->____version_3;
		__this->____version_3 = ((int32_t)il2cpp_codegen_add(L_0, 1));
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_1 = (Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C*)__this->____items_1;
		V_0 = L_1;
		int32_t L_2 = (int32_t)__this->____size_2;
		V_1 = L_2;
		int32_t L_3 = V_1;
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_4 = V_0;
		NullCheck(L_4);
		if ((!(((uint32_t)L_3) < ((uint32_t)((int32_t)(((RuntimeArray*)L_4)->max_length))))))
		{
			goto IL_0034;
		}
	}
	{
		int32_t L_5 = V_1;
		__this->____size_2 = ((int32_t)il2cpp_codegen_add(L_5, 1));
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_6 = V_0;
		int32_t L_7 = V_1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8 = ___0_item;
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2)L_8);
		return;
	}

IL_0034:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_9 = ___0_item;
		((  void (*) (List_1_t77B94703E05C519A9010DD0614F757F974E1CD8B*, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2, const RuntimeMethod*))il2cpp_codegen_get_method_pointer(il2cpp_rgctx_method(method->klass->rgctx_data, 11)))(__this, L_9, il2cpp_rgctx_method(method->klass->rgctx_data, 11));
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m4407E4C389F22B8CEC282C15D56516658746C383_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = (int32_t)__this->____size_2;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Add_mEBCF994CC3814631017F46A387B1A192ED6C85C7_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, RuntimeObject* ___0_item, const RuntimeMethod* method) 
{
	ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)__this->____version_3;
		__this->____version_3 = ((int32_t)il2cpp_codegen_add(L_0, 1));
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_1 = (ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918*)__this->____items_1;
		V_0 = L_1;
		int32_t L_2 = (int32_t)__this->____size_2;
		V_1 = L_2;
		int32_t L_3 = V_1;
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_4 = V_0;
		NullCheck(L_4);
		if ((!(((uint32_t)L_3) < ((uint32_t)((int32_t)(((RuntimeArray*)L_4)->max_length))))))
		{
			goto IL_0034;
		}
	}
	{
		int32_t L_5 = V_1;
		__this->____size_2 = ((int32_t)il2cpp_codegen_add(L_5, 1));
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_6 = V_0;
		int32_t L_7 = V_1;
		RuntimeObject* L_8 = ___0_item;
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (RuntimeObject*)L_8);
		return;
	}

IL_0034:
	{
		RuntimeObject* L_9 = ___0_item;
		((  void (*) (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D*, RuntimeObject*, const RuntimeMethod*))il2cpp_codegen_get_method_pointer(il2cpp_rgctx_method(method->klass->rgctx_data, 11)))(__this, L_9, il2cpp_rgctx_method(method->klass->rgctx_data, 11));
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Action_2_Invoke_m7BFCE0BBCF67689D263059B56A8D79161B698587_gshared_inline (Action_2_t156C43F079E7E68155FCDCD12DC77DD11AEF7E3C* __this, RuntimeObject* ___0_arg1, RuntimeObject* ___1_arg2, const RuntimeMethod* method) 
{
	typedef void (*FunctionPointerType) (RuntimeObject*, RuntimeObject*, RuntimeObject*, const RuntimeMethod*);
	((FunctionPointerType)__this->___invoke_impl_1)((Il2CppObject*)__this->___method_code_6, ___0_arg1, ___1_arg2, reinterpret_cast<RuntimeMethod*>(__this->___method_3));
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* Enumerator_get_Current_mCD39BA1871E5D5BE52D8AA0B27886D9B5B10BBF9_gshared_inline (Enumerator_tC25D6382B2C7E2606E12FC6637F714A98D52DE22* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->____current_3;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Action_1_Invoke_mF2422B2DD29F74CE66F791C3F68E288EC7C3DB9E_gshared_inline (Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* __this, RuntimeObject* ___0_obj, const RuntimeMethod* method) 
{
	typedef void (*FunctionPointerType) (RuntimeObject*, RuntimeObject*, const RuntimeMethod*);
	((FunctionPointerType)__this->___invoke_impl_1)((Il2CppObject*)__this->___method_code_6, ___0_obj, reinterpret_cast<RuntimeMethod*>(__this->___method_3));
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Add_m9BAF9FC9B01B86AB9E16C2A54BAB69042D04B974_gshared_inline (List_1_tA61AD775C53D69BA8C4CB85F6A1A7866AAC6997B* __this, KeyValuePair_2_tF11CA6D20F09EC4DAB7CB3C2C394F6F2C394E6B8 ___0_item, const RuntimeMethod* method) 
{
	KeyValuePair_2U5BU5D_tF5EFD20ACC3BB6CE90DBB51F3461B2C8F1D2E2F3* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)__this->____version_3;
		__this->____version_3 = ((int32_t)il2cpp_codegen_add(L_0, 1));
		KeyValuePair_2U5BU5D_tF5EFD20ACC3BB6CE90DBB51F3461B2C8F1D2E2F3* L_1 = (KeyValuePair_2U5BU5D_tF5EFD20ACC3BB6CE90DBB51F3461B2C8F1D2E2F3*)__this->____items_1;
		V_0 = L_1;
		int32_t L_2 = (int32_t)__this->____size_2;
		V_1 = L_2;
		int32_t L_3 = V_1;
		KeyValuePair_2U5BU5D_tF5EFD20ACC3BB6CE90DBB51F3461B2C8F1D2E2F3* L_4 = V_0;
		NullCheck(L_4);
		if ((!(((uint32_t)L_3) < ((uint32_t)((int32_t)(((RuntimeArray*)L_4)->max_length))))))
		{
			goto IL_0034;
		}
	}
	{
		int32_t L_5 = V_1;
		__this->____size_2 = ((int32_t)il2cpp_codegen_add(L_5, 1));
		KeyValuePair_2U5BU5D_tF5EFD20ACC3BB6CE90DBB51F3461B2C8F1D2E2F3* L_6 = V_0;
		int32_t L_7 = V_1;
		KeyValuePair_2_tF11CA6D20F09EC4DAB7CB3C2C394F6F2C394E6B8 L_8 = ___0_item;
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (KeyValuePair_2_tF11CA6D20F09EC4DAB7CB3C2C394F6F2C394E6B8)L_8);
		return;
	}

IL_0034:
	{
		KeyValuePair_2_tF11CA6D20F09EC4DAB7CB3C2C394F6F2C394E6B8 L_9 = ___0_item;
		((  void (*) (List_1_tA61AD775C53D69BA8C4CB85F6A1A7866AAC6997B*, KeyValuePair_2_tF11CA6D20F09EC4DAB7CB3C2C394F6F2C394E6B8, const RuntimeMethod*))il2cpp_codegen_get_method_pointer(il2cpp_rgctx_method(method->klass->rgctx_data, 11)))(__this, L_9, il2cpp_rgctx_method(method->klass->rgctx_data, 11));
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR KeyValuePair_2_tF11CA6D20F09EC4DAB7CB3C2C394F6F2C394E6B8 Enumerator_get_Current_m7362387DF032E42E572F7041F4C1A3CD21679DCD_gshared_inline (Enumerator_t7F9BA7E481A1947D155FE5B42578F1117860DC39* __this, const RuntimeMethod* method) 
{
	{
		KeyValuePair_2_tF11CA6D20F09EC4DAB7CB3C2C394F6F2C394E6B8 L_0 = (KeyValuePair_2_tF11CA6D20F09EC4DAB7CB3C2C394F6F2C394E6B8)__this->____current_3;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* KeyValuePair_2_get_Key_mADC45FA05C759E6F88D7DADDFE0C0E1ADBB3E501_gshared_inline (KeyValuePair_2_tF11CA6D20F09EC4DAB7CB3C2C394F6F2C394E6B8* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->___key_0;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t KeyValuePair_2_get_Value_m7A836D9634814B22DF33AD801EA10741ABFBDFE2_gshared_inline (KeyValuePair_2_tF11CA6D20F09EC4DAB7CB3C2C394F6F2C394E6B8* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = (int32_t)__this->___value_1;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Clear_mB511E4F0FAD5BBBFD635CE5E636119104E14A316_gshared_inline (List_1_tA61AD775C53D69BA8C4CB85F6A1A7866AAC6997B* __this, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->____version_3;
		__this->____version_3 = ((int32_t)il2cpp_codegen_add(L_0, 1));
		if (!true)
		{
			goto IL_0035;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->____size_2;
		V_0 = L_1;
		__this->____size_2 = 0;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_003c;
		}
	}
	{
		KeyValuePair_2U5BU5D_tF5EFD20ACC3BB6CE90DBB51F3461B2C8F1D2E2F3* L_3 = (KeyValuePair_2U5BU5D_tF5EFD20ACC3BB6CE90DBB51F3461B2C8F1D2E2F3*)__this->____items_1;
		int32_t L_4 = V_0;
		Array_Clear_m50BAA3751899858B097D3FF2ED31F284703FE5CB((RuntimeArray*)L_3, 0, L_4, NULL);
		return;
	}

IL_0035:
	{
		__this->____size_2 = 0;
	}

IL_003c:
	{
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RaycastResult_tEC6A7B7CABA99C386F054F01E498AEC426CF8023 Enumerator_get_Current_m53EF02F206B7F773103FC51D50AC1B974AE692F4_gshared_inline (Enumerator_tFEA671794CD7ED9545DB6E9B1D2E744410E5EB1C* __this, const RuntimeMethod* method) 
{
	{
		RaycastResult_tEC6A7B7CABA99C386F054F01E498AEC426CF8023 L_0 = (RaycastResult_tEC6A7B7CABA99C386F054F01E498AEC426CF8023)__this->____current_3;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* __this, float ___0_x, float ___1_y, float ___2_z, const RuntimeMethod* method) 
{
	{
		float L_0 = ___0_x;
		__this->___x_2 = L_0;
		float L_1 = ___1_y;
		__this->___y_3 = L_1;
		float L_2 = ___2_z;
		__this->___z_4 = L_2;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector3_Magnitude_m21652D951393A3D7CE92CE40049A0E7F76544D1B_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_vector, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___0_vector;
		float L_1 = L_0.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2 = ___0_vector;
		float L_3 = L_2.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4 = ___0_vector;
		float L_5 = L_4.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6 = ___0_vector;
		float L_7 = L_6.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8 = ___0_vector;
		float L_9 = L_8.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = ___0_vector;
		float L_11 = L_10.___z_4;
		il2cpp_codegen_runtime_class_init_inline(Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		double L_12;
		L_12 = sqrt(((double)((float)il2cpp_codegen_add(((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(L_1, L_3)), ((float)il2cpp_codegen_multiply(L_5, L_7)))), ((float)il2cpp_codegen_multiply(L_9, L_11))))));
		V_0 = ((float)L_12);
		goto IL_0034;
	}

IL_0034:
	{
		float L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_Division_mCC6BB24E372AB96B8380D1678446EF6A8BAE13BB_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_a, float ___1_d, const RuntimeMethod* method) 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___0_a;
		float L_1 = L_0.___x_2;
		float L_2 = ___1_d;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_3 = ___0_a;
		float L_4 = L_3.___y_3;
		float L_5 = ___1_d;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6 = ___0_a;
		float L_7 = L_6.___z_4;
		float L_8 = ___1_d;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_9;
		memset((&L_9), 0, sizeof(L_9));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_9), ((float)(L_1/L_2)), ((float)(L_4/L_5)), ((float)(L_7/L_8)), /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = V_0;
		return L_10;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_get_zero_m0C1249C3F25B1C70EAD3CC8B31259975A457AE39_inline (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ((Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var))->___zeroVector_5;
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Mathf_Max_mF5379E63D2BBAC76D090748695D833934F8AD051_inline (float ___0_a, float ___1_b, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	float G_B3_0 = 0.0f;
	{
		float L_0 = ___0_a;
		float L_1 = ___1_b;
		if ((((float)L_0) > ((float)L_1)))
		{
			goto IL_0008;
		}
	}
	{
		float L_2 = ___1_b;
		G_B3_0 = L_2;
		goto IL_0009;
	}

IL_0008:
	{
		float L_3 = ___0_a;
		G_B3_0 = L_3;
	}

IL_0009:
	{
		V_0 = G_B3_0;
		goto IL_000c;
	}

IL_000c:
	{
		float L_4 = V_0;
		return L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Mathf_Repeat_m6F1560A163481BB311D685294E1B463C3E4EB3BA_inline (float ___0_t, float ___1_length, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	{
		float L_0 = ___0_t;
		float L_1 = ___0_t;
		float L_2 = ___1_length;
		float L_3;
		L_3 = floorf(((float)(L_1/L_2)));
		float L_4 = ___1_length;
		float L_5 = ___1_length;
		float L_6;
		L_6 = Mathf_Clamp_mEB9AEA827D27D20FCC787F7375156AF46BB12BBF_inline(((float)il2cpp_codegen_subtract(L_0, ((float)il2cpp_codegen_multiply(L_3, L_4)))), (0.0f), L_5, NULL);
		V_0 = L_6;
		goto IL_001b;
	}

IL_001b:
	{
		float L_7 = V_0;
		return L_7;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F* __this, float ___0_r, float ___1_g, float ___2_b, float ___3_a, const RuntimeMethod* method) 
{
	{
		float L_0 = ___0_r;
		__this->___r_0 = L_0;
		float L_1 = ___1_g;
		__this->___g_1 = L_1;
		float L_2 = ___2_b;
		__this->___b_2 = L_2;
		float L_3 = ___3_a;
		__this->___a_3 = L_3;
		return;
	}
}
