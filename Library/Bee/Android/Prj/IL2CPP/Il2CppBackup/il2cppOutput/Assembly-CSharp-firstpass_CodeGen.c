﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void CW.Common.CwCameraLook::set_Listen(System.Boolean)
extern void CwCameraLook_set_Listen_mC1BBA7E20874CD9BD5FE0F1589CA2085DE238D5E (void);
// 0x00000002 System.Boolean CW.Common.CwCameraLook::get_Listen()
extern void CwCameraLook_get_Listen_mE3FDB48B49C40A4038A8E106978A1F4C1D2A72C9 (void);
// 0x00000003 System.Void CW.Common.CwCameraLook::set_Damping(System.Single)
extern void CwCameraLook_set_Damping_mDDF705DC41131662329B392415E7510F48554035 (void);
// 0x00000004 System.Single CW.Common.CwCameraLook::get_Damping()
extern void CwCameraLook_get_Damping_mFCF62C9E54A0536D225E69611FA16C0D0D43E454 (void);
// 0x00000005 System.Void CW.Common.CwCameraLook::set_Sensitivity(System.Single)
extern void CwCameraLook_set_Sensitivity_m87B9913B5644F9BD4C7738E508836C380444AE30 (void);
// 0x00000006 System.Single CW.Common.CwCameraLook::get_Sensitivity()
extern void CwCameraLook_get_Sensitivity_mDD890D29B0C916768A7CDD9CED9F0C6E9C3C480A (void);
// 0x00000007 System.Void CW.Common.CwCameraLook::set_PitchControls(CW.Common.CwInputManager/Axis)
extern void CwCameraLook_set_PitchControls_m1F410A7D89FB841EDD4250E07B21AAD5577D9E33 (void);
// 0x00000008 CW.Common.CwInputManager/Axis CW.Common.CwCameraLook::get_PitchControls()
extern void CwCameraLook_get_PitchControls_mD83183E6968F0148122462C5BA2C545E88496549 (void);
// 0x00000009 System.Void CW.Common.CwCameraLook::set_YawControls(CW.Common.CwInputManager/Axis)
extern void CwCameraLook_set_YawControls_mB2900DB40BF7FE55E795DDD604C610B39FBB7A6F (void);
// 0x0000000A CW.Common.CwInputManager/Axis CW.Common.CwCameraLook::get_YawControls()
extern void CwCameraLook_get_YawControls_m44B920B2AB4EBBB05620F5E9D203C61A6C04B96F (void);
// 0x0000000B System.Void CW.Common.CwCameraLook::set_RollControls(CW.Common.CwInputManager/Axis)
extern void CwCameraLook_set_RollControls_m0CEBCFCC15E11C705959B7035FA210D697BBB88C (void);
// 0x0000000C CW.Common.CwInputManager/Axis CW.Common.CwCameraLook::get_RollControls()
extern void CwCameraLook_get_RollControls_m79E3EDFA5DB47257C92059CF465A7289E78C28FC (void);
// 0x0000000D System.Void CW.Common.CwCameraLook::Start()
extern void CwCameraLook_Start_m576ECA9992440C326783126170534DF47ADD30D0 (void);
// 0x0000000E System.Void CW.Common.CwCameraLook::OnDisable()
extern void CwCameraLook_OnDisable_mA852D56A6D2FE46494BCDD97597B750C1470BD8A (void);
// 0x0000000F System.Void CW.Common.CwCameraLook::Update()
extern void CwCameraLook_Update_m80810E73159D9821B0FFAE04AB0D27E1DBDE6E5C (void);
// 0x00000010 System.Void CW.Common.CwCameraLook::OnApplicationFocus(System.Boolean)
extern void CwCameraLook_OnApplicationFocus_m415DA4C660190E74A6DC3184422BD8901B6757A6 (void);
// 0x00000011 System.Void CW.Common.CwCameraLook::AddToDelta()
extern void CwCameraLook_AddToDelta_m9E8C05A2F7323A549F2BA3DD193AEDBC74CB3C90 (void);
// 0x00000012 System.Void CW.Common.CwCameraLook::DampenDelta()
extern void CwCameraLook_DampenDelta_mC4BA3F86FFCFDDEFAB7004D7A50F94BA9882E3E4 (void);
// 0x00000013 System.Void CW.Common.CwCameraLook::.ctor()
extern void CwCameraLook__ctor_m2A93E54BB653B6247A16223CC50FF8392468FE52 (void);
// 0x00000014 System.Void CW.Common.CwCameraMove::set_Listen(System.Boolean)
extern void CwCameraMove_set_Listen_m903FDD6C1D86FDBA9B99B730D17C9195F2437415 (void);
// 0x00000015 System.Boolean CW.Common.CwCameraMove::get_Listen()
extern void CwCameraMove_get_Listen_m6D55EB7728EA11FD6F0E8724A945ECC2A9D950CA (void);
// 0x00000016 System.Void CW.Common.CwCameraMove::set_Damping(System.Single)
extern void CwCameraMove_set_Damping_m4D1CAFEDFF5BE70559059DF81CFA9CF880F3D28E (void);
// 0x00000017 System.Single CW.Common.CwCameraMove::get_Damping()
extern void CwCameraMove_get_Damping_m14887D5544CBDFA2CA78C2F6BB0265B6A5CC6A8B (void);
// 0x00000018 System.Void CW.Common.CwCameraMove::set_Sensitivity(System.Single)
extern void CwCameraMove_set_Sensitivity_m1BC9F39AA93DDEF5E9183D42A5189E47BC3E1926 (void);
// 0x00000019 System.Single CW.Common.CwCameraMove::get_Sensitivity()
extern void CwCameraMove_get_Sensitivity_mB9D5AA304B48417D60F6062D66CC4DA089183FDD (void);
// 0x0000001A System.Void CW.Common.CwCameraMove::set_HorizontalControls(CW.Common.CwInputManager/Axis)
extern void CwCameraMove_set_HorizontalControls_m58B9E89BB0B801B2748191B006A276CDFCB3D039 (void);
// 0x0000001B CW.Common.CwInputManager/Axis CW.Common.CwCameraMove::get_HorizontalControls()
extern void CwCameraMove_get_HorizontalControls_m515D7FEE87FF29592FF48F517680240F3AF991D8 (void);
// 0x0000001C System.Void CW.Common.CwCameraMove::set_DepthControls(CW.Common.CwInputManager/Axis)
extern void CwCameraMove_set_DepthControls_m8BCC2745FB45C51CC597183B76170B8A31C5B564 (void);
// 0x0000001D CW.Common.CwInputManager/Axis CW.Common.CwCameraMove::get_DepthControls()
extern void CwCameraMove_get_DepthControls_m41729125DFF75EEE98B1B594F7D5D5807BC673F5 (void);
// 0x0000001E System.Void CW.Common.CwCameraMove::set_VerticalControls(CW.Common.CwInputManager/Axis)
extern void CwCameraMove_set_VerticalControls_mF7D11C783F77F84B6820C6873DD8B97D876C0CE0 (void);
// 0x0000001F CW.Common.CwInputManager/Axis CW.Common.CwCameraMove::get_VerticalControls()
extern void CwCameraMove_get_VerticalControls_m23A3277FB5E730742DE2AE8AE9E5C1F2882EC225 (void);
// 0x00000020 System.Void CW.Common.CwCameraMove::Start()
extern void CwCameraMove_Start_mF69AD8DF75E1A6336D939BB38363D088BB8B8C6F (void);
// 0x00000021 System.Void CW.Common.CwCameraMove::Update()
extern void CwCameraMove_Update_m86B60F5DAD92C4BA3A5DE9F8A0231E11E3AA7202 (void);
// 0x00000022 System.Void CW.Common.CwCameraMove::AddToDelta()
extern void CwCameraMove_AddToDelta_m8D60AF27B12A2C74566350CBA556B6C397137B73 (void);
// 0x00000023 System.Void CW.Common.CwCameraMove::DampenDelta()
extern void CwCameraMove_DampenDelta_mD59D563A640C83BCE72086892149D515A3E06D04 (void);
// 0x00000024 System.Void CW.Common.CwCameraMove::.ctor()
extern void CwCameraMove__ctor_mA70F41956421329C6CD1A73105FC824E7F3A2EA4 (void);
// 0x00000025 System.Void CW.Common.CwCameraPivot::set_Listen(System.Boolean)
extern void CwCameraPivot_set_Listen_m951577ED29FF71092813B416788D0C99D00F6FC7 (void);
// 0x00000026 System.Boolean CW.Common.CwCameraPivot::get_Listen()
extern void CwCameraPivot_get_Listen_m5F41DD714B794FE8DE62E453EAE9E5B7B39F90B2 (void);
// 0x00000027 System.Void CW.Common.CwCameraPivot::set_Damping(System.Single)
extern void CwCameraPivot_set_Damping_mB41BAC011E0BD205556013953395035B723D01EC (void);
// 0x00000028 System.Single CW.Common.CwCameraPivot::get_Damping()
extern void CwCameraPivot_get_Damping_m9E26C61DC21D6AB1E2153B3334C8CE2741C0A05C (void);
// 0x00000029 System.Void CW.Common.CwCameraPivot::set_PitchControls(CW.Common.CwInputManager/Axis)
extern void CwCameraPivot_set_PitchControls_mD3EB8163C7F100917469427DC1BD223412AA4334 (void);
// 0x0000002A CW.Common.CwInputManager/Axis CW.Common.CwCameraPivot::get_PitchControls()
extern void CwCameraPivot_get_PitchControls_mE3C2E599AF479C07E9621157AEEF3E3D939C7F26 (void);
// 0x0000002B System.Void CW.Common.CwCameraPivot::set_YawControls(CW.Common.CwInputManager/Axis)
extern void CwCameraPivot_set_YawControls_m491BF362DFD9A9397AD68DC765B83D4771768A9D (void);
// 0x0000002C CW.Common.CwInputManager/Axis CW.Common.CwCameraPivot::get_YawControls()
extern void CwCameraPivot_get_YawControls_m5DD409936EF34793235A6327C72B3B77E4F36059 (void);
// 0x0000002D System.Void CW.Common.CwCameraPivot::OnEnable()
extern void CwCameraPivot_OnEnable_m5ECC5F5DC95962EF1920B2C40E33034492F7A6BF (void);
// 0x0000002E System.Void CW.Common.CwCameraPivot::Update()
extern void CwCameraPivot_Update_mE541F48653C0424297CE31736C659D142E4EA775 (void);
// 0x0000002F System.Void CW.Common.CwCameraPivot::AddToDelta()
extern void CwCameraPivot_AddToDelta_m739BC0A9A4AE2112EC42819B57FBFA0365018CDA (void);
// 0x00000030 System.Void CW.Common.CwCameraPivot::DampenDelta()
extern void CwCameraPivot_DampenDelta_mFFC631612116DB10B569F81D78F0B7725A149408 (void);
// 0x00000031 System.Void CW.Common.CwCameraPivot::.ctor()
extern void CwCameraPivot__ctor_mA507128A97E4FB0C5FFC0E90343258A8BAD49C99 (void);
// 0x00000032 System.Void CW.Common.CwDemo::set_UpgradeInputModule(System.Boolean)
extern void CwDemo_set_UpgradeInputModule_mCD4BF4DC104DD3F1946016235B7B3499B9F5FCB6 (void);
// 0x00000033 System.Boolean CW.Common.CwDemo::get_UpgradeInputModule()
extern void CwDemo_get_UpgradeInputModule_mE0D125DF29A2E754A630AB66D550F68582438176 (void);
// 0x00000034 System.Void CW.Common.CwDemo::set_ChangeExposureInHDRP(System.Boolean)
extern void CwDemo_set_ChangeExposureInHDRP_m799E8C2E52415429333329D91FDEBC2F509BE61B (void);
// 0x00000035 System.Boolean CW.Common.CwDemo::get_ChangeExposureInHDRP()
extern void CwDemo_get_ChangeExposureInHDRP_mECEC683010FE89A4F4A046B44B603C92B3A1C461 (void);
// 0x00000036 System.Void CW.Common.CwDemo::set_ChangeVisualEnvironmentInHDRP(System.Boolean)
extern void CwDemo_set_ChangeVisualEnvironmentInHDRP_m81F2AB7861F12806B4DFFC0628030C374B6BF1A7 (void);
// 0x00000037 System.Boolean CW.Common.CwDemo::get_ChangeVisualEnvironmentInHDRP()
extern void CwDemo_get_ChangeVisualEnvironmentInHDRP_m954AA58BD6D633B23D10D45C8BECDF00D19D0792 (void);
// 0x00000038 System.Void CW.Common.CwDemo::set_ChangeFogInHDRP(System.Boolean)
extern void CwDemo_set_ChangeFogInHDRP_mA24AF10AC0A1F3BB32E78C0276D6D05FB3C48FA5 (void);
// 0x00000039 System.Boolean CW.Common.CwDemo::get_ChangeFogInHDRP()
extern void CwDemo_get_ChangeFogInHDRP_mFCCE33588ADB1017DE4F6024C67537E02768F89D (void);
// 0x0000003A System.Void CW.Common.CwDemo::set_ChangeCloudsInHDRP(System.Boolean)
extern void CwDemo_set_ChangeCloudsInHDRP_mA86EACC6FA4157D5D305D4356B9C7F947B204665 (void);
// 0x0000003B System.Boolean CW.Common.CwDemo::get_ChangeCloudsInHDRP()
extern void CwDemo_get_ChangeCloudsInHDRP_m2F494C931AE2AEED1504219EBF0DF677D0059251 (void);
// 0x0000003C System.Void CW.Common.CwDemo::set_ChangeMotionBlurInHDRP(System.Boolean)
extern void CwDemo_set_ChangeMotionBlurInHDRP_mB5382676DFCF90F838972FDA0A9409A14DDE02F2 (void);
// 0x0000003D System.Boolean CW.Common.CwDemo::get_ChangeMotionBlurInHDRP()
extern void CwDemo_get_ChangeMotionBlurInHDRP_m435FF86E3C5B858493B6CDD7989E3DFEE47120C0 (void);
// 0x0000003E System.Void CW.Common.CwDemo::set_UpgradeLightsInHDRP(System.Boolean)
extern void CwDemo_set_UpgradeLightsInHDRP_m9D9D804EADC70ECC06245A446791D0C23FB84A55 (void);
// 0x0000003F System.Boolean CW.Common.CwDemo::get_UpgradeLightsInHDRP()
extern void CwDemo_get_UpgradeLightsInHDRP_m831519CF369A2C8A7621FDC4CC2F5570E9125332 (void);
// 0x00000040 System.Void CW.Common.CwDemo::set_UpgradeCamerasInHDRP(System.Boolean)
extern void CwDemo_set_UpgradeCamerasInHDRP_m411A6C889A336D4E6F2A3C3EC268D278DF0204CF (void);
// 0x00000041 System.Boolean CW.Common.CwDemo::get_UpgradeCamerasInHDRP()
extern void CwDemo_get_UpgradeCamerasInHDRP_mA3F97BD2DC3324FBC7DD5F27EFF61459766B625F (void);
// 0x00000042 System.Void CW.Common.CwDemo::OnEnable()
extern void CwDemo_OnEnable_mE19EA41ED15A3B40589CB3C10F4CC3AC46AA5A84 (void);
// 0x00000043 System.Void CW.Common.CwDemo::TryApplyURP()
extern void CwDemo_TryApplyURP_m508D72BFE3D3C85D9113DC577BEA301606B50846 (void);
// 0x00000044 System.Void CW.Common.CwDemo::TryApplyHDRP()
extern void CwDemo_TryApplyHDRP_m855878779C3538E6BA577909064B9EF98212A2ED (void);
// 0x00000045 System.Void CW.Common.CwDemo::TryCreateVolume()
extern void CwDemo_TryCreateVolume_m232B4AE82996AFC09DE3910C1C3A18DEA866BEA1 (void);
// 0x00000046 System.Void CW.Common.CwDemo::TryUpgradeLights()
extern void CwDemo_TryUpgradeLights_m59F0B19FDCDFBD27FBD599C2FFDA1DB3AA1B76AA (void);
// 0x00000047 System.Void CW.Common.CwDemo::TryUpgradeCameras()
extern void CwDemo_TryUpgradeCameras_mBBD1D346C43C7F34966E4148B8940C902E5438DE (void);
// 0x00000048 System.Void CW.Common.CwDemo::TryUpgradeEventSystem()
extern void CwDemo_TryUpgradeEventSystem_mFCE1585356FB321F95EFA37FF037609A8D312DA9 (void);
// 0x00000049 System.Void CW.Common.CwDemo::.ctor()
extern void CwDemo__ctor_m7D644911930F18AD984DA95D2958FA6E634B5EEB (void);
// 0x0000004A System.Void CW.Common.CwDemoButton::set_Link(CW.Common.CwDemoButton/LinkType)
extern void CwDemoButton_set_Link_m2946A2B3BFD2726F94844CB5CCA55356EE56207D (void);
// 0x0000004B CW.Common.CwDemoButton/LinkType CW.Common.CwDemoButton::get_Link()
extern void CwDemoButton_get_Link_m5605569B83BA07A681D64989DE8D4A59EB032725 (void);
// 0x0000004C System.Void CW.Common.CwDemoButton::set_UrlTarget(System.String)
extern void CwDemoButton_set_UrlTarget_m4D41139D335E491AB4523032CF1FE2D9E05FD7A8 (void);
// 0x0000004D System.String CW.Common.CwDemoButton::get_UrlTarget()
extern void CwDemoButton_get_UrlTarget_m128C0711C6A81D9E2EC0C64797D4C0FE97D1515B (void);
// 0x0000004E System.Void CW.Common.CwDemoButton::set_IsolateTarget(UnityEngine.Transform)
extern void CwDemoButton_set_IsolateTarget_m895E67BFA19C442C416AA57F42E75632E398A932 (void);
// 0x0000004F UnityEngine.Transform CW.Common.CwDemoButton::get_IsolateTarget()
extern void CwDemoButton_get_IsolateTarget_m5A6FF0799DA43F737152428109065FDE2E359532 (void);
// 0x00000050 System.Void CW.Common.CwDemoButton::set_IsolateToggle(CW.Common.CwDemoButton/ToggleType)
extern void CwDemoButton_set_IsolateToggle_m148E5490C10E4EC6C1789A8F71C208DA606503F3 (void);
// 0x00000051 CW.Common.CwDemoButton/ToggleType CW.Common.CwDemoButton::get_IsolateToggle()
extern void CwDemoButton_get_IsolateToggle_mEDCD0687BD72566E69C06D56A96A731CD1B5788C (void);
// 0x00000052 System.Void CW.Common.CwDemoButton::OnEnable()
extern void CwDemoButton_OnEnable_m4FA06E2D042CED6D7C7B410DB32D3E467BCB696F (void);
// 0x00000053 System.Void CW.Common.CwDemoButton::Update()
extern void CwDemoButton_Update_m1CEAFDE4FFAF5030173D3059FB8944AF0FB9E557 (void);
// 0x00000054 System.Void CW.Common.CwDemoButton::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void CwDemoButton_OnPointerDown_m2C8BB7C3D0632F2AB3495A53A131E39A8731FAF3 (void);
// 0x00000055 System.Int32 CW.Common.CwDemoButton::GetCurrentLevel()
extern void CwDemoButton_GetCurrentLevel_m6B1981C02570327CF559EE96BED52D477EDC3A13 (void);
// 0x00000056 System.Int32 CW.Common.CwDemoButton::GetLevelCount()
extern void CwDemoButton_GetLevelCount_mD954A66EE852B397FCEF7D3D17489CEFCAE7D0F4 (void);
// 0x00000057 System.Void CW.Common.CwDemoButton::LoadLevel(System.Int32)
extern void CwDemoButton_LoadLevel_m17E1D6096B6A0FD0F20E0E8BFD68BE6A1120710D (void);
// 0x00000058 System.Void CW.Common.CwDemoButton::.ctor()
extern void CwDemoButton__ctor_m3C16B9FBA805B9473EB9DE658866AA8043DC9203 (void);
// 0x00000059 System.Void CW.Common.CwDemoButtonBuilder::set_ButtonPrefab(UnityEngine.GameObject)
extern void CwDemoButtonBuilder_set_ButtonPrefab_mD0CA56B76280E42C6971D25EFBA5C976F9EFC80F (void);
// 0x0000005A UnityEngine.GameObject CW.Common.CwDemoButtonBuilder::get_ButtonPrefab()
extern void CwDemoButtonBuilder_get_ButtonPrefab_m149044BBAD58DD369D7E30CC0F2BE6861907FEF3 (void);
// 0x0000005B System.Void CW.Common.CwDemoButtonBuilder::set_ButtonRoot(UnityEngine.RectTransform)
extern void CwDemoButtonBuilder_set_ButtonRoot_m86A988EC84B6CD768B396C2A9831317680BA14CB (void);
// 0x0000005C UnityEngine.RectTransform CW.Common.CwDemoButtonBuilder::get_ButtonRoot()
extern void CwDemoButtonBuilder_get_ButtonRoot_m24DE9DE100C77C4B5399C992629D1C30DFAD6A33 (void);
// 0x0000005D System.Void CW.Common.CwDemoButtonBuilder::set_Icon(UnityEngine.Sprite)
extern void CwDemoButtonBuilder_set_Icon_mDDC6FD5CA57BFEFF9F087B0E733BB0F1710AA2E0 (void);
// 0x0000005E UnityEngine.Sprite CW.Common.CwDemoButtonBuilder::get_Icon()
extern void CwDemoButtonBuilder_get_Icon_m4FD016A1C402A912DEB7165E0757CB73F6EDA730 (void);
// 0x0000005F System.Void CW.Common.CwDemoButtonBuilder::set_Color(UnityEngine.Color)
extern void CwDemoButtonBuilder_set_Color_mE021C273082C8CCDDACE1BE1563D81A5DCB57163 (void);
// 0x00000060 UnityEngine.Color CW.Common.CwDemoButtonBuilder::get_Color()
extern void CwDemoButtonBuilder_get_Color_m3530FC7BFAAD65B88A8EED0A63628703B76753C3 (void);
// 0x00000061 System.Void CW.Common.CwDemoButtonBuilder::set_OverrideName(System.String)
extern void CwDemoButtonBuilder_set_OverrideName_m072379882C1AAD5C4187F46DC0C0DF51F7343C85 (void);
// 0x00000062 System.String CW.Common.CwDemoButtonBuilder::get_OverrideName()
extern void CwDemoButtonBuilder_get_OverrideName_m6164FBCB420515E8CED90DC9A1B14B615625D689 (void);
// 0x00000063 System.Void CW.Common.CwDemoButtonBuilder::Build()
extern void CwDemoButtonBuilder_Build_mE80EF53367B803C737DC04229F08820846CB545A (void);
// 0x00000064 System.Void CW.Common.CwDemoButtonBuilder::BuildAll()
extern void CwDemoButtonBuilder_BuildAll_m906D88BE6D462471B55E13E977A28BCBED6F1C70 (void);
// 0x00000065 UnityEngine.GameObject CW.Common.CwDemoButtonBuilder::DoInstantiate()
extern void CwDemoButtonBuilder_DoInstantiate_m927A2254854EE0F11C17DCCEB9080F8EC2BA9058 (void);
// 0x00000066 System.Void CW.Common.CwDemoButtonBuilder::.ctor()
extern void CwDemoButtonBuilder__ctor_m755B6D89BD90B95FFDCFBD7B4605A8688B790705 (void);
// 0x00000067 System.Void CW.Common.CwDepthTextureMode::set_DepthMode(UnityEngine.DepthTextureMode)
extern void CwDepthTextureMode_set_DepthMode_mF94166F9071CA4FAD713BCF0AF051CC87F6D8E52 (void);
// 0x00000068 UnityEngine.DepthTextureMode CW.Common.CwDepthTextureMode::get_DepthMode()
extern void CwDepthTextureMode_get_DepthMode_m314F383CDD920AA5E8434BEE124F05C0B97F645D (void);
// 0x00000069 System.Void CW.Common.CwDepthTextureMode::UpdateDepthMode()
extern void CwDepthTextureMode_UpdateDepthMode_mF968A493F9B8C2F1F3F7D6882E2D62D8CFC4F466 (void);
// 0x0000006A System.Void CW.Common.CwDepthTextureMode::Update()
extern void CwDepthTextureMode_Update_m5CFBB0B75A874479B9BE84B28AFD2BA0CD6EDF63 (void);
// 0x0000006B System.Void CW.Common.CwDepthTextureMode::.ctor()
extern void CwDepthTextureMode__ctor_m03144DC5A17F36D0D4FC7D22ED1C610ED9420CFC (void);
// 0x0000006C System.Void CW.Common.CwFollow::set_Follow(CW.Common.CwFollow/FollowType)
extern void CwFollow_set_Follow_m131E406CB66E12DCCA69DCC48B46AE70248A9B18 (void);
// 0x0000006D CW.Common.CwFollow/FollowType CW.Common.CwFollow::get_Follow()
extern void CwFollow_get_Follow_mBE1BFDF2AA2D1F43ADAB3EFFE604C3AFDE220613 (void);
// 0x0000006E System.Void CW.Common.CwFollow::set_Target(UnityEngine.Transform)
extern void CwFollow_set_Target_mAF4C7A7B35B1147753F71B03C966450C299FE956 (void);
// 0x0000006F UnityEngine.Transform CW.Common.CwFollow::get_Target()
extern void CwFollow_get_Target_m57FC7FCC49B44B31AA42DDD6D0DB9D2214892A79 (void);
// 0x00000070 System.Void CW.Common.CwFollow::set_Damping(System.Single)
extern void CwFollow_set_Damping_mF04969161CAA63F86B692615F5C4FCCB9FCF5F44 (void);
// 0x00000071 System.Single CW.Common.CwFollow::get_Damping()
extern void CwFollow_get_Damping_m725BEAF6F7B78B84DEBC673944B013E6D201AD9E (void);
// 0x00000072 System.Void CW.Common.CwFollow::set_Rotate(System.Boolean)
extern void CwFollow_set_Rotate_m54ADA3391F2A04567302746271BC663C49FDA8CF (void);
// 0x00000073 System.Boolean CW.Common.CwFollow::get_Rotate()
extern void CwFollow_get_Rotate_m3AAC51675C25B48C64EF4821407698C8AE11437F (void);
// 0x00000074 System.Void CW.Common.CwFollow::set_IgnoreZ(System.Boolean)
extern void CwFollow_set_IgnoreZ_m6644602D6BD90DCBBB27E1F10AC5C5AE2E40E6BA (void);
// 0x00000075 System.Boolean CW.Common.CwFollow::get_IgnoreZ()
extern void CwFollow_get_IgnoreZ_mADF919CDA9809F4F67F10FA09F40A69F05BD2F6A (void);
// 0x00000076 System.Void CW.Common.CwFollow::set_FollowIn(CW.Common.CwFollow/UpdateType)
extern void CwFollow_set_FollowIn_mCC594F560D1106263277CAADCA5E35F372F0BB87 (void);
// 0x00000077 CW.Common.CwFollow/UpdateType CW.Common.CwFollow::get_FollowIn()
extern void CwFollow_get_FollowIn_m15C13356591479A4B80D7CA766704D1A4DC0AAFC (void);
// 0x00000078 System.Void CW.Common.CwFollow::set_LocalPosition(UnityEngine.Vector3)
extern void CwFollow_set_LocalPosition_m6AB642760BC0C9E6C8F53BFEA6CB115F1B4B1D6C (void);
// 0x00000079 UnityEngine.Vector3 CW.Common.CwFollow::get_LocalPosition()
extern void CwFollow_get_LocalPosition_m6617050F3EE47D15758145904FFE4E464A7A9440 (void);
// 0x0000007A System.Void CW.Common.CwFollow::set_LocalRotation(UnityEngine.Vector3)
extern void CwFollow_set_LocalRotation_mF40F00E2AA19055DEDC8477E0CB29CA293F3CD2D (void);
// 0x0000007B UnityEngine.Vector3 CW.Common.CwFollow::get_LocalRotation()
extern void CwFollow_get_LocalRotation_mFA627538389A94C217F64A92317E51A6D42507A0 (void);
// 0x0000007C System.Void CW.Common.CwFollow::UpdatePosition()
extern void CwFollow_UpdatePosition_m764A0AFB2F660DA5475A8409210CF2A66720DCBE (void);
// 0x0000007D System.Void CW.Common.CwFollow::Update()
extern void CwFollow_Update_m21BE56577AE1FD3163819DDCA46B24ABC6EA2069 (void);
// 0x0000007E System.Void CW.Common.CwFollow::LateUpdate()
extern void CwFollow_LateUpdate_m948E2F2544ACAA592B83C64638C00A38AFBAD794 (void);
// 0x0000007F System.Void CW.Common.CwFollow::.ctor()
extern void CwFollow__ctor_m62AEF6F8FDA89411887601E696B66AEAC376243D (void);
// 0x00000080 System.Void CW.Common.CwInputManager::set_GuiLayers(UnityEngine.LayerMask)
extern void CwInputManager_set_GuiLayers_m707FE9D2324C404D7E6A432250EF666A95998911 (void);
// 0x00000081 UnityEngine.LayerMask CW.Common.CwInputManager::get_GuiLayers()
extern void CwInputManager_get_GuiLayers_m0B99E18B5EC564A21CCEC5A5EDD695420A417BD7 (void);
// 0x00000082 System.Void CW.Common.CwInputManager::add_OnFingerDown(System.Action`1<CW.Common.CwInputManager/Finger>)
extern void CwInputManager_add_OnFingerDown_m3EA0E58BE1484FDCA4F4FE4A510ACA14BB28CE88 (void);
// 0x00000083 System.Void CW.Common.CwInputManager::remove_OnFingerDown(System.Action`1<CW.Common.CwInputManager/Finger>)
extern void CwInputManager_remove_OnFingerDown_m754CF9134F0E259CE5027A2299FF6234FD8692F3 (void);
// 0x00000084 System.Void CW.Common.CwInputManager::add_OnFingerUpdate(System.Action`1<CW.Common.CwInputManager/Finger>)
extern void CwInputManager_add_OnFingerUpdate_m36EC36FC2FBC4034BF54F9FA5F613811AFCCFB6B (void);
// 0x00000085 System.Void CW.Common.CwInputManager::remove_OnFingerUpdate(System.Action`1<CW.Common.CwInputManager/Finger>)
extern void CwInputManager_remove_OnFingerUpdate_m03583ED8A4E8E7F3041EC9CA460D34EA59F1FAE7 (void);
// 0x00000086 System.Void CW.Common.CwInputManager::add_OnFingerUp(System.Action`1<CW.Common.CwInputManager/Finger>)
extern void CwInputManager_add_OnFingerUp_m3C1A63516635C4EF3DE1F7C3EEAE0BE9B90B2328 (void);
// 0x00000087 System.Void CW.Common.CwInputManager::remove_OnFingerUp(System.Action`1<CW.Common.CwInputManager/Finger>)
extern void CwInputManager_remove_OnFingerUp_m4DEF46E69DC0C2A60A82DAD61226FC203B86C359 (void);
// 0x00000088 System.Collections.Generic.List`1<CW.Common.CwInputManager/Finger> CW.Common.CwInputManager::get_Fingers()
extern void CwInputManager_get_Fingers_mFFB1F28BF2C601835D996D8B8E1AF61F564622F7 (void);
// 0x00000089 System.Single CW.Common.CwInputManager::get_ScaleFactor()
extern void CwInputManager_get_ScaleFactor_mD810121BD10ECB7D8EB89E853397A955E0163C5D (void);
// 0x0000008A System.Collections.Generic.List`1<CW.Common.CwInputManager/Finger> CW.Common.CwInputManager::GetFingers(System.Boolean,System.Boolean)
extern void CwInputManager_GetFingers_m9A64C51234060E81FA102986278731E7CEE31E41 (void);
// 0x0000008B System.Boolean CW.Common.CwInputManager::PointOverGui(UnityEngine.Vector2,System.Int32)
extern void CwInputManager_PointOverGui_m31E70D11AB7AFEECE257D0F8310AFDF1E382F16E (void);
// 0x0000008C System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> CW.Common.CwInputManager::RaycastGui(UnityEngine.Vector2,System.Int32)
extern void CwInputManager_RaycastGui_m8EB79B63DE684FD2C992325599153C546030E3B8 (void);
// 0x0000008D UnityEngine.Vector2 CW.Common.CwInputManager::GetAveragePosition(System.Collections.Generic.List`1<CW.Common.CwInputManager/Finger>)
extern void CwInputManager_GetAveragePosition_m6E11B35DD0C1B684C8B46982631EBE490FCB5A70 (void);
// 0x0000008E UnityEngine.Vector2 CW.Common.CwInputManager::GetAverageOldPosition(System.Collections.Generic.List`1<CW.Common.CwInputManager/Finger>)
extern void CwInputManager_GetAverageOldPosition_mBF1B884E3F8746DD24DD9F426C0470F22B76BD3F (void);
// 0x0000008F UnityEngine.Vector2 CW.Common.CwInputManager::GetAveragePullScaled(System.Collections.Generic.List`1<CW.Common.CwInputManager/Finger>)
extern void CwInputManager_GetAveragePullScaled_mD3B05732EBA151293D0B328EF4F243B42AA2EFB5 (void);
// 0x00000090 UnityEngine.Vector2 CW.Common.CwInputManager::GetAverageDeltaScaled(System.Collections.Generic.List`1<CW.Common.CwInputManager/Finger>)
extern void CwInputManager_GetAverageDeltaScaled_m24FE461D9CD4DC6AC1BE47FC591F8136DA7D436A (void);
// 0x00000091 System.Single CW.Common.CwInputManager::GetAverageTwistRadians(System.Collections.Generic.List`1<CW.Common.CwInputManager/Finger>)
extern void CwInputManager_GetAverageTwistRadians_mEA6B38920BAFE06D4477E48348D76FA0125375FB (void);
// 0x00000092 System.Void CW.Common.CwInputManager::EnsureThisComponentExists()
extern void CwInputManager_EnsureThisComponentExists_mC56FE8390345D5F5D437527CC18908CCB52545A8 (void);
// 0x00000093 System.Void CW.Common.CwInputManager::Update()
extern void CwInputManager_Update_mF2A2A913AB135FDDA61812444EE1588798F3143D (void);
// 0x00000094 CW.Common.CwInputManager/Finger CW.Common.CwInputManager::FindFinger(System.Int32)
extern void CwInputManager_FindFinger_m57341E7E2E6CD4B02B2BC6FE6E8CD3A75D07A941 (void);
// 0x00000095 System.Void CW.Common.CwInputManager::AddFinger(System.Int32,UnityEngine.Vector2,System.Single,System.Boolean)
extern void CwInputManager_AddFinger_m59214229CE1C09FF29119CF6D42DFDE28D597564 (void);
// 0x00000096 UnityEngine.Vector2 CW.Common.CwInputManager::Hermite(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern void CwInputManager_Hermite_mB6D140B836FB50E3F6F7BAA322E759F125E43DFC (void);
// 0x00000097 System.Single CW.Common.CwInputManager::HermiteInterpolate(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void CwInputManager_HermiteInterpolate_m81D039FA7B3F17C65C54CEE77D1325B1CC797102 (void);
// 0x00000098 System.Single CW.Common.CwInputManager::GetRadians(UnityEngine.Vector2,UnityEngine.Vector2)
extern void CwInputManager_GetRadians_m4ED5446A8B6B56364CA190D47C66333F9050CA20 (void);
// 0x00000099 System.Single CW.Common.CwInputManager::GetDeltaRadians(CW.Common.CwInputManager/Finger,UnityEngine.Vector2,UnityEngine.Vector2)
extern void CwInputManager_GetDeltaRadians_m40D882C62542609F2BC30D86C98A09D88D01BCB5 (void);
// 0x0000009A System.Void CW.Common.CwInputManager::.ctor()
extern void CwInputManager__ctor_mD5FC249B27ADB9F67496DF7563FCB89B0EC4B075 (void);
// 0x0000009B System.Void CW.Common.CwInputManager::.cctor()
extern void CwInputManager__cctor_mE35336C7A63231E331EAEFC65FD96523C38FC197 (void);
// 0x0000009C System.Void CW.Common.CwInputManager/Axis::.ctor(System.Int32,System.Boolean,CW.Common.CwInputManager/AxisGesture,System.Single,UnityEngine.KeyCode,UnityEngine.KeyCode,UnityEngine.KeyCode,UnityEngine.KeyCode,System.Single)
extern void Axis__ctor_m37815A1FAA240248DCE733A7077C418505123BA1 (void);
// 0x0000009D System.Single CW.Common.CwInputManager/Axis::GetValue(System.Single)
extern void Axis_GetValue_m4A7707296DE15BA3B328FC217F4885769C2F6F72 (void);
// 0x0000009E System.Void CW.Common.CwInputManager/Trigger::.ctor(System.Boolean,System.Boolean,UnityEngine.KeyCode)
extern void Trigger__ctor_m92E19461A441EB3009409F51BE4B904019FE1A4D (void);
// 0x0000009F System.Boolean CW.Common.CwInputManager/Trigger::WentDown(CW.Common.CwInputManager/Finger)
extern void Trigger_WentDown_mECBF1F265D4863FBB00BA61D66AC3E2085501AC4 (void);
// 0x000000A0 System.Boolean CW.Common.CwInputManager/Trigger::IsDown(CW.Common.CwInputManager/Finger)
extern void Trigger_IsDown_mDFDD741E21AEAA27CC3D0B9E259157BE5E9F1660 (void);
// 0x000000A1 System.Boolean CW.Common.CwInputManager/Trigger::WentUp(CW.Common.CwInputManager/Finger,System.Boolean)
extern void Trigger_WentUp_m20E4670475591BEF55514EB2B956EFCB09D00B97 (void);
// 0x000000A2 T CW.Common.CwInputManager/Link::Find(System.Collections.Generic.List`1<T>,CW.Common.CwInputManager/Finger)
// 0x000000A3 T CW.Common.CwInputManager/Link::Create(System.Collections.Generic.List`1<T>&,CW.Common.CwInputManager/Finger)
// 0x000000A4 System.Void CW.Common.CwInputManager/Link::ClearAll(System.Collections.Generic.List`1<T>)
// 0x000000A5 System.Void CW.Common.CwInputManager/Link::ClearAndRemove(System.Collections.Generic.List`1<T>,T)
// 0x000000A6 System.Void CW.Common.CwInputManager/Link::Clear()
extern void Link_Clear_m6F28422C0323FD9A10335FB838EF4244F40F2382 (void);
// 0x000000A7 System.Void CW.Common.CwInputManager/Link::.ctor()
extern void Link__ctor_mEECED0CCEC9F7C65EF525B51424A4064B590B308 (void);
// 0x000000A8 System.Single CW.Common.CwInputManager/Finger::get_SmoothScreenPositionDelta()
extern void Finger_get_SmoothScreenPositionDelta_mD2675DDF9AE1DD0F9FD3888A6A6309E7D5D7EFA7 (void);
// 0x000000A9 UnityEngine.Vector2 CW.Common.CwInputManager/Finger::GetSmoothScreenPosition(System.Single)
extern void Finger_GetSmoothScreenPosition_mBFF91729FBD3EF4BD121C330635FBF7086541158 (void);
// 0x000000AA System.Void CW.Common.CwInputManager/Finger::.ctor()
extern void Finger__ctor_m6DBEC0B913A2E9B47E410B69E92E495A1CA44D3F (void);
// 0x000000AB System.Void CW.Common.CwLightIntensity::set_Multiplier(System.Single)
extern void CwLightIntensity_set_Multiplier_mD2D814D5ED8571072EAF13EF0ACCC119EE30A598 (void);
// 0x000000AC System.Single CW.Common.CwLightIntensity::get_Multiplier()
extern void CwLightIntensity_get_Multiplier_m4FA0EE040FCF06CC53D44278EDEBE286EBB6C664 (void);
// 0x000000AD System.Void CW.Common.CwLightIntensity::set_IntensityInStandard(System.Single)
extern void CwLightIntensity_set_IntensityInStandard_m27E32EFBA2148DBFDF943D53AB7FFDA7B53B146F (void);
// 0x000000AE System.Single CW.Common.CwLightIntensity::get_IntensityInStandard()
extern void CwLightIntensity_get_IntensityInStandard_m989847BF9BF038342801253914CDE559D9CC6475 (void);
// 0x000000AF System.Void CW.Common.CwLightIntensity::set_IntensityInURP(System.Single)
extern void CwLightIntensity_set_IntensityInURP_m87093F12638485B06DFE23E6A620701A6048D49B (void);
// 0x000000B0 System.Single CW.Common.CwLightIntensity::get_IntensityInURP()
extern void CwLightIntensity_get_IntensityInURP_mD6D034E53B830FD3AF1E0B0B29D856AB282F878E (void);
// 0x000000B1 System.Void CW.Common.CwLightIntensity::set_IntensityInHDRP(System.Single)
extern void CwLightIntensity_set_IntensityInHDRP_m4791600FC15F37AF4158AD7324E1633114B82900 (void);
// 0x000000B2 System.Single CW.Common.CwLightIntensity::get_IntensityInHDRP()
extern void CwLightIntensity_get_IntensityInHDRP_m5F77F58053C1739C04CA6115FE69A60EFAFF645D (void);
// 0x000000B3 UnityEngine.Light CW.Common.CwLightIntensity::get_CachedLight()
extern void CwLightIntensity_get_CachedLight_mC6DEB31BBBF5D7E491CD8D09D0590098F984087F (void);
// 0x000000B4 System.Void CW.Common.CwLightIntensity::Update()
extern void CwLightIntensity_Update_mA759C95FD64312206ABA499299C8DF129DE953C0 (void);
// 0x000000B5 System.Void CW.Common.CwLightIntensity::ApplyIntensity(System.Single)
extern void CwLightIntensity_ApplyIntensity_mA5D02B60BB0BD145FF2CB961919EF3C5074C3D5C (void);
// 0x000000B6 System.Void CW.Common.CwLightIntensity::.ctor()
extern void CwLightIntensity__ctor_m918ABEBCADD14C04A640A6AE7EC2A2D894A850A4 (void);
// 0x000000B7 System.Void CW.Common.CwRotate::set_AngularVelocity(UnityEngine.Vector3)
extern void CwRotate_set_AngularVelocity_m9FEE98AD09B0B293AEB0961C91450AB676052873 (void);
// 0x000000B8 UnityEngine.Vector3 CW.Common.CwRotate::get_AngularVelocity()
extern void CwRotate_get_AngularVelocity_m1E6D941762A36F8B78054B28FAE6F80C6CB31707 (void);
// 0x000000B9 System.Void CW.Common.CwRotate::set_RelativeTo(UnityEngine.Space)
extern void CwRotate_set_RelativeTo_mE0C207E2C57F5BEF0E857E05754C0E23623A01C5 (void);
// 0x000000BA UnityEngine.Space CW.Common.CwRotate::get_RelativeTo()
extern void CwRotate_get_RelativeTo_m18562DCEF5B1D8757C691F3438D68B07095BACDC (void);
// 0x000000BB System.Void CW.Common.CwRotate::Update()
extern void CwRotate_Update_m5A5ECFDF1C0A50150E6C14B124196A648FC713F6 (void);
// 0x000000BC System.Void CW.Common.CwRotate::.ctor()
extern void CwRotate__ctor_mC6A323F5C4EC72284B04CD3A67BE7B3123C407CA (void);
// 0x000000BD System.Void CW.Common.CwChild::DestroyGameObjectIfInvalid()
extern void CwChild_DestroyGameObjectIfInvalid_m8A510C8ECEFC52BF27605086CC8F9663D7507837 (void);
// 0x000000BE CW.Common.CwChild/IHasChildren CW.Common.CwChild::GetParent()
// 0x000000BF System.Void CW.Common.CwChild::Start()
extern void CwChild_Start_mF17D6AD3F0426EDF35DBF794BE70D80156149DA6 (void);
// 0x000000C0 System.Void CW.Common.CwChild::.ctor()
extern void CwChild__ctor_mC1E1D68B6834C70229A23C5CB23A7C0CB5015F72 (void);
// 0x000000C1 System.Boolean CW.Common.CwChild/IHasChildren::HasChild(CW.Common.CwChild)
// 0x000000C2 System.Boolean CW.Common.CwRoot::get_Exists()
extern void CwRoot_get_Exists_mE200EC75F39E43304E8932CBEDE8D60194E0733B (void);
// 0x000000C3 UnityEngine.Transform CW.Common.CwRoot::get_Root()
extern void CwRoot_get_Root_m9F6C33242A34FFA722ECBFCA0D9D58E02810F61D (void);
// 0x000000C4 UnityEngine.Transform CW.Common.CwRoot::GetRoot()
extern void CwRoot_GetRoot_mBABDEB8BEEB73A77B6A1752645BEEB3CAD89546D (void);
// 0x000000C5 System.Void CW.Common.CwRoot::OnEnable()
extern void CwRoot_OnEnable_m07DDCEE0D33C53E74438C43CD6B86CBF06C0559B (void);
// 0x000000C6 System.Void CW.Common.CwRoot::OnDisable()
extern void CwRoot_OnDisable_mA6871C4D1AF2514F8220E01281F4D1C6025CDDB5 (void);
// 0x000000C7 System.Void CW.Common.CwRoot::.ctor()
extern void CwRoot__ctor_mD2282760B93A28D534CE56B5056346BE0B649160 (void);
// 0x000000C8 System.Void CW.Common.CwRoot::.cctor()
extern void CwRoot__cctor_m668B0EB53195C0C262BB7D874B3363B0CE4B6462 (void);
// 0x000000C9 System.Void CW.Common.CwSeedAttribute::.ctor()
extern void CwSeedAttribute__ctor_mC268592985DDE3C2F00743F5BD2EA96E1C568EE1 (void);
// 0x000000CA UnityEngine.Texture2D CW.Common.CwGuide::get_Icon()
extern void CwGuide_get_Icon_m8273AD7216BA58E2CDBC3563062F96BAD1C03E9A (void);
// 0x000000CB System.String CW.Common.CwGuide::get_Version()
extern void CwGuide_get_Version_m21B6C492AD2D08C87E6456A02C229729427E24CF (void);
// 0x000000CC System.Void CW.Common.CwGuide::.ctor()
extern void CwGuide__ctor_mFE5CC196D2B438ED00245E09BE5B125978FDD2E4 (void);
// 0x000000CD System.Void CW.Common.CwHelper::add_OnCameraPreRender(System.Action`1<UnityEngine.Camera>)
extern void CwHelper_add_OnCameraPreRender_mA71817A4DE07F851985767A763D44934C23CCB70 (void);
// 0x000000CE System.Void CW.Common.CwHelper::remove_OnCameraPreRender(System.Action`1<UnityEngine.Camera>)
extern void CwHelper_remove_OnCameraPreRender_m5B6782485E84F75D52C760238DB666A87A3E897D (void);
// 0x000000CF System.Void CW.Common.CwHelper::add_OnCameraPostRender(System.Action`1<UnityEngine.Camera>)
extern void CwHelper_add_OnCameraPostRender_m253AEBDB4E750D08A5AE266E2E8BBF29AFB69E16 (void);
// 0x000000D0 System.Void CW.Common.CwHelper::remove_OnCameraPostRender(System.Action`1<UnityEngine.Camera>)
extern void CwHelper_remove_OnCameraPostRender_mB1F849F01991C96C5ECD3B9613A77D2C677CE020 (void);
// 0x000000D1 System.Void CW.Common.CwHelper::.cctor()
extern void CwHelper__cctor_mC9E46747B9FADACD1BF59B6079C1EE7202EC77B5 (void);
// 0x000000D2 T CW.Common.CwHelper::GetOrAddComponent(UnityEngine.GameObject,System.Boolean)
// 0x000000D3 T CW.Common.CwHelper::AddComponent(UnityEngine.GameObject,System.Boolean)
// 0x000000D4 System.Boolean CW.Common.CwHelper::IndexInMask(System.Int32,System.Int32)
extern void CwHelper_IndexInMask_m6AD0F2F54247FC72FD8719E0E2F0C7ACC0C04513 (void);
// 0x000000D5 UnityEngine.Camera CW.Common.CwHelper::GetCamera(UnityEngine.Camera,UnityEngine.GameObject)
extern void CwHelper_GetCamera_mA54953B5D77B23342A8AEFEA9992D6CFC62CB70B (void);
// 0x000000D6 UnityEngine.Vector3 CW.Common.CwHelper::GetObserverPosition(UnityEngine.Transform)
extern void CwHelper_GetObserverPosition_m1BF55450B7A4C8AB92D278D6E3F756F7360B0292 (void);
// 0x000000D7 System.Boolean CW.Common.CwHelper::Enabled(UnityEngine.Behaviour)
extern void CwHelper_Enabled_mD9E9A832413D64688E133F17F26B9F071F1C5272 (void);
// 0x000000D8 System.Void CW.Common.CwHelper::BeginSeed()
extern void CwHelper_BeginSeed_mD07B4318C4C5DE8A49B87214CB8D075FFA603747 (void);
// 0x000000D9 System.Void CW.Common.CwHelper::BeginSeed(System.Int32)
extern void CwHelper_BeginSeed_m280BA8251FD9968A0623203B280A3585CA94632F (void);
// 0x000000DA System.Void CW.Common.CwHelper::EndSeed()
extern void CwHelper_EndSeed_mEAB4547C35D2940FDEAE7281DA7E1132B25C8458 (void);
// 0x000000DB UnityEngine.Color CW.Common.CwHelper::Brighten(UnityEngine.Color,System.Single,System.Boolean)
extern void CwHelper_Brighten_m4FA09EF53EB1E850ACA115610A3D6F2CE2117968 (void);
// 0x000000DC UnityEngine.Color CW.Common.CwHelper::Premultiply(UnityEngine.Color)
extern void CwHelper_Premultiply_m723B04EFE539FFC41FA2C75FFDA2BDE841E4EE2A (void);
// 0x000000DD System.Single CW.Common.CwHelper::Saturate(System.Single)
extern void CwHelper_Saturate_mE9B25F9973EE7D6D2957FE13E0FE747D77AA5683 (void);
// 0x000000DE UnityEngine.Color CW.Common.CwHelper::Saturate(UnityEngine.Color)
extern void CwHelper_Saturate_m46B0E1EA2DF3134E7C516B3D80B56F5192A94B14 (void);
// 0x000000DF System.Void CW.Common.CwHelper::Resize(System.Collections.Generic.List`1<T>,System.Int32)
// 0x000000E0 System.Single CW.Common.CwHelper::Sharpness(System.Single,System.Single)
extern void CwHelper_Sharpness_m8CFE49EC8471FA1A712F87698828D0A0B7A112D5 (void);
// 0x000000E1 UnityEngine.Color CW.Common.CwHelper::ToLinear(UnityEngine.Color)
extern void CwHelper_ToLinear_mF1A4AF09C33C756895F91F2F1067CA83D898307A (void);
// 0x000000E2 System.Single CW.Common.CwHelper::ToLinear(System.Single)
extern void CwHelper_ToLinear_m7CD9DDF5FC331BA4D851747F4E8ADED21D2212A9 (void);
// 0x000000E3 UnityEngine.Color CW.Common.CwHelper::ToGamma(UnityEngine.Color)
extern void CwHelper_ToGamma_mAD70FDB542E9F3C45C5CF4835666E1F243841E0B (void);
// 0x000000E4 System.Single CW.Common.CwHelper::ToGamma(System.Single)
extern void CwHelper_ToGamma_m1756D84A141E9BF6864EFC7989725471EF5823A5 (void);
// 0x000000E5 System.Single CW.Common.CwHelper::UniformScale(UnityEngine.Vector3)
extern void CwHelper_UniformScale_mE08F2FAC586FBBED2E410D00425CB0DE3F8BD34C (void);
// 0x000000E6 System.Void CW.Common.CwHelper::BeginActive(UnityEngine.RenderTexture)
extern void CwHelper_BeginActive_m9DB5B6546F1FC3A21FE1EF9DFF62B65D02D48FFE (void);
// 0x000000E7 System.Void CW.Common.CwHelper::EndActive()
extern void CwHelper_EndActive_m8B7A2D408695795E608A2800452778C5A22A2F22 (void);
// 0x000000E8 System.Void CW.Common.CwHelper::SetTempMaterial(UnityEngine.Material)
extern void CwHelper_SetTempMaterial_mA51CDD1D40A995FB3232B1BC70ACADD94FE97C1C (void);
// 0x000000E9 System.Void CW.Common.CwHelper::SetTempMaterial(UnityEngine.Material,UnityEngine.Material)
extern void CwHelper_SetTempMaterial_m0AC8813D49DB2EAACD61EA12A3F9123883602EFD (void);
// 0x000000EA System.Void CW.Common.CwHelper::SetTempMaterial(System.Collections.Generic.List`1<UnityEngine.Material>)
extern void CwHelper_SetTempMaterial_mF58D1C31CF943C3BDB366DCCA1044BCB31F9171C (void);
// 0x000000EB System.Void CW.Common.CwHelper::SetTempMaterial(UnityEngine.MaterialPropertyBlock)
extern void CwHelper_SetTempMaterial_mE376B371C53B74589A348764E2FC938A5082080D (void);
// 0x000000EC System.Void CW.Common.CwHelper::AddMaterial(UnityEngine.Renderer,UnityEngine.Material)
extern void CwHelper_AddMaterial_m7161BE23825C75F941AA1DD331F36A8CF06E9EB6 (void);
// 0x000000ED System.Void CW.Common.CwHelper::ReplaceMaterial(UnityEngine.Renderer,UnityEngine.Material)
extern void CwHelper_ReplaceMaterial_m7D04ED4FFEAC0638630DEEDAD368F9EE9572D169 (void);
// 0x000000EE System.Void CW.Common.CwHelper::RemoveMaterial(UnityEngine.Renderer,UnityEngine.Material)
extern void CwHelper_RemoveMaterial_m3FBFE62B5ACB04A8D2B6D471F0D3E125B8654CEF (void);
// 0x000000EF UnityEngine.Texture2D CW.Common.CwHelper::CreateTempTexture2D(System.String,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean)
extern void CwHelper_CreateTempTexture2D_mA4EEDBB5545C1BC99434B695B246BA5F47A0B463 (void);
// 0x000000F0 UnityEngine.Material CW.Common.CwHelper::CreateTempMaterial(System.String,System.String)
extern void CwHelper_CreateTempMaterial_mDA57806775964417C4F7669C76A87BB086DB8911 (void);
// 0x000000F1 UnityEngine.Material CW.Common.CwHelper::CreateTempMaterial(System.String,UnityEngine.Shader)
extern void CwHelper_CreateTempMaterial_m3BE49286AF2A1756DA1E88182E768033BC875ED7 (void);
// 0x000000F2 UnityEngine.Material CW.Common.CwHelper::CreateTempMaterial(System.String,UnityEngine.Material)
extern void CwHelper_CreateTempMaterial_mB70E91372CD632E54E9E1777C2B21D3542A65735 (void);
// 0x000000F3 T CW.Common.CwHelper::Destroy(T)
// 0x000000F4 UnityEngine.GameObject CW.Common.CwHelper::CreateGameObject(System.String,System.Int32,UnityEngine.Transform,System.String)
extern void CwHelper_CreateGameObject_mB2DAA57CAA80BE7B7A22166F67E38F4803ABC48D (void);
// 0x000000F5 UnityEngine.GameObject CW.Common.CwHelper::CreateGameObject(System.String,System.Int32,UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3,System.String)
extern void CwHelper_CreateGameObject_m2B0CEF4B06C0F558D687D0FAB5CA4AC84235524E (void);
// 0x000000F6 T CW.Common.CwHelper::CreateElement(UnityEngine.Transform)
// 0x000000F7 System.Single CW.Common.CwHelper::Reciprocal(System.Single)
extern void CwHelper_Reciprocal_m82F674E6EFDB2D64F4F74C2FC5FFC2C77963D92B (void);
// 0x000000F8 System.Double CW.Common.CwHelper::Reciprocal(System.Double)
extern void CwHelper_Reciprocal_m8231C683D7BC1F1D1A97262D60A347BC42F27CE6 (void);
// 0x000000F9 System.Single CW.Common.CwHelper::Divide(System.Single,System.Single)
extern void CwHelper_Divide_m2D997A53F17738FDB12893B3F9804E466A6B5673 (void);
// 0x000000FA System.Double CW.Common.CwHelper::Divide(System.Double,System.Double)
extern void CwHelper_Divide_mC837891D4A53345A9ED41E45023E212BF4389F3D (void);
// 0x000000FB System.Single CW.Common.CwHelper::Acos(System.Single)
extern void CwHelper_Acos_m9392C09779ADEB4D1A4B05A3D6635AD563E22BA2 (void);
// 0x000000FC System.Double CW.Common.CwHelper::Acos(System.Double)
extern void CwHelper_Acos_mA257560C3BD12D2A3363BF1297CBBBAF34AAB78C (void);
// 0x000000FD System.Single CW.Common.CwHelper::DampenFactor(System.Single,System.Single)
extern void CwHelper_DampenFactor_m730A8463F0CBE628CD66EF69E1B5969091015C9A (void);
// 0x000000FE System.Single CW.Common.CwHelper::DampenFactor(System.Single,System.Single,System.Single)
extern void CwHelper_DampenFactor_m97324224BCC9E377CDA9B55633BD5E12D434163E (void);
// 0x000000FF System.Single CW.Common.CwHelper::Atan2(UnityEngine.Vector2)
extern void CwHelper_Atan2_mC5049475C136DEE52806C5A13112192F44532F85 (void);
// 0x00000100 System.Int32 CW.Common.CwHelper::Mod(System.Int32,System.Int32)
extern void CwHelper_Mod_m51FD78C817A0A79005F4E9183BD0D29CE8E6DD40 (void);
// 0x00000101 System.Single CW.Common.CwHelper::Mod(System.Single,System.Single)
extern void CwHelper_Mod_m903A853695B88CCFE54AABC1154D996337921E42 (void);
// 0x00000102 UnityEngine.Texture2D CW.Common.CwHelper::GetReadableCopy(UnityEngine.Texture,UnityEngine.TextureFormat,System.Boolean,System.Int32,System.Int32)
extern void CwHelper_GetReadableCopy_m941B305D51AD867C81B27C71BFE9422256B2F783 (void);
// 0x00000103 System.Void CW.Common.CwHelper/<>c::.cctor()
extern void U3CU3Ec__cctor_m8E4201ECEA771B6CF4729D98E0D459449CFB180D (void);
// 0x00000104 System.Void CW.Common.CwHelper/<>c::.ctor()
extern void U3CU3Ec__ctor_m855D461BEACFD501B20F20FD2ADDEFBFCD6AD109 (void);
// 0x00000105 System.Void CW.Common.CwHelper/<>c::<.cctor>b__11_0(UnityEngine.Camera)
extern void U3CU3Ec_U3C_cctorU3Eb__11_0_mF3F7FEC2F35F9B5C341ECC220CCE9A2F55DDD60E (void);
// 0x00000106 System.Void CW.Common.CwHelper/<>c::<.cctor>b__11_1(UnityEngine.Camera)
extern void U3CU3Ec_U3C_cctorU3Eb__11_1_m18A55FBCD2BE2FF0A5779F15B2081C331AAC7E83 (void);
// 0x00000107 System.Void CW.Common.CwHelper/<>c::<.cctor>b__11_2(UnityEngine.Rendering.ScriptableRenderContext,UnityEngine.Camera)
extern void U3CU3Ec_U3C_cctorU3Eb__11_2_m691D280AD42BC6381246DE74F73C8DBEE05740BC (void);
// 0x00000108 System.Void CW.Common.CwHelper/<>c::<.cctor>b__11_3(UnityEngine.Rendering.ScriptableRenderContext,UnityEngine.Camera)
extern void U3CU3Ec_U3C_cctorU3Eb__11_3_mB36C13FABAB7212FD1A5FB83BC1441EA672DD3EE (void);
// 0x00000109 System.Int32 CW.Common.CwInput::GetTouchCount()
extern void CwInput_GetTouchCount_m72863DC75CD03D6184275414A5367C45DEE322F0 (void);
// 0x0000010A System.Void CW.Common.CwInput::GetTouch(System.Int32,System.Int32&,UnityEngine.Vector2&,System.Single&,System.Boolean&)
extern void CwInput_GetTouch_m73F989D9873E8DE49CB86AA36E52BC6720F63F2D (void);
// 0x0000010B UnityEngine.Vector2 CW.Common.CwInput::GetMousePosition()
extern void CwInput_GetMousePosition_m48A314D721594181D0590686FAD66B07D5FC2E43 (void);
// 0x0000010C System.Boolean CW.Common.CwInput::GetKeyWentDown(UnityEngine.KeyCode)
extern void CwInput_GetKeyWentDown_m7E0BA8DE675E2AE9E71E230C4125BEEE891595C4 (void);
// 0x0000010D System.Boolean CW.Common.CwInput::GetKeyIsHeld(UnityEngine.KeyCode)
extern void CwInput_GetKeyIsHeld_mAF8284015932D27D4191F0397291253BC0DABC90 (void);
// 0x0000010E System.Boolean CW.Common.CwInput::GetKeyWentUp(UnityEngine.KeyCode)
extern void CwInput_GetKeyWentUp_m0C90BB3501CC472EAAE90090E641B6AD6AB6005A (void);
// 0x0000010F System.Boolean CW.Common.CwInput::GetMouseWentDown(System.Int32)
extern void CwInput_GetMouseWentDown_m886FA03DDB83144B6FCD536D1A762131AFA6019E (void);
// 0x00000110 System.Boolean CW.Common.CwInput::GetMouseIsHeld(System.Int32)
extern void CwInput_GetMouseIsHeld_mFDF9659A6339FB9D3942D0F4F476FA5D115C96CD (void);
// 0x00000111 System.Boolean CW.Common.CwInput::GetMouseWentUp(System.Int32)
extern void CwInput_GetMouseWentUp_m94BAAF290B7C825095434239FDCD564835C0E12E (void);
// 0x00000112 System.Single CW.Common.CwInput::GetMouseWheelDelta()
extern void CwInput_GetMouseWheelDelta_m4D7C549927F10503215B3F42E49592D053FAA416 (void);
// 0x00000113 System.Boolean CW.Common.CwInput::GetMouseExists()
extern void CwInput_GetMouseExists_m0E1B17EFC8192A5DB5B9940B3A599677003F6F3B (void);
// 0x00000114 System.Boolean CW.Common.CwInput::GetKeyboardExists()
extern void CwInput_GetKeyboardExists_m6148767704F07B060110EC89778B51F76594468D (void);
// 0x00000115 System.Void CW.Common.CwShaderBundle::set_Title(System.String)
extern void CwShaderBundle_set_Title_mABA92B347C8C4F1888C5493478837423F98F91A0 (void);
// 0x00000116 System.String CW.Common.CwShaderBundle::get_Title()
extern void CwShaderBundle_get_Title_m1200553367E9BD27188F14A93E532C09E4577F4D (void);
// 0x00000117 System.Void CW.Common.CwShaderBundle::set_Target(UnityEngine.Shader)
extern void CwShaderBundle_set_Target_m22F9DDDF0D46C037123C0E79F41C2F865FD8ABE1 (void);
// 0x00000118 UnityEngine.Shader CW.Common.CwShaderBundle::get_Target()
extern void CwShaderBundle_get_Target_mC81E9DB558BA9A40B2A758C212918CC5C535C0EB (void);
// 0x00000119 System.Void CW.Common.CwShaderBundle::set_VariantHash(System.Int32)
extern void CwShaderBundle_set_VariantHash_mD6BDAB128090BED60003720A88AA9704923E186D (void);
// 0x0000011A System.Int32 CW.Common.CwShaderBundle::get_VariantHash()
extern void CwShaderBundle_get_VariantHash_m044DE14165CFA947B205E2541EA1D291544543FE (void);
// 0x0000011B System.Void CW.Common.CwShaderBundle::set_ProjectHash(System.Int32)
extern void CwShaderBundle_set_ProjectHash_mC7165DF126A72C84BB0CB92DC7C67FB0CE25035E (void);
// 0x0000011C System.Int32 CW.Common.CwShaderBundle::get_ProjectHash()
extern void CwShaderBundle_get_ProjectHash_m96EA1F0B807273E5425FF0EB38D2B9F8C786CA24 (void);
// 0x0000011D System.Collections.Generic.List`1<CW.Common.CwShaderBundle/ShaderVariant> CW.Common.CwShaderBundle::get_Variants()
extern void CwShaderBundle_get_Variants_mB7AE5630BD2F186688C4E8720A13C79C3C16826C (void);
// 0x0000011E System.Boolean CW.Common.CwShaderBundle::get_Dirty()
extern void CwShaderBundle_get_Dirty_m1E0BCCAC647BC9EDB0BF48B2B7A5364F856E3EF3 (void);
// 0x0000011F System.Int32 CW.Common.CwShaderBundle::GetProjectHash()
extern void CwShaderBundle_GetProjectHash_m7E6BE07EF0115BCA08367C01ADF98F48EAA96BEC (void);
// 0x00000120 CW.Common.CwShaderBundle/Pipeline CW.Common.CwShaderBundle::DetectProjectPipeline()
extern void CwShaderBundle_DetectProjectPipeline_m9B7C42FE7202960821B1F188F4A985F9490949AE (void);
// 0x00000121 System.Boolean CW.Common.CwShaderBundle::IsStandard(CW.Common.CwShaderBundle/Pipeline)
extern void CwShaderBundle_IsStandard_m73CF4D6CAB6AEA3948A1D18A87AB87F378E64F5E (void);
// 0x00000122 System.Boolean CW.Common.CwShaderBundle::IsScriptable(CW.Common.CwShaderBundle/Pipeline)
extern void CwShaderBundle_IsScriptable_m228C7267122FAD870545FC65D2816F32EB43AD3B (void);
// 0x00000123 System.Boolean CW.Common.CwShaderBundle::IsURP(CW.Common.CwShaderBundle/Pipeline)
extern void CwShaderBundle_IsURP_m71C4A1038F86CF04DA66435333C449E81BDAC91B (void);
// 0x00000124 System.Boolean CW.Common.CwShaderBundle::IsHDRP(CW.Common.CwShaderBundle/Pipeline)
extern void CwShaderBundle_IsHDRP_mB5943E13CCBE3A12887B7AAB012BFF16D9DC79E2 (void);
// 0x00000125 System.Void CW.Common.CwShaderBundle::.ctor()
extern void CwShaderBundle__ctor_m4D1E6C1F0EA9985613823C236000145BE29D5142 (void);
// 0x00000126 System.String CW.Common.CwShaderBundle/ShaderVariant::get_HashString()
extern void ShaderVariant_get_HashString_m05E9900D728E32F245ACEEEEBDB079B4419BF0DF (void);
// 0x00000127 System.Void CW.Common.CwShaderBundle/ShaderVariant::.ctor()
extern void ShaderVariant__ctor_m62A30B3B051D197CA6DE73D21BC85D8121DA284B (void);
// 0x00000128 System.Void Lean.Touch.LeanPulseScale::set_BaseScale(UnityEngine.Vector3)
extern void LeanPulseScale_set_BaseScale_m4B768B064BAB62AD08F305A195642E05D6B22391 (void);
// 0x00000129 UnityEngine.Vector3 Lean.Touch.LeanPulseScale::get_BaseScale()
extern void LeanPulseScale_get_BaseScale_m27D6D46137B94F4081AA89ACD12DF253183D2FC1 (void);
// 0x0000012A System.Void Lean.Touch.LeanPulseScale::set_Size(System.Single)
extern void LeanPulseScale_set_Size_mAE3D13FA565AFC5666F25552620765675F2DCA34 (void);
// 0x0000012B System.Single Lean.Touch.LeanPulseScale::get_Size()
extern void LeanPulseScale_get_Size_m5ED588012827D464CD4EEDA85FD4F8C84E22C0D6 (void);
// 0x0000012C System.Void Lean.Touch.LeanPulseScale::set_PulseInterval(System.Single)
extern void LeanPulseScale_set_PulseInterval_m2C5C7519A1DC36DA76F26903B5DB0BCAA015D699 (void);
// 0x0000012D System.Single Lean.Touch.LeanPulseScale::get_PulseInterval()
extern void LeanPulseScale_get_PulseInterval_m492C9D07F01F1DCF480A182DCA5A6EDCBD45DC10 (void);
// 0x0000012E System.Void Lean.Touch.LeanPulseScale::set_PulseSize(System.Single)
extern void LeanPulseScale_set_PulseSize_m9DEECF0CBA5F1C2CE7D20ED04E4D471576F7B16C (void);
// 0x0000012F System.Single Lean.Touch.LeanPulseScale::get_PulseSize()
extern void LeanPulseScale_get_PulseSize_m139062A4C0B457F07C3E67D2F2E5C3ACFB7A3DCB (void);
// 0x00000130 System.Void Lean.Touch.LeanPulseScale::set_Damping(System.Single)
extern void LeanPulseScale_set_Damping_mE405B5B718CDD62AA7924D0A353D4CC7691818DE (void);
// 0x00000131 System.Single Lean.Touch.LeanPulseScale::get_Damping()
extern void LeanPulseScale_get_Damping_m6ECAFE5CFBB901B79AF6218B9A3E37E7830EBE93 (void);
// 0x00000132 System.Void Lean.Touch.LeanPulseScale::Update()
extern void LeanPulseScale_Update_m2BC8B83015F15A629FCD11BCD047F5FAAC1E4CB6 (void);
// 0x00000133 System.Void Lean.Touch.LeanPulseScale::.ctor()
extern void LeanPulseScale__ctor_m8F1705BC190A5B9117B95C4C2D648C3D0DF48DA1 (void);
// 0x00000134 System.Void Lean.Touch.LeanTouchEvents::OnEnable()
extern void LeanTouchEvents_OnEnable_mC414AB54B7A99A7E68309090C6F029D1CFAF169F (void);
// 0x00000135 System.Void Lean.Touch.LeanTouchEvents::OnDisable()
extern void LeanTouchEvents_OnDisable_mE1F3C9C7F96517CF14F32FEDD36532F36FBEF20F (void);
// 0x00000136 System.Void Lean.Touch.LeanTouchEvents::HandleFingerDown(Lean.Touch.LeanFinger)
extern void LeanTouchEvents_HandleFingerDown_mAE022048D4128DCE5A3211D9F39125A9C951C729 (void);
// 0x00000137 System.Void Lean.Touch.LeanTouchEvents::HandleFingerUpdate(Lean.Touch.LeanFinger)
extern void LeanTouchEvents_HandleFingerUpdate_m17D46206BEF5488ED68CD9050C1503B00530C33E (void);
// 0x00000138 System.Void Lean.Touch.LeanTouchEvents::HandleFingerUp(Lean.Touch.LeanFinger)
extern void LeanTouchEvents_HandleFingerUp_m6E5D7DDCB5A95E134D4B42125A0EAB19E7CF1FC6 (void);
// 0x00000139 System.Void Lean.Touch.LeanTouchEvents::HandleFingerTap(Lean.Touch.LeanFinger)
extern void LeanTouchEvents_HandleFingerTap_mE944D4026F95BDD6D11EAC2AD65C9AF51736745B (void);
// 0x0000013A System.Void Lean.Touch.LeanTouchEvents::HandleFingerSwipe(Lean.Touch.LeanFinger)
extern void LeanTouchEvents_HandleFingerSwipe_mE0F4D3AD8DA99C3C853FFDC523830A70A39EF166 (void);
// 0x0000013B System.Void Lean.Touch.LeanTouchEvents::HandleGesture(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanTouchEvents_HandleGesture_mA895A22DA9065F52E833971692CF505577867E7F (void);
// 0x0000013C System.Void Lean.Touch.LeanTouchEvents::.ctor()
extern void LeanTouchEvents__ctor_m7E262CCD26482369D4776CA6D1240B8E2305EE33 (void);
// 0x0000013D System.Void Lean.Touch.LeanDragCamera::set_Sensitivity(System.Single)
extern void LeanDragCamera_set_Sensitivity_m1C81D4182C940F9E8DB3C68916588153D3F886F0 (void);
// 0x0000013E System.Single Lean.Touch.LeanDragCamera::get_Sensitivity()
extern void LeanDragCamera_get_Sensitivity_m66FAA935BBA3AD970CDBAA16B7FFF89CBD997978 (void);
// 0x0000013F System.Void Lean.Touch.LeanDragCamera::set_Damping(System.Single)
extern void LeanDragCamera_set_Damping_m8604E9F60D1F8C64E28975992EAFC012AB1858A8 (void);
// 0x00000140 System.Single Lean.Touch.LeanDragCamera::get_Damping()
extern void LeanDragCamera_get_Damping_m77C362A45C19B100348BE4D8F1B1DA489FC4D490 (void);
// 0x00000141 System.Void Lean.Touch.LeanDragCamera::set_Inertia(System.Single)
extern void LeanDragCamera_set_Inertia_m4097B290AD3CA6E67D594143F7C45B7B5BC855D0 (void);
// 0x00000142 System.Single Lean.Touch.LeanDragCamera::get_Inertia()
extern void LeanDragCamera_get_Inertia_mE8809A2F82FDA976BF1AA4B6FBBECDE051059F8E (void);
// 0x00000143 System.Void Lean.Touch.LeanDragCamera::MoveToSelection()
extern void LeanDragCamera_MoveToSelection_m05ECCF1E496F57DC13BACE4B9A0C1361905704A7 (void);
// 0x00000144 System.Void Lean.Touch.LeanDragCamera::AddFinger(Lean.Touch.LeanFinger)
extern void LeanDragCamera_AddFinger_m55F183983349EE625FAB24DC31640C8AE7E47D04 (void);
// 0x00000145 System.Void Lean.Touch.LeanDragCamera::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanDragCamera_RemoveFinger_mAEDA725C89F6E51495BD7B613340B674DD9384CF (void);
// 0x00000146 System.Void Lean.Touch.LeanDragCamera::RemoveAllFingers()
extern void LeanDragCamera_RemoveAllFingers_m861FF6CDD3A9264A534402BD3298F71EA9CECF00 (void);
// 0x00000147 System.Void Lean.Touch.LeanDragCamera::Awake()
extern void LeanDragCamera_Awake_m451DE0D445CAA3BEFE2AA73EC014BF84803B2DE5 (void);
// 0x00000148 System.Void Lean.Touch.LeanDragCamera::LateUpdate()
extern void LeanDragCamera_LateUpdate_m6E6C00ED3DFC57DC5CA56867D8E1E1747DA5DBE0 (void);
// 0x00000149 System.Void Lean.Touch.LeanDragCamera::.ctor()
extern void LeanDragCamera__ctor_mAF25EB44908D467FE8D8A4275E24E88DD305C4D9 (void);
// 0x0000014A System.Void Lean.Touch.LeanDragTrail::set_Prefab(UnityEngine.LineRenderer)
extern void LeanDragTrail_set_Prefab_m57B4216117AA799A0A5D652E24818925989B2256 (void);
// 0x0000014B UnityEngine.LineRenderer Lean.Touch.LeanDragTrail::get_Prefab()
extern void LeanDragTrail_get_Prefab_mF3EA0412E30A49053BF93A1C924A0BEA524CACDD (void);
// 0x0000014C System.Void Lean.Touch.LeanDragTrail::set_MaxTrails(System.Int32)
extern void LeanDragTrail_set_MaxTrails_m250A8263465A844AD221DF25E48BC2B0A13932BF (void);
// 0x0000014D System.Int32 Lean.Touch.LeanDragTrail::get_MaxTrails()
extern void LeanDragTrail_get_MaxTrails_mD26AC87BD9A93B70F795E5F26E85DD52C0BE7668 (void);
// 0x0000014E System.Void Lean.Touch.LeanDragTrail::set_FadeTime(System.Single)
extern void LeanDragTrail_set_FadeTime_m4D80442C83183E97BB03563426A8AA83E1E30BE2 (void);
// 0x0000014F System.Single Lean.Touch.LeanDragTrail::get_FadeTime()
extern void LeanDragTrail_get_FadeTime_m60CA250833D19D827883195D1EC6CAE07F67C0A1 (void);
// 0x00000150 System.Void Lean.Touch.LeanDragTrail::set_StartColor(UnityEngine.Color)
extern void LeanDragTrail_set_StartColor_m06B53E1CC698613C7E9B973CEE226CAA287861FE (void);
// 0x00000151 UnityEngine.Color Lean.Touch.LeanDragTrail::get_StartColor()
extern void LeanDragTrail_get_StartColor_mEF04D976AAFE97AA4587C40A439BD7EE32EC10BF (void);
// 0x00000152 System.Void Lean.Touch.LeanDragTrail::set_EndColor(UnityEngine.Color)
extern void LeanDragTrail_set_EndColor_mB4F9B16C99AE9E2584DBB4AE946B65F2039BABF7 (void);
// 0x00000153 UnityEngine.Color Lean.Touch.LeanDragTrail::get_EndColor()
extern void LeanDragTrail_get_EndColor_mFF28EB8193B208911CFD6FDEB77592128B02D4A0 (void);
// 0x00000154 System.Void Lean.Touch.LeanDragTrail::AddFinger(Lean.Touch.LeanFinger)
extern void LeanDragTrail_AddFinger_mDE236F2FB6F9878A16FC99DFB67056C29202494C (void);
// 0x00000155 System.Void Lean.Touch.LeanDragTrail::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanDragTrail_RemoveFinger_m56853CAC438C26D7FADE3EE31E3C04E86E583638 (void);
// 0x00000156 System.Void Lean.Touch.LeanDragTrail::RemoveAllFingers()
extern void LeanDragTrail_RemoveAllFingers_m29AA096F3F7A8E79A564F6ED942AE72D99AD17C5 (void);
// 0x00000157 System.Void Lean.Touch.LeanDragTrail::Awake()
extern void LeanDragTrail_Awake_m721E49DF8B683ABF90B6D8D752F67B091216BBBC (void);
// 0x00000158 System.Void Lean.Touch.LeanDragTrail::OnEnable()
extern void LeanDragTrail_OnEnable_m3E9CBD4EDED41BE7763E6A4A592A3194BB38D9CC (void);
// 0x00000159 System.Void Lean.Touch.LeanDragTrail::OnDisable()
extern void LeanDragTrail_OnDisable_mF28073A9679EE770A917FAD8D47D52D8727C12DC (void);
// 0x0000015A System.Void Lean.Touch.LeanDragTrail::Update()
extern void LeanDragTrail_Update_m51726097926466FD817E240056463A129A1A1EE9 (void);
// 0x0000015B System.Void Lean.Touch.LeanDragTrail::UpdateLine(Lean.Touch.LeanDragTrail/FingerData,Lean.Touch.LeanFinger,UnityEngine.LineRenderer)
extern void LeanDragTrail_UpdateLine_mBF446EBAF0B9B567293D2BE814185A1A2CC173B2 (void);
// 0x0000015C System.Void Lean.Touch.LeanDragTrail::HandleFingerUp(Lean.Touch.LeanFinger)
extern void LeanDragTrail_HandleFingerUp_m6267BADCA9509726D122A079C7F5FF610F32CDD1 (void);
// 0x0000015D System.Void Lean.Touch.LeanDragTrail::.ctor()
extern void LeanDragTrail__ctor_mEBF2377F038164F28196A85BA034D9813411C2E1 (void);
// 0x0000015E System.Void Lean.Touch.LeanDragTrail/FingerData::.ctor()
extern void FingerData__ctor_m7450073E6148306E9748D344D40FA8154D198E89 (void);
// 0x0000015F System.Void Lean.Touch.LeanDragTranslate::set_Camera(UnityEngine.Camera)
extern void LeanDragTranslate_set_Camera_m57BD987556651DB0FA1E26BC398A4D9F14102424 (void);
// 0x00000160 UnityEngine.Camera Lean.Touch.LeanDragTranslate::get_Camera()
extern void LeanDragTranslate_get_Camera_mD8943DF5C386021E8D4BA56F08CA27B63C62630A (void);
// 0x00000161 System.Void Lean.Touch.LeanDragTranslate::set_Sensitivity(System.Single)
extern void LeanDragTranslate_set_Sensitivity_mEA76EC88BBFF3D30984233922EE7FCFF9B1EA395 (void);
// 0x00000162 System.Single Lean.Touch.LeanDragTranslate::get_Sensitivity()
extern void LeanDragTranslate_get_Sensitivity_m4DDFEBDE698AE5501C4EA5393DCCD345EC7BA30A (void);
// 0x00000163 System.Void Lean.Touch.LeanDragTranslate::set_Damping(System.Single)
extern void LeanDragTranslate_set_Damping_m6DA53C16705527F1A629B734824CEBF06381E7D6 (void);
// 0x00000164 System.Single Lean.Touch.LeanDragTranslate::get_Damping()
extern void LeanDragTranslate_get_Damping_m57A4DB93B6A308A4180974F96265D8E4C57CA042 (void);
// 0x00000165 System.Void Lean.Touch.LeanDragTranslate::set_Inertia(System.Single)
extern void LeanDragTranslate_set_Inertia_mE7BFB371ECE90D225C3BB8F332797709E66BE1C3 (void);
// 0x00000166 System.Single Lean.Touch.LeanDragTranslate::get_Inertia()
extern void LeanDragTranslate_get_Inertia_m8979F156FD97D2235D545679DE8ADB48B9FC4C03 (void);
// 0x00000167 System.Void Lean.Touch.LeanDragTranslate::AddFinger(Lean.Touch.LeanFinger)
extern void LeanDragTranslate_AddFinger_mF898588B94440EEC0AEA5A58B7E47F840A37574C (void);
// 0x00000168 System.Void Lean.Touch.LeanDragTranslate::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanDragTranslate_RemoveFinger_m7183F45CF5D81903A756AFAC6BCD4D69ECBC2E67 (void);
// 0x00000169 System.Void Lean.Touch.LeanDragTranslate::RemoveAllFingers()
extern void LeanDragTranslate_RemoveAllFingers_mCDC54370FA3659FAFBBFA96F9494F220D617AB35 (void);
// 0x0000016A System.Void Lean.Touch.LeanDragTranslate::Awake()
extern void LeanDragTranslate_Awake_m9F426040EC37B9BBE800A8BAFCCF10A6C525656D (void);
// 0x0000016B System.Void Lean.Touch.LeanDragTranslate::Update()
extern void LeanDragTranslate_Update_m04A86B2C707C9498AE80B958E5630CE0A145B90A (void);
// 0x0000016C System.Void Lean.Touch.LeanDragTranslate::TranslateUI(UnityEngine.Vector2)
extern void LeanDragTranslate_TranslateUI_m290024974B434075C67EF4BFA72976BF0E36C456 (void);
// 0x0000016D System.Void Lean.Touch.LeanDragTranslate::Translate(UnityEngine.Vector2)
extern void LeanDragTranslate_Translate_m9035D8268916D959CC219ED59069949E2566E220 (void);
// 0x0000016E System.Void Lean.Touch.LeanDragTranslate::.ctor()
extern void LeanDragTranslate__ctor_m9FBC5CBA225990503BDBDA962B53D0C31DE8EA5A (void);
// 0x0000016F System.Int32 Lean.Touch.LeanFingerData::Count(System.Collections.Generic.List`1<T>)
// 0x00000170 System.Boolean Lean.Touch.LeanFingerData::Exists(System.Collections.Generic.List`1<T>,Lean.Touch.LeanFinger)
// 0x00000171 System.Void Lean.Touch.LeanFingerData::Remove(System.Collections.Generic.List`1<T>,Lean.Touch.LeanFinger,System.Collections.Generic.Stack`1<T>)
// 0x00000172 System.Void Lean.Touch.LeanFingerData::RemoveAll(System.Collections.Generic.List`1<T>,System.Collections.Generic.Stack`1<T>)
// 0x00000173 T Lean.Touch.LeanFingerData::Find(System.Collections.Generic.List`1<T>,Lean.Touch.LeanFinger)
// 0x00000174 T Lean.Touch.LeanFingerData::FindOrCreate(System.Collections.Generic.List`1<T>&,Lean.Touch.LeanFinger)
// 0x00000175 System.Void Lean.Touch.LeanFingerData::.ctor()
extern void LeanFingerData__ctor_m50A8752F194A483144775982725072F53EB585A9 (void);
// 0x00000176 System.Void Lean.Touch.LeanFingerDown::set_IgnoreStartedOverGui(System.Boolean)
extern void LeanFingerDown_set_IgnoreStartedOverGui_m7CFC0C1A9630AB821056E10C8A70AC17A2CAE924 (void);
// 0x00000177 System.Boolean Lean.Touch.LeanFingerDown::get_IgnoreStartedOverGui()
extern void LeanFingerDown_get_IgnoreStartedOverGui_m6E3AD1D3FA285747AF3E055D06252426A0B5C89B (void);
// 0x00000178 System.Void Lean.Touch.LeanFingerDown::set_RequiredButtons(Lean.Touch.LeanFingerDown/ButtonTypes)
extern void LeanFingerDown_set_RequiredButtons_m2DE3094CD987297C98C9923EFA1580D664D487DC (void);
// 0x00000179 Lean.Touch.LeanFingerDown/ButtonTypes Lean.Touch.LeanFingerDown::get_RequiredButtons()
extern void LeanFingerDown_get_RequiredButtons_m970A438AA531DDE093A9C313DED1E33989412E9D (void);
// 0x0000017A System.Void Lean.Touch.LeanFingerDown::set_RequiredSelectable(Lean.Common.LeanSelectable)
extern void LeanFingerDown_set_RequiredSelectable_m08078C78B237003E222906304448B0CB5A3D68DB (void);
// 0x0000017B Lean.Common.LeanSelectable Lean.Touch.LeanFingerDown::get_RequiredSelectable()
extern void LeanFingerDown_get_RequiredSelectable_m46A924E417DD143E4698881190A9DC2210231CA7 (void);
// 0x0000017C Lean.Touch.LeanFingerDown/LeanFingerEvent Lean.Touch.LeanFingerDown::get_OnFinger()
extern void LeanFingerDown_get_OnFinger_m2B537EF0D841E3A20E7943301110C0323BC70366 (void);
// 0x0000017D Lean.Touch.LeanFingerDown/Vector3Event Lean.Touch.LeanFingerDown::get_OnWorld()
extern void LeanFingerDown_get_OnWorld_m6791C41B8F97FF73AC8C5CE3456AD0AD9FC3065E (void);
// 0x0000017E Lean.Touch.LeanFingerDown/Vector2Event Lean.Touch.LeanFingerDown::get_OnScreen()
extern void LeanFingerDown_get_OnScreen_mD9754163AB3E5DC296BC390C3AC3F4128C01F36B (void);
// 0x0000017F System.Void Lean.Touch.LeanFingerDown::Awake()
extern void LeanFingerDown_Awake_mB7BD1153A5A5161E382988EB276B65E1AA51CD72 (void);
// 0x00000180 System.Void Lean.Touch.LeanFingerDown::OnEnable()
extern void LeanFingerDown_OnEnable_mC6A4F9D327CB873D79C7A58B59EE94561679725C (void);
// 0x00000181 System.Void Lean.Touch.LeanFingerDown::OnDisable()
extern void LeanFingerDown_OnDisable_mA7FE3171275279350B08420017ACC83FA49AAE2A (void);
// 0x00000182 System.Boolean Lean.Touch.LeanFingerDown::UseFinger(Lean.Touch.LeanFinger)
extern void LeanFingerDown_UseFinger_mB64DF637128A6A7B124D89ECA8538DE9D9C29AB4 (void);
// 0x00000183 System.Void Lean.Touch.LeanFingerDown::InvokeFinger(Lean.Touch.LeanFinger)
extern void LeanFingerDown_InvokeFinger_mB7672DF59C09525D07C3BE8E8DE7E1196FE79FCB (void);
// 0x00000184 System.Void Lean.Touch.LeanFingerDown::HandleFingerDown(Lean.Touch.LeanFinger)
extern void LeanFingerDown_HandleFingerDown_mF6BC0EA194BDFD9FECF39FA2342779EFEA29E46B (void);
// 0x00000185 System.Boolean Lean.Touch.LeanFingerDown::RequiredButtonPressed(Lean.Touch.LeanFinger)
extern void LeanFingerDown_RequiredButtonPressed_mFB97FAA2925C17ED2DD0EE9DE58C86069C902D85 (void);
// 0x00000186 System.Void Lean.Touch.LeanFingerDown::.ctor()
extern void LeanFingerDown__ctor_m5F625A3552A4FD79F0C7B49912EF809A98B0B479 (void);
// 0x00000187 System.Void Lean.Touch.LeanFingerDown/LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_m5A19379784332B46F874D4B9AE9929C9BFA9F6D9 (void);
// 0x00000188 System.Void Lean.Touch.LeanFingerDown/Vector3Event::.ctor()
extern void Vector3Event__ctor_m2065324DD97EAC86B7FFC927570F589E8F1B75C5 (void);
// 0x00000189 System.Void Lean.Touch.LeanFingerDown/Vector2Event::.ctor()
extern void Vector2Event__ctor_mDD272750EEB9A8CC6E2DB693ABCEFB3DBC36688F (void);
// 0x0000018A System.Void Lean.Touch.LeanFingerFilter::.ctor(System.Boolean)
extern void LeanFingerFilter__ctor_mCC3A54FC24D242DBB1B114241DD9E015EEAC5050 (void);
// 0x0000018B System.Void Lean.Touch.LeanFingerFilter::.ctor(Lean.Touch.LeanFingerFilter/FilterType,System.Boolean,System.Int32,System.Int32,Lean.Common.LeanSelectable)
extern void LeanFingerFilter__ctor_m4B9A9ED2D804D825B39EA2069B4BCB9F62B8F92B (void);
// 0x0000018C System.Void Lean.Touch.LeanFingerFilter::UpdateRequiredSelectable(UnityEngine.GameObject)
extern void LeanFingerFilter_UpdateRequiredSelectable_m8ED738073CC8F1BA59DC9CC3109725AA700ACD39 (void);
// 0x0000018D System.Void Lean.Touch.LeanFingerFilter::AddFinger(Lean.Touch.LeanFinger)
extern void LeanFingerFilter_AddFinger_m05398926AC1626D0B41B2490D7E80F47843D74EF (void);
// 0x0000018E System.Void Lean.Touch.LeanFingerFilter::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanFingerFilter_RemoveFinger_mB8924B3B08004F37FA5EA002266832056DA5265A (void);
// 0x0000018F System.Void Lean.Touch.LeanFingerFilter::RemoveAllFingers()
extern void LeanFingerFilter_RemoveAllFingers_m0BEF38CD9EFC6C2FC689D1CA5680619974164C6F (void);
// 0x00000190 System.Collections.Generic.List`1<Lean.Touch.LeanFinger> Lean.Touch.LeanFingerFilter::UpdateAndGetFingers(System.Boolean)
extern void LeanFingerFilter_UpdateAndGetFingers_mD81CA832EC55AF7692514C70AC4A02F71B3C8573 (void);
// 0x00000191 System.Boolean Lean.Touch.LeanFingerFilter::SimulatedFingersExist(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanFingerFilter_SimulatedFingersExist_mD392B4EBC14B8F8AC5904593AED8CAC67A190366 (void);
// 0x00000192 System.Void Lean.Touch.LeanFingerOld::set_IgnoreStartedOverGui(System.Boolean)
extern void LeanFingerOld_set_IgnoreStartedOverGui_m7A63071DD65630C0B4734541F0A68A090C1EEE0C (void);
// 0x00000193 System.Boolean Lean.Touch.LeanFingerOld::get_IgnoreStartedOverGui()
extern void LeanFingerOld_get_IgnoreStartedOverGui_m8DE7FF041FB4CC3618D7F6758F419B65BD0A7D0C (void);
// 0x00000194 System.Void Lean.Touch.LeanFingerOld::set_IgnoreIsOverGui(System.Boolean)
extern void LeanFingerOld_set_IgnoreIsOverGui_m9D9B680147D189B60ADE3B56891E2EDEACB5E2FB (void);
// 0x00000195 System.Boolean Lean.Touch.LeanFingerOld::get_IgnoreIsOverGui()
extern void LeanFingerOld_get_IgnoreIsOverGui_m04FAF6B4BBDDC03A55BAFD7DEFC229AA044EA016 (void);
// 0x00000196 System.Void Lean.Touch.LeanFingerOld::set_RequiredSelectable(Lean.Common.LeanSelectable)
extern void LeanFingerOld_set_RequiredSelectable_mC9B95F2440F4F91C5A3A35924895A56B0A40E65F (void);
// 0x00000197 Lean.Common.LeanSelectable Lean.Touch.LeanFingerOld::get_RequiredSelectable()
extern void LeanFingerOld_get_RequiredSelectable_m8E00340E045841D1758C46B9886622E0DA0053D2 (void);
// 0x00000198 Lean.Touch.LeanFingerOld/LeanFingerEvent Lean.Touch.LeanFingerOld::get_OnFinger()
extern void LeanFingerOld_get_OnFinger_m78C3B08F8D1AC8CEFB0A24E7C311414C415C54B9 (void);
// 0x00000199 Lean.Touch.LeanFingerOld/Vector3Event Lean.Touch.LeanFingerOld::get_OnWorld()
extern void LeanFingerOld_get_OnWorld_m7FD52693FBEE9BF5A60FF85002F9B92344C7A6EA (void);
// 0x0000019A Lean.Touch.LeanFingerOld/Vector2Event Lean.Touch.LeanFingerOld::get_OnScreen()
extern void LeanFingerOld_get_OnScreen_mFEA8A4F233973708E7CFC1D36DD0A12AD0145D66 (void);
// 0x0000019B System.Void Lean.Touch.LeanFingerOld::Start()
extern void LeanFingerOld_Start_m5B458588E1FFA58276E649B33E839662CB280CF0 (void);
// 0x0000019C System.Void Lean.Touch.LeanFingerOld::OnEnable()
extern void LeanFingerOld_OnEnable_mDB644842C4717FE6A1048B864C80C77E6FAAD318 (void);
// 0x0000019D System.Void Lean.Touch.LeanFingerOld::OnDisable()
extern void LeanFingerOld_OnDisable_m8960FC5621E19A6BF761EA83AF79C05F5D9DB194 (void);
// 0x0000019E System.Void Lean.Touch.LeanFingerOld::HandleFingerOld(Lean.Touch.LeanFinger)
extern void LeanFingerOld_HandleFingerOld_m0F15F2B9AFB43E82089232FE34F6C545103B602E (void);
// 0x0000019F System.Void Lean.Touch.LeanFingerOld::.ctor()
extern void LeanFingerOld__ctor_mAC02FD851F47668E4891F5B0DDB58B0475948D73 (void);
// 0x000001A0 System.Void Lean.Touch.LeanFingerOld/LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_m81C9A08B0437D28134F73713211DAB97B4EDDE93 (void);
// 0x000001A1 System.Void Lean.Touch.LeanFingerOld/Vector3Event::.ctor()
extern void Vector3Event__ctor_mB1A204AF67A624D749B896533FA385389AA83A7F (void);
// 0x000001A2 System.Void Lean.Touch.LeanFingerOld/Vector2Event::.ctor()
extern void Vector2Event__ctor_m2C7589C510A2388532CC0FF82C24578CC16F8760 (void);
// 0x000001A3 System.Void Lean.Touch.LeanFingerSwipe::set_IgnoreStartedOverGui(System.Boolean)
extern void LeanFingerSwipe_set_IgnoreStartedOverGui_m59129D2FDB5EAEB275B487F28C316DB4EAA8ED27 (void);
// 0x000001A4 System.Boolean Lean.Touch.LeanFingerSwipe::get_IgnoreStartedOverGui()
extern void LeanFingerSwipe_get_IgnoreStartedOverGui_m1A8BA28F2ABA9C806BDF36AEA54A328BB20EC969 (void);
// 0x000001A5 System.Void Lean.Touch.LeanFingerSwipe::set_IgnoreIsOverGui(System.Boolean)
extern void LeanFingerSwipe_set_IgnoreIsOverGui_m8BCD5A328B014D462491D001942466F284E9CFD7 (void);
// 0x000001A6 System.Boolean Lean.Touch.LeanFingerSwipe::get_IgnoreIsOverGui()
extern void LeanFingerSwipe_get_IgnoreIsOverGui_m5A39F372A535318932B28B4B9F5F5809C174DFBD (void);
// 0x000001A7 System.Void Lean.Touch.LeanFingerSwipe::set_RequiredSelectable(Lean.Common.LeanSelectable)
extern void LeanFingerSwipe_set_RequiredSelectable_m2903DD987618F8DD6F369F35D1776EBF50E02F73 (void);
// 0x000001A8 Lean.Common.LeanSelectable Lean.Touch.LeanFingerSwipe::get_RequiredSelectable()
extern void LeanFingerSwipe_get_RequiredSelectable_mD2DF985222EEFAF25236D982B1D6AA7DAF66411D (void);
// 0x000001A9 System.Void Lean.Touch.LeanFingerSwipe::Start()
extern void LeanFingerSwipe_Start_mA5F83708E39B812EA0E8E80C64D893E4D59F51F0 (void);
// 0x000001AA System.Void Lean.Touch.LeanFingerSwipe::OnEnable()
extern void LeanFingerSwipe_OnEnable_m1B74E05D8C5E5E65872A2E5B1ED9DA706D0E646C (void);
// 0x000001AB System.Void Lean.Touch.LeanFingerSwipe::OnDisable()
extern void LeanFingerSwipe_OnDisable_m7B2360FF28472EC8F5CA7EED80074F9C86359DF1 (void);
// 0x000001AC System.Void Lean.Touch.LeanFingerSwipe::HandleFingerSwipe(Lean.Touch.LeanFinger)
extern void LeanFingerSwipe_HandleFingerSwipe_m8CA4A5A7A28A33F07E7DADD3FCC75A9DF766981C (void);
// 0x000001AD System.Void Lean.Touch.LeanFingerSwipe::.ctor()
extern void LeanFingerSwipe__ctor_mAA12E9F3B30965413EC84DA9BE3281589779CAEA (void);
// 0x000001AE System.Void Lean.Touch.LeanFingerTap::set_IgnoreStartedOverGui(System.Boolean)
extern void LeanFingerTap_set_IgnoreStartedOverGui_mF439D6DCE86EACACD1B64D4E0F4024E9AE8F5FF2 (void);
// 0x000001AF System.Boolean Lean.Touch.LeanFingerTap::get_IgnoreStartedOverGui()
extern void LeanFingerTap_get_IgnoreStartedOverGui_mB02D8067E0021CBD0442B419099CC1BBAE419168 (void);
// 0x000001B0 System.Void Lean.Touch.LeanFingerTap::set_IgnoreIsOverGui(System.Boolean)
extern void LeanFingerTap_set_IgnoreIsOverGui_m69044F4BFEA5028196EF2D4757D8B7D17880D01C (void);
// 0x000001B1 System.Boolean Lean.Touch.LeanFingerTap::get_IgnoreIsOverGui()
extern void LeanFingerTap_get_IgnoreIsOverGui_m44F14978A6FB794C15598EEAD2B8133DC8FE61DA (void);
// 0x000001B2 System.Void Lean.Touch.LeanFingerTap::set_RequiredSelectable(Lean.Common.LeanSelectable)
extern void LeanFingerTap_set_RequiredSelectable_mF88A73DCA146844334FCFB7B2477748DA79F5113 (void);
// 0x000001B3 Lean.Common.LeanSelectable Lean.Touch.LeanFingerTap::get_RequiredSelectable()
extern void LeanFingerTap_get_RequiredSelectable_mF870D5F7F3F91EC0F81AAAED47A144C7538E0F83 (void);
// 0x000001B4 System.Void Lean.Touch.LeanFingerTap::set_RequiredTapCount(System.Int32)
extern void LeanFingerTap_set_RequiredTapCount_mA0612CFDBB30C9EAB701FC170B80B9DB5A56E287 (void);
// 0x000001B5 System.Int32 Lean.Touch.LeanFingerTap::get_RequiredTapCount()
extern void LeanFingerTap_get_RequiredTapCount_mEA1093741E790DD0270F45881E876B012E15733C (void);
// 0x000001B6 System.Void Lean.Touch.LeanFingerTap::set_RequiredTapInterval(System.Int32)
extern void LeanFingerTap_set_RequiredTapInterval_m481830CE8C8CCCEC16A5650B0558EFEBD8783F7B (void);
// 0x000001B7 System.Int32 Lean.Touch.LeanFingerTap::get_RequiredTapInterval()
extern void LeanFingerTap_get_RequiredTapInterval_m9C7AE59B981AB3EB85CF3EB7228FC6672472A897 (void);
// 0x000001B8 Lean.Touch.LeanFingerTap/LeanFingerEvent Lean.Touch.LeanFingerTap::get_OnFinger()
extern void LeanFingerTap_get_OnFinger_m63D5FA1484C40838633A42FA08FDB05BA0655F21 (void);
// 0x000001B9 Lean.Touch.LeanFingerTap/IntEvent Lean.Touch.LeanFingerTap::get_OnCount()
extern void LeanFingerTap_get_OnCount_m069DE54A016FAF7A36C72AD7AD1BDEA42841E169 (void);
// 0x000001BA Lean.Touch.LeanFingerTap/Vector3Event Lean.Touch.LeanFingerTap::get_OnWorld()
extern void LeanFingerTap_get_OnWorld_m4ED25B5F883DDE96096E5D41BA2267F8E693810F (void);
// 0x000001BB Lean.Touch.LeanFingerTap/Vector2Event Lean.Touch.LeanFingerTap::get_OnScreen()
extern void LeanFingerTap_get_OnScreen_mDA41E80ED2528F6566C9498FA44CE202F3709143 (void);
// 0x000001BC System.Void Lean.Touch.LeanFingerTap::Start()
extern void LeanFingerTap_Start_m9A1D0A01FCF6C7793C508C36B37B7BDDC30A90C7 (void);
// 0x000001BD System.Void Lean.Touch.LeanFingerTap::OnEnable()
extern void LeanFingerTap_OnEnable_mC892C650831ADBCF6F4C0269097A65A7B9C86940 (void);
// 0x000001BE System.Void Lean.Touch.LeanFingerTap::OnDisable()
extern void LeanFingerTap_OnDisable_m520948BC519A56D004EF9B7E76CB319BFEF0885D (void);
// 0x000001BF System.Void Lean.Touch.LeanFingerTap::HandleFingerTap(Lean.Touch.LeanFinger)
extern void LeanFingerTap_HandleFingerTap_m59E8A9D4A9D72E84EE853B9A278BAC4C62040420 (void);
// 0x000001C0 System.Void Lean.Touch.LeanFingerTap::.ctor()
extern void LeanFingerTap__ctor_mC5EB5DEBD46A54B9BE08CF4E71A7FC739D50C31C (void);
// 0x000001C1 System.Void Lean.Touch.LeanFingerTap/LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_m066C6057317457EC86CE5F05C51FCF1A52FF91C6 (void);
// 0x000001C2 System.Void Lean.Touch.LeanFingerTap/Vector3Event::.ctor()
extern void Vector3Event__ctor_m7CC3B8D85064BB05E82B262B471858E8812C70F5 (void);
// 0x000001C3 System.Void Lean.Touch.LeanFingerTap/Vector2Event::.ctor()
extern void Vector2Event__ctor_m0F1176A2133260E502E2E8472DF82E2D641920AD (void);
// 0x000001C4 System.Void Lean.Touch.LeanFingerTap/IntEvent::.ctor()
extern void IntEvent__ctor_m755DDF5118C169A7E72DA407292284B67620AE48 (void);
// 0x000001C5 System.Void Lean.Touch.LeanFingerUp::set_IgnoreIsOverGui(System.Boolean)
extern void LeanFingerUp_set_IgnoreIsOverGui_m5FDFF292685474633CF9E3F90A770B786FDF45ED (void);
// 0x000001C6 System.Boolean Lean.Touch.LeanFingerUp::get_IgnoreIsOverGui()
extern void LeanFingerUp_get_IgnoreIsOverGui_mB3B8D6FD3F9CF4ED9CB79B5D62E5424BD1990937 (void);
// 0x000001C7 System.Void Lean.Touch.LeanFingerUp::OnEnable()
extern void LeanFingerUp_OnEnable_m66D6CE5867739D3964F60CEAB74BEB33CA1174DF (void);
// 0x000001C8 System.Void Lean.Touch.LeanFingerUp::OnDisable()
extern void LeanFingerUp_OnDisable_mE5614B69C21F006796DECC11EB21543A711FAC15 (void);
// 0x000001C9 System.Boolean Lean.Touch.LeanFingerUp::UseFinger(Lean.Touch.LeanFinger)
extern void LeanFingerUp_UseFinger_m54A66DB86303A529A1D9736FE4AC3F1E45C2A510 (void);
// 0x000001CA System.Void Lean.Touch.LeanFingerUp::HandleFingerDown(Lean.Touch.LeanFinger)
extern void LeanFingerUp_HandleFingerDown_m15D1343D4DFF6E1F6A2CFB6C61AA1901AAA3E7FF (void);
// 0x000001CB System.Void Lean.Touch.LeanFingerUp::HandleFingerUp(Lean.Touch.LeanFinger)
extern void LeanFingerUp_HandleFingerUp_m32C1CC4992B8A9530F3674B658621F9085DF3E37 (void);
// 0x000001CC System.Void Lean.Touch.LeanFingerUp::.ctor()
extern void LeanFingerUp__ctor_m11A5E4E86FDD1F93463EB1BC410E523250F326CA (void);
// 0x000001CD System.Void Lean.Touch.LeanFingerUpdate::set_IgnoreStartedOverGui(System.Boolean)
extern void LeanFingerUpdate_set_IgnoreStartedOverGui_mC4BAC12B1EE4497A38B88CF52BE96935DD1F0E8D (void);
// 0x000001CE System.Boolean Lean.Touch.LeanFingerUpdate::get_IgnoreStartedOverGui()
extern void LeanFingerUpdate_get_IgnoreStartedOverGui_m497BE9F13287E56C7C6AE1D75F4244D318F84D06 (void);
// 0x000001CF System.Void Lean.Touch.LeanFingerUpdate::set_IgnoreIsOverGui(System.Boolean)
extern void LeanFingerUpdate_set_IgnoreIsOverGui_mCE69C225B511F2E80F321800BD035BDD97179D09 (void);
// 0x000001D0 System.Boolean Lean.Touch.LeanFingerUpdate::get_IgnoreIsOverGui()
extern void LeanFingerUpdate_get_IgnoreIsOverGui_m91E05E4EF2ACD9BFE0D6A51258707433A5C5A8CF (void);
// 0x000001D1 System.Void Lean.Touch.LeanFingerUpdate::set_IgnoreIfStatic(System.Boolean)
extern void LeanFingerUpdate_set_IgnoreIfStatic_m82B5A0EC08AA000990094C3C25BA1781D91B454E (void);
// 0x000001D2 System.Boolean Lean.Touch.LeanFingerUpdate::get_IgnoreIfStatic()
extern void LeanFingerUpdate_get_IgnoreIfStatic_m31C8F8C7082DE0498E1B82E9221C8FECA6D8F5E2 (void);
// 0x000001D3 System.Void Lean.Touch.LeanFingerUpdate::set_IgnoreIfDown(System.Boolean)
extern void LeanFingerUpdate_set_IgnoreIfDown_mEF1385F7F7C2F23102B68D3F22EB2FF17622D691 (void);
// 0x000001D4 System.Boolean Lean.Touch.LeanFingerUpdate::get_IgnoreIfDown()
extern void LeanFingerUpdate_get_IgnoreIfDown_mA4E739E6A3361FCC79B839D8F325832B281E9F00 (void);
// 0x000001D5 System.Void Lean.Touch.LeanFingerUpdate::set_IgnoreIfUp(System.Boolean)
extern void LeanFingerUpdate_set_IgnoreIfUp_m0DD974938ACDDFBE7663C6C13AB739F13B8A31FB (void);
// 0x000001D6 System.Boolean Lean.Touch.LeanFingerUpdate::get_IgnoreIfUp()
extern void LeanFingerUpdate_get_IgnoreIfUp_mA28589DE76F3E3E0C0F4F3780BF225B55642807C (void);
// 0x000001D7 System.Void Lean.Touch.LeanFingerUpdate::set_IgnoreIfHover(System.Boolean)
extern void LeanFingerUpdate_set_IgnoreIfHover_m1B24B4CE5A26823AC7519D1163EFB1CD4C920988 (void);
// 0x000001D8 System.Boolean Lean.Touch.LeanFingerUpdate::get_IgnoreIfHover()
extern void LeanFingerUpdate_get_IgnoreIfHover_mD1BBFF6CE0795D9B06A3F2760BBBBEB749B6A7DE (void);
// 0x000001D9 System.Void Lean.Touch.LeanFingerUpdate::set_RequiredSelectable(Lean.Common.LeanSelectable)
extern void LeanFingerUpdate_set_RequiredSelectable_m49218D8554E1F00228CE87ED0370AFBF0FCD00F1 (void);
// 0x000001DA Lean.Common.LeanSelectable Lean.Touch.LeanFingerUpdate::get_RequiredSelectable()
extern void LeanFingerUpdate_get_RequiredSelectable_m35FECFEE9C190A051F9AEE115F926A23B31B123E (void);
// 0x000001DB Lean.Touch.LeanFingerUpdate/LeanFingerEvent Lean.Touch.LeanFingerUpdate::get_OnFinger()
extern void LeanFingerUpdate_get_OnFinger_m0181E2684FBC94097E356BBD6C5431A7F07DD6BB (void);
// 0x000001DC System.Void Lean.Touch.LeanFingerUpdate::set_Coordinate(Lean.Touch.LeanFingerUpdate/CoordinateType)
extern void LeanFingerUpdate_set_Coordinate_m6C397437637DC7E53DD3FFBBAE5EE52F974E5B9A (void);
// 0x000001DD Lean.Touch.LeanFingerUpdate/CoordinateType Lean.Touch.LeanFingerUpdate::get_Coordinate()
extern void LeanFingerUpdate_get_Coordinate_m735CED9DA7A759F9AB06368DC5A6EBA7B36FABAC (void);
// 0x000001DE System.Void Lean.Touch.LeanFingerUpdate::set_Multiplier(System.Single)
extern void LeanFingerUpdate_set_Multiplier_mB8CD1271A38AE59EA714116F6AB04D649B947727 (void);
// 0x000001DF System.Single Lean.Touch.LeanFingerUpdate::get_Multiplier()
extern void LeanFingerUpdate_get_Multiplier_m850E484986045D089FFB83D465E10ADA35FF9CBD (void);
// 0x000001E0 Lean.Touch.LeanFingerUpdate/Vector2Event Lean.Touch.LeanFingerUpdate::get_OnDelta()
extern void LeanFingerUpdate_get_OnDelta_m0580341A4E98431B872A86A7264FE013117AE5A8 (void);
// 0x000001E1 Lean.Touch.LeanFingerUpdate/FloatEvent Lean.Touch.LeanFingerUpdate::get_OnDistance()
extern void LeanFingerUpdate_get_OnDistance_mFF1EC0DDC68DA7D5B96BA8EAB1BF54482E38FD4B (void);
// 0x000001E2 Lean.Touch.LeanFingerUpdate/Vector3Event Lean.Touch.LeanFingerUpdate::get_OnWorldFrom()
extern void LeanFingerUpdate_get_OnWorldFrom_m8FDAE12099717B4588B0CC096AC7337D393D25C6 (void);
// 0x000001E3 Lean.Touch.LeanFingerUpdate/Vector3Event Lean.Touch.LeanFingerUpdate::get_OnWorldTo()
extern void LeanFingerUpdate_get_OnWorldTo_m275B2F0FA61BB70D6F6F4868B913514DBB799661 (void);
// 0x000001E4 Lean.Touch.LeanFingerUpdate/Vector3Event Lean.Touch.LeanFingerUpdate::get_OnWorldDelta()
extern void LeanFingerUpdate_get_OnWorldDelta_m32738C79242BD6F2E3FE8E3D310BAD18B94F2E58 (void);
// 0x000001E5 Lean.Touch.LeanFingerUpdate/Vector3Vector3Event Lean.Touch.LeanFingerUpdate::get_OnWorldFromTo()
extern void LeanFingerUpdate_get_OnWorldFromTo_mE3C381F84CFC7D19236E8BE73CC7A29586690832 (void);
// 0x000001E6 System.Void Lean.Touch.LeanFingerUpdate::Awake()
extern void LeanFingerUpdate_Awake_m117475503EE491D9D1026E1114E8C13ED8536B3C (void);
// 0x000001E7 System.Void Lean.Touch.LeanFingerUpdate::OnEnable()
extern void LeanFingerUpdate_OnEnable_m2D704112E5CDE0359583183F901289991D313306 (void);
// 0x000001E8 System.Void Lean.Touch.LeanFingerUpdate::OnDisable()
extern void LeanFingerUpdate_OnDisable_m5F92BEC0B13A52C477AE44B3477C23C248C93D39 (void);
// 0x000001E9 System.Void Lean.Touch.LeanFingerUpdate::HandleFingerUpdate(Lean.Touch.LeanFinger)
extern void LeanFingerUpdate_HandleFingerUpdate_mC61E61F0484880E241662681ABAA948588DBEF80 (void);
// 0x000001EA System.Void Lean.Touch.LeanFingerUpdate::.ctor()
extern void LeanFingerUpdate__ctor_m9A0FD241DCDC335321A28F50D4DB8D028C45AE8E (void);
// 0x000001EB System.Void Lean.Touch.LeanFingerUpdate/LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_m7EBD0C5160FD5E9970F4C3E3B222E38257011E9E (void);
// 0x000001EC System.Void Lean.Touch.LeanFingerUpdate/FloatEvent::.ctor()
extern void FloatEvent__ctor_m107B522FAFD2A75A23B64F77F9567615665E470A (void);
// 0x000001ED System.Void Lean.Touch.LeanFingerUpdate/Vector2Event::.ctor()
extern void Vector2Event__ctor_mB0001E331ABE245FC18CDE62E5FFB46E007B4B1A (void);
// 0x000001EE System.Void Lean.Touch.LeanFingerUpdate/Vector3Event::.ctor()
extern void Vector3Event__ctor_m78C757EA5F95440DC43ED6347D7AD11E9B6CD930 (void);
// 0x000001EF System.Void Lean.Touch.LeanFingerUpdate/Vector3Vector3Event::.ctor()
extern void Vector3Vector3Event__ctor_mD49AD0C93EAC1EAA3523D5C9128C86943D8A6136 (void);
// 0x000001F0 System.Void Lean.Touch.LeanPinchScale::set_Camera(UnityEngine.Camera)
extern void LeanPinchScale_set_Camera_mEFFCF244901A0FA73CBFEE909B9BC3FA74771C10 (void);
// 0x000001F1 UnityEngine.Camera Lean.Touch.LeanPinchScale::get_Camera()
extern void LeanPinchScale_get_Camera_mBBC1E187F972DB947DA12FC9725DA42E4F13A488 (void);
// 0x000001F2 System.Void Lean.Touch.LeanPinchScale::set_Relative(System.Boolean)
extern void LeanPinchScale_set_Relative_mE2E6DAC64F39F7C93C107E8FDC130F2DE7990359 (void);
// 0x000001F3 System.Boolean Lean.Touch.LeanPinchScale::get_Relative()
extern void LeanPinchScale_get_Relative_m87C664C51BB58FCBA36E80318DDBF0EAD9A900D6 (void);
// 0x000001F4 System.Void Lean.Touch.LeanPinchScale::set_Sensitivity(System.Single)
extern void LeanPinchScale_set_Sensitivity_mB4F748043497417E80F4D30457F051B5E13EA980 (void);
// 0x000001F5 System.Single Lean.Touch.LeanPinchScale::get_Sensitivity()
extern void LeanPinchScale_get_Sensitivity_m0E5FCC2A55F93BE3B2BCC0D5C0A4C8FFA00707D7 (void);
// 0x000001F6 System.Void Lean.Touch.LeanPinchScale::set_Damping(System.Single)
extern void LeanPinchScale_set_Damping_m8D58B351301244787EA0A39016B5106BF10945B9 (void);
// 0x000001F7 System.Single Lean.Touch.LeanPinchScale::get_Damping()
extern void LeanPinchScale_get_Damping_m983688BCCC013CE442FF233028FE41E0705AC5FF (void);
// 0x000001F8 System.Void Lean.Touch.LeanPinchScale::AddFinger(Lean.Touch.LeanFinger)
extern void LeanPinchScale_AddFinger_m064F72EBA3855AD66D3ACF155C63558B6FB6A7B3 (void);
// 0x000001F9 System.Void Lean.Touch.LeanPinchScale::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanPinchScale_RemoveFinger_m1C7CBDFB9C4D6E7299EEA2700A09742E96459B41 (void);
// 0x000001FA System.Void Lean.Touch.LeanPinchScale::RemoveAllFingers()
extern void LeanPinchScale_RemoveAllFingers_mD3A054A5D0681DB5A704BA674629D6752E5C6B10 (void);
// 0x000001FB System.Void Lean.Touch.LeanPinchScale::Awake()
extern void LeanPinchScale_Awake_m9A08BB54FC091D8018F39F015138D76AD73D3CA4 (void);
// 0x000001FC System.Void Lean.Touch.LeanPinchScale::Update()
extern void LeanPinchScale_Update_m38153051EAA298C7B1440307E3258DEB486243D5 (void);
// 0x000001FD System.Void Lean.Touch.LeanPinchScale::TranslateUI(System.Single,UnityEngine.Vector2)
extern void LeanPinchScale_TranslateUI_m110D5D7F7096A72DCBFB88EC446DEB1636739E12 (void);
// 0x000001FE System.Void Lean.Touch.LeanPinchScale::Translate(System.Single,UnityEngine.Vector2)
extern void LeanPinchScale_Translate_m02EF40F24334C6884A083368C30C894630F01685 (void);
// 0x000001FF System.Void Lean.Touch.LeanPinchScale::.ctor()
extern void LeanPinchScale__ctor_m714B453B2175D1933D36C4789C4EF8FBB56DE8CD (void);
// 0x00000200 System.Void Lean.Touch.LeanScreenDepth::.ctor(Lean.Touch.LeanScreenDepth/ConversionType,System.Int32,System.Single)
extern void LeanScreenDepth__ctor_m317000555909FB83237D72C83E4CE97B9BD82BE3 (void);
// 0x00000201 UnityEngine.Vector3 Lean.Touch.LeanScreenDepth::Convert(UnityEngine.Vector2,UnityEngine.GameObject,UnityEngine.Transform)
extern void LeanScreenDepth_Convert_m7A9AD8A2702E65EAEC8757784C7796C0F54A77FB (void);
// 0x00000202 UnityEngine.Vector3 Lean.Touch.LeanScreenDepth::ConvertDelta(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.GameObject,UnityEngine.Transform)
extern void LeanScreenDepth_ConvertDelta_m2832CD0C407CBCFDFAB2BE1B8D91C2380397F299 (void);
// 0x00000203 System.Boolean Lean.Touch.LeanScreenDepth::TryConvert(UnityEngine.Vector3&,UnityEngine.Vector2,UnityEngine.GameObject,UnityEngine.Transform)
extern void LeanScreenDepth_TryConvert_m37BE63E86D3A1DA403AEA64BEDD802C3B5A883A6 (void);
// 0x00000204 System.Boolean Lean.Touch.LeanScreenDepth::Exists(UnityEngine.GameObject,T&)
// 0x00000205 System.Boolean Lean.Touch.LeanScreenDepth::IsChildOf(UnityEngine.Transform,UnityEngine.Transform)
extern void LeanScreenDepth_IsChildOf_mD2A8698585D736BD56D913467C066B5D143B6C96 (void);
// 0x00000206 System.Void Lean.Touch.LeanScreenDepth::.cctor()
extern void LeanScreenDepth__cctor_m843AC9408301A69F3AF769B6136DBEC2F16FAF47 (void);
// 0x00000207 System.Void Lean.Touch.LeanSelectableByFinger::set_Use(Lean.Touch.LeanSelectableByFinger/UseType)
extern void LeanSelectableByFinger_set_Use_mBC2C4A9C711A3E35F0013EF158386F67FCF54AE0 (void);
// 0x00000208 Lean.Touch.LeanSelectableByFinger/UseType Lean.Touch.LeanSelectableByFinger::get_Use()
extern void LeanSelectableByFinger_get_Use_m3C920F0D822E47F4E8AEA21961132E3F5B054E64 (void);
// 0x00000209 Lean.Touch.LeanSelectableByFinger/LeanFingerEvent Lean.Touch.LeanSelectableByFinger::get_OnSelectedFinger()
extern void LeanSelectableByFinger_get_OnSelectedFinger_m249B7CB7296CFA84970FDCD20792CA0A7C0CE317 (void);
// 0x0000020A Lean.Touch.LeanSelectableByFinger/LeanFingerEvent Lean.Touch.LeanSelectableByFinger::get_OnSelectedFingerUp()
extern void LeanSelectableByFinger_get_OnSelectedFingerUp_m42CBC975EEFDBFD562215FA14E7EE8FECA864BDF (void);
// 0x0000020B Lean.Touch.LeanSelectableByFinger/LeanSelectFingerEvent Lean.Touch.LeanSelectableByFinger::get_OnSelectedSelectFinger()
extern void LeanSelectableByFinger_get_OnSelectedSelectFinger_m3B1543436E1C6C0CB2AB8DA84A29E404E86D93E2 (void);
// 0x0000020C Lean.Touch.LeanSelectableByFinger/LeanSelectFingerEvent Lean.Touch.LeanSelectableByFinger::get_OnSelectedSelectFingerUp()
extern void LeanSelectableByFinger_get_OnSelectedSelectFingerUp_m2DD5987FC615F5B3EA39EA0135E320638B208BCB (void);
// 0x0000020D System.Void Lean.Touch.LeanSelectableByFinger::add_OnAnySelectedFinger(System.Action`3<Lean.Touch.LeanSelectByFinger,Lean.Touch.LeanSelectableByFinger,Lean.Touch.LeanFinger>)
extern void LeanSelectableByFinger_add_OnAnySelectedFinger_m180429FB0C6C74231DB80BC55A5B1BC5755BFB51 (void);
// 0x0000020E System.Void Lean.Touch.LeanSelectableByFinger::remove_OnAnySelectedFinger(System.Action`3<Lean.Touch.LeanSelectByFinger,Lean.Touch.LeanSelectableByFinger,Lean.Touch.LeanFinger>)
extern void LeanSelectableByFinger_remove_OnAnySelectedFinger_m419C283A825D1B3A92EC8239BA564FF59C5C3D7E (void);
// 0x0000020F Lean.Touch.LeanFinger Lean.Touch.LeanSelectableByFinger::get_SelectingFinger()
extern void LeanSelectableByFinger_get_SelectingFinger_m58161BDD661EF14E3804B233E6851509B8AC53E9 (void);
// 0x00000210 System.Collections.Generic.List`1<Lean.Touch.LeanSelectableByFinger/SelectedPair> Lean.Touch.LeanSelectableByFinger::get_SelectingPairs()
extern void LeanSelectableByFinger_get_SelectingPairs_m34E3077D6DD525CB24DCF0565D54A8CDEEB97AC9 (void);
// 0x00000211 System.Void Lean.Touch.LeanSelectableByFinger::SelectSelf(Lean.Touch.LeanFinger)
extern void LeanSelectableByFinger_SelectSelf_m37727BDCCA8A8CD0D1B468D579DD0E66C3075335 (void);
// 0x00000212 System.Collections.Generic.List`1<Lean.Touch.LeanFinger> Lean.Touch.LeanSelectableByFinger::GetFingers(System.Boolean,System.Boolean,System.Int32,Lean.Common.LeanSelectable)
extern void LeanSelectableByFinger_GetFingers_m9B19725E9E43A6A867A70885C088DA9CD1713BF0 (void);
// 0x00000213 Lean.Touch.LeanSelectableByFinger Lean.Touch.LeanSelectableByFinger::FindSelectable(Lean.Touch.LeanFinger)
extern void LeanSelectableByFinger_FindSelectable_m158FE6F104EDFBD3DEF6962625353E1B9ADF1585 (void);
// 0x00000214 System.Boolean Lean.Touch.LeanSelectableByFinger::IsSelectedBy(Lean.Touch.LeanFinger)
extern void LeanSelectableByFinger_IsSelectedBy_mE1ABC50D05805F1310FF99BCEAA27EDC96C8278C (void);
// 0x00000215 System.Void Lean.Touch.LeanSelectableByFinger::InvokeAnySelectedFinger(Lean.Touch.LeanSelectByFinger,Lean.Touch.LeanSelectableByFinger,Lean.Touch.LeanFinger)
extern void LeanSelectableByFinger_InvokeAnySelectedFinger_mB22BFD020E7049C76ACE8E61E36351CDCB4C0C64 (void);
// 0x00000216 System.Void Lean.Touch.LeanSelectableByFinger::OnEnable()
extern void LeanSelectableByFinger_OnEnable_m1CADF924C67E7342AB38AFB9436518F3B9364458 (void);
// 0x00000217 System.Void Lean.Touch.LeanSelectableByFinger::OnDisable()
extern void LeanSelectableByFinger_OnDisable_m310830F10B969FAC1D428CF2EC51F48B39326A3F (void);
// 0x00000218 System.Boolean Lean.Touch.LeanSelectableByFinger::get_AnyFingersSet()
extern void LeanSelectableByFinger_get_AnyFingersSet_mD094F2DDA14605C5340BC0A7FA13F93659555211 (void);
// 0x00000219 System.Void Lean.Touch.LeanSelectableByFinger::HandleFingerUp(Lean.Touch.LeanFinger)
extern void LeanSelectableByFinger_HandleFingerUp_m54F15E8525CA9916032201777DF67725F796B5D6 (void);
// 0x0000021A System.Void Lean.Touch.LeanSelectableByFinger::.ctor()
extern void LeanSelectableByFinger__ctor_m18195B103EDF0661C2FB312EF5D364508A878D03 (void);
// 0x0000021B System.Void Lean.Touch.LeanSelectableByFinger/LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_m093B9C465E7D71D3A48EA735993667CB556F1078 (void);
// 0x0000021C System.Void Lean.Touch.LeanSelectableByFinger/LeanSelectFingerEvent::.ctor()
extern void LeanSelectFingerEvent__ctor_m692F05856A4A69AB9DE5D3AC167AE2DDF031B50D (void);
// 0x0000021D Lean.Touch.LeanSelectableByFinger Lean.Touch.LeanSelectableByFingerBehaviour::get_Selectable()
extern void LeanSelectableByFingerBehaviour_get_Selectable_mDDBC292777C08143DA6FDCB85F9B505846722CB7 (void);
// 0x0000021E System.Void Lean.Touch.LeanSelectableByFingerBehaviour::Register()
extern void LeanSelectableByFingerBehaviour_Register_m4242F1D49A85C22D17706C579DAC35BB8E141286 (void);
// 0x0000021F System.Void Lean.Touch.LeanSelectableByFingerBehaviour::Register(Lean.Touch.LeanSelectableByFinger)
extern void LeanSelectableByFingerBehaviour_Register_mA3C4C9FF035486417CC5B17F28E951CE3E825C31 (void);
// 0x00000220 System.Void Lean.Touch.LeanSelectableByFingerBehaviour::Unregister()
extern void LeanSelectableByFingerBehaviour_Unregister_m0368607F5BA3852D7E261EC4E4B388880E5A035F (void);
// 0x00000221 System.Void Lean.Touch.LeanSelectableByFingerBehaviour::OnEnable()
extern void LeanSelectableByFingerBehaviour_OnEnable_mDB8044530F9BDDDAA6C3D8AC582B8D590B552CB9 (void);
// 0x00000222 System.Void Lean.Touch.LeanSelectableByFingerBehaviour::Start()
extern void LeanSelectableByFingerBehaviour_Start_m7FD3DC511E8956A5391F9600A9734649EEF348E7 (void);
// 0x00000223 System.Void Lean.Touch.LeanSelectableByFingerBehaviour::OnDisable()
extern void LeanSelectableByFingerBehaviour_OnDisable_mBE3EF75E2C368457EB6C85CFE0613BF84CA5A78D (void);
// 0x00000224 System.Void Lean.Touch.LeanSelectableByFingerBehaviour::OnSelected(Lean.Common.LeanSelect)
extern void LeanSelectableByFingerBehaviour_OnSelected_mA0863BE4D4FEA9DEF948DABBC9BC2DCC066958A5 (void);
// 0x00000225 System.Void Lean.Touch.LeanSelectableByFingerBehaviour::OnSelectedSelectFinger(Lean.Touch.LeanSelectByFinger,Lean.Touch.LeanFinger)
extern void LeanSelectableByFingerBehaviour_OnSelectedSelectFinger_m6267803F78A8B4A4DF6A90DAA010919ED0060C5E (void);
// 0x00000226 System.Void Lean.Touch.LeanSelectableByFingerBehaviour::OnSelectedSelectFingerUp(Lean.Touch.LeanSelectByFinger,Lean.Touch.LeanFinger)
extern void LeanSelectableByFingerBehaviour_OnSelectedSelectFingerUp_m8FB142F00EB59B82546932C0D125D1DE6BC9FB86 (void);
// 0x00000227 System.Void Lean.Touch.LeanSelectableByFingerBehaviour::OnDeselected(Lean.Common.LeanSelect)
extern void LeanSelectableByFingerBehaviour_OnDeselected_m13837E9FDF9E17358CD44F08DF6BE8BAB7AD5F06 (void);
// 0x00000228 System.Void Lean.Touch.LeanSelectableByFingerBehaviour::.ctor()
extern void LeanSelectableByFingerBehaviour__ctor_m0FCE4E850934BE31569552EB917517AC7421FB62 (void);
// 0x00000229 System.Void Lean.Touch.LeanSelectByFinger::set_DeselectWithFingers(System.Boolean)
extern void LeanSelectByFinger_set_DeselectWithFingers_m6A94AB0E138D9AC46AA6E44364BDD5CB7056BC3A (void);
// 0x0000022A System.Boolean Lean.Touch.LeanSelectByFinger::get_DeselectWithFingers()
extern void LeanSelectByFinger_get_DeselectWithFingers_m386609DE7D27BE63114357FB8CB756D35CAB0984 (void);
// 0x0000022B Lean.Touch.LeanSelectByFinger/LeanSelectableLeanFingerEvent Lean.Touch.LeanSelectByFinger::get_OnSelectedFinger()
extern void LeanSelectByFinger_get_OnSelectedFinger_mB79C1C7EF8C1D02AF5830B8CB4A2189E8B11BFEE (void);
// 0x0000022C System.Void Lean.Touch.LeanSelectByFinger::add_OnAnySelectedFinger(System.Action`3<Lean.Touch.LeanSelectByFinger,Lean.Common.LeanSelectable,Lean.Touch.LeanFinger>)
extern void LeanSelectByFinger_add_OnAnySelectedFinger_m5043BBE03F53BEAF4A4D17472F12D765306C327E (void);
// 0x0000022D System.Void Lean.Touch.LeanSelectByFinger::remove_OnAnySelectedFinger(System.Action`3<Lean.Touch.LeanSelectByFinger,Lean.Common.LeanSelectable,Lean.Touch.LeanFinger>)
extern void LeanSelectByFinger_remove_OnAnySelectedFinger_m08CA01DC818E22E507E45CEE3F09D89CBF9EF594 (void);
// 0x0000022E System.Void Lean.Touch.LeanSelectByFinger::SelectStartScreenPosition(Lean.Touch.LeanFinger)
extern void LeanSelectByFinger_SelectStartScreenPosition_m79B8FC370D893BE312824C898D606D8150DB2274 (void);
// 0x0000022F System.Void Lean.Touch.LeanSelectByFinger::SelectScreenPosition(Lean.Touch.LeanFinger)
extern void LeanSelectByFinger_SelectScreenPosition_mD4D61BD9FA7193B4604A16667B766E90CF7C4728 (void);
// 0x00000230 System.Void Lean.Touch.LeanSelectByFinger::SelectScreenPosition(Lean.Touch.LeanFinger,UnityEngine.Vector2)
extern void LeanSelectByFinger_SelectScreenPosition_mCC4AEA308C422EE60AFE77E69E29C4685D19579E (void);
// 0x00000231 System.Void Lean.Touch.LeanSelectByFinger::Select(Lean.Common.LeanSelectable,Lean.Touch.LeanFinger)
extern void LeanSelectByFinger_Select_m4FB61F03FA876187D2E48394B20AB4BA1AEEE526 (void);
// 0x00000232 System.Void Lean.Touch.LeanSelectByFinger::Update()
extern void LeanSelectByFinger_Update_m680DE52D2D369C54CA7C36FA523DD41A85C33265 (void);
// 0x00000233 System.Boolean Lean.Touch.LeanSelectByFinger::ShouldRemoveSelectable(Lean.Common.LeanSelectable)
extern void LeanSelectByFinger_ShouldRemoveSelectable_m701B9286A22DC14793E275DA25F9BAFCA91BB78A (void);
// 0x00000234 System.Void Lean.Touch.LeanSelectByFinger::ReplaceSelection(System.Collections.Generic.List`1<Lean.Common.LeanSelectable>,Lean.Touch.LeanFinger)
extern void LeanSelectByFinger_ReplaceSelection_mA96488E1D1A99A352191F5843CB908774CB83D6D (void);
// 0x00000235 System.Void Lean.Touch.LeanSelectByFinger::.ctor()
extern void LeanSelectByFinger__ctor_m700D7DEB2DDFA63BC4D5A0B8C6F5C9875A461B0E (void);
// 0x00000236 System.Void Lean.Touch.LeanSelectByFinger/LeanSelectableLeanFingerEvent::.ctor()
extern void LeanSelectableLeanFingerEvent__ctor_mA419D40EBFA75FE721918BCE3417351928E10083 (void);
// 0x00000237 System.Void Lean.Touch.LeanSwipeBase::set_RequiredAngle(System.Single)
extern void LeanSwipeBase_set_RequiredAngle_mC410B0160CC9E0B26C489EA981CDF4548FE77FF9 (void);
// 0x00000238 System.Single Lean.Touch.LeanSwipeBase::get_RequiredAngle()
extern void LeanSwipeBase_get_RequiredAngle_mEAB17DF24812CD4FF27B02B03F2835C24D46EFC1 (void);
// 0x00000239 System.Void Lean.Touch.LeanSwipeBase::set_RequiredArc(System.Single)
extern void LeanSwipeBase_set_RequiredArc_mFCA4B087D9E3F7617693D741FD207A6B693CF0D0 (void);
// 0x0000023A System.Single Lean.Touch.LeanSwipeBase::get_RequiredArc()
extern void LeanSwipeBase_get_RequiredArc_m9A9445128D3E7A33D5A7D9C296BACE52985300D9 (void);
// 0x0000023B Lean.Touch.LeanSwipeBase/LeanFingerEvent Lean.Touch.LeanSwipeBase::get_OnFinger()
extern void LeanSwipeBase_get_OnFinger_m146E52130AFEBC3703CA327A5FC1A32D17C3D950 (void);
// 0x0000023C System.Void Lean.Touch.LeanSwipeBase::set_Modify(Lean.Touch.LeanSwipeBase/ModifyType)
extern void LeanSwipeBase_set_Modify_mC331356BA9C7F4923590BE83C356F3420F480260 (void);
// 0x0000023D Lean.Touch.LeanSwipeBase/ModifyType Lean.Touch.LeanSwipeBase::get_Modify()
extern void LeanSwipeBase_get_Modify_m0D78838D1F695B51AF63F3C5BA4BD76967635DF2 (void);
// 0x0000023E System.Void Lean.Touch.LeanSwipeBase::set_Coordinate(Lean.Touch.LeanSwipeBase/CoordinateType)
extern void LeanSwipeBase_set_Coordinate_m8AF018B41CD520B4682B7FA748A2619CC1F576D9 (void);
// 0x0000023F Lean.Touch.LeanSwipeBase/CoordinateType Lean.Touch.LeanSwipeBase::get_Coordinate()
extern void LeanSwipeBase_get_Coordinate_m343512AFA8BD0E7A1C02CF6578FAF3A486899BB5 (void);
// 0x00000240 System.Void Lean.Touch.LeanSwipeBase::set_Multiplier(System.Single)
extern void LeanSwipeBase_set_Multiplier_m0E7A3119304A366A68A42E5258B3310F041E03A3 (void);
// 0x00000241 System.Single Lean.Touch.LeanSwipeBase::get_Multiplier()
extern void LeanSwipeBase_get_Multiplier_mE3A7A6304B5B4B064B0C9ECBC408CB565D2D79F1 (void);
// 0x00000242 Lean.Touch.LeanSwipeBase/Vector2Event Lean.Touch.LeanSwipeBase::get_OnDelta()
extern void LeanSwipeBase_get_OnDelta_m0EF61FD538559D28C186867B1B7CB6898D8B39A6 (void);
// 0x00000243 Lean.Touch.LeanSwipeBase/FloatEvent Lean.Touch.LeanSwipeBase::get_OnDistance()
extern void LeanSwipeBase_get_OnDistance_m96364752A06CED974E5021D773B97745C7C34FC4 (void);
// 0x00000244 Lean.Touch.LeanSwipeBase/Vector3Event Lean.Touch.LeanSwipeBase::get_OnWorldFrom()
extern void LeanSwipeBase_get_OnWorldFrom_m2192CF00007530F723354BB27DCE0A5DAFE3C8DF (void);
// 0x00000245 Lean.Touch.LeanSwipeBase/Vector3Event Lean.Touch.LeanSwipeBase::get_OnWorldTo()
extern void LeanSwipeBase_get_OnWorldTo_mEEEB0425D11AC151D6EEF146D46A27F84577F16C (void);
// 0x00000246 Lean.Touch.LeanSwipeBase/Vector3Event Lean.Touch.LeanSwipeBase::get_OnWorldDelta()
extern void LeanSwipeBase_get_OnWorldDelta_m3BDD82B8E91757C6E62BD3E3BD2DD09DCAFDAF67 (void);
// 0x00000247 Lean.Touch.LeanSwipeBase/Vector3Vector3Event Lean.Touch.LeanSwipeBase::get_OnWorldFromTo()
extern void LeanSwipeBase_get_OnWorldFromTo_m26347F1C5CE67B03B257CE5C8825E376DB8424D0 (void);
// 0x00000248 System.Boolean Lean.Touch.LeanSwipeBase::AngleIsValid(UnityEngine.Vector2)
extern void LeanSwipeBase_AngleIsValid_m9A99554522D9780D8C5F1E1CC1F11637BEA02E45 (void);
// 0x00000249 System.Void Lean.Touch.LeanSwipeBase::HandleFingerSwipe(Lean.Touch.LeanFinger,UnityEngine.Vector2,UnityEngine.Vector2)
extern void LeanSwipeBase_HandleFingerSwipe_mB0DA705C01A3E252B600B7C6321DA3127386D840 (void);
// 0x0000024A System.Void Lean.Touch.LeanSwipeBase::.ctor()
extern void LeanSwipeBase__ctor_m16DC32C99FEF9E473A719A42EC01716D67E5EFFC (void);
// 0x0000024B System.Void Lean.Touch.LeanSwipeBase/LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_mCFD24D9CA2BF018E605D403C31342635EA02D93B (void);
// 0x0000024C System.Void Lean.Touch.LeanSwipeBase/FloatEvent::.ctor()
extern void FloatEvent__ctor_mDAFDB87D07C9FFD4F5C233F7967E9FC9F7165CD9 (void);
// 0x0000024D System.Void Lean.Touch.LeanSwipeBase/Vector2Event::.ctor()
extern void Vector2Event__ctor_m9D0B2416E70349E2895A29B022EBE89EC1C2678D (void);
// 0x0000024E System.Void Lean.Touch.LeanSwipeBase/Vector3Event::.ctor()
extern void Vector3Event__ctor_m13A940B94A6C96EC2DDAC1FCCDD1EB54141AEA0A (void);
// 0x0000024F System.Void Lean.Touch.LeanSwipeBase/Vector3Vector3Event::.ctor()
extern void Vector3Vector3Event__ctor_mDA3312D25717F07FC8AF44780B9A560955FDE4A6 (void);
// 0x00000250 System.Void Lean.Touch.LeanTouchSimulator::set_PinchTwistKey(UnityEngine.KeyCode)
extern void LeanTouchSimulator_set_PinchTwistKey_mEC8876AC55B49968963381824DBC01524542A3AA (void);
// 0x00000251 UnityEngine.KeyCode Lean.Touch.LeanTouchSimulator::get_PinchTwistKey()
extern void LeanTouchSimulator_get_PinchTwistKey_m54FC76AFB8A1042957EBBDF2E8739C1B3948E015 (void);
// 0x00000252 System.Void Lean.Touch.LeanTouchSimulator::set_MovePivotKey(UnityEngine.KeyCode)
extern void LeanTouchSimulator_set_MovePivotKey_m307FC3B20169DE28C806460AFB7FE2A7659186D8 (void);
// 0x00000253 UnityEngine.KeyCode Lean.Touch.LeanTouchSimulator::get_MovePivotKey()
extern void LeanTouchSimulator_get_MovePivotKey_m034C8FD481BECCCB460B24CD4FA4E213647288F8 (void);
// 0x00000254 System.Void Lean.Touch.LeanTouchSimulator::set_MultiDragKey(UnityEngine.KeyCode)
extern void LeanTouchSimulator_set_MultiDragKey_m9934345CCDCDB086B281A9C6C84B7B30B7356B59 (void);
// 0x00000255 UnityEngine.KeyCode Lean.Touch.LeanTouchSimulator::get_MultiDragKey()
extern void LeanTouchSimulator_get_MultiDragKey_m983C2334F86ABDD9016FBB7CA8629DD69417BE1A (void);
// 0x00000256 System.Void Lean.Touch.LeanTouchSimulator::set_FingerTexture(UnityEngine.Texture2D)
extern void LeanTouchSimulator_set_FingerTexture_m151ABD32058C47532035AF908DAE52682DC111D8 (void);
// 0x00000257 UnityEngine.Texture2D Lean.Touch.LeanTouchSimulator::get_FingerTexture()
extern void LeanTouchSimulator_get_FingerTexture_mC657921BE290234B44B2D8A33912A571C2AADB0D (void);
// 0x00000258 System.Void Lean.Touch.LeanTouchSimulator::OnEnable()
extern void LeanTouchSimulator_OnEnable_mFFB2C2BE4B85F0601B4F1ADFB88D27DB68A36AB3 (void);
// 0x00000259 System.Void Lean.Touch.LeanTouchSimulator::OnDisable()
extern void LeanTouchSimulator_OnDisable_m9ED170F0802AED9625BF932B230D46CAAC632912 (void);
// 0x0000025A System.Void Lean.Touch.LeanTouchSimulator::OnGUI()
extern void LeanTouchSimulator_OnGUI_mC0C893A99D2ED5A2FAD9D2E737A13CB2AB02F840 (void);
// 0x0000025B System.Void Lean.Touch.LeanTouchSimulator::HandleSimulateFingers()
extern void LeanTouchSimulator_HandleSimulateFingers_m3280D2B09F6CF1BEB1F65E062F78590553B5F9A4 (void);
// 0x0000025C System.Void Lean.Touch.LeanTouchSimulator::.ctor()
extern void LeanTouchSimulator__ctor_m20C55E4CE257F128808870742837B674915D6277 (void);
// 0x0000025D System.Void Lean.Touch.LeanTwistRotate::set_Camera(UnityEngine.Camera)
extern void LeanTwistRotate_set_Camera_m350424F9FB7685B0D24AE79E5B47FC998E88DDAE (void);
// 0x0000025E UnityEngine.Camera Lean.Touch.LeanTwistRotate::get_Camera()
extern void LeanTwistRotate_get_Camera_mF7A361D625278286C6F0F4C73468BC8639A651D8 (void);
// 0x0000025F System.Void Lean.Touch.LeanTwistRotate::set_Relative(System.Boolean)
extern void LeanTwistRotate_set_Relative_m1257B53F9A04FF0E3D0D35620C76656B050BEA48 (void);
// 0x00000260 System.Boolean Lean.Touch.LeanTwistRotate::get_Relative()
extern void LeanTwistRotate_get_Relative_m3C9277F6F7712DA99FE45F94CB0B1043568F4C9F (void);
// 0x00000261 System.Void Lean.Touch.LeanTwistRotate::set_Damping(System.Single)
extern void LeanTwistRotate_set_Damping_m6F689E2A5439F75C6E70E3E85954D406D3F5DF69 (void);
// 0x00000262 System.Single Lean.Touch.LeanTwistRotate::get_Damping()
extern void LeanTwistRotate_get_Damping_mF91DB5FF8A50AA7D5C02DA899576B1C02083BF1F (void);
// 0x00000263 System.Void Lean.Touch.LeanTwistRotate::AddFinger(Lean.Touch.LeanFinger)
extern void LeanTwistRotate_AddFinger_m9C44D20A341B17F8FDD974B888D5197898B34829 (void);
// 0x00000264 System.Void Lean.Touch.LeanTwistRotate::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanTwistRotate_RemoveFinger_mF0CE0CD921E2F6B788A216FE51052154E1EC02FE (void);
// 0x00000265 System.Void Lean.Touch.LeanTwistRotate::RemoveAllFingers()
extern void LeanTwistRotate_RemoveAllFingers_m96FC116013D95AFBBE251739F77C248F799930D2 (void);
// 0x00000266 System.Void Lean.Touch.LeanTwistRotate::Awake()
extern void LeanTwistRotate_Awake_m7975D3E4DB07DB24A68DCA67F45B0B45D1D3738C (void);
// 0x00000267 System.Void Lean.Touch.LeanTwistRotate::Update()
extern void LeanTwistRotate_Update_mBCEAB598F17A634312D665D9E0053891DD3CF3C1 (void);
// 0x00000268 System.Void Lean.Touch.LeanTwistRotate::TranslateUI(System.Single,UnityEngine.Vector2)
extern void LeanTwistRotate_TranslateUI_m9F7A60327C406D5EFAF0B609F81467282594CD6A (void);
// 0x00000269 System.Void Lean.Touch.LeanTwistRotate::Translate(System.Single,UnityEngine.Vector2)
extern void LeanTwistRotate_Translate_mA77AFECA0351EF4B8B596246D32425D6D43A90FA (void);
// 0x0000026A System.Void Lean.Touch.LeanTwistRotate::RotateUI(System.Single)
extern void LeanTwistRotate_RotateUI_mB5157F14702EADF14933665EF89AD86413D28DE6 (void);
// 0x0000026B System.Void Lean.Touch.LeanTwistRotate::Rotate(System.Single)
extern void LeanTwistRotate_Rotate_m7FEF262169B0E23D33C867241A5CAA0BC7C40837 (void);
// 0x0000026C System.Void Lean.Touch.LeanTwistRotate::.ctor()
extern void LeanTwistRotate__ctor_m8B79B4708334ECF30A023761E6A2280DD4E0C686 (void);
// 0x0000026D System.Void Lean.Touch.LeanTwistRotateAxis::set_Axis(UnityEngine.Vector3)
extern void LeanTwistRotateAxis_set_Axis_m7B585E9E74857FCE1395704FD23780AC32375DE9 (void);
// 0x0000026E UnityEngine.Vector3 Lean.Touch.LeanTwistRotateAxis::get_Axis()
extern void LeanTwistRotateAxis_get_Axis_mCE9FEA7893F7549E29DD6F1D6AD2936850DB79A6 (void);
// 0x0000026F System.Void Lean.Touch.LeanTwistRotateAxis::set_Space(UnityEngine.Space)
extern void LeanTwistRotateAxis_set_Space_m2048D9550AC85949CB85B28BCCB90B51066727C7 (void);
// 0x00000270 UnityEngine.Space Lean.Touch.LeanTwistRotateAxis::get_Space()
extern void LeanTwistRotateAxis_get_Space_m8A340C6348993D8A62D260ADDE37CD3BFA08ED9A (void);
// 0x00000271 System.Void Lean.Touch.LeanTwistRotateAxis::set_Sensitivity(System.Single)
extern void LeanTwistRotateAxis_set_Sensitivity_m1771F75AD678FE8A7A914265910BB6DF277A2185 (void);
// 0x00000272 System.Single Lean.Touch.LeanTwistRotateAxis::get_Sensitivity()
extern void LeanTwistRotateAxis_get_Sensitivity_mB940902D4BDBC2BB03B34812887AA4B5DDDEFF03 (void);
// 0x00000273 System.Void Lean.Touch.LeanTwistRotateAxis::AddFinger(Lean.Touch.LeanFinger)
extern void LeanTwistRotateAxis_AddFinger_mEF7E4D701E5ADB521A13F83AE25D8F61E09B418A (void);
// 0x00000274 System.Void Lean.Touch.LeanTwistRotateAxis::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanTwistRotateAxis_RemoveFinger_m92A3B2BB9634723661A897821C11014D710B49C8 (void);
// 0x00000275 System.Void Lean.Touch.LeanTwistRotateAxis::RemoveAllFingers()
extern void LeanTwistRotateAxis_RemoveAllFingers_mC368843ACB62386CFDC9CBB2E4A6B97689BE7F25 (void);
// 0x00000276 System.Void Lean.Touch.LeanTwistRotateAxis::Awake()
extern void LeanTwistRotateAxis_Awake_mAAD9A57F7CF73FD4F46C94706FDDF3FCC15AC569 (void);
// 0x00000277 System.Void Lean.Touch.LeanTwistRotateAxis::Update()
extern void LeanTwistRotateAxis_Update_m9E1659E95DD277422E1A45592AA0EBFAB8099899 (void);
// 0x00000278 System.Void Lean.Touch.LeanTwistRotateAxis::.ctor()
extern void LeanTwistRotateAxis__ctor_m1E15A2C9C2A59218C3F4CF7E1F1708FB4DF95576 (void);
// 0x00000279 System.Boolean Lean.Touch.LeanFinger::get_IsActive()
extern void LeanFinger_get_IsActive_m187C1AA12031AA7E55C48162BBC1DA79174E1071 (void);
// 0x0000027A System.Single Lean.Touch.LeanFinger::get_SnapshotDuration()
extern void LeanFinger_get_SnapshotDuration_m4E7A4CC4C49AC6308F0336664883DDF335CA0F67 (void);
// 0x0000027B System.Boolean Lean.Touch.LeanFinger::get_IsOverGui()
extern void LeanFinger_get_IsOverGui_mCF313973B1E1035AB322AA1B3077105069278E34 (void);
// 0x0000027C System.Boolean Lean.Touch.LeanFinger::get_Down()
extern void LeanFinger_get_Down_mB57C1F49D26C67B2C08F3C0E275B4B405E3CDEC9 (void);
// 0x0000027D System.Boolean Lean.Touch.LeanFinger::get_Up()
extern void LeanFinger_get_Up_m1F478DD3BF6DB0252014A8157E873DAF24348586 (void);
// 0x0000027E UnityEngine.Vector2 Lean.Touch.LeanFinger::get_LastSnapshotScreenDelta()
extern void LeanFinger_get_LastSnapshotScreenDelta_mC6BCCA2600E753A518A0227505C5B5F814D83587 (void);
// 0x0000027F UnityEngine.Vector2 Lean.Touch.LeanFinger::get_LastSnapshotScaledDelta()
extern void LeanFinger_get_LastSnapshotScaledDelta_m920920E7430D962D45649CE0292444F3E4849C22 (void);
// 0x00000280 UnityEngine.Vector2 Lean.Touch.LeanFinger::get_ScreenDelta()
extern void LeanFinger_get_ScreenDelta_m71CDA01F17B9664BEE576B1BDE030CCF75240AD6 (void);
// 0x00000281 UnityEngine.Vector2 Lean.Touch.LeanFinger::get_ScaledDelta()
extern void LeanFinger_get_ScaledDelta_m9F29F001B37315D360921C755FB3925A130B7AA3 (void);
// 0x00000282 UnityEngine.Vector2 Lean.Touch.LeanFinger::get_SwipeScreenDelta()
extern void LeanFinger_get_SwipeScreenDelta_m6D0ECD39FA7D90FF51D2F520E53F5EAD0BA8EC6B (void);
// 0x00000283 UnityEngine.Vector2 Lean.Touch.LeanFinger::get_SwipeScaledDelta()
extern void LeanFinger_get_SwipeScaledDelta_m84A7BD2A7E1709ACC6A2EC6D8450DA5E3E4505C2 (void);
// 0x00000284 UnityEngine.Vector2 Lean.Touch.LeanFinger::GetSmoothScreenPosition(System.Single)
extern void LeanFinger_GetSmoothScreenPosition_mB77A03E70985C80A112506D60763309135170CD9 (void);
// 0x00000285 System.Single Lean.Touch.LeanFinger::get_SmoothScreenPositionDelta()
extern void LeanFinger_get_SmoothScreenPositionDelta_m481400CB3A8A4E860AEA881D3DAFE26C3AC1F72B (void);
// 0x00000286 UnityEngine.Ray Lean.Touch.LeanFinger::GetRay(UnityEngine.Camera)
extern void LeanFinger_GetRay_mC28C0EEF67C2F1DEE49E82954017E044793CE5F1 (void);
// 0x00000287 UnityEngine.Ray Lean.Touch.LeanFinger::GetStartRay(UnityEngine.Camera)
extern void LeanFinger_GetStartRay_mD547BDCBA9C957B0794EA21C2F2C06268F36B257 (void);
// 0x00000288 UnityEngine.Vector2 Lean.Touch.LeanFinger::GetSnapshotScreenDelta(System.Single)
extern void LeanFinger_GetSnapshotScreenDelta_m988680318F883372CE5DFBC1B8735F630CEB58BF (void);
// 0x00000289 UnityEngine.Vector2 Lean.Touch.LeanFinger::GetSnapshotScaledDelta(System.Single)
extern void LeanFinger_GetSnapshotScaledDelta_m33BE10443C7522F2DEB9545365A5E9B7F6946BE0 (void);
// 0x0000028A UnityEngine.Vector2 Lean.Touch.LeanFinger::GetSnapshotScreenPosition(System.Single)
extern void LeanFinger_GetSnapshotScreenPosition_m8A921B210FA28F3B8E8278B86E97E77305FE343E (void);
// 0x0000028B UnityEngine.Vector3 Lean.Touch.LeanFinger::GetSnapshotWorldPosition(System.Single,System.Single,UnityEngine.Camera)
extern void LeanFinger_GetSnapshotWorldPosition_m6BA0D2F8E8651C739C1E6A1F07AA7CC611FA425C (void);
// 0x0000028C System.Single Lean.Touch.LeanFinger::GetRadians(UnityEngine.Vector2)
extern void LeanFinger_GetRadians_mA46DD0F0E0DD1A5F704CB494C06AC4C2B2938EF7 (void);
// 0x0000028D System.Single Lean.Touch.LeanFinger::GetDegrees(UnityEngine.Vector2)
extern void LeanFinger_GetDegrees_mD04F5A0671F3BD51689452543033886545404B54 (void);
// 0x0000028E System.Single Lean.Touch.LeanFinger::GetLastRadians(UnityEngine.Vector2)
extern void LeanFinger_GetLastRadians_m7E2BA868EAF1E1E93995E58CCC74E4D1782104F1 (void);
// 0x0000028F System.Single Lean.Touch.LeanFinger::GetLastDegrees(UnityEngine.Vector2)
extern void LeanFinger_GetLastDegrees_m30E53FC3061763AC5B2D2A061F44AFAF5DD5921E (void);
// 0x00000290 System.Single Lean.Touch.LeanFinger::GetDeltaRadians(UnityEngine.Vector2)
extern void LeanFinger_GetDeltaRadians_m99F8DD4F01E806C9D17E5BF0713697033CACB804 (void);
// 0x00000291 System.Single Lean.Touch.LeanFinger::GetDeltaRadians(UnityEngine.Vector2,UnityEngine.Vector2)
extern void LeanFinger_GetDeltaRadians_m35A118B31AD9BF0D52C0A5EE399CF2A87B86691E (void);
// 0x00000292 System.Single Lean.Touch.LeanFinger::GetDeltaDegrees(UnityEngine.Vector2)
extern void LeanFinger_GetDeltaDegrees_mF4BB08964D329EE0109CAF2B8690BD72B921BCBD (void);
// 0x00000293 System.Single Lean.Touch.LeanFinger::GetDeltaDegrees(UnityEngine.Vector2,UnityEngine.Vector2)
extern void LeanFinger_GetDeltaDegrees_mEF7B5CCD801B5B8208CC60D16305F8B12A7AECDD (void);
// 0x00000294 System.Single Lean.Touch.LeanFinger::GetScreenDistance(UnityEngine.Vector2)
extern void LeanFinger_GetScreenDistance_m7BD5C7325694D57F1D59B8D119908BF31B97EB02 (void);
// 0x00000295 System.Single Lean.Touch.LeanFinger::GetScaledDistance(UnityEngine.Vector2)
extern void LeanFinger_GetScaledDistance_mDFA1BA6F9AE3E0C03BE4C05AF27753C2AC3FDAB7 (void);
// 0x00000296 System.Single Lean.Touch.LeanFinger::GetLastScreenDistance(UnityEngine.Vector2)
extern void LeanFinger_GetLastScreenDistance_mC65F4CC190D71163327D6FEBCC3E07A8E45CEDA7 (void);
// 0x00000297 System.Single Lean.Touch.LeanFinger::GetLastScaledDistance(UnityEngine.Vector2)
extern void LeanFinger_GetLastScaledDistance_m6BB3BE44652F214ACEFB78E2C2DB09E88CC72FAB (void);
// 0x00000298 System.Single Lean.Touch.LeanFinger::GetStartScreenDistance(UnityEngine.Vector2)
extern void LeanFinger_GetStartScreenDistance_m9F7BF1F7EA17C3E47A808DD4666B4D832F100792 (void);
// 0x00000299 System.Single Lean.Touch.LeanFinger::GetStartScaledDistance(UnityEngine.Vector2)
extern void LeanFinger_GetStartScaledDistance_mC8F2C410975340247FA17B126522519DCA998A10 (void);
// 0x0000029A UnityEngine.Vector3 Lean.Touch.LeanFinger::GetStartWorldPosition(System.Single,UnityEngine.Camera)
extern void LeanFinger_GetStartWorldPosition_mF1878403A48D2B7D2F0CD9087A191C2144A1C3F6 (void);
// 0x0000029B UnityEngine.Vector3 Lean.Touch.LeanFinger::GetLastWorldPosition(System.Single,UnityEngine.Camera)
extern void LeanFinger_GetLastWorldPosition_m312D97F63BFBABE19C2CDAACC6311DDE92E45D6B (void);
// 0x0000029C UnityEngine.Vector3 Lean.Touch.LeanFinger::GetWorldPosition(System.Single,UnityEngine.Camera)
extern void LeanFinger_GetWorldPosition_m63BBE135F913E37A60AA495CF7FC69D2B697CD69 (void);
// 0x0000029D UnityEngine.Vector3 Lean.Touch.LeanFinger::GetWorldDelta(System.Single,UnityEngine.Camera)
extern void LeanFinger_GetWorldDelta_m403CDB6E467BB2BD0927674FB76531CD6F371716 (void);
// 0x0000029E UnityEngine.Vector3 Lean.Touch.LeanFinger::GetWorldDelta(System.Single,System.Single,UnityEngine.Camera)
extern void LeanFinger_GetWorldDelta_mA1F69412C75B8355A933C02A28B094FA23E18610 (void);
// 0x0000029F System.Void Lean.Touch.LeanFinger::ClearSnapshots(System.Int32)
extern void LeanFinger_ClearSnapshots_mF162FD7281F98B5E1E0931AE27AA9B4F7A1DBA9E (void);
// 0x000002A0 System.Void Lean.Touch.LeanFinger::RecordSnapshot()
extern void LeanFinger_RecordSnapshot_mAE3635890DB234C2F9AD4997ACAB995EEBD856D2 (void);
// 0x000002A1 System.Void Lean.Touch.LeanFinger::.ctor()
extern void LeanFinger__ctor_m13E48E6164F1F89D80B45D132CBEB29C274B28BC (void);
// 0x000002A2 UnityEngine.Vector2 Lean.Touch.LeanGesture::GetScreenCenter()
extern void LeanGesture_GetScreenCenter_m0843C8DCF5EEA72404C4495EFC3D3C6DE050D429 (void);
// 0x000002A3 UnityEngine.Vector2 Lean.Touch.LeanGesture::GetScreenCenter(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetScreenCenter_mB53F13E2C96624F8A9B03156E06D562925402955 (void);
// 0x000002A4 System.Boolean Lean.Touch.LeanGesture::TryGetScreenCenter(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2&)
extern void LeanGesture_TryGetScreenCenter_mE0AA0193BF973B5B39CEAD8728811B8708F7B907 (void);
// 0x000002A5 UnityEngine.Vector2 Lean.Touch.LeanGesture::GetLastScreenCenter()
extern void LeanGesture_GetLastScreenCenter_mE78F128D1789451EA3CF9A2206ED0FF1E95BEB48 (void);
// 0x000002A6 UnityEngine.Vector2 Lean.Touch.LeanGesture::GetLastScreenCenter(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetLastScreenCenter_m1E682F188DDC687F9DB91088401C28A2EF9A8F11 (void);
// 0x000002A7 System.Boolean Lean.Touch.LeanGesture::TryGetLastScreenCenter(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2&)
extern void LeanGesture_TryGetLastScreenCenter_m036B9F6D038D22AB13F10AFB25ACC83E7A6E46DB (void);
// 0x000002A8 UnityEngine.Vector2 Lean.Touch.LeanGesture::GetStartScreenCenter()
extern void LeanGesture_GetStartScreenCenter_mB24C255ADC15D489D71141AFC132A19F679E46CC (void);
// 0x000002A9 UnityEngine.Vector2 Lean.Touch.LeanGesture::GetStartScreenCenter(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetStartScreenCenter_mE32898F28278ACDE8A288D9620C5B28AF54F9B53 (void);
// 0x000002AA System.Boolean Lean.Touch.LeanGesture::TryGetStartScreenCenter(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2&)
extern void LeanGesture_TryGetStartScreenCenter_m2E55257E7F56CB292D740BC88D52F4079D34F570 (void);
// 0x000002AB UnityEngine.Vector2 Lean.Touch.LeanGesture::GetScreenDelta()
extern void LeanGesture_GetScreenDelta_mA271A261FDD051B4E10345967BC0E693C3CA5645 (void);
// 0x000002AC UnityEngine.Vector2 Lean.Touch.LeanGesture::GetScreenDelta(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetScreenDelta_mC51532A01DCAE638C470F3B4373A8EBE776C154A (void);
// 0x000002AD System.Boolean Lean.Touch.LeanGesture::TryGetScreenDelta(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2&)
extern void LeanGesture_TryGetScreenDelta_mEC82733B097F154A1EB8C99017BA337CBAAF9F23 (void);
// 0x000002AE UnityEngine.Vector2 Lean.Touch.LeanGesture::GetScaledDelta()
extern void LeanGesture_GetScaledDelta_mEDCFD0FC522248E3A7F93F9FD623D5B1BF318B43 (void);
// 0x000002AF UnityEngine.Vector2 Lean.Touch.LeanGesture::GetScaledDelta(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetScaledDelta_mB6F662577CE91A307556F1D887EC635B586205DE (void);
// 0x000002B0 System.Boolean Lean.Touch.LeanGesture::TryGetScaledDelta(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2&)
extern void LeanGesture_TryGetScaledDelta_mD4C25322E14F5B6D86CE98D6FBAE6A8E5581604A (void);
// 0x000002B1 UnityEngine.Vector3 Lean.Touch.LeanGesture::GetWorldDelta(System.Single,UnityEngine.Camera)
extern void LeanGesture_GetWorldDelta_m001CC4B486F739F3676A2C204FAEECCA1F25E043 (void);
// 0x000002B2 UnityEngine.Vector3 Lean.Touch.LeanGesture::GetWorldDelta(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,System.Single,UnityEngine.Camera)
extern void LeanGesture_GetWorldDelta_mE619FEF4AA91E0244F4ABAD964E40DC9BE4320CC (void);
// 0x000002B3 System.Boolean Lean.Touch.LeanGesture::TryGetWorldDelta(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,System.Single,UnityEngine.Vector3&,UnityEngine.Camera)
extern void LeanGesture_TryGetWorldDelta_mAAC254E66D8A15FDA39576E247517EAFF3F65B83 (void);
// 0x000002B4 System.Single Lean.Touch.LeanGesture::GetScreenDistance()
extern void LeanGesture_GetScreenDistance_mE7003DFBFA44E9730CA53CFE3BE4B789D26CEDB5 (void);
// 0x000002B5 System.Single Lean.Touch.LeanGesture::GetScreenDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetScreenDistance_m66CC128493767DF75DF8DC5E2F202D55F74DCC32 (void);
// 0x000002B6 System.Single Lean.Touch.LeanGesture::GetScreenDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2)
extern void LeanGesture_GetScreenDistance_m392B2EED4C096D1F585312B1180616D5E95F7939 (void);
// 0x000002B7 System.Boolean Lean.Touch.LeanGesture::TryGetScreenDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,System.Single&)
extern void LeanGesture_TryGetScreenDistance_m11518F012E770AD0BE8119CE600E8203AC230E3E (void);
// 0x000002B8 System.Single Lean.Touch.LeanGesture::GetScaledDistance()
extern void LeanGesture_GetScaledDistance_m0DD1C0F924258D86647232406D3799D54B724F5C (void);
// 0x000002B9 System.Single Lean.Touch.LeanGesture::GetScaledDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetScaledDistance_mB9E5F83DFF7589E8415A0B2BD3BD594FAC9ED4C1 (void);
// 0x000002BA System.Single Lean.Touch.LeanGesture::GetScaledDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2)
extern void LeanGesture_GetScaledDistance_m762722AC7D36EF7BAE54A2A5612E400BA9FC7FC6 (void);
// 0x000002BB System.Boolean Lean.Touch.LeanGesture::TryGetScaledDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,System.Single&)
extern void LeanGesture_TryGetScaledDistance_mF6085A852520A6BFD131744E72580B9F5E323633 (void);
// 0x000002BC System.Single Lean.Touch.LeanGesture::GetLastScreenDistance()
extern void LeanGesture_GetLastScreenDistance_m50A1A0C138801064825D23525E228DA683264DDA (void);
// 0x000002BD System.Single Lean.Touch.LeanGesture::GetLastScreenDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetLastScreenDistance_mB92242C611BAA657F0E3FFC10F817CD585D401A6 (void);
// 0x000002BE System.Single Lean.Touch.LeanGesture::GetLastScreenDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2)
extern void LeanGesture_GetLastScreenDistance_mC5D5F4D4B65E3F360669B1BD8DD0D68AFE349EEB (void);
// 0x000002BF System.Boolean Lean.Touch.LeanGesture::TryGetLastScreenDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,System.Single&)
extern void LeanGesture_TryGetLastScreenDistance_m6D5DCF2CB33BF151BE7DF791EE3396633D430A17 (void);
// 0x000002C0 System.Single Lean.Touch.LeanGesture::GetLastScaledDistance()
extern void LeanGesture_GetLastScaledDistance_m1034B646C0DA00C3A714A7A1C988B6BEC52E7627 (void);
// 0x000002C1 System.Single Lean.Touch.LeanGesture::GetLastScaledDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetLastScaledDistance_mFEA5DD8AFF5A73CF7720B9AE94F3A2539CB8481A (void);
// 0x000002C2 System.Single Lean.Touch.LeanGesture::GetLastScaledDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2)
extern void LeanGesture_GetLastScaledDistance_mAA60368A022E9C9793456ECD05B3A9B20981C62E (void);
// 0x000002C3 System.Boolean Lean.Touch.LeanGesture::TryGetLastScaledDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,System.Single&)
extern void LeanGesture_TryGetLastScaledDistance_m4D7191A82CC9F5693709BBC973769694833991E8 (void);
// 0x000002C4 System.Single Lean.Touch.LeanGesture::GetStartScreenDistance()
extern void LeanGesture_GetStartScreenDistance_m451E32C0BFDD33D6ABC97360E1B7C0158A28D343 (void);
// 0x000002C5 System.Single Lean.Touch.LeanGesture::GetStartScreenDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetStartScreenDistance_mD05407DBF42825CB5211C7D6B23BC818D0734D07 (void);
// 0x000002C6 System.Single Lean.Touch.LeanGesture::GetStartScreenDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2)
extern void LeanGesture_GetStartScreenDistance_m9F9F4B7A408142A519B6C765772FD47EC827D4F7 (void);
// 0x000002C7 System.Boolean Lean.Touch.LeanGesture::TryGetStartScreenDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,System.Single&)
extern void LeanGesture_TryGetStartScreenDistance_mF8A9C22AE95A9106682637633C307BBA96CE73A7 (void);
// 0x000002C8 System.Single Lean.Touch.LeanGesture::GetStartScaledDistance()
extern void LeanGesture_GetStartScaledDistance_m312AD2BD9098D51CA3E56C2FF80EA63837AFA9F4 (void);
// 0x000002C9 System.Single Lean.Touch.LeanGesture::GetStartScaledDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetStartScaledDistance_mD2CECFBF13AECD07C6BFA04B22CD3DB63769C8F9 (void);
// 0x000002CA System.Single Lean.Touch.LeanGesture::GetStartScaledDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2)
extern void LeanGesture_GetStartScaledDistance_mA1BB20CD718AD089AEBC441B48E4F1E356C40F75 (void);
// 0x000002CB System.Boolean Lean.Touch.LeanGesture::TryGetStartScaledDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,System.Single&)
extern void LeanGesture_TryGetStartScaledDistance_mF9613BC693B3C168C9D20016A7F03A25099A2C71 (void);
// 0x000002CC System.Single Lean.Touch.LeanGesture::GetPinchScale(System.Single)
extern void LeanGesture_GetPinchScale_m558BFE4346A40810062CD13907819CB44CC5D519 (void);
// 0x000002CD System.Single Lean.Touch.LeanGesture::GetPinchScale(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,System.Single)
extern void LeanGesture_GetPinchScale_mCEFEA55BE3D6995096A04D9A9A265BD5A989BC3E (void);
// 0x000002CE System.Boolean Lean.Touch.LeanGesture::TryGetPinchScale(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,UnityEngine.Vector2,System.Single&,System.Single)
extern void LeanGesture_TryGetPinchScale_mC9CF4DB112D4EA14BB4036DC4AD66A7CD38BF2D7 (void);
// 0x000002CF System.Single Lean.Touch.LeanGesture::GetPinchRatio(System.Single)
extern void LeanGesture_GetPinchRatio_m73328152C95EB01C16B0782A6170506D112C3BFA (void);
// 0x000002D0 System.Single Lean.Touch.LeanGesture::GetPinchRatio(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,System.Single)
extern void LeanGesture_GetPinchRatio_mE986D8F99B54B2CD5B025BDE337A1257CA049394 (void);
// 0x000002D1 System.Boolean Lean.Touch.LeanGesture::TryGetPinchRatio(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,UnityEngine.Vector2,System.Single&,System.Single)
extern void LeanGesture_TryGetPinchRatio_m7896EBC828A485A33E706350D35BEA98525EC502 (void);
// 0x000002D2 System.Single Lean.Touch.LeanGesture::GetTwistDegrees()
extern void LeanGesture_GetTwistDegrees_m42301E5586566074403D4D34130053D176A229AE (void);
// 0x000002D3 System.Single Lean.Touch.LeanGesture::GetTwistDegrees(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetTwistDegrees_mA5A9EECB3088ACDFF63D0576002B4A2B04A243A1 (void);
// 0x000002D4 System.Single Lean.Touch.LeanGesture::GetTwistDegrees(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,UnityEngine.Vector2)
extern void LeanGesture_GetTwistDegrees_mF6E7C38A02604FA07D01079E9855DD3D3A2E10B9 (void);
// 0x000002D5 System.Boolean Lean.Touch.LeanGesture::TryGetTwistDegrees(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,UnityEngine.Vector2,System.Single&)
extern void LeanGesture_TryGetTwistDegrees_m00BCA9CD76EE9D1CD6DBC41955FC858C1AB5F1B8 (void);
// 0x000002D6 System.Single Lean.Touch.LeanGesture::GetTwistRadians()
extern void LeanGesture_GetTwistRadians_mFCD7A86FB202F153389DCF255B19E7C6DB6DFDBC (void);
// 0x000002D7 System.Single Lean.Touch.LeanGesture::GetTwistRadians(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetTwistRadians_mD7468AC6FEDA55E6446DCE8283E329BDA8021734 (void);
// 0x000002D8 System.Single Lean.Touch.LeanGesture::GetTwistRadians(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,UnityEngine.Vector2)
extern void LeanGesture_GetTwistRadians_m33801F0E4A2998B11D45D014B3B735504FE5F1B1 (void);
// 0x000002D9 System.Boolean Lean.Touch.LeanGesture::TryGetTwistRadians(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,UnityEngine.Vector2,System.Single&)
extern void LeanGesture_TryGetTwistRadians_m4BEBFB54D2DC25F9F73F3790282E2BE4B1301E41 (void);
// 0x000002DA UnityEngine.Vector3 Lean.Touch.LeanSnapshot::GetWorldPosition(System.Single,UnityEngine.Camera)
extern void LeanSnapshot_GetWorldPosition_mDB486AF1F02CC9BB3AC436B37068E08555C01B93 (void);
// 0x000002DB Lean.Touch.LeanSnapshot Lean.Touch.LeanSnapshot::Pop()
extern void LeanSnapshot_Pop_mD2CEC21623B7E8FE13CCA810C07F4E77423FC670 (void);
// 0x000002DC System.Boolean Lean.Touch.LeanSnapshot::TryGetScreenPosition(System.Collections.Generic.List`1<Lean.Touch.LeanSnapshot>,System.Single,UnityEngine.Vector2&)
extern void LeanSnapshot_TryGetScreenPosition_mB2916415E1A607256CB52311E063AD5EAB875EA2 (void);
// 0x000002DD System.Boolean Lean.Touch.LeanSnapshot::TryGetSnapshot(System.Collections.Generic.List`1<Lean.Touch.LeanSnapshot>,System.Int32,System.Single&,UnityEngine.Vector2&)
extern void LeanSnapshot_TryGetSnapshot_mDCB3AEC3C43B5F21B4A37D90F33BA416CB8A891F (void);
// 0x000002DE System.Int32 Lean.Touch.LeanSnapshot::GetLowerIndex(System.Collections.Generic.List`1<Lean.Touch.LeanSnapshot>,System.Single)
extern void LeanSnapshot_GetLowerIndex_mC4354BDDCAC8930F1B7D7BBADF266CC3666B72B3 (void);
// 0x000002DF System.Void Lean.Touch.LeanSnapshot::.ctor()
extern void LeanSnapshot__ctor_mC54AA27EA5FB56CCEA2FFBB818A641AD903C2CF7 (void);
// 0x000002E0 System.Void Lean.Touch.LeanSnapshot::.cctor()
extern void LeanSnapshot__cctor_m7232290A0AEDDCED1A74BD7867EDFFE1AD160AEC (void);
// 0x000002E1 System.Void Lean.Touch.LeanTouch::add_OnFingerDown(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_add_OnFingerDown_m2E56F938EABEF32E2BFCAE63FDCE9531CFD64C8E (void);
// 0x000002E2 System.Void Lean.Touch.LeanTouch::remove_OnFingerDown(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_remove_OnFingerDown_mC28728EB87ED09ABB6D7588FAD667C17B91223A4 (void);
// 0x000002E3 System.Void Lean.Touch.LeanTouch::add_OnFingerUpdate(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_add_OnFingerUpdate_mA786500D7D02C997D1C487ABEF230C80D7CBFB9B (void);
// 0x000002E4 System.Void Lean.Touch.LeanTouch::remove_OnFingerUpdate(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_remove_OnFingerUpdate_mD0DF62C210AD829E7103F78E29D2E2E8B4351C92 (void);
// 0x000002E5 System.Void Lean.Touch.LeanTouch::add_OnFingerUp(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_add_OnFingerUp_mC2726DD35564E0B4BAFE74A733DDC29F768F0E2E (void);
// 0x000002E6 System.Void Lean.Touch.LeanTouch::remove_OnFingerUp(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_remove_OnFingerUp_m4779CFA6F17D71FF23789057ACCC9B04D5F5CED7 (void);
// 0x000002E7 System.Void Lean.Touch.LeanTouch::add_OnFingerOld(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_add_OnFingerOld_m08E026CB371529E9A8254BB9B3C82CEBA087D2A0 (void);
// 0x000002E8 System.Void Lean.Touch.LeanTouch::remove_OnFingerOld(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_remove_OnFingerOld_m3F348423F2EF67F8D3E5BB7D15C1840E7682CDD9 (void);
// 0x000002E9 System.Void Lean.Touch.LeanTouch::add_OnFingerTap(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_add_OnFingerTap_m0802FACCBF3C4B92576241E5B2F46A26B4990F07 (void);
// 0x000002EA System.Void Lean.Touch.LeanTouch::remove_OnFingerTap(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_remove_OnFingerTap_m2159327C89954FC3929FA2B85FD2295295818AE4 (void);
// 0x000002EB System.Void Lean.Touch.LeanTouch::add_OnFingerSwipe(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_add_OnFingerSwipe_m363049691F98764F06ABE81F45899531BE1AD7E5 (void);
// 0x000002EC System.Void Lean.Touch.LeanTouch::remove_OnFingerSwipe(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_remove_OnFingerSwipe_m74E13C41E8621C078FA57D6E8E9F70470A8D2AD9 (void);
// 0x000002ED System.Void Lean.Touch.LeanTouch::add_OnGesture(System.Action`1<System.Collections.Generic.List`1<Lean.Touch.LeanFinger>>)
extern void LeanTouch_add_OnGesture_mF4525E24D51F05A940E7EE0C73033942AF9BB07F (void);
// 0x000002EE System.Void Lean.Touch.LeanTouch::remove_OnGesture(System.Action`1<System.Collections.Generic.List`1<Lean.Touch.LeanFinger>>)
extern void LeanTouch_remove_OnGesture_mAD89251E3BBE424D24FFAEB335FBC3F033DB02B1 (void);
// 0x000002EF System.Void Lean.Touch.LeanTouch::add_OnFingerExpired(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_add_OnFingerExpired_m5A8D0D603DD8CDD4ACA19BA10932D9B4758C295E (void);
// 0x000002F0 System.Void Lean.Touch.LeanTouch::remove_OnFingerExpired(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_remove_OnFingerExpired_m55D8D1051A030D56C696B704A94AA141A909E2FE (void);
// 0x000002F1 System.Void Lean.Touch.LeanTouch::add_OnFingerInactive(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_add_OnFingerInactive_m0BBD293C30719C22C0450433AA22C33605DFA892 (void);
// 0x000002F2 System.Void Lean.Touch.LeanTouch::remove_OnFingerInactive(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_remove_OnFingerInactive_m9588937E0578A605793B800CD516FC16EC6D090D (void);
// 0x000002F3 System.Void Lean.Touch.LeanTouch::add_OnSimulateFingers(System.Action)
extern void LeanTouch_add_OnSimulateFingers_m84243198DC16B9603EEAB4B50BFC177230E61844 (void);
// 0x000002F4 System.Void Lean.Touch.LeanTouch::remove_OnSimulateFingers(System.Action)
extern void LeanTouch_remove_OnSimulateFingers_mA967C68790D03B90A9360515539D9B61F12EEE9B (void);
// 0x000002F5 System.Void Lean.Touch.LeanTouch::set_TapThreshold(System.Single)
extern void LeanTouch_set_TapThreshold_mAE881243CD136B5EAEECFF351433159A5DED820D (void);
// 0x000002F6 System.Single Lean.Touch.LeanTouch::get_TapThreshold()
extern void LeanTouch_get_TapThreshold_mFE27250D6104795BBD85FD7BD0F914469E2ED1B5 (void);
// 0x000002F7 System.Single Lean.Touch.LeanTouch::get_CurrentTapThreshold()
extern void LeanTouch_get_CurrentTapThreshold_m0680E602895DD4187B397575889F8CDD87729CF4 (void);
// 0x000002F8 System.Void Lean.Touch.LeanTouch::set_SwipeThreshold(System.Single)
extern void LeanTouch_set_SwipeThreshold_m09DED68B40040EEF2920B96069BC24D5CD3B97E0 (void);
// 0x000002F9 System.Single Lean.Touch.LeanTouch::get_SwipeThreshold()
extern void LeanTouch_get_SwipeThreshold_m8FB727022F13AA3DDF040A5021599E0A3251AF1C (void);
// 0x000002FA System.Single Lean.Touch.LeanTouch::get_CurrentSwipeThreshold()
extern void LeanTouch_get_CurrentSwipeThreshold_m0E7EF447C90D19610FE0852C4F992A3351CA7858 (void);
// 0x000002FB System.Void Lean.Touch.LeanTouch::set_ReferenceDpi(System.Int32)
extern void LeanTouch_set_ReferenceDpi_m4056E9211750128B3385A37D3B52A2AF6C8A6F0F (void);
// 0x000002FC System.Int32 Lean.Touch.LeanTouch::get_ReferenceDpi()
extern void LeanTouch_get_ReferenceDpi_mE2B85BC689AD5513209A3A9CDCCD91E69D97D6C7 (void);
// 0x000002FD System.Int32 Lean.Touch.LeanTouch::get_CurrentReferenceDpi()
extern void LeanTouch_get_CurrentReferenceDpi_mFD0B6020B347D2FD9AA408942291678380B27E0A (void);
// 0x000002FE System.Void Lean.Touch.LeanTouch::set_GuiLayers(UnityEngine.LayerMask)
extern void LeanTouch_set_GuiLayers_mBEF94C7F478CCBCAEB0C974C952F6EB66D0D7CAC (void);
// 0x000002FF UnityEngine.LayerMask Lean.Touch.LeanTouch::get_GuiLayers()
extern void LeanTouch_get_GuiLayers_m50AA727955348471094CD1D2E174C5D8D6644992 (void);
// 0x00000300 UnityEngine.LayerMask Lean.Touch.LeanTouch::get_CurrentGuiLayers()
extern void LeanTouch_get_CurrentGuiLayers_m2298663D6E09270D986BF2B8222E99B30BCF7EDE (void);
// 0x00000301 System.Void Lean.Touch.LeanTouch::set_UseTouch(System.Boolean)
extern void LeanTouch_set_UseTouch_m582BA72AA19749C017386080FC332A5D367E4378 (void);
// 0x00000302 System.Boolean Lean.Touch.LeanTouch::get_UseTouch()
extern void LeanTouch_get_UseTouch_m75D5ECB8FC37684B68E5D230AA9FAA6B2B77241A (void);
// 0x00000303 System.Void Lean.Touch.LeanTouch::set_UseHover(System.Boolean)
extern void LeanTouch_set_UseHover_mFFB08098481C789383DAAC0BEE0762F265FC124F (void);
// 0x00000304 System.Boolean Lean.Touch.LeanTouch::get_UseHover()
extern void LeanTouch_get_UseHover_mEEE31010CD11877FA14EB6F9F320D330ABC3F99F (void);
// 0x00000305 System.Void Lean.Touch.LeanTouch::set_UseMouse(System.Boolean)
extern void LeanTouch_set_UseMouse_m8323CF05D124A6CEBBE98C45C7766A49E47AD14C (void);
// 0x00000306 System.Boolean Lean.Touch.LeanTouch::get_UseMouse()
extern void LeanTouch_get_UseMouse_mE0D3DAA013109ACF0B5E0E5F18BF63A6BCC11FFE (void);
// 0x00000307 System.Void Lean.Touch.LeanTouch::set_UseSimulator(System.Boolean)
extern void LeanTouch_set_UseSimulator_mE86E37D0D63F6EC21F5047821C8F521EEAFF7F0E (void);
// 0x00000308 System.Boolean Lean.Touch.LeanTouch::get_UseSimulator()
extern void LeanTouch_get_UseSimulator_m02E9B7238F2F20C0AE5F65A335148F9C695E7AD8 (void);
// 0x00000309 System.Void Lean.Touch.LeanTouch::set_DisableMouseEmulation(System.Boolean)
extern void LeanTouch_set_DisableMouseEmulation_m4FAD9C78B268E4A14760CBE162598C270FF8A4CA (void);
// 0x0000030A System.Boolean Lean.Touch.LeanTouch::get_DisableMouseEmulation()
extern void LeanTouch_get_DisableMouseEmulation_m381D310CE9BFC3219BBC9189377AB33341996DE8 (void);
// 0x0000030B System.Void Lean.Touch.LeanTouch::set_RecordFingers(System.Boolean)
extern void LeanTouch_set_RecordFingers_mE5C42AB1B504099DCA1F2327DFC948D3050A4BF0 (void);
// 0x0000030C System.Boolean Lean.Touch.LeanTouch::get_RecordFingers()
extern void LeanTouch_get_RecordFingers_m01E0316CF3949DE554A3971C93127F2B71C1CF41 (void);
// 0x0000030D System.Void Lean.Touch.LeanTouch::set_RecordThreshold(System.Single)
extern void LeanTouch_set_RecordThreshold_mAB8F1B37BF74B7BB1BEC00BFDDEDCA26E4A1E7CB (void);
// 0x0000030E System.Single Lean.Touch.LeanTouch::get_RecordThreshold()
extern void LeanTouch_get_RecordThreshold_m6B8E6F378B975C473839FBBA86290E83BA41270E (void);
// 0x0000030F System.Void Lean.Touch.LeanTouch::set_RecordLimit(System.Single)
extern void LeanTouch_set_RecordLimit_mE1B3EF4908DED3F10BC5E6E2F1C0FAF78D637174 (void);
// 0x00000310 System.Single Lean.Touch.LeanTouch::get_RecordLimit()
extern void LeanTouch_get_RecordLimit_mD2A16DE05F30877C11EB33AE2EBEC31A68030797 (void);
// 0x00000311 Lean.Touch.LeanTouch Lean.Touch.LeanTouch::get_Instance()
extern void LeanTouch_get_Instance_mBBC65A9CC76EB0303FE32C3BCAE0CC7946C4A61F (void);
// 0x00000312 System.Single Lean.Touch.LeanTouch::get_ScalingFactor()
extern void LeanTouch_get_ScalingFactor_m4CAB375511E8DEAF7DC6B6AA6C2A1424B94E6D51 (void);
// 0x00000313 System.Single Lean.Touch.LeanTouch::get_ScreenFactor()
extern void LeanTouch_get_ScreenFactor_mFC5899186F9BA622976569E83D2096AD8C4E88CA (void);
// 0x00000314 System.Boolean Lean.Touch.LeanTouch::get_GuiInUse()
extern void LeanTouch_get_GuiInUse_m3315AC1F165E1CD3A08AFDE43AB4FF351EC523A5 (void);
// 0x00000315 System.Boolean Lean.Touch.LeanTouch::ElementOverlapped(UnityEngine.GameObject,UnityEngine.Vector2)
extern void LeanTouch_ElementOverlapped_m1CAFC6F51029647D0C3FE646A2C4D1A0BEBC7716 (void);
// 0x00000316 UnityEngine.EventSystems.EventSystem Lean.Touch.LeanTouch::GetEventSystem()
extern void LeanTouch_GetEventSystem_mFD85F8F0F31C5522B450D9FD45779606260A3C48 (void);
// 0x00000317 System.Boolean Lean.Touch.LeanTouch::PointOverGui(UnityEngine.Vector2)
extern void LeanTouch_PointOverGui_mF59BFC133EB266CE57341BE19D7BF26786E92531 (void);
// 0x00000318 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> Lean.Touch.LeanTouch::RaycastGui(UnityEngine.Vector2)
extern void LeanTouch_RaycastGui_m569DB00564461157996171DDD44B1C1249AB32DF (void);
// 0x00000319 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> Lean.Touch.LeanTouch::RaycastGui(UnityEngine.Vector2,UnityEngine.LayerMask)
extern void LeanTouch_RaycastGui_m1FC6B9273BAB9F96F86B05160DC7D67F439771AF (void);
// 0x0000031A System.Collections.Generic.List`1<Lean.Touch.LeanFinger> Lean.Touch.LeanTouch::GetFingers(System.Boolean,System.Boolean,System.Int32,System.Boolean)
extern void LeanTouch_GetFingers_m92F5C0E0E9C8ABC5A477FB5D9D72372DB113252F (void);
// 0x0000031B System.Void Lean.Touch.LeanTouch::SimulateTap(UnityEngine.Vector2,System.Single,System.Int32)
extern void LeanTouch_SimulateTap_m670B99980252EF891C41E627064636D2209BE71D (void);
// 0x0000031C System.Void Lean.Touch.LeanTouch::Clear()
extern void LeanTouch_Clear_m038D327F430CBCEA4FFBEF056834B04738908D61 (void);
// 0x0000031D System.Void Lean.Touch.LeanTouch::UpdateMouseEmulation()
extern void LeanTouch_UpdateMouseEmulation_mD72C5A268AFD49117207D721CED70E119F85976D (void);
// 0x0000031E System.Void Lean.Touch.LeanTouch::OnEnable()
extern void LeanTouch_OnEnable_m2EFCB061AC2F98451E89C23E8B7579B5A27A665C (void);
// 0x0000031F System.Void Lean.Touch.LeanTouch::OnDisable()
extern void LeanTouch_OnDisable_mE141904911A23C7B065EF85E0264805FC97B2705 (void);
// 0x00000320 System.Void Lean.Touch.LeanTouch::Update()
extern void LeanTouch_Update_m8C05D509BE1D246F459277C160C67B8F1B6F3019 (void);
// 0x00000321 System.Void Lean.Touch.LeanTouch::UpdateFingers(System.Single,System.Boolean)
extern void LeanTouch_UpdateFingers_m756088AD23417400111EB1085236D7BB752B8B6C (void);
// 0x00000322 System.Void Lean.Touch.LeanTouch::BeginFingers(System.Single)
extern void LeanTouch_BeginFingers_mAD2E255FB3BFFF4DEDE0AA087B7AB341E9FD97A5 (void);
// 0x00000323 System.Void Lean.Touch.LeanTouch::EndFingers(System.Single)
extern void LeanTouch_EndFingers_mD9A4ED85FCFAFD47AE7B9832F0688AF2ACABB84B (void);
// 0x00000324 System.Void Lean.Touch.LeanTouch::PollFingers()
extern void LeanTouch_PollFingers_m74C4885EBD84D7D9BA4297AB894D3B422C8731CF (void);
// 0x00000325 System.Void Lean.Touch.LeanTouch::UpdateEvents()
extern void LeanTouch_UpdateEvents_m90CDAE85D462BB36E4530476B7CF83A3DC1EC61D (void);
// 0x00000326 Lean.Touch.LeanFinger Lean.Touch.LeanTouch::AddFinger(System.Int32,UnityEngine.Vector2,System.Single,System.Boolean)
extern void LeanTouch_AddFinger_m22E19CCFD14A44FCC4901C6FDFDCF69B53D5C2FD (void);
// 0x00000327 Lean.Touch.LeanFinger Lean.Touch.LeanTouch::FindFinger(System.Int32)
extern void LeanTouch_FindFinger_mF0757248A41C04D204E9BCEE91438E57D4644049 (void);
// 0x00000328 System.Int32 Lean.Touch.LeanTouch::FindInactiveFingerIndex(System.Int32)
extern void LeanTouch_FindInactiveFingerIndex_m3295ADF95130FB9F1DE22DDE0F5F25DB0B265053 (void);
// 0x00000329 System.Void Lean.Touch.LeanTouch::.ctor()
extern void LeanTouch__ctor_m51C014B43ACC0A1F7D53A4331667143D6D10B89D (void);
// 0x0000032A System.Void Lean.Touch.LeanTouch::.cctor()
extern void LeanTouch__cctor_m6D16213A01F9E60D22CC131830F7D3320CAE9048 (void);
// 0x0000032B System.Void Lean.Common.LeanDestroy::set_Execute(Lean.Common.LeanDestroy/ExecuteType)
extern void LeanDestroy_set_Execute_m5C29E31056B8139967F89C45B8D447836062DAE9 (void);
// 0x0000032C Lean.Common.LeanDestroy/ExecuteType Lean.Common.LeanDestroy::get_Execute()
extern void LeanDestroy_get_Execute_m481B0CBE35A667EAA83678C5498882D1AEDE0546 (void);
// 0x0000032D System.Void Lean.Common.LeanDestroy::set_Target(UnityEngine.GameObject)
extern void LeanDestroy_set_Target_mD718EFA46FEE09131B8B915D389A95D56D46FF65 (void);
// 0x0000032E UnityEngine.GameObject Lean.Common.LeanDestroy::get_Target()
extern void LeanDestroy_get_Target_m30B946B4D8587451136B9AAB2308D8D5422292F9 (void);
// 0x0000032F System.Void Lean.Common.LeanDestroy::set_Seconds(System.Single)
extern void LeanDestroy_set_Seconds_mDAD6221A4E324B314AB58B92956DE68F126ECF45 (void);
// 0x00000330 System.Single Lean.Common.LeanDestroy::get_Seconds()
extern void LeanDestroy_get_Seconds_mE1EDED00363E72029C4154C84D8953DC56B0389D (void);
// 0x00000331 System.Void Lean.Common.LeanDestroy::Update()
extern void LeanDestroy_Update_m4788151349CB14FC1919255CC9FBD0E1D8B056CB (void);
// 0x00000332 System.Void Lean.Common.LeanDestroy::DestroyNow()
extern void LeanDestroy_DestroyNow_m2F3B8BCB8ED431FCC7EE6E7317A1FD84FF92BC9E (void);
// 0x00000333 System.Void Lean.Common.LeanDestroy::.ctor()
extern void LeanDestroy__ctor_m870FFF17B12BC66ECC0A1C46355DEF4C8A842DDC (void);
// 0x00000334 System.Void Lean.Common.LeanFormatString::set_Format(System.String)
extern void LeanFormatString_set_Format_m44BC70AF63CB152D7CCEA963C201F071257EDF56 (void);
// 0x00000335 System.String Lean.Common.LeanFormatString::get_Format()
extern void LeanFormatString_get_Format_mC8915C244025C812AD23E7DC225B9BAC09C678B4 (void);
// 0x00000336 Lean.Common.LeanFormatString/StringEvent Lean.Common.LeanFormatString::get_OnString()
extern void LeanFormatString_get_OnString_mAEA8872283296CDB0137A782BD7CE49770A5BC94 (void);
// 0x00000337 System.Void Lean.Common.LeanFormatString::SetString(System.String)
extern void LeanFormatString_SetString_m8D34E974327EAB919F519DEB1783452FB3A9AB1E (void);
// 0x00000338 System.Void Lean.Common.LeanFormatString::SetString(System.String,System.String)
extern void LeanFormatString_SetString_m9F8B437E60F2E07935E34A100D515D4D73723C52 (void);
// 0x00000339 System.Void Lean.Common.LeanFormatString::SetString(System.Int32)
extern void LeanFormatString_SetString_m8F493F752F5512E72CAE6A441A07583E33C0CC25 (void);
// 0x0000033A System.Void Lean.Common.LeanFormatString::SetString(System.Int32,System.Int32)
extern void LeanFormatString_SetString_mB2BE0D52A16DDD83004E38DE7FFC4494768885C2 (void);
// 0x0000033B System.Void Lean.Common.LeanFormatString::SetString(System.Single)
extern void LeanFormatString_SetString_m2437E031907355F53C319AA4BFD23C657BF1977C (void);
// 0x0000033C System.Void Lean.Common.LeanFormatString::SetString(System.Single,System.Single)
extern void LeanFormatString_SetString_m58FA40B9691AD4AC08AC7AF928D472A388C9A731 (void);
// 0x0000033D System.Void Lean.Common.LeanFormatString::SetString(UnityEngine.Vector2)
extern void LeanFormatString_SetString_mFC595BB40EDB35B7C11FE607055D5CF7DDFDA262 (void);
// 0x0000033E System.Void Lean.Common.LeanFormatString::SetString(UnityEngine.Vector2,UnityEngine.Vector2)
extern void LeanFormatString_SetString_m876CE712B0011822BF9161405056100B243AA3A8 (void);
// 0x0000033F System.Void Lean.Common.LeanFormatString::SetString(UnityEngine.Vector3)
extern void LeanFormatString_SetString_m34E176B98EBBCD00635EED9AFD9444F6C759CA24 (void);
// 0x00000340 System.Void Lean.Common.LeanFormatString::SetString(UnityEngine.Vector3,UnityEngine.Vector3)
extern void LeanFormatString_SetString_m9C6E82EE5FE63366EAFD04D22746790647A5AEAD (void);
// 0x00000341 System.Void Lean.Common.LeanFormatString::SetString(UnityEngine.Vector4)
extern void LeanFormatString_SetString_m8031C96BBB16DD6368CA6927A7E40FB79B1524DF (void);
// 0x00000342 System.Void Lean.Common.LeanFormatString::SetString(UnityEngine.Vector4,UnityEngine.Vector4)
extern void LeanFormatString_SetString_mE42B62823E25A93A7E657F720F6ECCE6E1B34EE9 (void);
// 0x00000343 System.Void Lean.Common.LeanFormatString::SetString(System.Single,System.Int32)
extern void LeanFormatString_SetString_mD779B6F5DD7178CD857B422242EFA01C1DE25C99 (void);
// 0x00000344 System.Void Lean.Common.LeanFormatString::SetString(System.Int32,System.Single)
extern void LeanFormatString_SetString_mC9A49868C45EC0A6253D0B321D8DAECEA8BBCF8D (void);
// 0x00000345 System.Void Lean.Common.LeanFormatString::SendString(System.Object)
extern void LeanFormatString_SendString_m9851DD3FFE8B07C743265B961F78D4F8065B9A52 (void);
// 0x00000346 System.Void Lean.Common.LeanFormatString::SendString(System.Object,System.Object)
extern void LeanFormatString_SendString_m07D565F1CE9F6A76EE31FFC46C3B344AD653C419 (void);
// 0x00000347 System.Void Lean.Common.LeanFormatString::.ctor()
extern void LeanFormatString__ctor_mE9E92C890B48F40081B87F48C3F3752EE92F8DD1 (void);
// 0x00000348 System.Void Lean.Common.LeanFormatString/StringEvent::.ctor()
extern void StringEvent__ctor_m0EFECAE65155F17A84EA7E5F220261C6E03ABA0B (void);
// 0x00000349 System.Int32 Lean.Common.LeanPath::get_PointCount()
extern void LeanPath_get_PointCount_mF5F7F552E76CE7E52AA8B9FA38760AF801136B88 (void);
// 0x0000034A System.Int32 Lean.Common.LeanPath::GetPointCount(System.Int32)
extern void LeanPath_GetPointCount_m904AC0FF40B2AE9750C9541C035CF128F9FB6094 (void);
// 0x0000034B UnityEngine.Vector3 Lean.Common.LeanPath::GetSmoothedPoint(System.Single)
extern void LeanPath_GetSmoothedPoint_m7284BFFDC1350CD161F27992008413A28EB6A309 (void);
// 0x0000034C UnityEngine.Vector3 Lean.Common.LeanPath::GetPoint(System.Int32,System.Int32)
extern void LeanPath_GetPoint_m809DDF4B617B43CB30CA6EF2C33AA8AD3A4D6659 (void);
// 0x0000034D UnityEngine.Vector3 Lean.Common.LeanPath::GetPointRaw(System.Int32,System.Int32)
extern void LeanPath_GetPointRaw_mEFEC1A35269B12C264B32A53883F27E347327D19 (void);
// 0x0000034E System.Void Lean.Common.LeanPath::SetLine(UnityEngine.Vector3,UnityEngine.Vector3)
extern void LeanPath_SetLine_m5221734FC5F5B4482B46A24C5CC2C8355B16BB34 (void);
// 0x0000034F System.Boolean Lean.Common.LeanPath::TryGetClosest(UnityEngine.Vector3,UnityEngine.Vector3&,System.Int32&,System.Int32&,System.Int32)
extern void LeanPath_TryGetClosest_m961C2E4329FDA0D8F6B54898725ACBD69317C659 (void);
// 0x00000350 System.Boolean Lean.Common.LeanPath::TryGetClosest(UnityEngine.Vector3,UnityEngine.Vector3&,System.Int32)
extern void LeanPath_TryGetClosest_m445515693D3D0C0908FB0533F01DC6E59238C2B9 (void);
// 0x00000351 System.Boolean Lean.Common.LeanPath::TryGetClosest(UnityEngine.Ray,UnityEngine.Vector3&,System.Int32&,System.Int32&,System.Int32)
extern void LeanPath_TryGetClosest_mC5CDF1FF80E51741388D1DE012E07E52B722485B (void);
// 0x00000352 System.Boolean Lean.Common.LeanPath::TryGetClosest(UnityEngine.Ray,UnityEngine.Vector3&,System.Int32)
extern void LeanPath_TryGetClosest_m150E1C0790AF8C436ED99E02AD665914ED0E54D8 (void);
// 0x00000353 System.Boolean Lean.Common.LeanPath::TryGetClosest(UnityEngine.Ray,UnityEngine.Vector3&,System.Int32,System.Single)
extern void LeanPath_TryGetClosest_m9EDCC556842AB6C4487B06DB5D37FA9C454DCD8C (void);
// 0x00000354 UnityEngine.Vector3 Lean.Common.LeanPath::GetClosestPoint(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void LeanPath_GetClosestPoint_m35BCB88CADA5172B750A9C1DC015CF57704CDA95 (void);
// 0x00000355 UnityEngine.Vector3 Lean.Common.LeanPath::GetClosestPoint(UnityEngine.Ray,UnityEngine.Vector3,UnityEngine.Vector3)
extern void LeanPath_GetClosestPoint_m8C1E2BD9CAB8AD2A3D281FADAA705D2A9D3224C4 (void);
// 0x00000356 System.Single Lean.Common.LeanPath::GetClosestDistance(UnityEngine.Ray,UnityEngine.Vector3)
extern void LeanPath_GetClosestDistance_m25EDF5A03B96A0615B55298C1458859FC6253AC8 (void);
// 0x00000357 System.Int32 Lean.Common.LeanPath::Mod(System.Int32,System.Int32)
extern void LeanPath_Mod_mA52316BA69AE1DC6A8B5F15D76589B4055DCFA66 (void);
// 0x00000358 System.Single Lean.Common.LeanPath::CubicInterpolate(System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LeanPath_CubicInterpolate_m1AA1DCBB91FEE95D41E5082FD32446ED979B4D84 (void);
// 0x00000359 System.Void Lean.Common.LeanPath::UpdateVisual()
extern void LeanPath_UpdateVisual_mC4B1C0DD129E0D88C1ED43B4F29B4A1B5DEE8202 (void);
// 0x0000035A System.Void Lean.Common.LeanPath::Update()
extern void LeanPath_Update_m87AF4F8022BC7ECB2057DA94C1932B3E2392D673 (void);
// 0x0000035B System.Void Lean.Common.LeanPath::.ctor()
extern void LeanPath__ctor_m50BA53BF46D63335083336D43580FF76737CB380 (void);
// 0x0000035C System.Void Lean.Common.LeanPath::.cctor()
extern void LeanPath__cctor_m17D84FAF9DA3FD05C14B36E46B09BB4928292D36 (void);
// 0x0000035D UnityEngine.Vector3 Lean.Common.LeanPlane::GetClosest(UnityEngine.Vector3,System.Single)
extern void LeanPlane_GetClosest_m31305D784A9F385A4030A64D6145AB7E9E7B1848 (void);
// 0x0000035E System.Boolean Lean.Common.LeanPlane::TryRaycast(UnityEngine.Ray,UnityEngine.Vector3&,System.Single,System.Boolean)
extern void LeanPlane_TryRaycast_m082ED056209FFFD090AE7F6331C2341D56EEC608 (void);
// 0x0000035F UnityEngine.Vector3 Lean.Common.LeanPlane::GetClosest(UnityEngine.Ray,System.Single)
extern void LeanPlane_GetClosest_mF04997CFE9B18AEEA1DF3C31E19EFFB4DDB278F2 (void);
// 0x00000360 System.Boolean Lean.Common.LeanPlane::RayToPlane(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Ray,System.Single&)
extern void LeanPlane_RayToPlane_mBD9C38BE30C7D51157BEAE1270C63709A453D8BB (void);
// 0x00000361 System.Void Lean.Common.LeanPlane::.ctor()
extern void LeanPlane__ctor_m6136ABF83F1F703CF82F2CA908BD8962CE681CEA (void);
// 0x00000362 System.Void Lean.Common.LeanRoll::set_Angle(System.Single)
extern void LeanRoll_set_Angle_mE887D61B151551F0514A8AE3CA243E87A8910B8C (void);
// 0x00000363 System.Single Lean.Common.LeanRoll::get_Angle()
extern void LeanRoll_get_Angle_mEAE28B2DBAAB93691705B724E3B560E3C2C4BDE2 (void);
// 0x00000364 System.Void Lean.Common.LeanRoll::set_Clamp(System.Boolean)
extern void LeanRoll_set_Clamp_m0907C0FC8FA63A2DF12B97E2125C24B70CBB6E18 (void);
// 0x00000365 System.Boolean Lean.Common.LeanRoll::get_Clamp()
extern void LeanRoll_get_Clamp_m88E823DC225A2981FC017909BCAA471849F5FB31 (void);
// 0x00000366 System.Void Lean.Common.LeanRoll::set_ClampMin(System.Single)
extern void LeanRoll_set_ClampMin_mACCED40B2FC690691CF51DE4E658DF62845D5D46 (void);
// 0x00000367 System.Single Lean.Common.LeanRoll::get_ClampMin()
extern void LeanRoll_get_ClampMin_m6EE98C23D42A1A116B549C27E9A9CDAAFED17169 (void);
// 0x00000368 System.Void Lean.Common.LeanRoll::set_ClampMax(System.Single)
extern void LeanRoll_set_ClampMax_m9087288BFB7F016D5AE8C16D77CAD2FE92E3D9D1 (void);
// 0x00000369 System.Single Lean.Common.LeanRoll::get_ClampMax()
extern void LeanRoll_get_ClampMax_mE4BDF0B55CF54A9DD139E23EA5E376A113C6CE45 (void);
// 0x0000036A System.Void Lean.Common.LeanRoll::set_Damping(System.Single)
extern void LeanRoll_set_Damping_m18C84B8122542D69247EB83C97C3D0A7FDA57CCA (void);
// 0x0000036B System.Single Lean.Common.LeanRoll::get_Damping()
extern void LeanRoll_get_Damping_mC449B549B4AF7D89E4456883D6FCB5BAA2CA4C84 (void);
// 0x0000036C System.Void Lean.Common.LeanRoll::IncrementAngle(System.Single)
extern void LeanRoll_IncrementAngle_m3F6D6F82CDF3857CC56A3B3BB08EFBEA7BAC6375 (void);
// 0x0000036D System.Void Lean.Common.LeanRoll::DecrementAngle(System.Single)
extern void LeanRoll_DecrementAngle_mF980FD369DC11AF3E44E8FC7176A74C776C01AA7 (void);
// 0x0000036E System.Void Lean.Common.LeanRoll::RotateToDelta(UnityEngine.Vector2)
extern void LeanRoll_RotateToDelta_mDB7CFA88C6866D016C8867A84F9EDBD7663A4062 (void);
// 0x0000036F System.Void Lean.Common.LeanRoll::SnapToTarget()
extern void LeanRoll_SnapToTarget_m38DE0B392D53013FAF766B9975F04E3BCA9226F1 (void);
// 0x00000370 System.Void Lean.Common.LeanRoll::Start()
extern void LeanRoll_Start_m9AC4D7BB13467134AF2E8C78F60FBB04966DF729 (void);
// 0x00000371 System.Void Lean.Common.LeanRoll::Update()
extern void LeanRoll_Update_m05A1404797C5EE0BB5F3A6A3AD72A3520E5CD354 (void);
// 0x00000372 System.Void Lean.Common.LeanRoll::.ctor()
extern void LeanRoll__ctor_m241DAF9B4D59659AEF203451CDF3B63E0BA24977 (void);
// 0x00000373 System.Void Lean.Common.LeanSelect::set_DeselectWithNothing(System.Boolean)
extern void LeanSelect_set_DeselectWithNothing_m96D5EAE0EEB615F298F2644201276366A760536C (void);
// 0x00000374 System.Boolean Lean.Common.LeanSelect::get_DeselectWithNothing()
extern void LeanSelect_get_DeselectWithNothing_m6E5FC30F06BAE82F950E1A6705665BCC659082D5 (void);
// 0x00000375 System.Void Lean.Common.LeanSelect::set_Limit(Lean.Common.LeanSelect/LimitType)
extern void LeanSelect_set_Limit_m1DA527E7B5FF03948AD4B561A0F5FB5031C04964 (void);
// 0x00000376 Lean.Common.LeanSelect/LimitType Lean.Common.LeanSelect::get_Limit()
extern void LeanSelect_get_Limit_m47EA6BE60E037C5C994F01A81C728C0D0D01FECB (void);
// 0x00000377 System.Void Lean.Common.LeanSelect::set_MaxSelectables(System.Int32)
extern void LeanSelect_set_MaxSelectables_m2E43A08FDDE829DAC1E44349ED6809ECDB2DF329 (void);
// 0x00000378 System.Int32 Lean.Common.LeanSelect::get_MaxSelectables()
extern void LeanSelect_get_MaxSelectables_m1A6011E15712E78F14DC334A12E411EEFC7FFFC8 (void);
// 0x00000379 System.Void Lean.Common.LeanSelect::set_Reselect(Lean.Common.LeanSelect/ReselectType)
extern void LeanSelect_set_Reselect_mA5CAFC19DB17E24C7BBD30039066CE18ABBE22C8 (void);
// 0x0000037A Lean.Common.LeanSelect/ReselectType Lean.Common.LeanSelect::get_Reselect()
extern void LeanSelect_get_Reselect_m21BB1AFD8981E47B83CEF1170EEB8032AFE562BB (void);
// 0x0000037B System.Collections.Generic.List`1<Lean.Common.LeanSelectable> Lean.Common.LeanSelect::get_Selectables()
extern void LeanSelect_get_Selectables_m0541739B28A2D6DE8DDB291B67F36C3D4BB7255C (void);
// 0x0000037C Lean.Common.LeanSelect/LeanSelectableEvent Lean.Common.LeanSelect::get_OnSelected()
extern void LeanSelect_get_OnSelected_m3C80CDF2BB1C018C1FD67A55D148EDA9E9FFC907 (void);
// 0x0000037D Lean.Common.LeanSelect/LeanSelectableEvent Lean.Common.LeanSelect::get_OnDeselected()
extern void LeanSelect_get_OnDeselected_m533685C0474A82E4171383017FCB2EF3F6FB3EC8 (void);
// 0x0000037E UnityEngine.Events.UnityEvent Lean.Common.LeanSelect::get_OnNothing()
extern void LeanSelect_get_OnNothing_m3A87EBDF988FCC05BAC09023CF229676FD836D88 (void);
// 0x0000037F System.Void Lean.Common.LeanSelect::add_OnAnySelected(System.Action`2<Lean.Common.LeanSelect,Lean.Common.LeanSelectable>)
extern void LeanSelect_add_OnAnySelected_mCBC3335FADE43EE74713529033E1862D9A4FA7CB (void);
// 0x00000380 System.Void Lean.Common.LeanSelect::remove_OnAnySelected(System.Action`2<Lean.Common.LeanSelect,Lean.Common.LeanSelectable>)
extern void LeanSelect_remove_OnAnySelected_mE81C8107A509B588D2844CFB9D9DAE50930405EE (void);
// 0x00000381 System.Void Lean.Common.LeanSelect::add_OnAnyDeselected(System.Action`2<Lean.Common.LeanSelect,Lean.Common.LeanSelectable>)
extern void LeanSelect_add_OnAnyDeselected_m8D59CD367791FED7F05E552087DEAAA38EAE5D9B (void);
// 0x00000382 System.Void Lean.Common.LeanSelect::remove_OnAnyDeselected(System.Action`2<Lean.Common.LeanSelect,Lean.Common.LeanSelectable>)
extern void LeanSelect_remove_OnAnyDeselected_m92803FDD673B11B87F9C54B31A277648D8A08C83 (void);
// 0x00000383 System.Boolean Lean.Common.LeanSelect::IsSelected(Lean.Common.LeanSelectable)
extern void LeanSelect_IsSelected_m882CF8026A3B01FE1F1A5374536481252A04FC16 (void);
// 0x00000384 System.Void Lean.Common.LeanSelect::Select(Lean.Common.LeanSelectable)
extern void LeanSelect_Select_mB2CE2E0E829BA535BBDB80F7BC38054655C5D150 (void);
// 0x00000385 System.Void Lean.Common.LeanSelect::Deselect(Lean.Common.LeanSelectable)
extern void LeanSelect_Deselect_mD29FDB8833FDBCE9CD91833CBE489E52D001C628 (void);
// 0x00000386 System.Boolean Lean.Common.LeanSelect::TrySelect(Lean.Common.LeanSelectable)
extern void LeanSelect_TrySelect_mAA0CABFECC04FE41D594CFF3B1D944E738AE35E9 (void);
// 0x00000387 System.Boolean Lean.Common.LeanSelect::TryReselect(Lean.Common.LeanSelectable)
extern void LeanSelect_TryReselect_m2F88C4B33A2F49E72EAC9FF4321E905D10A06D2D (void);
// 0x00000388 System.Boolean Lean.Common.LeanSelect::TryDeselect(Lean.Common.LeanSelectable)
extern void LeanSelect_TryDeselect_m908988308BE386DB0EE1E3FC7D7C465CDBBEBB5F (void);
// 0x00000389 System.Boolean Lean.Common.LeanSelect::TryDeselect(System.Int32)
extern void LeanSelect_TryDeselect_m1F043BCFE0AF219FE3D252F4F5E63F7BD25E455D (void);
// 0x0000038A System.Void Lean.Common.LeanSelect::DeselectAll()
extern void LeanSelect_DeselectAll_mFE5E913DADC65729C87BE27A69D04658F3785E18 (void);
// 0x0000038B System.Void Lean.Common.LeanSelect::Cull(System.Int32)
extern void LeanSelect_Cull_m74A83CB52DDCA433C22E4F9830F22A8E20A078F2 (void);
// 0x0000038C System.Void Lean.Common.LeanSelect::OnEnable()
extern void LeanSelect_OnEnable_m3C1CA4EABCE9D1BEE4D216FF5421E07A393E46D2 (void);
// 0x0000038D System.Void Lean.Common.LeanSelect::OnDisable()
extern void LeanSelect_OnDisable_m89F60E7601E09C57440F0411CEEE9EDD563D9334 (void);
// 0x0000038E System.Void Lean.Common.LeanSelect::OnDestroy()
extern void LeanSelect_OnDestroy_mAA479854F63C0D809850EC48D625DBCC75CDC04E (void);
// 0x0000038F System.Void Lean.Common.LeanSelect::.ctor()
extern void LeanSelect__ctor_m1D98D8A3CDA79B09257B6715C3D6A127D0FB09B9 (void);
// 0x00000390 System.Void Lean.Common.LeanSelect::.cctor()
extern void LeanSelect__cctor_m16B8DE2064E0D0191A004953151CAFD300A627A4 (void);
// 0x00000391 System.Void Lean.Common.LeanSelect/LeanSelectableEvent::.ctor()
extern void LeanSelectableEvent__ctor_mF85F656B71FB189CE41973B9F90133305E4889DF (void);
// 0x00000392 System.Void Lean.Common.LeanSelectable::set_SelfSelected(System.Boolean)
extern void LeanSelectable_set_SelfSelected_m3B119374E382EB78103F159BFC08C1DDB8220186 (void);
// 0x00000393 System.Boolean Lean.Common.LeanSelectable::get_SelfSelected()
extern void LeanSelectable_get_SelfSelected_m092BB429EE8A9983CEB84B287CAFCE436DE4EF75 (void);
// 0x00000394 Lean.Common.LeanSelectable/LeanSelectEvent Lean.Common.LeanSelectable::get_OnSelected()
extern void LeanSelectable_get_OnSelected_mEE84613146ACD47032B93CB4E8988F526A15988B (void);
// 0x00000395 Lean.Common.LeanSelectable/LeanSelectEvent Lean.Common.LeanSelectable::get_OnDeselected()
extern void LeanSelectable_get_OnDeselected_m055BD9B88E71CAB5E6C4D429CA271308F56EBC29 (void);
// 0x00000396 System.Void Lean.Common.LeanSelectable::add_OnAnyEnabled(System.Action`1<Lean.Common.LeanSelectable>)
extern void LeanSelectable_add_OnAnyEnabled_mE8C99B3F6FEDF172E77B2BDAB39FD1FE08D91BF8 (void);
// 0x00000397 System.Void Lean.Common.LeanSelectable::remove_OnAnyEnabled(System.Action`1<Lean.Common.LeanSelectable>)
extern void LeanSelectable_remove_OnAnyEnabled_mD48231C5388141EEEA0A98289D82D46759507577 (void);
// 0x00000398 System.Void Lean.Common.LeanSelectable::add_OnAnyDisabled(System.Action`1<Lean.Common.LeanSelectable>)
extern void LeanSelectable_add_OnAnyDisabled_m7D9AF4A62DC7189BEA8AD97B3B0E09AEF07224B8 (void);
// 0x00000399 System.Void Lean.Common.LeanSelectable::remove_OnAnyDisabled(System.Action`1<Lean.Common.LeanSelectable>)
extern void LeanSelectable_remove_OnAnyDisabled_m978AF768489C7EE61BC56590D1E359146315D87B (void);
// 0x0000039A System.Void Lean.Common.LeanSelectable::add_OnAnySelected(System.Action`2<Lean.Common.LeanSelect,Lean.Common.LeanSelectable>)
extern void LeanSelectable_add_OnAnySelected_m06916055558D3230F3988C01A8097171E7A39248 (void);
// 0x0000039B System.Void Lean.Common.LeanSelectable::remove_OnAnySelected(System.Action`2<Lean.Common.LeanSelect,Lean.Common.LeanSelectable>)
extern void LeanSelectable_remove_OnAnySelected_m9F438F7C15015AEC827AB0C6BCD00A208FAB71E3 (void);
// 0x0000039C System.Void Lean.Common.LeanSelectable::add_OnAnyDeselected(System.Action`2<Lean.Common.LeanSelect,Lean.Common.LeanSelectable>)
extern void LeanSelectable_add_OnAnyDeselected_mFA9DE7F64AB71F46D2AAA77E58486D57EDB507B2 (void);
// 0x0000039D System.Void Lean.Common.LeanSelectable::remove_OnAnyDeselected(System.Action`2<Lean.Common.LeanSelect,Lean.Common.LeanSelectable>)
extern void LeanSelectable_remove_OnAnyDeselected_m5B2A4C22F2850FDCED2C7A808D26CBE3BDD8C35D (void);
// 0x0000039E System.Int32 Lean.Common.LeanSelectable::get_SelectedCount()
extern void LeanSelectable_get_SelectedCount_m68F30EB52466B6A0AB9F60449D8E9713457B3940 (void);
// 0x0000039F System.Boolean Lean.Common.LeanSelectable::get_IsSelected()
extern void LeanSelectable_get_IsSelected_m55508746D046549000EB2085D5EA859B5ED855CB (void);
// 0x000003A0 System.Int32 Lean.Common.LeanSelectable::get_IsSelectedCount()
extern void LeanSelectable_get_IsSelectedCount_m91D6757A7652F1D8AA7CCC847DD6021915783E77 (void);
// 0x000003A1 System.Void Lean.Common.LeanSelectable::Deselect()
extern void LeanSelectable_Deselect_mCB84D48D0F71C8345F70321595197109042D2A27 (void);
// 0x000003A2 System.Void Lean.Common.LeanSelectable::DeselectAll()
extern void LeanSelectable_DeselectAll_m4D43396ED0D131513830F2A49E7742F009F4BE08 (void);
// 0x000003A3 System.Void Lean.Common.LeanSelectable::InvokeOnSelected(Lean.Common.LeanSelect)
extern void LeanSelectable_InvokeOnSelected_m3C81335F17D41C2F14CEBF38F31B275E59D6FAE9 (void);
// 0x000003A4 System.Void Lean.Common.LeanSelectable::InvokeOnDeslected(Lean.Common.LeanSelect)
extern void LeanSelectable_InvokeOnDeslected_m6B165447EB743395D368821C3E598822DFDEA7DF (void);
// 0x000003A5 System.Void Lean.Common.LeanSelectable::OnEnable()
extern void LeanSelectable_OnEnable_m2BECBBA774A3DA623EDB2C6482D65882947FA6FD (void);
// 0x000003A6 System.Void Lean.Common.LeanSelectable::OnDisable()
extern void LeanSelectable_OnDisable_m59F3853E5EFDA75B2E35DA0E37F18E2C4E736680 (void);
// 0x000003A7 System.Void Lean.Common.LeanSelectable::OnDestroy()
extern void LeanSelectable_OnDestroy_mFE969691889D2B97EC1CE32A980EEFAA9E244CCD (void);
// 0x000003A8 System.Void Lean.Common.LeanSelectable::.ctor()
extern void LeanSelectable__ctor_m64F3EFA449DC2206C65BFD65AFB8A5C0B2B0E738 (void);
// 0x000003A9 System.Void Lean.Common.LeanSelectable::.cctor()
extern void LeanSelectable__cctor_m0DB2F5F487EE2D3579644575756DA358869D5286 (void);
// 0x000003AA System.Void Lean.Common.LeanSelectable/LeanSelectEvent::.ctor()
extern void LeanSelectEvent__ctor_m298B432186C5138B38B93C71D379689F44C0B204 (void);
// 0x000003AB Lean.Common.LeanSelectable Lean.Common.LeanSelectableBehaviour::get_Selectable()
extern void LeanSelectableBehaviour_get_Selectable_mDA4E928E7FE887A93CF29BBC0EDD11483D52B97E (void);
// 0x000003AC System.Void Lean.Common.LeanSelectableBehaviour::Register()
extern void LeanSelectableBehaviour_Register_m59BCA4108A01A335DA517DC30A64C29ADB64DB80 (void);
// 0x000003AD System.Void Lean.Common.LeanSelectableBehaviour::Register(Lean.Common.LeanSelectable)
extern void LeanSelectableBehaviour_Register_mDADF1B6384CBA28AC26D2161AA5831FC1D7B9747 (void);
// 0x000003AE System.Void Lean.Common.LeanSelectableBehaviour::Unregister()
extern void LeanSelectableBehaviour_Unregister_m21AA701DA1065421693A87AA7CD10DB32AD9318C (void);
// 0x000003AF System.Void Lean.Common.LeanSelectableBehaviour::OnEnable()
extern void LeanSelectableBehaviour_OnEnable_m11C1074ED7800B1AE7FF3355ED6F6B564EF91D9D (void);
// 0x000003B0 System.Void Lean.Common.LeanSelectableBehaviour::Start()
extern void LeanSelectableBehaviour_Start_mCC6D17C6D1520EA1C4BAF1C4F3BB0801ECE7402B (void);
// 0x000003B1 System.Void Lean.Common.LeanSelectableBehaviour::OnDisable()
extern void LeanSelectableBehaviour_OnDisable_mFDF5E11937776F863480234CA5DB2103F0CEDCE5 (void);
// 0x000003B2 System.Void Lean.Common.LeanSelectableBehaviour::OnSelected(Lean.Common.LeanSelect)
extern void LeanSelectableBehaviour_OnSelected_m4037909C4E9F254A345795C9351B21884FF52248 (void);
// 0x000003B3 System.Void Lean.Common.LeanSelectableBehaviour::OnDeselected(Lean.Common.LeanSelect)
extern void LeanSelectableBehaviour_OnDeselected_m6D0284D8BDCE6F3596579DB616EB621060A10039 (void);
// 0x000003B4 System.Void Lean.Common.LeanSelectableBehaviour::.ctor()
extern void LeanSelectableBehaviour__ctor_m24001ACBC3695134BE8306864960F29F6A4573B9 (void);
// 0x000003B5 System.Void Lean.Common.LeanSelectableGraphicColor::set_DefaultColor(UnityEngine.Color)
extern void LeanSelectableGraphicColor_set_DefaultColor_m04E4A634432FEA2A06EA9D4A206A923C7235C8A8 (void);
// 0x000003B6 UnityEngine.Color Lean.Common.LeanSelectableGraphicColor::get_DefaultColor()
extern void LeanSelectableGraphicColor_get_DefaultColor_m471842FCD7B965642321981E8443ECE91D3A92B9 (void);
// 0x000003B7 System.Void Lean.Common.LeanSelectableGraphicColor::set_SelectedColor(UnityEngine.Color)
extern void LeanSelectableGraphicColor_set_SelectedColor_mF266529928AD6E74DD5AAC127B8B66B4C853F084 (void);
// 0x000003B8 UnityEngine.Color Lean.Common.LeanSelectableGraphicColor::get_SelectedColor()
extern void LeanSelectableGraphicColor_get_SelectedColor_m1FC1925B3DEBA9C0DDADF948E09E70AF2CDBAE7B (void);
// 0x000003B9 System.Void Lean.Common.LeanSelectableGraphicColor::OnSelected(Lean.Common.LeanSelect)
extern void LeanSelectableGraphicColor_OnSelected_m105017121C7C20B4B3A407FA3E8EC0BD99F2A696 (void);
// 0x000003BA System.Void Lean.Common.LeanSelectableGraphicColor::OnDeselected(Lean.Common.LeanSelect)
extern void LeanSelectableGraphicColor_OnDeselected_mBBEF2C2E5D4B9CF53858B4723E8D466B92379BFF (void);
// 0x000003BB System.Void Lean.Common.LeanSelectableGraphicColor::UpdateColor()
extern void LeanSelectableGraphicColor_UpdateColor_m8C4B31A043489DD4DA3BFC8DAA3CC0F985E3FE41 (void);
// 0x000003BC System.Void Lean.Common.LeanSelectableGraphicColor::.ctor()
extern void LeanSelectableGraphicColor__ctor_m3B890E4A02B1C0E7EC5969CA311D4CC7F03434B1 (void);
// 0x000003BD System.Void Lean.Common.LeanSelectableRendererColor::set_DefaultColor(UnityEngine.Color)
extern void LeanSelectableRendererColor_set_DefaultColor_m1CCCA7BD2A7E89B727F6EBEF14240BFB0553DAB7 (void);
// 0x000003BE UnityEngine.Color Lean.Common.LeanSelectableRendererColor::get_DefaultColor()
extern void LeanSelectableRendererColor_get_DefaultColor_mBBA833F9D97DD57E02CA62F5CB3E06ACCD6292EF (void);
// 0x000003BF System.Void Lean.Common.LeanSelectableRendererColor::set_SelectedColor(UnityEngine.Color)
extern void LeanSelectableRendererColor_set_SelectedColor_mDED68B9DAD22DF8C010784056B6CB8EE59489E0C (void);
// 0x000003C0 UnityEngine.Color Lean.Common.LeanSelectableRendererColor::get_SelectedColor()
extern void LeanSelectableRendererColor_get_SelectedColor_m1789A269D339ADEE19BC55F124E9CA04FF33C81B (void);
// 0x000003C1 System.Void Lean.Common.LeanSelectableRendererColor::OnSelected(Lean.Common.LeanSelect)
extern void LeanSelectableRendererColor_OnSelected_mBDD16192639BDC6D919A7124D386F89C4B9218F5 (void);
// 0x000003C2 System.Void Lean.Common.LeanSelectableRendererColor::OnDeselected(Lean.Common.LeanSelect)
extern void LeanSelectableRendererColor_OnDeselected_mADD2E795E00EA66A0CBA84232DB469B563BB71F4 (void);
// 0x000003C3 System.Void Lean.Common.LeanSelectableRendererColor::Start()
extern void LeanSelectableRendererColor_Start_mB4C768F2029D6F8D4EB2B2424CD86B1D51095818 (void);
// 0x000003C4 System.Void Lean.Common.LeanSelectableRendererColor::UpdateColor()
extern void LeanSelectableRendererColor_UpdateColor_mBB4BFB9173F24C7C758D291DC6AAFA77F91F484C (void);
// 0x000003C5 System.Void Lean.Common.LeanSelectableRendererColor::.ctor()
extern void LeanSelectableRendererColor__ctor_m514753D2E70569C0B05EE8674786B76B4CA8E846 (void);
// 0x000003C6 System.Void Lean.Common.LeanSelectableSpriteRendererColor::set_DefaultColor(UnityEngine.Color)
extern void LeanSelectableSpriteRendererColor_set_DefaultColor_m276192DD1E9B19CD0799180F6BB9247572F4232B (void);
// 0x000003C7 UnityEngine.Color Lean.Common.LeanSelectableSpriteRendererColor::get_DefaultColor()
extern void LeanSelectableSpriteRendererColor_get_DefaultColor_m8D2CBDCC7577986F67D9C04DD9F11D40EF76BD0D (void);
// 0x000003C8 System.Void Lean.Common.LeanSelectableSpriteRendererColor::set_SelectedColor(UnityEngine.Color)
extern void LeanSelectableSpriteRendererColor_set_SelectedColor_mD61014AF5613A92CDE8668CAD70DB68C02A3ADF1 (void);
// 0x000003C9 UnityEngine.Color Lean.Common.LeanSelectableSpriteRendererColor::get_SelectedColor()
extern void LeanSelectableSpriteRendererColor_get_SelectedColor_m49805D8A311F4B734CA86528C2BAE5437452E806 (void);
// 0x000003CA System.Void Lean.Common.LeanSelectableSpriteRendererColor::OnSelected(Lean.Common.LeanSelect)
extern void LeanSelectableSpriteRendererColor_OnSelected_mA0482CB6294A989841715178F2BC601F49C77477 (void);
// 0x000003CB System.Void Lean.Common.LeanSelectableSpriteRendererColor::OnDeselected(Lean.Common.LeanSelect)
extern void LeanSelectableSpriteRendererColor_OnDeselected_m7C8E1081624ABE0358F0CB6B357E7694A431D98A (void);
// 0x000003CC System.Void Lean.Common.LeanSelectableSpriteRendererColor::UpdateColor()
extern void LeanSelectableSpriteRendererColor_UpdateColor_mB959E44683EB3D85A96956DB84A4AEB79A124D2B (void);
// 0x000003CD System.Void Lean.Common.LeanSelectableSpriteRendererColor::.ctor()
extern void LeanSelectableSpriteRendererColor__ctor_mD0A401E22773FF1503E454D73F1E7A2B9E57BC2E (void);
// 0x000003CE System.Void Lean.Common.LeanSpawn::set_Prefab(UnityEngine.Transform)
extern void LeanSpawn_set_Prefab_m9EF621030E54E351A4BA12F6A3F02B3CBA712EDC (void);
// 0x000003CF UnityEngine.Transform Lean.Common.LeanSpawn::get_Prefab()
extern void LeanSpawn_get_Prefab_mB50202E0832F14C4C9A0599EE005812CA96B7424 (void);
// 0x000003D0 System.Void Lean.Common.LeanSpawn::set_DefaultPosition(Lean.Common.LeanSpawn/SourceType)
extern void LeanSpawn_set_DefaultPosition_mC45A0DC850B31A783E27971EE2539D7EC218BD3C (void);
// 0x000003D1 Lean.Common.LeanSpawn/SourceType Lean.Common.LeanSpawn::get_DefaultPosition()
extern void LeanSpawn_get_DefaultPosition_m977F92325E8630DF382313785A9565D373C1E887 (void);
// 0x000003D2 System.Void Lean.Common.LeanSpawn::set_DefaultRotation(Lean.Common.LeanSpawn/SourceType)
extern void LeanSpawn_set_DefaultRotation_m3332F942D2F460104B36D1F039571557661982C1 (void);
// 0x000003D3 Lean.Common.LeanSpawn/SourceType Lean.Common.LeanSpawn::get_DefaultRotation()
extern void LeanSpawn_get_DefaultRotation_m71C99E2B15F8D895EE8055F9E3D87A2B8D76732F (void);
// 0x000003D4 System.Void Lean.Common.LeanSpawn::Spawn()
extern void LeanSpawn_Spawn_m4B2658D62A3E17E4D8EC41B5606010E05445CE16 (void);
// 0x000003D5 System.Void Lean.Common.LeanSpawn::Spawn(UnityEngine.Vector3)
extern void LeanSpawn_Spawn_mB6149967EADA16BBA46AD9B5580384B79ED8E5FD (void);
// 0x000003D6 System.Void Lean.Common.LeanSpawn::.ctor()
extern void LeanSpawn__ctor_mAD61599D9F51F60C2A90BB4EDA5813A6CB7BEF17 (void);
// 0x000003D7 UnityEngine.Vector2 Lean.Common.LeanCommon::Hermite(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern void LeanCommon_Hermite_m933D6F37B112788EA496FEEC7663E6553B0CC3F3 (void);
// 0x000003D8 System.Single Lean.Common.LeanCommon::HermiteInterpolate(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LeanCommon_HermiteInterpolate_m18965FC0DCD5D7A0F11486C2497BCDCD4CC205B2 (void);
// 0x000003D9 System.Void Lean.Common.LeanScreenQuery::.ctor(Lean.Common.LeanScreenQuery/MethodType)
extern void LeanScreenQuery__ctor_m59065C77ACB27AC2072C31D92B4D418EC9C60A85 (void);
// 0x000003DA System.Void Lean.Common.LeanScreenQuery::.ctor(Lean.Common.LeanScreenQuery/MethodType,UnityEngine.LayerMask)
extern void LeanScreenQuery__ctor_m96FE813B432186035779FED836415F1E1E16B23A (void);
// 0x000003DB System.Void Lean.Common.LeanScreenQuery::ChangeLayers(UnityEngine.GameObject,System.Boolean,System.Boolean)
extern void LeanScreenQuery_ChangeLayers_m0FE445BF645A2E5B913D4ABE847CCE70C1F87D25 (void);
// 0x000003DC System.Void Lean.Common.LeanScreenQuery::RevertLayers()
extern void LeanScreenQuery_RevertLayers_mC14E59AC2206E7FA05F7BC48F400DCC67858FA48 (void);
// 0x000003DD T Lean.Common.LeanScreenQuery::Query(UnityEngine.GameObject,UnityEngine.Vector2)
// 0x000003DE System.Boolean Lean.Common.LeanScreenQuery::TryQuery(UnityEngine.GameObject,UnityEngine.Vector2,T&,UnityEngine.Component&,UnityEngine.Vector3&)
// 0x000003DF System.Boolean Lean.Common.LeanScreenQuery::TryResult(UnityEngine.Component,T&,UnityEngine.Component&)
// 0x000003E0 System.Boolean Lean.Common.LeanScreenQuery::TryGetComponent(UnityEngine.Component,T&,UnityEngine.Component&)
// 0x000003E1 System.Boolean Lean.Common.LeanScreenQuery::TryGetComponentInParent(UnityEngine.Component,T&,UnityEngine.Component&)
// 0x000003E2 System.Boolean Lean.Common.LeanScreenQuery::TryGetComponentInChildren(UnityEngine.Component,T&,UnityEngine.Component&)
// 0x000003E3 System.Int32 Lean.Common.LeanScreenQuery::GetClosestRaycastHitsIndex(System.Int32)
extern void LeanScreenQuery_GetClosestRaycastHitsIndex_mCA37AA7178583AABC833765E4D9434EBA5BC91E5 (void);
// 0x000003E4 System.Void Lean.Common.LeanScreenQuery::DoRaycast3D(UnityEngine.Camera,UnityEngine.Vector2,UnityEngine.Component&,System.Single&,UnityEngine.Vector3&)
extern void LeanScreenQuery_DoRaycast3D_m79EEB1F96BF3BB4CC4BCB70E9F1667E4CA7D993E (void);
// 0x000003E5 System.Void Lean.Common.LeanScreenQuery::DoRaycast2D(UnityEngine.Camera,UnityEngine.Vector2,UnityEngine.Component&,System.Single&,UnityEngine.Vector3&)
extern void LeanScreenQuery_DoRaycast2D_m57CB1F6A5464D8D88EB1E5E83DA7BA15BAB854B7 (void);
// 0x000003E6 System.Void Lean.Common.LeanScreenQuery::DoRaycastUI(UnityEngine.Vector2,UnityEngine.Component&,System.Single&,UnityEngine.Vector3&)
extern void LeanScreenQuery_DoRaycastUI_m82C6B1269710BCB6377C4F1D25B4B4351DB89B99 (void);
// 0x000003E7 System.Void Lean.Common.LeanScreenQuery::.cctor()
extern void LeanScreenQuery__cctor_m14B930C96207D6A19AB70FBC4871869A6005184D (void);
static Il2CppMethodPointer s_methodPointers[999] = 
{
	CwCameraLook_set_Listen_mC1BBA7E20874CD9BD5FE0F1589CA2085DE238D5E,
	CwCameraLook_get_Listen_mE3FDB48B49C40A4038A8E106978A1F4C1D2A72C9,
	CwCameraLook_set_Damping_mDDF705DC41131662329B392415E7510F48554035,
	CwCameraLook_get_Damping_mFCF62C9E54A0536D225E69611FA16C0D0D43E454,
	CwCameraLook_set_Sensitivity_m87B9913B5644F9BD4C7738E508836C380444AE30,
	CwCameraLook_get_Sensitivity_mDD890D29B0C916768A7CDD9CED9F0C6E9C3C480A,
	CwCameraLook_set_PitchControls_m1F410A7D89FB841EDD4250E07B21AAD5577D9E33,
	CwCameraLook_get_PitchControls_mD83183E6968F0148122462C5BA2C545E88496549,
	CwCameraLook_set_YawControls_mB2900DB40BF7FE55E795DDD604C610B39FBB7A6F,
	CwCameraLook_get_YawControls_m44B920B2AB4EBBB05620F5E9D203C61A6C04B96F,
	CwCameraLook_set_RollControls_m0CEBCFCC15E11C705959B7035FA210D697BBB88C,
	CwCameraLook_get_RollControls_m79E3EDFA5DB47257C92059CF465A7289E78C28FC,
	CwCameraLook_Start_m576ECA9992440C326783126170534DF47ADD30D0,
	CwCameraLook_OnDisable_mA852D56A6D2FE46494BCDD97597B750C1470BD8A,
	CwCameraLook_Update_m80810E73159D9821B0FFAE04AB0D27E1DBDE6E5C,
	CwCameraLook_OnApplicationFocus_m415DA4C660190E74A6DC3184422BD8901B6757A6,
	CwCameraLook_AddToDelta_m9E8C05A2F7323A549F2BA3DD193AEDBC74CB3C90,
	CwCameraLook_DampenDelta_mC4BA3F86FFCFDDEFAB7004D7A50F94BA9882E3E4,
	CwCameraLook__ctor_m2A93E54BB653B6247A16223CC50FF8392468FE52,
	CwCameraMove_set_Listen_m903FDD6C1D86FDBA9B99B730D17C9195F2437415,
	CwCameraMove_get_Listen_m6D55EB7728EA11FD6F0E8724A945ECC2A9D950CA,
	CwCameraMove_set_Damping_m4D1CAFEDFF5BE70559059DF81CFA9CF880F3D28E,
	CwCameraMove_get_Damping_m14887D5544CBDFA2CA78C2F6BB0265B6A5CC6A8B,
	CwCameraMove_set_Sensitivity_m1BC9F39AA93DDEF5E9183D42A5189E47BC3E1926,
	CwCameraMove_get_Sensitivity_mB9D5AA304B48417D60F6062D66CC4DA089183FDD,
	CwCameraMove_set_HorizontalControls_m58B9E89BB0B801B2748191B006A276CDFCB3D039,
	CwCameraMove_get_HorizontalControls_m515D7FEE87FF29592FF48F517680240F3AF991D8,
	CwCameraMove_set_DepthControls_m8BCC2745FB45C51CC597183B76170B8A31C5B564,
	CwCameraMove_get_DepthControls_m41729125DFF75EEE98B1B594F7D5D5807BC673F5,
	CwCameraMove_set_VerticalControls_mF7D11C783F77F84B6820C6873DD8B97D876C0CE0,
	CwCameraMove_get_VerticalControls_m23A3277FB5E730742DE2AE8AE9E5C1F2882EC225,
	CwCameraMove_Start_mF69AD8DF75E1A6336D939BB38363D088BB8B8C6F,
	CwCameraMove_Update_m86B60F5DAD92C4BA3A5DE9F8A0231E11E3AA7202,
	CwCameraMove_AddToDelta_m8D60AF27B12A2C74566350CBA556B6C397137B73,
	CwCameraMove_DampenDelta_mD59D563A640C83BCE72086892149D515A3E06D04,
	CwCameraMove__ctor_mA70F41956421329C6CD1A73105FC824E7F3A2EA4,
	CwCameraPivot_set_Listen_m951577ED29FF71092813B416788D0C99D00F6FC7,
	CwCameraPivot_get_Listen_m5F41DD714B794FE8DE62E453EAE9E5B7B39F90B2,
	CwCameraPivot_set_Damping_mB41BAC011E0BD205556013953395035B723D01EC,
	CwCameraPivot_get_Damping_m9E26C61DC21D6AB1E2153B3334C8CE2741C0A05C,
	CwCameraPivot_set_PitchControls_mD3EB8163C7F100917469427DC1BD223412AA4334,
	CwCameraPivot_get_PitchControls_mE3C2E599AF479C07E9621157AEEF3E3D939C7F26,
	CwCameraPivot_set_YawControls_m491BF362DFD9A9397AD68DC765B83D4771768A9D,
	CwCameraPivot_get_YawControls_m5DD409936EF34793235A6327C72B3B77E4F36059,
	CwCameraPivot_OnEnable_m5ECC5F5DC95962EF1920B2C40E33034492F7A6BF,
	CwCameraPivot_Update_mE541F48653C0424297CE31736C659D142E4EA775,
	CwCameraPivot_AddToDelta_m739BC0A9A4AE2112EC42819B57FBFA0365018CDA,
	CwCameraPivot_DampenDelta_mFFC631612116DB10B569F81D78F0B7725A149408,
	CwCameraPivot__ctor_mA507128A97E4FB0C5FFC0E90343258A8BAD49C99,
	CwDemo_set_UpgradeInputModule_mCD4BF4DC104DD3F1946016235B7B3499B9F5FCB6,
	CwDemo_get_UpgradeInputModule_mE0D125DF29A2E754A630AB66D550F68582438176,
	CwDemo_set_ChangeExposureInHDRP_m799E8C2E52415429333329D91FDEBC2F509BE61B,
	CwDemo_get_ChangeExposureInHDRP_mECEC683010FE89A4F4A046B44B603C92B3A1C461,
	CwDemo_set_ChangeVisualEnvironmentInHDRP_m81F2AB7861F12806B4DFFC0628030C374B6BF1A7,
	CwDemo_get_ChangeVisualEnvironmentInHDRP_m954AA58BD6D633B23D10D45C8BECDF00D19D0792,
	CwDemo_set_ChangeFogInHDRP_mA24AF10AC0A1F3BB32E78C0276D6D05FB3C48FA5,
	CwDemo_get_ChangeFogInHDRP_mFCCE33588ADB1017DE4F6024C67537E02768F89D,
	CwDemo_set_ChangeCloudsInHDRP_mA86EACC6FA4157D5D305D4356B9C7F947B204665,
	CwDemo_get_ChangeCloudsInHDRP_m2F494C931AE2AEED1504219EBF0DF677D0059251,
	CwDemo_set_ChangeMotionBlurInHDRP_mB5382676DFCF90F838972FDA0A9409A14DDE02F2,
	CwDemo_get_ChangeMotionBlurInHDRP_m435FF86E3C5B858493B6CDD7989E3DFEE47120C0,
	CwDemo_set_UpgradeLightsInHDRP_m9D9D804EADC70ECC06245A446791D0C23FB84A55,
	CwDemo_get_UpgradeLightsInHDRP_m831519CF369A2C8A7621FDC4CC2F5570E9125332,
	CwDemo_set_UpgradeCamerasInHDRP_m411A6C889A336D4E6F2A3C3EC268D278DF0204CF,
	CwDemo_get_UpgradeCamerasInHDRP_mA3F97BD2DC3324FBC7DD5F27EFF61459766B625F,
	CwDemo_OnEnable_mE19EA41ED15A3B40589CB3C10F4CC3AC46AA5A84,
	CwDemo_TryApplyURP_m508D72BFE3D3C85D9113DC577BEA301606B50846,
	CwDemo_TryApplyHDRP_m855878779C3538E6BA577909064B9EF98212A2ED,
	CwDemo_TryCreateVolume_m232B4AE82996AFC09DE3910C1C3A18DEA866BEA1,
	CwDemo_TryUpgradeLights_m59F0B19FDCDFBD27FBD599C2FFDA1DB3AA1B76AA,
	CwDemo_TryUpgradeCameras_mBBD1D346C43C7F34966E4148B8940C902E5438DE,
	CwDemo_TryUpgradeEventSystem_mFCE1585356FB321F95EFA37FF037609A8D312DA9,
	CwDemo__ctor_m7D644911930F18AD984DA95D2958FA6E634B5EEB,
	CwDemoButton_set_Link_m2946A2B3BFD2726F94844CB5CCA55356EE56207D,
	CwDemoButton_get_Link_m5605569B83BA07A681D64989DE8D4A59EB032725,
	CwDemoButton_set_UrlTarget_m4D41139D335E491AB4523032CF1FE2D9E05FD7A8,
	CwDemoButton_get_UrlTarget_m128C0711C6A81D9E2EC0C64797D4C0FE97D1515B,
	CwDemoButton_set_IsolateTarget_m895E67BFA19C442C416AA57F42E75632E398A932,
	CwDemoButton_get_IsolateTarget_m5A6FF0799DA43F737152428109065FDE2E359532,
	CwDemoButton_set_IsolateToggle_m148E5490C10E4EC6C1789A8F71C208DA606503F3,
	CwDemoButton_get_IsolateToggle_mEDCD0687BD72566E69C06D56A96A731CD1B5788C,
	CwDemoButton_OnEnable_m4FA06E2D042CED6D7C7B410DB32D3E467BCB696F,
	CwDemoButton_Update_m1CEAFDE4FFAF5030173D3059FB8944AF0FB9E557,
	CwDemoButton_OnPointerDown_m2C8BB7C3D0632F2AB3495A53A131E39A8731FAF3,
	CwDemoButton_GetCurrentLevel_m6B1981C02570327CF559EE96BED52D477EDC3A13,
	CwDemoButton_GetLevelCount_mD954A66EE852B397FCEF7D3D17489CEFCAE7D0F4,
	CwDemoButton_LoadLevel_m17E1D6096B6A0FD0F20E0E8BFD68BE6A1120710D,
	CwDemoButton__ctor_m3C16B9FBA805B9473EB9DE658866AA8043DC9203,
	CwDemoButtonBuilder_set_ButtonPrefab_mD0CA56B76280E42C6971D25EFBA5C976F9EFC80F,
	CwDemoButtonBuilder_get_ButtonPrefab_m149044BBAD58DD369D7E30CC0F2BE6861907FEF3,
	CwDemoButtonBuilder_set_ButtonRoot_m86A988EC84B6CD768B396C2A9831317680BA14CB,
	CwDemoButtonBuilder_get_ButtonRoot_m24DE9DE100C77C4B5399C992629D1C30DFAD6A33,
	CwDemoButtonBuilder_set_Icon_mDDC6FD5CA57BFEFF9F087B0E733BB0F1710AA2E0,
	CwDemoButtonBuilder_get_Icon_m4FD016A1C402A912DEB7165E0757CB73F6EDA730,
	CwDemoButtonBuilder_set_Color_mE021C273082C8CCDDACE1BE1563D81A5DCB57163,
	CwDemoButtonBuilder_get_Color_m3530FC7BFAAD65B88A8EED0A63628703B76753C3,
	CwDemoButtonBuilder_set_OverrideName_m072379882C1AAD5C4187F46DC0C0DF51F7343C85,
	CwDemoButtonBuilder_get_OverrideName_m6164FBCB420515E8CED90DC9A1B14B615625D689,
	CwDemoButtonBuilder_Build_mE80EF53367B803C737DC04229F08820846CB545A,
	CwDemoButtonBuilder_BuildAll_m906D88BE6D462471B55E13E977A28BCBED6F1C70,
	CwDemoButtonBuilder_DoInstantiate_m927A2254854EE0F11C17DCCEB9080F8EC2BA9058,
	CwDemoButtonBuilder__ctor_m755B6D89BD90B95FFDCFBD7B4605A8688B790705,
	CwDepthTextureMode_set_DepthMode_mF94166F9071CA4FAD713BCF0AF051CC87F6D8E52,
	CwDepthTextureMode_get_DepthMode_m314F383CDD920AA5E8434BEE124F05C0B97F645D,
	CwDepthTextureMode_UpdateDepthMode_mF968A493F9B8C2F1F3F7D6882E2D62D8CFC4F466,
	CwDepthTextureMode_Update_m5CFBB0B75A874479B9BE84B28AFD2BA0CD6EDF63,
	CwDepthTextureMode__ctor_m03144DC5A17F36D0D4FC7D22ED1C610ED9420CFC,
	CwFollow_set_Follow_m131E406CB66E12DCCA69DCC48B46AE70248A9B18,
	CwFollow_get_Follow_mBE1BFDF2AA2D1F43ADAB3EFFE604C3AFDE220613,
	CwFollow_set_Target_mAF4C7A7B35B1147753F71B03C966450C299FE956,
	CwFollow_get_Target_m57FC7FCC49B44B31AA42DDD6D0DB9D2214892A79,
	CwFollow_set_Damping_mF04969161CAA63F86B692615F5C4FCCB9FCF5F44,
	CwFollow_get_Damping_m725BEAF6F7B78B84DEBC673944B013E6D201AD9E,
	CwFollow_set_Rotate_m54ADA3391F2A04567302746271BC663C49FDA8CF,
	CwFollow_get_Rotate_m3AAC51675C25B48C64EF4821407698C8AE11437F,
	CwFollow_set_IgnoreZ_m6644602D6BD90DCBBB27E1F10AC5C5AE2E40E6BA,
	CwFollow_get_IgnoreZ_mADF919CDA9809F4F67F10FA09F40A69F05BD2F6A,
	CwFollow_set_FollowIn_mCC594F560D1106263277CAADCA5E35F372F0BB87,
	CwFollow_get_FollowIn_m15C13356591479A4B80D7CA766704D1A4DC0AAFC,
	CwFollow_set_LocalPosition_m6AB642760BC0C9E6C8F53BFEA6CB115F1B4B1D6C,
	CwFollow_get_LocalPosition_m6617050F3EE47D15758145904FFE4E464A7A9440,
	CwFollow_set_LocalRotation_mF40F00E2AA19055DEDC8477E0CB29CA293F3CD2D,
	CwFollow_get_LocalRotation_mFA627538389A94C217F64A92317E51A6D42507A0,
	CwFollow_UpdatePosition_m764A0AFB2F660DA5475A8409210CF2A66720DCBE,
	CwFollow_Update_m21BE56577AE1FD3163819DDCA46B24ABC6EA2069,
	CwFollow_LateUpdate_m948E2F2544ACAA592B83C64638C00A38AFBAD794,
	CwFollow__ctor_m62AEF6F8FDA89411887601E696B66AEAC376243D,
	CwInputManager_set_GuiLayers_m707FE9D2324C404D7E6A432250EF666A95998911,
	CwInputManager_get_GuiLayers_m0B99E18B5EC564A21CCEC5A5EDD695420A417BD7,
	CwInputManager_add_OnFingerDown_m3EA0E58BE1484FDCA4F4FE4A510ACA14BB28CE88,
	CwInputManager_remove_OnFingerDown_m754CF9134F0E259CE5027A2299FF6234FD8692F3,
	CwInputManager_add_OnFingerUpdate_m36EC36FC2FBC4034BF54F9FA5F613811AFCCFB6B,
	CwInputManager_remove_OnFingerUpdate_m03583ED8A4E8E7F3041EC9CA460D34EA59F1FAE7,
	CwInputManager_add_OnFingerUp_m3C1A63516635C4EF3DE1F7C3EEAE0BE9B90B2328,
	CwInputManager_remove_OnFingerUp_m4DEF46E69DC0C2A60A82DAD61226FC203B86C359,
	CwInputManager_get_Fingers_mFFB1F28BF2C601835D996D8B8E1AF61F564622F7,
	CwInputManager_get_ScaleFactor_mD810121BD10ECB7D8EB89E853397A955E0163C5D,
	CwInputManager_GetFingers_m9A64C51234060E81FA102986278731E7CEE31E41,
	CwInputManager_PointOverGui_m31E70D11AB7AFEECE257D0F8310AFDF1E382F16E,
	CwInputManager_RaycastGui_m8EB79B63DE684FD2C992325599153C546030E3B8,
	CwInputManager_GetAveragePosition_m6E11B35DD0C1B684C8B46982631EBE490FCB5A70,
	CwInputManager_GetAverageOldPosition_mBF1B884E3F8746DD24DD9F426C0470F22B76BD3F,
	CwInputManager_GetAveragePullScaled_mD3B05732EBA151293D0B328EF4F243B42AA2EFB5,
	CwInputManager_GetAverageDeltaScaled_m24FE461D9CD4DC6AC1BE47FC591F8136DA7D436A,
	CwInputManager_GetAverageTwistRadians_mEA6B38920BAFE06D4477E48348D76FA0125375FB,
	CwInputManager_EnsureThisComponentExists_mC56FE8390345D5F5D437527CC18908CCB52545A8,
	CwInputManager_Update_mF2A2A913AB135FDDA61812444EE1588798F3143D,
	CwInputManager_FindFinger_m57341E7E2E6CD4B02B2BC6FE6E8CD3A75D07A941,
	CwInputManager_AddFinger_m59214229CE1C09FF29119CF6D42DFDE28D597564,
	CwInputManager_Hermite_mB6D140B836FB50E3F6F7BAA322E759F125E43DFC,
	CwInputManager_HermiteInterpolate_m81D039FA7B3F17C65C54CEE77D1325B1CC797102,
	CwInputManager_GetRadians_m4ED5446A8B6B56364CA190D47C66333F9050CA20,
	CwInputManager_GetDeltaRadians_m40D882C62542609F2BC30D86C98A09D88D01BCB5,
	CwInputManager__ctor_mD5FC249B27ADB9F67496DF7563FCB89B0EC4B075,
	CwInputManager__cctor_mE35336C7A63231E331EAEFC65FD96523C38FC197,
	Axis__ctor_m37815A1FAA240248DCE733A7077C418505123BA1,
	Axis_GetValue_m4A7707296DE15BA3B328FC217F4885769C2F6F72,
	Trigger__ctor_m92E19461A441EB3009409F51BE4B904019FE1A4D,
	Trigger_WentDown_mECBF1F265D4863FBB00BA61D66AC3E2085501AC4,
	Trigger_IsDown_mDFDD741E21AEAA27CC3D0B9E259157BE5E9F1660,
	Trigger_WentUp_m20E4670475591BEF55514EB2B956EFCB09D00B97,
	NULL,
	NULL,
	NULL,
	NULL,
	Link_Clear_m6F28422C0323FD9A10335FB838EF4244F40F2382,
	Link__ctor_mEECED0CCEC9F7C65EF525B51424A4064B590B308,
	Finger_get_SmoothScreenPositionDelta_mD2675DDF9AE1DD0F9FD3888A6A6309E7D5D7EFA7,
	Finger_GetSmoothScreenPosition_mBFF91729FBD3EF4BD121C330635FBF7086541158,
	Finger__ctor_m6DBEC0B913A2E9B47E410B69E92E495A1CA44D3F,
	CwLightIntensity_set_Multiplier_mD2D814D5ED8571072EAF13EF0ACCC119EE30A598,
	CwLightIntensity_get_Multiplier_m4FA0EE040FCF06CC53D44278EDEBE286EBB6C664,
	CwLightIntensity_set_IntensityInStandard_m27E32EFBA2148DBFDF943D53AB7FFDA7B53B146F,
	CwLightIntensity_get_IntensityInStandard_m989847BF9BF038342801253914CDE559D9CC6475,
	CwLightIntensity_set_IntensityInURP_m87093F12638485B06DFE23E6A620701A6048D49B,
	CwLightIntensity_get_IntensityInURP_mD6D034E53B830FD3AF1E0B0B29D856AB282F878E,
	CwLightIntensity_set_IntensityInHDRP_m4791600FC15F37AF4158AD7324E1633114B82900,
	CwLightIntensity_get_IntensityInHDRP_m5F77F58053C1739C04CA6115FE69A60EFAFF645D,
	CwLightIntensity_get_CachedLight_mC6DEB31BBBF5D7E491CD8D09D0590098F984087F,
	CwLightIntensity_Update_mA759C95FD64312206ABA499299C8DF129DE953C0,
	CwLightIntensity_ApplyIntensity_mA5D02B60BB0BD145FF2CB961919EF3C5074C3D5C,
	CwLightIntensity__ctor_m918ABEBCADD14C04A640A6AE7EC2A2D894A850A4,
	CwRotate_set_AngularVelocity_m9FEE98AD09B0B293AEB0961C91450AB676052873,
	CwRotate_get_AngularVelocity_m1E6D941762A36F8B78054B28FAE6F80C6CB31707,
	CwRotate_set_RelativeTo_mE0C207E2C57F5BEF0E857E05754C0E23623A01C5,
	CwRotate_get_RelativeTo_m18562DCEF5B1D8757C691F3438D68B07095BACDC,
	CwRotate_Update_m5A5ECFDF1C0A50150E6C14B124196A648FC713F6,
	CwRotate__ctor_mC6A323F5C4EC72284B04CD3A67BE7B3123C407CA,
	CwChild_DestroyGameObjectIfInvalid_m8A510C8ECEFC52BF27605086CC8F9663D7507837,
	NULL,
	CwChild_Start_mF17D6AD3F0426EDF35DBF794BE70D80156149DA6,
	CwChild__ctor_mC1E1D68B6834C70229A23C5CB23A7C0CB5015F72,
	NULL,
	CwRoot_get_Exists_mE200EC75F39E43304E8932CBEDE8D60194E0733B,
	CwRoot_get_Root_m9F6C33242A34FFA722ECBFCA0D9D58E02810F61D,
	CwRoot_GetRoot_mBABDEB8BEEB73A77B6A1752645BEEB3CAD89546D,
	CwRoot_OnEnable_m07DDCEE0D33C53E74438C43CD6B86CBF06C0559B,
	CwRoot_OnDisable_mA6871C4D1AF2514F8220E01281F4D1C6025CDDB5,
	CwRoot__ctor_mD2282760B93A28D534CE56B5056346BE0B649160,
	CwRoot__cctor_m668B0EB53195C0C262BB7D874B3363B0CE4B6462,
	CwSeedAttribute__ctor_mC268592985DDE3C2F00743F5BD2EA96E1C568EE1,
	CwGuide_get_Icon_m8273AD7216BA58E2CDBC3563062F96BAD1C03E9A,
	CwGuide_get_Version_m21B6C492AD2D08C87E6456A02C229729427E24CF,
	CwGuide__ctor_mFE5CC196D2B438ED00245E09BE5B125978FDD2E4,
	CwHelper_add_OnCameraPreRender_mA71817A4DE07F851985767A763D44934C23CCB70,
	CwHelper_remove_OnCameraPreRender_m5B6782485E84F75D52C760238DB666A87A3E897D,
	CwHelper_add_OnCameraPostRender_m253AEBDB4E750D08A5AE266E2E8BBF29AFB69E16,
	CwHelper_remove_OnCameraPostRender_mB1F849F01991C96C5ECD3B9613A77D2C677CE020,
	CwHelper__cctor_mC9E46747B9FADACD1BF59B6079C1EE7202EC77B5,
	NULL,
	NULL,
	CwHelper_IndexInMask_m6AD0F2F54247FC72FD8719E0E2F0C7ACC0C04513,
	CwHelper_GetCamera_mA54953B5D77B23342A8AEFEA9992D6CFC62CB70B,
	CwHelper_GetObserverPosition_m1BF55450B7A4C8AB92D278D6E3F756F7360B0292,
	CwHelper_Enabled_mD9E9A832413D64688E133F17F26B9F071F1C5272,
	CwHelper_BeginSeed_mD07B4318C4C5DE8A49B87214CB8D075FFA603747,
	CwHelper_BeginSeed_m280BA8251FD9968A0623203B280A3585CA94632F,
	CwHelper_EndSeed_mEAB4547C35D2940FDEAE7281DA7E1132B25C8458,
	CwHelper_Brighten_m4FA09EF53EB1E850ACA115610A3D6F2CE2117968,
	CwHelper_Premultiply_m723B04EFE539FFC41FA2C75FFDA2BDE841E4EE2A,
	CwHelper_Saturate_mE9B25F9973EE7D6D2957FE13E0FE747D77AA5683,
	CwHelper_Saturate_m46B0E1EA2DF3134E7C516B3D80B56F5192A94B14,
	NULL,
	CwHelper_Sharpness_m8CFE49EC8471FA1A712F87698828D0A0B7A112D5,
	CwHelper_ToLinear_mF1A4AF09C33C756895F91F2F1067CA83D898307A,
	CwHelper_ToLinear_m7CD9DDF5FC331BA4D851747F4E8ADED21D2212A9,
	CwHelper_ToGamma_mAD70FDB542E9F3C45C5CF4835666E1F243841E0B,
	CwHelper_ToGamma_m1756D84A141E9BF6864EFC7989725471EF5823A5,
	CwHelper_UniformScale_mE08F2FAC586FBBED2E410D00425CB0DE3F8BD34C,
	CwHelper_BeginActive_m9DB5B6546F1FC3A21FE1EF9DFF62B65D02D48FFE,
	CwHelper_EndActive_m8B7A2D408695795E608A2800452778C5A22A2F22,
	CwHelper_SetTempMaterial_mA51CDD1D40A995FB3232B1BC70ACADD94FE97C1C,
	CwHelper_SetTempMaterial_m0AC8813D49DB2EAACD61EA12A3F9123883602EFD,
	CwHelper_SetTempMaterial_mF58D1C31CF943C3BDB366DCCA1044BCB31F9171C,
	CwHelper_SetTempMaterial_mE376B371C53B74589A348764E2FC938A5082080D,
	CwHelper_AddMaterial_m7161BE23825C75F941AA1DD331F36A8CF06E9EB6,
	CwHelper_ReplaceMaterial_m7D04ED4FFEAC0638630DEEDAD368F9EE9572D169,
	CwHelper_RemoveMaterial_m3FBFE62B5ACB04A8D2B6D471F0D3E125B8654CEF,
	CwHelper_CreateTempTexture2D_mA4EEDBB5545C1BC99434B695B246BA5F47A0B463,
	CwHelper_CreateTempMaterial_mDA57806775964417C4F7669C76A87BB086DB8911,
	CwHelper_CreateTempMaterial_m3BE49286AF2A1756DA1E88182E768033BC875ED7,
	CwHelper_CreateTempMaterial_mB70E91372CD632E54E9E1777C2B21D3542A65735,
	NULL,
	CwHelper_CreateGameObject_mB2DAA57CAA80BE7B7A22166F67E38F4803ABC48D,
	CwHelper_CreateGameObject_m2B0CEF4B06C0F558D687D0FAB5CA4AC84235524E,
	NULL,
	CwHelper_Reciprocal_m82F674E6EFDB2D64F4F74C2FC5FFC2C77963D92B,
	CwHelper_Reciprocal_m8231C683D7BC1F1D1A97262D60A347BC42F27CE6,
	CwHelper_Divide_m2D997A53F17738FDB12893B3F9804E466A6B5673,
	CwHelper_Divide_mC837891D4A53345A9ED41E45023E212BF4389F3D,
	CwHelper_Acos_m9392C09779ADEB4D1A4B05A3D6635AD563E22BA2,
	CwHelper_Acos_mA257560C3BD12D2A3363BF1297CBBBAF34AAB78C,
	CwHelper_DampenFactor_m730A8463F0CBE628CD66EF69E1B5969091015C9A,
	CwHelper_DampenFactor_m97324224BCC9E377CDA9B55633BD5E12D434163E,
	CwHelper_Atan2_mC5049475C136DEE52806C5A13112192F44532F85,
	CwHelper_Mod_m51FD78C817A0A79005F4E9183BD0D29CE8E6DD40,
	CwHelper_Mod_m903A853695B88CCFE54AABC1154D996337921E42,
	CwHelper_GetReadableCopy_m941B305D51AD867C81B27C71BFE9422256B2F783,
	U3CU3Ec__cctor_m8E4201ECEA771B6CF4729D98E0D459449CFB180D,
	U3CU3Ec__ctor_m855D461BEACFD501B20F20FD2ADDEFBFCD6AD109,
	U3CU3Ec_U3C_cctorU3Eb__11_0_mF3F7FEC2F35F9B5C341ECC220CCE9A2F55DDD60E,
	U3CU3Ec_U3C_cctorU3Eb__11_1_m18A55FBCD2BE2FF0A5779F15B2081C331AAC7E83,
	U3CU3Ec_U3C_cctorU3Eb__11_2_m691D280AD42BC6381246DE74F73C8DBEE05740BC,
	U3CU3Ec_U3C_cctorU3Eb__11_3_mB36C13FABAB7212FD1A5FB83BC1441EA672DD3EE,
	CwInput_GetTouchCount_m72863DC75CD03D6184275414A5367C45DEE322F0,
	CwInput_GetTouch_m73F989D9873E8DE49CB86AA36E52BC6720F63F2D,
	CwInput_GetMousePosition_m48A314D721594181D0590686FAD66B07D5FC2E43,
	CwInput_GetKeyWentDown_m7E0BA8DE675E2AE9E71E230C4125BEEE891595C4,
	CwInput_GetKeyIsHeld_mAF8284015932D27D4191F0397291253BC0DABC90,
	CwInput_GetKeyWentUp_m0C90BB3501CC472EAAE90090E641B6AD6AB6005A,
	CwInput_GetMouseWentDown_m886FA03DDB83144B6FCD536D1A762131AFA6019E,
	CwInput_GetMouseIsHeld_mFDF9659A6339FB9D3942D0F4F476FA5D115C96CD,
	CwInput_GetMouseWentUp_m94BAAF290B7C825095434239FDCD564835C0E12E,
	CwInput_GetMouseWheelDelta_m4D7C549927F10503215B3F42E49592D053FAA416,
	CwInput_GetMouseExists_m0E1B17EFC8192A5DB5B9940B3A599677003F6F3B,
	CwInput_GetKeyboardExists_m6148767704F07B060110EC89778B51F76594468D,
	CwShaderBundle_set_Title_mABA92B347C8C4F1888C5493478837423F98F91A0,
	CwShaderBundle_get_Title_m1200553367E9BD27188F14A93E532C09E4577F4D,
	CwShaderBundle_set_Target_m22F9DDDF0D46C037123C0E79F41C2F865FD8ABE1,
	CwShaderBundle_get_Target_mC81E9DB558BA9A40B2A758C212918CC5C535C0EB,
	CwShaderBundle_set_VariantHash_mD6BDAB128090BED60003720A88AA9704923E186D,
	CwShaderBundle_get_VariantHash_m044DE14165CFA947B205E2541EA1D291544543FE,
	CwShaderBundle_set_ProjectHash_mC7165DF126A72C84BB0CB92DC7C67FB0CE25035E,
	CwShaderBundle_get_ProjectHash_m96EA1F0B807273E5425FF0EB38D2B9F8C786CA24,
	CwShaderBundle_get_Variants_mB7AE5630BD2F186688C4E8720A13C79C3C16826C,
	CwShaderBundle_get_Dirty_m1E0BCCAC647BC9EDB0BF48B2B7A5364F856E3EF3,
	CwShaderBundle_GetProjectHash_m7E6BE07EF0115BCA08367C01ADF98F48EAA96BEC,
	CwShaderBundle_DetectProjectPipeline_m9B7C42FE7202960821B1F188F4A985F9490949AE,
	CwShaderBundle_IsStandard_m73CF4D6CAB6AEA3948A1D18A87AB87F378E64F5E,
	CwShaderBundle_IsScriptable_m228C7267122FAD870545FC65D2816F32EB43AD3B,
	CwShaderBundle_IsURP_m71C4A1038F86CF04DA66435333C449E81BDAC91B,
	CwShaderBundle_IsHDRP_mB5943E13CCBE3A12887B7AAB012BFF16D9DC79E2,
	CwShaderBundle__ctor_m4D1E6C1F0EA9985613823C236000145BE29D5142,
	ShaderVariant_get_HashString_m05E9900D728E32F245ACEEEEBDB079B4419BF0DF,
	ShaderVariant__ctor_m62A30B3B051D197CA6DE73D21BC85D8121DA284B,
	LeanPulseScale_set_BaseScale_m4B768B064BAB62AD08F305A195642E05D6B22391,
	LeanPulseScale_get_BaseScale_m27D6D46137B94F4081AA89ACD12DF253183D2FC1,
	LeanPulseScale_set_Size_mAE3D13FA565AFC5666F25552620765675F2DCA34,
	LeanPulseScale_get_Size_m5ED588012827D464CD4EEDA85FD4F8C84E22C0D6,
	LeanPulseScale_set_PulseInterval_m2C5C7519A1DC36DA76F26903B5DB0BCAA015D699,
	LeanPulseScale_get_PulseInterval_m492C9D07F01F1DCF480A182DCA5A6EDCBD45DC10,
	LeanPulseScale_set_PulseSize_m9DEECF0CBA5F1C2CE7D20ED04E4D471576F7B16C,
	LeanPulseScale_get_PulseSize_m139062A4C0B457F07C3E67D2F2E5C3ACFB7A3DCB,
	LeanPulseScale_set_Damping_mE405B5B718CDD62AA7924D0A353D4CC7691818DE,
	LeanPulseScale_get_Damping_m6ECAFE5CFBB901B79AF6218B9A3E37E7830EBE93,
	LeanPulseScale_Update_m2BC8B83015F15A629FCD11BCD047F5FAAC1E4CB6,
	LeanPulseScale__ctor_m8F1705BC190A5B9117B95C4C2D648C3D0DF48DA1,
	LeanTouchEvents_OnEnable_mC414AB54B7A99A7E68309090C6F029D1CFAF169F,
	LeanTouchEvents_OnDisable_mE1F3C9C7F96517CF14F32FEDD36532F36FBEF20F,
	LeanTouchEvents_HandleFingerDown_mAE022048D4128DCE5A3211D9F39125A9C951C729,
	LeanTouchEvents_HandleFingerUpdate_m17D46206BEF5488ED68CD9050C1503B00530C33E,
	LeanTouchEvents_HandleFingerUp_m6E5D7DDCB5A95E134D4B42125A0EAB19E7CF1FC6,
	LeanTouchEvents_HandleFingerTap_mE944D4026F95BDD6D11EAC2AD65C9AF51736745B,
	LeanTouchEvents_HandleFingerSwipe_mE0F4D3AD8DA99C3C853FFDC523830A70A39EF166,
	LeanTouchEvents_HandleGesture_mA895A22DA9065F52E833971692CF505577867E7F,
	LeanTouchEvents__ctor_m7E262CCD26482369D4776CA6D1240B8E2305EE33,
	LeanDragCamera_set_Sensitivity_m1C81D4182C940F9E8DB3C68916588153D3F886F0,
	LeanDragCamera_get_Sensitivity_m66FAA935BBA3AD970CDBAA16B7FFF89CBD997978,
	LeanDragCamera_set_Damping_m8604E9F60D1F8C64E28975992EAFC012AB1858A8,
	LeanDragCamera_get_Damping_m77C362A45C19B100348BE4D8F1B1DA489FC4D490,
	LeanDragCamera_set_Inertia_m4097B290AD3CA6E67D594143F7C45B7B5BC855D0,
	LeanDragCamera_get_Inertia_mE8809A2F82FDA976BF1AA4B6FBBECDE051059F8E,
	LeanDragCamera_MoveToSelection_m05ECCF1E496F57DC13BACE4B9A0C1361905704A7,
	LeanDragCamera_AddFinger_m55F183983349EE625FAB24DC31640C8AE7E47D04,
	LeanDragCamera_RemoveFinger_mAEDA725C89F6E51495BD7B613340B674DD9384CF,
	LeanDragCamera_RemoveAllFingers_m861FF6CDD3A9264A534402BD3298F71EA9CECF00,
	LeanDragCamera_Awake_m451DE0D445CAA3BEFE2AA73EC014BF84803B2DE5,
	LeanDragCamera_LateUpdate_m6E6C00ED3DFC57DC5CA56867D8E1E1747DA5DBE0,
	LeanDragCamera__ctor_mAF25EB44908D467FE8D8A4275E24E88DD305C4D9,
	LeanDragTrail_set_Prefab_m57B4216117AA799A0A5D652E24818925989B2256,
	LeanDragTrail_get_Prefab_mF3EA0412E30A49053BF93A1C924A0BEA524CACDD,
	LeanDragTrail_set_MaxTrails_m250A8263465A844AD221DF25E48BC2B0A13932BF,
	LeanDragTrail_get_MaxTrails_mD26AC87BD9A93B70F795E5F26E85DD52C0BE7668,
	LeanDragTrail_set_FadeTime_m4D80442C83183E97BB03563426A8AA83E1E30BE2,
	LeanDragTrail_get_FadeTime_m60CA250833D19D827883195D1EC6CAE07F67C0A1,
	LeanDragTrail_set_StartColor_m06B53E1CC698613C7E9B973CEE226CAA287861FE,
	LeanDragTrail_get_StartColor_mEF04D976AAFE97AA4587C40A439BD7EE32EC10BF,
	LeanDragTrail_set_EndColor_mB4F9B16C99AE9E2584DBB4AE946B65F2039BABF7,
	LeanDragTrail_get_EndColor_mFF28EB8193B208911CFD6FDEB77592128B02D4A0,
	LeanDragTrail_AddFinger_mDE236F2FB6F9878A16FC99DFB67056C29202494C,
	LeanDragTrail_RemoveFinger_m56853CAC438C26D7FADE3EE31E3C04E86E583638,
	LeanDragTrail_RemoveAllFingers_m29AA096F3F7A8E79A564F6ED942AE72D99AD17C5,
	LeanDragTrail_Awake_m721E49DF8B683ABF90B6D8D752F67B091216BBBC,
	LeanDragTrail_OnEnable_m3E9CBD4EDED41BE7763E6A4A592A3194BB38D9CC,
	LeanDragTrail_OnDisable_mF28073A9679EE770A917FAD8D47D52D8727C12DC,
	LeanDragTrail_Update_m51726097926466FD817E240056463A129A1A1EE9,
	LeanDragTrail_UpdateLine_mBF446EBAF0B9B567293D2BE814185A1A2CC173B2,
	LeanDragTrail_HandleFingerUp_m6267BADCA9509726D122A079C7F5FF610F32CDD1,
	LeanDragTrail__ctor_mEBF2377F038164F28196A85BA034D9813411C2E1,
	FingerData__ctor_m7450073E6148306E9748D344D40FA8154D198E89,
	LeanDragTranslate_set_Camera_m57BD987556651DB0FA1E26BC398A4D9F14102424,
	LeanDragTranslate_get_Camera_mD8943DF5C386021E8D4BA56F08CA27B63C62630A,
	LeanDragTranslate_set_Sensitivity_mEA76EC88BBFF3D30984233922EE7FCFF9B1EA395,
	LeanDragTranslate_get_Sensitivity_m4DDFEBDE698AE5501C4EA5393DCCD345EC7BA30A,
	LeanDragTranslate_set_Damping_m6DA53C16705527F1A629B734824CEBF06381E7D6,
	LeanDragTranslate_get_Damping_m57A4DB93B6A308A4180974F96265D8E4C57CA042,
	LeanDragTranslate_set_Inertia_mE7BFB371ECE90D225C3BB8F332797709E66BE1C3,
	LeanDragTranslate_get_Inertia_m8979F156FD97D2235D545679DE8ADB48B9FC4C03,
	LeanDragTranslate_AddFinger_mF898588B94440EEC0AEA5A58B7E47F840A37574C,
	LeanDragTranslate_RemoveFinger_m7183F45CF5D81903A756AFAC6BCD4D69ECBC2E67,
	LeanDragTranslate_RemoveAllFingers_mCDC54370FA3659FAFBBFA96F9494F220D617AB35,
	LeanDragTranslate_Awake_m9F426040EC37B9BBE800A8BAFCCF10A6C525656D,
	LeanDragTranslate_Update_m04A86B2C707C9498AE80B958E5630CE0A145B90A,
	LeanDragTranslate_TranslateUI_m290024974B434075C67EF4BFA72976BF0E36C456,
	LeanDragTranslate_Translate_m9035D8268916D959CC219ED59069949E2566E220,
	LeanDragTranslate__ctor_m9FBC5CBA225990503BDBDA962B53D0C31DE8EA5A,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	LeanFingerData__ctor_m50A8752F194A483144775982725072F53EB585A9,
	LeanFingerDown_set_IgnoreStartedOverGui_m7CFC0C1A9630AB821056E10C8A70AC17A2CAE924,
	LeanFingerDown_get_IgnoreStartedOverGui_m6E3AD1D3FA285747AF3E055D06252426A0B5C89B,
	LeanFingerDown_set_RequiredButtons_m2DE3094CD987297C98C9923EFA1580D664D487DC,
	LeanFingerDown_get_RequiredButtons_m970A438AA531DDE093A9C313DED1E33989412E9D,
	LeanFingerDown_set_RequiredSelectable_m08078C78B237003E222906304448B0CB5A3D68DB,
	LeanFingerDown_get_RequiredSelectable_m46A924E417DD143E4698881190A9DC2210231CA7,
	LeanFingerDown_get_OnFinger_m2B537EF0D841E3A20E7943301110C0323BC70366,
	LeanFingerDown_get_OnWorld_m6791C41B8F97FF73AC8C5CE3456AD0AD9FC3065E,
	LeanFingerDown_get_OnScreen_mD9754163AB3E5DC296BC390C3AC3F4128C01F36B,
	LeanFingerDown_Awake_mB7BD1153A5A5161E382988EB276B65E1AA51CD72,
	LeanFingerDown_OnEnable_mC6A4F9D327CB873D79C7A58B59EE94561679725C,
	LeanFingerDown_OnDisable_mA7FE3171275279350B08420017ACC83FA49AAE2A,
	LeanFingerDown_UseFinger_mB64DF637128A6A7B124D89ECA8538DE9D9C29AB4,
	LeanFingerDown_InvokeFinger_mB7672DF59C09525D07C3BE8E8DE7E1196FE79FCB,
	LeanFingerDown_HandleFingerDown_mF6BC0EA194BDFD9FECF39FA2342779EFEA29E46B,
	LeanFingerDown_RequiredButtonPressed_mFB97FAA2925C17ED2DD0EE9DE58C86069C902D85,
	LeanFingerDown__ctor_m5F625A3552A4FD79F0C7B49912EF809A98B0B479,
	LeanFingerEvent__ctor_m5A19379784332B46F874D4B9AE9929C9BFA9F6D9,
	Vector3Event__ctor_m2065324DD97EAC86B7FFC927570F589E8F1B75C5,
	Vector2Event__ctor_mDD272750EEB9A8CC6E2DB693ABCEFB3DBC36688F,
	LeanFingerFilter__ctor_mCC3A54FC24D242DBB1B114241DD9E015EEAC5050,
	LeanFingerFilter__ctor_m4B9A9ED2D804D825B39EA2069B4BCB9F62B8F92B,
	LeanFingerFilter_UpdateRequiredSelectable_m8ED738073CC8F1BA59DC9CC3109725AA700ACD39,
	LeanFingerFilter_AddFinger_m05398926AC1626D0B41B2490D7E80F47843D74EF,
	LeanFingerFilter_RemoveFinger_mB8924B3B08004F37FA5EA002266832056DA5265A,
	LeanFingerFilter_RemoveAllFingers_m0BEF38CD9EFC6C2FC689D1CA5680619974164C6F,
	LeanFingerFilter_UpdateAndGetFingers_mD81CA832EC55AF7692514C70AC4A02F71B3C8573,
	LeanFingerFilter_SimulatedFingersExist_mD392B4EBC14B8F8AC5904593AED8CAC67A190366,
	LeanFingerOld_set_IgnoreStartedOverGui_m7A63071DD65630C0B4734541F0A68A090C1EEE0C,
	LeanFingerOld_get_IgnoreStartedOverGui_m8DE7FF041FB4CC3618D7F6758F419B65BD0A7D0C,
	LeanFingerOld_set_IgnoreIsOverGui_m9D9B680147D189B60ADE3B56891E2EDEACB5E2FB,
	LeanFingerOld_get_IgnoreIsOverGui_m04FAF6B4BBDDC03A55BAFD7DEFC229AA044EA016,
	LeanFingerOld_set_RequiredSelectable_mC9B95F2440F4F91C5A3A35924895A56B0A40E65F,
	LeanFingerOld_get_RequiredSelectable_m8E00340E045841D1758C46B9886622E0DA0053D2,
	LeanFingerOld_get_OnFinger_m78C3B08F8D1AC8CEFB0A24E7C311414C415C54B9,
	LeanFingerOld_get_OnWorld_m7FD52693FBEE9BF5A60FF85002F9B92344C7A6EA,
	LeanFingerOld_get_OnScreen_mFEA8A4F233973708E7CFC1D36DD0A12AD0145D66,
	LeanFingerOld_Start_m5B458588E1FFA58276E649B33E839662CB280CF0,
	LeanFingerOld_OnEnable_mDB644842C4717FE6A1048B864C80C77E6FAAD318,
	LeanFingerOld_OnDisable_m8960FC5621E19A6BF761EA83AF79C05F5D9DB194,
	LeanFingerOld_HandleFingerOld_m0F15F2B9AFB43E82089232FE34F6C545103B602E,
	LeanFingerOld__ctor_mAC02FD851F47668E4891F5B0DDB58B0475948D73,
	LeanFingerEvent__ctor_m81C9A08B0437D28134F73713211DAB97B4EDDE93,
	Vector3Event__ctor_mB1A204AF67A624D749B896533FA385389AA83A7F,
	Vector2Event__ctor_m2C7589C510A2388532CC0FF82C24578CC16F8760,
	LeanFingerSwipe_set_IgnoreStartedOverGui_m59129D2FDB5EAEB275B487F28C316DB4EAA8ED27,
	LeanFingerSwipe_get_IgnoreStartedOverGui_m1A8BA28F2ABA9C806BDF36AEA54A328BB20EC969,
	LeanFingerSwipe_set_IgnoreIsOverGui_m8BCD5A328B014D462491D001942466F284E9CFD7,
	LeanFingerSwipe_get_IgnoreIsOverGui_m5A39F372A535318932B28B4B9F5F5809C174DFBD,
	LeanFingerSwipe_set_RequiredSelectable_m2903DD987618F8DD6F369F35D1776EBF50E02F73,
	LeanFingerSwipe_get_RequiredSelectable_mD2DF985222EEFAF25236D982B1D6AA7DAF66411D,
	LeanFingerSwipe_Start_mA5F83708E39B812EA0E8E80C64D893E4D59F51F0,
	LeanFingerSwipe_OnEnable_m1B74E05D8C5E5E65872A2E5B1ED9DA706D0E646C,
	LeanFingerSwipe_OnDisable_m7B2360FF28472EC8F5CA7EED80074F9C86359DF1,
	LeanFingerSwipe_HandleFingerSwipe_m8CA4A5A7A28A33F07E7DADD3FCC75A9DF766981C,
	LeanFingerSwipe__ctor_mAA12E9F3B30965413EC84DA9BE3281589779CAEA,
	LeanFingerTap_set_IgnoreStartedOverGui_mF439D6DCE86EACACD1B64D4E0F4024E9AE8F5FF2,
	LeanFingerTap_get_IgnoreStartedOverGui_mB02D8067E0021CBD0442B419099CC1BBAE419168,
	LeanFingerTap_set_IgnoreIsOverGui_m69044F4BFEA5028196EF2D4757D8B7D17880D01C,
	LeanFingerTap_get_IgnoreIsOverGui_m44F14978A6FB794C15598EEAD2B8133DC8FE61DA,
	LeanFingerTap_set_RequiredSelectable_mF88A73DCA146844334FCFB7B2477748DA79F5113,
	LeanFingerTap_get_RequiredSelectable_mF870D5F7F3F91EC0F81AAAED47A144C7538E0F83,
	LeanFingerTap_set_RequiredTapCount_mA0612CFDBB30C9EAB701FC170B80B9DB5A56E287,
	LeanFingerTap_get_RequiredTapCount_mEA1093741E790DD0270F45881E876B012E15733C,
	LeanFingerTap_set_RequiredTapInterval_m481830CE8C8CCCEC16A5650B0558EFEBD8783F7B,
	LeanFingerTap_get_RequiredTapInterval_m9C7AE59B981AB3EB85CF3EB7228FC6672472A897,
	LeanFingerTap_get_OnFinger_m63D5FA1484C40838633A42FA08FDB05BA0655F21,
	LeanFingerTap_get_OnCount_m069DE54A016FAF7A36C72AD7AD1BDEA42841E169,
	LeanFingerTap_get_OnWorld_m4ED25B5F883DDE96096E5D41BA2267F8E693810F,
	LeanFingerTap_get_OnScreen_mDA41E80ED2528F6566C9498FA44CE202F3709143,
	LeanFingerTap_Start_m9A1D0A01FCF6C7793C508C36B37B7BDDC30A90C7,
	LeanFingerTap_OnEnable_mC892C650831ADBCF6F4C0269097A65A7B9C86940,
	LeanFingerTap_OnDisable_m520948BC519A56D004EF9B7E76CB319BFEF0885D,
	LeanFingerTap_HandleFingerTap_m59E8A9D4A9D72E84EE853B9A278BAC4C62040420,
	LeanFingerTap__ctor_mC5EB5DEBD46A54B9BE08CF4E71A7FC739D50C31C,
	LeanFingerEvent__ctor_m066C6057317457EC86CE5F05C51FCF1A52FF91C6,
	Vector3Event__ctor_m7CC3B8D85064BB05E82B262B471858E8812C70F5,
	Vector2Event__ctor_m0F1176A2133260E502E2E8472DF82E2D641920AD,
	IntEvent__ctor_m755DDF5118C169A7E72DA407292284B67620AE48,
	LeanFingerUp_set_IgnoreIsOverGui_m5FDFF292685474633CF9E3F90A770B786FDF45ED,
	LeanFingerUp_get_IgnoreIsOverGui_mB3B8D6FD3F9CF4ED9CB79B5D62E5424BD1990937,
	LeanFingerUp_OnEnable_m66D6CE5867739D3964F60CEAB74BEB33CA1174DF,
	LeanFingerUp_OnDisable_mE5614B69C21F006796DECC11EB21543A711FAC15,
	LeanFingerUp_UseFinger_m54A66DB86303A529A1D9736FE4AC3F1E45C2A510,
	LeanFingerUp_HandleFingerDown_m15D1343D4DFF6E1F6A2CFB6C61AA1901AAA3E7FF,
	LeanFingerUp_HandleFingerUp_m32C1CC4992B8A9530F3674B658621F9085DF3E37,
	LeanFingerUp__ctor_m11A5E4E86FDD1F93463EB1BC410E523250F326CA,
	LeanFingerUpdate_set_IgnoreStartedOverGui_mC4BAC12B1EE4497A38B88CF52BE96935DD1F0E8D,
	LeanFingerUpdate_get_IgnoreStartedOverGui_m497BE9F13287E56C7C6AE1D75F4244D318F84D06,
	LeanFingerUpdate_set_IgnoreIsOverGui_mCE69C225B511F2E80F321800BD035BDD97179D09,
	LeanFingerUpdate_get_IgnoreIsOverGui_m91E05E4EF2ACD9BFE0D6A51258707433A5C5A8CF,
	LeanFingerUpdate_set_IgnoreIfStatic_m82B5A0EC08AA000990094C3C25BA1781D91B454E,
	LeanFingerUpdate_get_IgnoreIfStatic_m31C8F8C7082DE0498E1B82E9221C8FECA6D8F5E2,
	LeanFingerUpdate_set_IgnoreIfDown_mEF1385F7F7C2F23102B68D3F22EB2FF17622D691,
	LeanFingerUpdate_get_IgnoreIfDown_mA4E739E6A3361FCC79B839D8F325832B281E9F00,
	LeanFingerUpdate_set_IgnoreIfUp_m0DD974938ACDDFBE7663C6C13AB739F13B8A31FB,
	LeanFingerUpdate_get_IgnoreIfUp_mA28589DE76F3E3E0C0F4F3780BF225B55642807C,
	LeanFingerUpdate_set_IgnoreIfHover_m1B24B4CE5A26823AC7519D1163EFB1CD4C920988,
	LeanFingerUpdate_get_IgnoreIfHover_mD1BBFF6CE0795D9B06A3F2760BBBBEB749B6A7DE,
	LeanFingerUpdate_set_RequiredSelectable_m49218D8554E1F00228CE87ED0370AFBF0FCD00F1,
	LeanFingerUpdate_get_RequiredSelectable_m35FECFEE9C190A051F9AEE115F926A23B31B123E,
	LeanFingerUpdate_get_OnFinger_m0181E2684FBC94097E356BBD6C5431A7F07DD6BB,
	LeanFingerUpdate_set_Coordinate_m6C397437637DC7E53DD3FFBBAE5EE52F974E5B9A,
	LeanFingerUpdate_get_Coordinate_m735CED9DA7A759F9AB06368DC5A6EBA7B36FABAC,
	LeanFingerUpdate_set_Multiplier_mB8CD1271A38AE59EA714116F6AB04D649B947727,
	LeanFingerUpdate_get_Multiplier_m850E484986045D089FFB83D465E10ADA35FF9CBD,
	LeanFingerUpdate_get_OnDelta_m0580341A4E98431B872A86A7264FE013117AE5A8,
	LeanFingerUpdate_get_OnDistance_mFF1EC0DDC68DA7D5B96BA8EAB1BF54482E38FD4B,
	LeanFingerUpdate_get_OnWorldFrom_m8FDAE12099717B4588B0CC096AC7337D393D25C6,
	LeanFingerUpdate_get_OnWorldTo_m275B2F0FA61BB70D6F6F4868B913514DBB799661,
	LeanFingerUpdate_get_OnWorldDelta_m32738C79242BD6F2E3FE8E3D310BAD18B94F2E58,
	LeanFingerUpdate_get_OnWorldFromTo_mE3C381F84CFC7D19236E8BE73CC7A29586690832,
	LeanFingerUpdate_Awake_m117475503EE491D9D1026E1114E8C13ED8536B3C,
	LeanFingerUpdate_OnEnable_m2D704112E5CDE0359583183F901289991D313306,
	LeanFingerUpdate_OnDisable_m5F92BEC0B13A52C477AE44B3477C23C248C93D39,
	LeanFingerUpdate_HandleFingerUpdate_mC61E61F0484880E241662681ABAA948588DBEF80,
	LeanFingerUpdate__ctor_m9A0FD241DCDC335321A28F50D4DB8D028C45AE8E,
	LeanFingerEvent__ctor_m7EBD0C5160FD5E9970F4C3E3B222E38257011E9E,
	FloatEvent__ctor_m107B522FAFD2A75A23B64F77F9567615665E470A,
	Vector2Event__ctor_mB0001E331ABE245FC18CDE62E5FFB46E007B4B1A,
	Vector3Event__ctor_m78C757EA5F95440DC43ED6347D7AD11E9B6CD930,
	Vector3Vector3Event__ctor_mD49AD0C93EAC1EAA3523D5C9128C86943D8A6136,
	LeanPinchScale_set_Camera_mEFFCF244901A0FA73CBFEE909B9BC3FA74771C10,
	LeanPinchScale_get_Camera_mBBC1E187F972DB947DA12FC9725DA42E4F13A488,
	LeanPinchScale_set_Relative_mE2E6DAC64F39F7C93C107E8FDC130F2DE7990359,
	LeanPinchScale_get_Relative_m87C664C51BB58FCBA36E80318DDBF0EAD9A900D6,
	LeanPinchScale_set_Sensitivity_mB4F748043497417E80F4D30457F051B5E13EA980,
	LeanPinchScale_get_Sensitivity_m0E5FCC2A55F93BE3B2BCC0D5C0A4C8FFA00707D7,
	LeanPinchScale_set_Damping_m8D58B351301244787EA0A39016B5106BF10945B9,
	LeanPinchScale_get_Damping_m983688BCCC013CE442FF233028FE41E0705AC5FF,
	LeanPinchScale_AddFinger_m064F72EBA3855AD66D3ACF155C63558B6FB6A7B3,
	LeanPinchScale_RemoveFinger_m1C7CBDFB9C4D6E7299EEA2700A09742E96459B41,
	LeanPinchScale_RemoveAllFingers_mD3A054A5D0681DB5A704BA674629D6752E5C6B10,
	LeanPinchScale_Awake_m9A08BB54FC091D8018F39F015138D76AD73D3CA4,
	LeanPinchScale_Update_m38153051EAA298C7B1440307E3258DEB486243D5,
	LeanPinchScale_TranslateUI_m110D5D7F7096A72DCBFB88EC446DEB1636739E12,
	LeanPinchScale_Translate_m02EF40F24334C6884A083368C30C894630F01685,
	LeanPinchScale__ctor_m714B453B2175D1933D36C4789C4EF8FBB56DE8CD,
	LeanScreenDepth__ctor_m317000555909FB83237D72C83E4CE97B9BD82BE3,
	LeanScreenDepth_Convert_m7A9AD8A2702E65EAEC8757784C7796C0F54A77FB,
	LeanScreenDepth_ConvertDelta_m2832CD0C407CBCFDFAB2BE1B8D91C2380397F299,
	LeanScreenDepth_TryConvert_m37BE63E86D3A1DA403AEA64BEDD802C3B5A883A6,
	NULL,
	LeanScreenDepth_IsChildOf_mD2A8698585D736BD56D913467C066B5D143B6C96,
	LeanScreenDepth__cctor_m843AC9408301A69F3AF769B6136DBEC2F16FAF47,
	LeanSelectableByFinger_set_Use_mBC2C4A9C711A3E35F0013EF158386F67FCF54AE0,
	LeanSelectableByFinger_get_Use_m3C920F0D822E47F4E8AEA21961132E3F5B054E64,
	LeanSelectableByFinger_get_OnSelectedFinger_m249B7CB7296CFA84970FDCD20792CA0A7C0CE317,
	LeanSelectableByFinger_get_OnSelectedFingerUp_m42CBC975EEFDBFD562215FA14E7EE8FECA864BDF,
	LeanSelectableByFinger_get_OnSelectedSelectFinger_m3B1543436E1C6C0CB2AB8DA84A29E404E86D93E2,
	LeanSelectableByFinger_get_OnSelectedSelectFingerUp_m2DD5987FC615F5B3EA39EA0135E320638B208BCB,
	LeanSelectableByFinger_add_OnAnySelectedFinger_m180429FB0C6C74231DB80BC55A5B1BC5755BFB51,
	LeanSelectableByFinger_remove_OnAnySelectedFinger_m419C283A825D1B3A92EC8239BA564FF59C5C3D7E,
	LeanSelectableByFinger_get_SelectingFinger_m58161BDD661EF14E3804B233E6851509B8AC53E9,
	LeanSelectableByFinger_get_SelectingPairs_m34E3077D6DD525CB24DCF0565D54A8CDEEB97AC9,
	LeanSelectableByFinger_SelectSelf_m37727BDCCA8A8CD0D1B468D579DD0E66C3075335,
	LeanSelectableByFinger_GetFingers_m9B19725E9E43A6A867A70885C088DA9CD1713BF0,
	LeanSelectableByFinger_FindSelectable_m158FE6F104EDFBD3DEF6962625353E1B9ADF1585,
	LeanSelectableByFinger_IsSelectedBy_mE1ABC50D05805F1310FF99BCEAA27EDC96C8278C,
	LeanSelectableByFinger_InvokeAnySelectedFinger_mB22BFD020E7049C76ACE8E61E36351CDCB4C0C64,
	LeanSelectableByFinger_OnEnable_m1CADF924C67E7342AB38AFB9436518F3B9364458,
	LeanSelectableByFinger_OnDisable_m310830F10B969FAC1D428CF2EC51F48B39326A3F,
	LeanSelectableByFinger_get_AnyFingersSet_mD094F2DDA14605C5340BC0A7FA13F93659555211,
	LeanSelectableByFinger_HandleFingerUp_m54F15E8525CA9916032201777DF67725F796B5D6,
	LeanSelectableByFinger__ctor_m18195B103EDF0661C2FB312EF5D364508A878D03,
	LeanFingerEvent__ctor_m093B9C465E7D71D3A48EA735993667CB556F1078,
	LeanSelectFingerEvent__ctor_m692F05856A4A69AB9DE5D3AC167AE2DDF031B50D,
	LeanSelectableByFingerBehaviour_get_Selectable_mDDBC292777C08143DA6FDCB85F9B505846722CB7,
	LeanSelectableByFingerBehaviour_Register_m4242F1D49A85C22D17706C579DAC35BB8E141286,
	LeanSelectableByFingerBehaviour_Register_mA3C4C9FF035486417CC5B17F28E951CE3E825C31,
	LeanSelectableByFingerBehaviour_Unregister_m0368607F5BA3852D7E261EC4E4B388880E5A035F,
	LeanSelectableByFingerBehaviour_OnEnable_mDB8044530F9BDDDAA6C3D8AC582B8D590B552CB9,
	LeanSelectableByFingerBehaviour_Start_m7FD3DC511E8956A5391F9600A9734649EEF348E7,
	LeanSelectableByFingerBehaviour_OnDisable_mBE3EF75E2C368457EB6C85CFE0613BF84CA5A78D,
	LeanSelectableByFingerBehaviour_OnSelected_mA0863BE4D4FEA9DEF948DABBC9BC2DCC066958A5,
	LeanSelectableByFingerBehaviour_OnSelectedSelectFinger_m6267803F78A8B4A4DF6A90DAA010919ED0060C5E,
	LeanSelectableByFingerBehaviour_OnSelectedSelectFingerUp_m8FB142F00EB59B82546932C0D125D1DE6BC9FB86,
	LeanSelectableByFingerBehaviour_OnDeselected_m13837E9FDF9E17358CD44F08DF6BE8BAB7AD5F06,
	LeanSelectableByFingerBehaviour__ctor_m0FCE4E850934BE31569552EB917517AC7421FB62,
	LeanSelectByFinger_set_DeselectWithFingers_m6A94AB0E138D9AC46AA6E44364BDD5CB7056BC3A,
	LeanSelectByFinger_get_DeselectWithFingers_m386609DE7D27BE63114357FB8CB756D35CAB0984,
	LeanSelectByFinger_get_OnSelectedFinger_mB79C1C7EF8C1D02AF5830B8CB4A2189E8B11BFEE,
	LeanSelectByFinger_add_OnAnySelectedFinger_m5043BBE03F53BEAF4A4D17472F12D765306C327E,
	LeanSelectByFinger_remove_OnAnySelectedFinger_m08CA01DC818E22E507E45CEE3F09D89CBF9EF594,
	LeanSelectByFinger_SelectStartScreenPosition_m79B8FC370D893BE312824C898D606D8150DB2274,
	LeanSelectByFinger_SelectScreenPosition_mD4D61BD9FA7193B4604A16667B766E90CF7C4728,
	LeanSelectByFinger_SelectScreenPosition_mCC4AEA308C422EE60AFE77E69E29C4685D19579E,
	LeanSelectByFinger_Select_m4FB61F03FA876187D2E48394B20AB4BA1AEEE526,
	LeanSelectByFinger_Update_m680DE52D2D369C54CA7C36FA523DD41A85C33265,
	LeanSelectByFinger_ShouldRemoveSelectable_m701B9286A22DC14793E275DA25F9BAFCA91BB78A,
	LeanSelectByFinger_ReplaceSelection_mA96488E1D1A99A352191F5843CB908774CB83D6D,
	LeanSelectByFinger__ctor_m700D7DEB2DDFA63BC4D5A0B8C6F5C9875A461B0E,
	LeanSelectableLeanFingerEvent__ctor_mA419D40EBFA75FE721918BCE3417351928E10083,
	LeanSwipeBase_set_RequiredAngle_mC410B0160CC9E0B26C489EA981CDF4548FE77FF9,
	LeanSwipeBase_get_RequiredAngle_mEAB17DF24812CD4FF27B02B03F2835C24D46EFC1,
	LeanSwipeBase_set_RequiredArc_mFCA4B087D9E3F7617693D741FD207A6B693CF0D0,
	LeanSwipeBase_get_RequiredArc_m9A9445128D3E7A33D5A7D9C296BACE52985300D9,
	LeanSwipeBase_get_OnFinger_m146E52130AFEBC3703CA327A5FC1A32D17C3D950,
	LeanSwipeBase_set_Modify_mC331356BA9C7F4923590BE83C356F3420F480260,
	LeanSwipeBase_get_Modify_m0D78838D1F695B51AF63F3C5BA4BD76967635DF2,
	LeanSwipeBase_set_Coordinate_m8AF018B41CD520B4682B7FA748A2619CC1F576D9,
	LeanSwipeBase_get_Coordinate_m343512AFA8BD0E7A1C02CF6578FAF3A486899BB5,
	LeanSwipeBase_set_Multiplier_m0E7A3119304A366A68A42E5258B3310F041E03A3,
	LeanSwipeBase_get_Multiplier_mE3A7A6304B5B4B064B0C9ECBC408CB565D2D79F1,
	LeanSwipeBase_get_OnDelta_m0EF61FD538559D28C186867B1B7CB6898D8B39A6,
	LeanSwipeBase_get_OnDistance_m96364752A06CED974E5021D773B97745C7C34FC4,
	LeanSwipeBase_get_OnWorldFrom_m2192CF00007530F723354BB27DCE0A5DAFE3C8DF,
	LeanSwipeBase_get_OnWorldTo_mEEEB0425D11AC151D6EEF146D46A27F84577F16C,
	LeanSwipeBase_get_OnWorldDelta_m3BDD82B8E91757C6E62BD3E3BD2DD09DCAFDAF67,
	LeanSwipeBase_get_OnWorldFromTo_m26347F1C5CE67B03B257CE5C8825E376DB8424D0,
	LeanSwipeBase_AngleIsValid_m9A99554522D9780D8C5F1E1CC1F11637BEA02E45,
	LeanSwipeBase_HandleFingerSwipe_mB0DA705C01A3E252B600B7C6321DA3127386D840,
	LeanSwipeBase__ctor_m16DC32C99FEF9E473A719A42EC01716D67E5EFFC,
	LeanFingerEvent__ctor_mCFD24D9CA2BF018E605D403C31342635EA02D93B,
	FloatEvent__ctor_mDAFDB87D07C9FFD4F5C233F7967E9FC9F7165CD9,
	Vector2Event__ctor_m9D0B2416E70349E2895A29B022EBE89EC1C2678D,
	Vector3Event__ctor_m13A940B94A6C96EC2DDAC1FCCDD1EB54141AEA0A,
	Vector3Vector3Event__ctor_mDA3312D25717F07FC8AF44780B9A560955FDE4A6,
	LeanTouchSimulator_set_PinchTwistKey_mEC8876AC55B49968963381824DBC01524542A3AA,
	LeanTouchSimulator_get_PinchTwistKey_m54FC76AFB8A1042957EBBDF2E8739C1B3948E015,
	LeanTouchSimulator_set_MovePivotKey_m307FC3B20169DE28C806460AFB7FE2A7659186D8,
	LeanTouchSimulator_get_MovePivotKey_m034C8FD481BECCCB460B24CD4FA4E213647288F8,
	LeanTouchSimulator_set_MultiDragKey_m9934345CCDCDB086B281A9C6C84B7B30B7356B59,
	LeanTouchSimulator_get_MultiDragKey_m983C2334F86ABDD9016FBB7CA8629DD69417BE1A,
	LeanTouchSimulator_set_FingerTexture_m151ABD32058C47532035AF908DAE52682DC111D8,
	LeanTouchSimulator_get_FingerTexture_mC657921BE290234B44B2D8A33912A571C2AADB0D,
	LeanTouchSimulator_OnEnable_mFFB2C2BE4B85F0601B4F1ADFB88D27DB68A36AB3,
	LeanTouchSimulator_OnDisable_m9ED170F0802AED9625BF932B230D46CAAC632912,
	LeanTouchSimulator_OnGUI_mC0C893A99D2ED5A2FAD9D2E737A13CB2AB02F840,
	LeanTouchSimulator_HandleSimulateFingers_m3280D2B09F6CF1BEB1F65E062F78590553B5F9A4,
	LeanTouchSimulator__ctor_m20C55E4CE257F128808870742837B674915D6277,
	LeanTwistRotate_set_Camera_m350424F9FB7685B0D24AE79E5B47FC998E88DDAE,
	LeanTwistRotate_get_Camera_mF7A361D625278286C6F0F4C73468BC8639A651D8,
	LeanTwistRotate_set_Relative_m1257B53F9A04FF0E3D0D35620C76656B050BEA48,
	LeanTwistRotate_get_Relative_m3C9277F6F7712DA99FE45F94CB0B1043568F4C9F,
	LeanTwistRotate_set_Damping_m6F689E2A5439F75C6E70E3E85954D406D3F5DF69,
	LeanTwistRotate_get_Damping_mF91DB5FF8A50AA7D5C02DA899576B1C02083BF1F,
	LeanTwistRotate_AddFinger_m9C44D20A341B17F8FDD974B888D5197898B34829,
	LeanTwistRotate_RemoveFinger_mF0CE0CD921E2F6B788A216FE51052154E1EC02FE,
	LeanTwistRotate_RemoveAllFingers_m96FC116013D95AFBBE251739F77C248F799930D2,
	LeanTwistRotate_Awake_m7975D3E4DB07DB24A68DCA67F45B0B45D1D3738C,
	LeanTwistRotate_Update_mBCEAB598F17A634312D665D9E0053891DD3CF3C1,
	LeanTwistRotate_TranslateUI_m9F7A60327C406D5EFAF0B609F81467282594CD6A,
	LeanTwistRotate_Translate_mA77AFECA0351EF4B8B596246D32425D6D43A90FA,
	LeanTwistRotate_RotateUI_mB5157F14702EADF14933665EF89AD86413D28DE6,
	LeanTwistRotate_Rotate_m7FEF262169B0E23D33C867241A5CAA0BC7C40837,
	LeanTwistRotate__ctor_m8B79B4708334ECF30A023761E6A2280DD4E0C686,
	LeanTwistRotateAxis_set_Axis_m7B585E9E74857FCE1395704FD23780AC32375DE9,
	LeanTwistRotateAxis_get_Axis_mCE9FEA7893F7549E29DD6F1D6AD2936850DB79A6,
	LeanTwistRotateAxis_set_Space_m2048D9550AC85949CB85B28BCCB90B51066727C7,
	LeanTwistRotateAxis_get_Space_m8A340C6348993D8A62D260ADDE37CD3BFA08ED9A,
	LeanTwistRotateAxis_set_Sensitivity_m1771F75AD678FE8A7A914265910BB6DF277A2185,
	LeanTwistRotateAxis_get_Sensitivity_mB940902D4BDBC2BB03B34812887AA4B5DDDEFF03,
	LeanTwistRotateAxis_AddFinger_mEF7E4D701E5ADB521A13F83AE25D8F61E09B418A,
	LeanTwistRotateAxis_RemoveFinger_m92A3B2BB9634723661A897821C11014D710B49C8,
	LeanTwistRotateAxis_RemoveAllFingers_mC368843ACB62386CFDC9CBB2E4A6B97689BE7F25,
	LeanTwistRotateAxis_Awake_mAAD9A57F7CF73FD4F46C94706FDDF3FCC15AC569,
	LeanTwistRotateAxis_Update_m9E1659E95DD277422E1A45592AA0EBFAB8099899,
	LeanTwistRotateAxis__ctor_m1E15A2C9C2A59218C3F4CF7E1F1708FB4DF95576,
	LeanFinger_get_IsActive_m187C1AA12031AA7E55C48162BBC1DA79174E1071,
	LeanFinger_get_SnapshotDuration_m4E7A4CC4C49AC6308F0336664883DDF335CA0F67,
	LeanFinger_get_IsOverGui_mCF313973B1E1035AB322AA1B3077105069278E34,
	LeanFinger_get_Down_mB57C1F49D26C67B2C08F3C0E275B4B405E3CDEC9,
	LeanFinger_get_Up_m1F478DD3BF6DB0252014A8157E873DAF24348586,
	LeanFinger_get_LastSnapshotScreenDelta_mC6BCCA2600E753A518A0227505C5B5F814D83587,
	LeanFinger_get_LastSnapshotScaledDelta_m920920E7430D962D45649CE0292444F3E4849C22,
	LeanFinger_get_ScreenDelta_m71CDA01F17B9664BEE576B1BDE030CCF75240AD6,
	LeanFinger_get_ScaledDelta_m9F29F001B37315D360921C755FB3925A130B7AA3,
	LeanFinger_get_SwipeScreenDelta_m6D0ECD39FA7D90FF51D2F520E53F5EAD0BA8EC6B,
	LeanFinger_get_SwipeScaledDelta_m84A7BD2A7E1709ACC6A2EC6D8450DA5E3E4505C2,
	LeanFinger_GetSmoothScreenPosition_mB77A03E70985C80A112506D60763309135170CD9,
	LeanFinger_get_SmoothScreenPositionDelta_m481400CB3A8A4E860AEA881D3DAFE26C3AC1F72B,
	LeanFinger_GetRay_mC28C0EEF67C2F1DEE49E82954017E044793CE5F1,
	LeanFinger_GetStartRay_mD547BDCBA9C957B0794EA21C2F2C06268F36B257,
	LeanFinger_GetSnapshotScreenDelta_m988680318F883372CE5DFBC1B8735F630CEB58BF,
	LeanFinger_GetSnapshotScaledDelta_m33BE10443C7522F2DEB9545365A5E9B7F6946BE0,
	LeanFinger_GetSnapshotScreenPosition_m8A921B210FA28F3B8E8278B86E97E77305FE343E,
	LeanFinger_GetSnapshotWorldPosition_m6BA0D2F8E8651C739C1E6A1F07AA7CC611FA425C,
	LeanFinger_GetRadians_mA46DD0F0E0DD1A5F704CB494C06AC4C2B2938EF7,
	LeanFinger_GetDegrees_mD04F5A0671F3BD51689452543033886545404B54,
	LeanFinger_GetLastRadians_m7E2BA868EAF1E1E93995E58CCC74E4D1782104F1,
	LeanFinger_GetLastDegrees_m30E53FC3061763AC5B2D2A061F44AFAF5DD5921E,
	LeanFinger_GetDeltaRadians_m99F8DD4F01E806C9D17E5BF0713697033CACB804,
	LeanFinger_GetDeltaRadians_m35A118B31AD9BF0D52C0A5EE399CF2A87B86691E,
	LeanFinger_GetDeltaDegrees_mF4BB08964D329EE0109CAF2B8690BD72B921BCBD,
	LeanFinger_GetDeltaDegrees_mEF7B5CCD801B5B8208CC60D16305F8B12A7AECDD,
	LeanFinger_GetScreenDistance_m7BD5C7325694D57F1D59B8D119908BF31B97EB02,
	LeanFinger_GetScaledDistance_mDFA1BA6F9AE3E0C03BE4C05AF27753C2AC3FDAB7,
	LeanFinger_GetLastScreenDistance_mC65F4CC190D71163327D6FEBCC3E07A8E45CEDA7,
	LeanFinger_GetLastScaledDistance_m6BB3BE44652F214ACEFB78E2C2DB09E88CC72FAB,
	LeanFinger_GetStartScreenDistance_m9F7BF1F7EA17C3E47A808DD4666B4D832F100792,
	LeanFinger_GetStartScaledDistance_mC8F2C410975340247FA17B126522519DCA998A10,
	LeanFinger_GetStartWorldPosition_mF1878403A48D2B7D2F0CD9087A191C2144A1C3F6,
	LeanFinger_GetLastWorldPosition_m312D97F63BFBABE19C2CDAACC6311DDE92E45D6B,
	LeanFinger_GetWorldPosition_m63BBE135F913E37A60AA495CF7FC69D2B697CD69,
	LeanFinger_GetWorldDelta_m403CDB6E467BB2BD0927674FB76531CD6F371716,
	LeanFinger_GetWorldDelta_mA1F69412C75B8355A933C02A28B094FA23E18610,
	LeanFinger_ClearSnapshots_mF162FD7281F98B5E1E0931AE27AA9B4F7A1DBA9E,
	LeanFinger_RecordSnapshot_mAE3635890DB234C2F9AD4997ACAB995EEBD856D2,
	LeanFinger__ctor_m13E48E6164F1F89D80B45D132CBEB29C274B28BC,
	LeanGesture_GetScreenCenter_m0843C8DCF5EEA72404C4495EFC3D3C6DE050D429,
	LeanGesture_GetScreenCenter_mB53F13E2C96624F8A9B03156E06D562925402955,
	LeanGesture_TryGetScreenCenter_mE0AA0193BF973B5B39CEAD8728811B8708F7B907,
	LeanGesture_GetLastScreenCenter_mE78F128D1789451EA3CF9A2206ED0FF1E95BEB48,
	LeanGesture_GetLastScreenCenter_m1E682F188DDC687F9DB91088401C28A2EF9A8F11,
	LeanGesture_TryGetLastScreenCenter_m036B9F6D038D22AB13F10AFB25ACC83E7A6E46DB,
	LeanGesture_GetStartScreenCenter_mB24C255ADC15D489D71141AFC132A19F679E46CC,
	LeanGesture_GetStartScreenCenter_mE32898F28278ACDE8A288D9620C5B28AF54F9B53,
	LeanGesture_TryGetStartScreenCenter_m2E55257E7F56CB292D740BC88D52F4079D34F570,
	LeanGesture_GetScreenDelta_mA271A261FDD051B4E10345967BC0E693C3CA5645,
	LeanGesture_GetScreenDelta_mC51532A01DCAE638C470F3B4373A8EBE776C154A,
	LeanGesture_TryGetScreenDelta_mEC82733B097F154A1EB8C99017BA337CBAAF9F23,
	LeanGesture_GetScaledDelta_mEDCFD0FC522248E3A7F93F9FD623D5B1BF318B43,
	LeanGesture_GetScaledDelta_mB6F662577CE91A307556F1D887EC635B586205DE,
	LeanGesture_TryGetScaledDelta_mD4C25322E14F5B6D86CE98D6FBAE6A8E5581604A,
	LeanGesture_GetWorldDelta_m001CC4B486F739F3676A2C204FAEECCA1F25E043,
	LeanGesture_GetWorldDelta_mE619FEF4AA91E0244F4ABAD964E40DC9BE4320CC,
	LeanGesture_TryGetWorldDelta_mAAC254E66D8A15FDA39576E247517EAFF3F65B83,
	LeanGesture_GetScreenDistance_mE7003DFBFA44E9730CA53CFE3BE4B789D26CEDB5,
	LeanGesture_GetScreenDistance_m66CC128493767DF75DF8DC5E2F202D55F74DCC32,
	LeanGesture_GetScreenDistance_m392B2EED4C096D1F585312B1180616D5E95F7939,
	LeanGesture_TryGetScreenDistance_m11518F012E770AD0BE8119CE600E8203AC230E3E,
	LeanGesture_GetScaledDistance_m0DD1C0F924258D86647232406D3799D54B724F5C,
	LeanGesture_GetScaledDistance_mB9E5F83DFF7589E8415A0B2BD3BD594FAC9ED4C1,
	LeanGesture_GetScaledDistance_m762722AC7D36EF7BAE54A2A5612E400BA9FC7FC6,
	LeanGesture_TryGetScaledDistance_mF6085A852520A6BFD131744E72580B9F5E323633,
	LeanGesture_GetLastScreenDistance_m50A1A0C138801064825D23525E228DA683264DDA,
	LeanGesture_GetLastScreenDistance_mB92242C611BAA657F0E3FFC10F817CD585D401A6,
	LeanGesture_GetLastScreenDistance_mC5D5F4D4B65E3F360669B1BD8DD0D68AFE349EEB,
	LeanGesture_TryGetLastScreenDistance_m6D5DCF2CB33BF151BE7DF791EE3396633D430A17,
	LeanGesture_GetLastScaledDistance_m1034B646C0DA00C3A714A7A1C988B6BEC52E7627,
	LeanGesture_GetLastScaledDistance_mFEA5DD8AFF5A73CF7720B9AE94F3A2539CB8481A,
	LeanGesture_GetLastScaledDistance_mAA60368A022E9C9793456ECD05B3A9B20981C62E,
	LeanGesture_TryGetLastScaledDistance_m4D7191A82CC9F5693709BBC973769694833991E8,
	LeanGesture_GetStartScreenDistance_m451E32C0BFDD33D6ABC97360E1B7C0158A28D343,
	LeanGesture_GetStartScreenDistance_mD05407DBF42825CB5211C7D6B23BC818D0734D07,
	LeanGesture_GetStartScreenDistance_m9F9F4B7A408142A519B6C765772FD47EC827D4F7,
	LeanGesture_TryGetStartScreenDistance_mF8A9C22AE95A9106682637633C307BBA96CE73A7,
	LeanGesture_GetStartScaledDistance_m312AD2BD9098D51CA3E56C2FF80EA63837AFA9F4,
	LeanGesture_GetStartScaledDistance_mD2CECFBF13AECD07C6BFA04B22CD3DB63769C8F9,
	LeanGesture_GetStartScaledDistance_mA1BB20CD718AD089AEBC441B48E4F1E356C40F75,
	LeanGesture_TryGetStartScaledDistance_mF9613BC693B3C168C9D20016A7F03A25099A2C71,
	LeanGesture_GetPinchScale_m558BFE4346A40810062CD13907819CB44CC5D519,
	LeanGesture_GetPinchScale_mCEFEA55BE3D6995096A04D9A9A265BD5A989BC3E,
	LeanGesture_TryGetPinchScale_mC9CF4DB112D4EA14BB4036DC4AD66A7CD38BF2D7,
	LeanGesture_GetPinchRatio_m73328152C95EB01C16B0782A6170506D112C3BFA,
	LeanGesture_GetPinchRatio_mE986D8F99B54B2CD5B025BDE337A1257CA049394,
	LeanGesture_TryGetPinchRatio_m7896EBC828A485A33E706350D35BEA98525EC502,
	LeanGesture_GetTwistDegrees_m42301E5586566074403D4D34130053D176A229AE,
	LeanGesture_GetTwistDegrees_mA5A9EECB3088ACDFF63D0576002B4A2B04A243A1,
	LeanGesture_GetTwistDegrees_mF6E7C38A02604FA07D01079E9855DD3D3A2E10B9,
	LeanGesture_TryGetTwistDegrees_m00BCA9CD76EE9D1CD6DBC41955FC858C1AB5F1B8,
	LeanGesture_GetTwistRadians_mFCD7A86FB202F153389DCF255B19E7C6DB6DFDBC,
	LeanGesture_GetTwistRadians_mD7468AC6FEDA55E6446DCE8283E329BDA8021734,
	LeanGesture_GetTwistRadians_m33801F0E4A2998B11D45D014B3B735504FE5F1B1,
	LeanGesture_TryGetTwistRadians_m4BEBFB54D2DC25F9F73F3790282E2BE4B1301E41,
	LeanSnapshot_GetWorldPosition_mDB486AF1F02CC9BB3AC436B37068E08555C01B93,
	LeanSnapshot_Pop_mD2CEC21623B7E8FE13CCA810C07F4E77423FC670,
	LeanSnapshot_TryGetScreenPosition_mB2916415E1A607256CB52311E063AD5EAB875EA2,
	LeanSnapshot_TryGetSnapshot_mDCB3AEC3C43B5F21B4A37D90F33BA416CB8A891F,
	LeanSnapshot_GetLowerIndex_mC4354BDDCAC8930F1B7D7BBADF266CC3666B72B3,
	LeanSnapshot__ctor_mC54AA27EA5FB56CCEA2FFBB818A641AD903C2CF7,
	LeanSnapshot__cctor_m7232290A0AEDDCED1A74BD7867EDFFE1AD160AEC,
	LeanTouch_add_OnFingerDown_m2E56F938EABEF32E2BFCAE63FDCE9531CFD64C8E,
	LeanTouch_remove_OnFingerDown_mC28728EB87ED09ABB6D7588FAD667C17B91223A4,
	LeanTouch_add_OnFingerUpdate_mA786500D7D02C997D1C487ABEF230C80D7CBFB9B,
	LeanTouch_remove_OnFingerUpdate_mD0DF62C210AD829E7103F78E29D2E2E8B4351C92,
	LeanTouch_add_OnFingerUp_mC2726DD35564E0B4BAFE74A733DDC29F768F0E2E,
	LeanTouch_remove_OnFingerUp_m4779CFA6F17D71FF23789057ACCC9B04D5F5CED7,
	LeanTouch_add_OnFingerOld_m08E026CB371529E9A8254BB9B3C82CEBA087D2A0,
	LeanTouch_remove_OnFingerOld_m3F348423F2EF67F8D3E5BB7D15C1840E7682CDD9,
	LeanTouch_add_OnFingerTap_m0802FACCBF3C4B92576241E5B2F46A26B4990F07,
	LeanTouch_remove_OnFingerTap_m2159327C89954FC3929FA2B85FD2295295818AE4,
	LeanTouch_add_OnFingerSwipe_m363049691F98764F06ABE81F45899531BE1AD7E5,
	LeanTouch_remove_OnFingerSwipe_m74E13C41E8621C078FA57D6E8E9F70470A8D2AD9,
	LeanTouch_add_OnGesture_mF4525E24D51F05A940E7EE0C73033942AF9BB07F,
	LeanTouch_remove_OnGesture_mAD89251E3BBE424D24FFAEB335FBC3F033DB02B1,
	LeanTouch_add_OnFingerExpired_m5A8D0D603DD8CDD4ACA19BA10932D9B4758C295E,
	LeanTouch_remove_OnFingerExpired_m55D8D1051A030D56C696B704A94AA141A909E2FE,
	LeanTouch_add_OnFingerInactive_m0BBD293C30719C22C0450433AA22C33605DFA892,
	LeanTouch_remove_OnFingerInactive_m9588937E0578A605793B800CD516FC16EC6D090D,
	LeanTouch_add_OnSimulateFingers_m84243198DC16B9603EEAB4B50BFC177230E61844,
	LeanTouch_remove_OnSimulateFingers_mA967C68790D03B90A9360515539D9B61F12EEE9B,
	LeanTouch_set_TapThreshold_mAE881243CD136B5EAEECFF351433159A5DED820D,
	LeanTouch_get_TapThreshold_mFE27250D6104795BBD85FD7BD0F914469E2ED1B5,
	LeanTouch_get_CurrentTapThreshold_m0680E602895DD4187B397575889F8CDD87729CF4,
	LeanTouch_set_SwipeThreshold_m09DED68B40040EEF2920B96069BC24D5CD3B97E0,
	LeanTouch_get_SwipeThreshold_m8FB727022F13AA3DDF040A5021599E0A3251AF1C,
	LeanTouch_get_CurrentSwipeThreshold_m0E7EF447C90D19610FE0852C4F992A3351CA7858,
	LeanTouch_set_ReferenceDpi_m4056E9211750128B3385A37D3B52A2AF6C8A6F0F,
	LeanTouch_get_ReferenceDpi_mE2B85BC689AD5513209A3A9CDCCD91E69D97D6C7,
	LeanTouch_get_CurrentReferenceDpi_mFD0B6020B347D2FD9AA408942291678380B27E0A,
	LeanTouch_set_GuiLayers_mBEF94C7F478CCBCAEB0C974C952F6EB66D0D7CAC,
	LeanTouch_get_GuiLayers_m50AA727955348471094CD1D2E174C5D8D6644992,
	LeanTouch_get_CurrentGuiLayers_m2298663D6E09270D986BF2B8222E99B30BCF7EDE,
	LeanTouch_set_UseTouch_m582BA72AA19749C017386080FC332A5D367E4378,
	LeanTouch_get_UseTouch_m75D5ECB8FC37684B68E5D230AA9FAA6B2B77241A,
	LeanTouch_set_UseHover_mFFB08098481C789383DAAC0BEE0762F265FC124F,
	LeanTouch_get_UseHover_mEEE31010CD11877FA14EB6F9F320D330ABC3F99F,
	LeanTouch_set_UseMouse_m8323CF05D124A6CEBBE98C45C7766A49E47AD14C,
	LeanTouch_get_UseMouse_mE0D3DAA013109ACF0B5E0E5F18BF63A6BCC11FFE,
	LeanTouch_set_UseSimulator_mE86E37D0D63F6EC21F5047821C8F521EEAFF7F0E,
	LeanTouch_get_UseSimulator_m02E9B7238F2F20C0AE5F65A335148F9C695E7AD8,
	LeanTouch_set_DisableMouseEmulation_m4FAD9C78B268E4A14760CBE162598C270FF8A4CA,
	LeanTouch_get_DisableMouseEmulation_m381D310CE9BFC3219BBC9189377AB33341996DE8,
	LeanTouch_set_RecordFingers_mE5C42AB1B504099DCA1F2327DFC948D3050A4BF0,
	LeanTouch_get_RecordFingers_m01E0316CF3949DE554A3971C93127F2B71C1CF41,
	LeanTouch_set_RecordThreshold_mAB8F1B37BF74B7BB1BEC00BFDDEDCA26E4A1E7CB,
	LeanTouch_get_RecordThreshold_m6B8E6F378B975C473839FBBA86290E83BA41270E,
	LeanTouch_set_RecordLimit_mE1B3EF4908DED3F10BC5E6E2F1C0FAF78D637174,
	LeanTouch_get_RecordLimit_mD2A16DE05F30877C11EB33AE2EBEC31A68030797,
	LeanTouch_get_Instance_mBBC65A9CC76EB0303FE32C3BCAE0CC7946C4A61F,
	LeanTouch_get_ScalingFactor_m4CAB375511E8DEAF7DC6B6AA6C2A1424B94E6D51,
	LeanTouch_get_ScreenFactor_mFC5899186F9BA622976569E83D2096AD8C4E88CA,
	LeanTouch_get_GuiInUse_m3315AC1F165E1CD3A08AFDE43AB4FF351EC523A5,
	LeanTouch_ElementOverlapped_m1CAFC6F51029647D0C3FE646A2C4D1A0BEBC7716,
	LeanTouch_GetEventSystem_mFD85F8F0F31C5522B450D9FD45779606260A3C48,
	LeanTouch_PointOverGui_mF59BFC133EB266CE57341BE19D7BF26786E92531,
	LeanTouch_RaycastGui_m569DB00564461157996171DDD44B1C1249AB32DF,
	LeanTouch_RaycastGui_m1FC6B9273BAB9F96F86B05160DC7D67F439771AF,
	LeanTouch_GetFingers_m92F5C0E0E9C8ABC5A477FB5D9D72372DB113252F,
	LeanTouch_SimulateTap_m670B99980252EF891C41E627064636D2209BE71D,
	LeanTouch_Clear_m038D327F430CBCEA4FFBEF056834B04738908D61,
	LeanTouch_UpdateMouseEmulation_mD72C5A268AFD49117207D721CED70E119F85976D,
	LeanTouch_OnEnable_m2EFCB061AC2F98451E89C23E8B7579B5A27A665C,
	LeanTouch_OnDisable_mE141904911A23C7B065EF85E0264805FC97B2705,
	LeanTouch_Update_m8C05D509BE1D246F459277C160C67B8F1B6F3019,
	LeanTouch_UpdateFingers_m756088AD23417400111EB1085236D7BB752B8B6C,
	LeanTouch_BeginFingers_mAD2E255FB3BFFF4DEDE0AA087B7AB341E9FD97A5,
	LeanTouch_EndFingers_mD9A4ED85FCFAFD47AE7B9832F0688AF2ACABB84B,
	LeanTouch_PollFingers_m74C4885EBD84D7D9BA4297AB894D3B422C8731CF,
	LeanTouch_UpdateEvents_m90CDAE85D462BB36E4530476B7CF83A3DC1EC61D,
	LeanTouch_AddFinger_m22E19CCFD14A44FCC4901C6FDFDCF69B53D5C2FD,
	LeanTouch_FindFinger_mF0757248A41C04D204E9BCEE91438E57D4644049,
	LeanTouch_FindInactiveFingerIndex_m3295ADF95130FB9F1DE22DDE0F5F25DB0B265053,
	LeanTouch__ctor_m51C014B43ACC0A1F7D53A4331667143D6D10B89D,
	LeanTouch__cctor_m6D16213A01F9E60D22CC131830F7D3320CAE9048,
	LeanDestroy_set_Execute_m5C29E31056B8139967F89C45B8D447836062DAE9,
	LeanDestroy_get_Execute_m481B0CBE35A667EAA83678C5498882D1AEDE0546,
	LeanDestroy_set_Target_mD718EFA46FEE09131B8B915D389A95D56D46FF65,
	LeanDestroy_get_Target_m30B946B4D8587451136B9AAB2308D8D5422292F9,
	LeanDestroy_set_Seconds_mDAD6221A4E324B314AB58B92956DE68F126ECF45,
	LeanDestroy_get_Seconds_mE1EDED00363E72029C4154C84D8953DC56B0389D,
	LeanDestroy_Update_m4788151349CB14FC1919255CC9FBD0E1D8B056CB,
	LeanDestroy_DestroyNow_m2F3B8BCB8ED431FCC7EE6E7317A1FD84FF92BC9E,
	LeanDestroy__ctor_m870FFF17B12BC66ECC0A1C46355DEF4C8A842DDC,
	LeanFormatString_set_Format_m44BC70AF63CB152D7CCEA963C201F071257EDF56,
	LeanFormatString_get_Format_mC8915C244025C812AD23E7DC225B9BAC09C678B4,
	LeanFormatString_get_OnString_mAEA8872283296CDB0137A782BD7CE49770A5BC94,
	LeanFormatString_SetString_m8D34E974327EAB919F519DEB1783452FB3A9AB1E,
	LeanFormatString_SetString_m9F8B437E60F2E07935E34A100D515D4D73723C52,
	LeanFormatString_SetString_m8F493F752F5512E72CAE6A441A07583E33C0CC25,
	LeanFormatString_SetString_mB2BE0D52A16DDD83004E38DE7FFC4494768885C2,
	LeanFormatString_SetString_m2437E031907355F53C319AA4BFD23C657BF1977C,
	LeanFormatString_SetString_m58FA40B9691AD4AC08AC7AF928D472A388C9A731,
	LeanFormatString_SetString_mFC595BB40EDB35B7C11FE607055D5CF7DDFDA262,
	LeanFormatString_SetString_m876CE712B0011822BF9161405056100B243AA3A8,
	LeanFormatString_SetString_m34E176B98EBBCD00635EED9AFD9444F6C759CA24,
	LeanFormatString_SetString_m9C6E82EE5FE63366EAFD04D22746790647A5AEAD,
	LeanFormatString_SetString_m8031C96BBB16DD6368CA6927A7E40FB79B1524DF,
	LeanFormatString_SetString_mE42B62823E25A93A7E657F720F6ECCE6E1B34EE9,
	LeanFormatString_SetString_mD779B6F5DD7178CD857B422242EFA01C1DE25C99,
	LeanFormatString_SetString_mC9A49868C45EC0A6253D0B321D8DAECEA8BBCF8D,
	LeanFormatString_SendString_m9851DD3FFE8B07C743265B961F78D4F8065B9A52,
	LeanFormatString_SendString_m07D565F1CE9F6A76EE31FFC46C3B344AD653C419,
	LeanFormatString__ctor_mE9E92C890B48F40081B87F48C3F3752EE92F8DD1,
	StringEvent__ctor_m0EFECAE65155F17A84EA7E5F220261C6E03ABA0B,
	LeanPath_get_PointCount_mF5F7F552E76CE7E52AA8B9FA38760AF801136B88,
	LeanPath_GetPointCount_m904AC0FF40B2AE9750C9541C035CF128F9FB6094,
	LeanPath_GetSmoothedPoint_m7284BFFDC1350CD161F27992008413A28EB6A309,
	LeanPath_GetPoint_m809DDF4B617B43CB30CA6EF2C33AA8AD3A4D6659,
	LeanPath_GetPointRaw_mEFEC1A35269B12C264B32A53883F27E347327D19,
	LeanPath_SetLine_m5221734FC5F5B4482B46A24C5CC2C8355B16BB34,
	LeanPath_TryGetClosest_m961C2E4329FDA0D8F6B54898725ACBD69317C659,
	LeanPath_TryGetClosest_m445515693D3D0C0908FB0533F01DC6E59238C2B9,
	LeanPath_TryGetClosest_mC5CDF1FF80E51741388D1DE012E07E52B722485B,
	LeanPath_TryGetClosest_m150E1C0790AF8C436ED99E02AD665914ED0E54D8,
	LeanPath_TryGetClosest_m9EDCC556842AB6C4487B06DB5D37FA9C454DCD8C,
	LeanPath_GetClosestPoint_m35BCB88CADA5172B750A9C1DC015CF57704CDA95,
	LeanPath_GetClosestPoint_m8C1E2BD9CAB8AD2A3D281FADAA705D2A9D3224C4,
	LeanPath_GetClosestDistance_m25EDF5A03B96A0615B55298C1458859FC6253AC8,
	LeanPath_Mod_mA52316BA69AE1DC6A8B5F15D76589B4055DCFA66,
	LeanPath_CubicInterpolate_m1AA1DCBB91FEE95D41E5082FD32446ED979B4D84,
	LeanPath_UpdateVisual_mC4B1C0DD129E0D88C1ED43B4F29B4A1B5DEE8202,
	LeanPath_Update_m87AF4F8022BC7ECB2057DA94C1932B3E2392D673,
	LeanPath__ctor_m50BA53BF46D63335083336D43580FF76737CB380,
	LeanPath__cctor_m17D84FAF9DA3FD05C14B36E46B09BB4928292D36,
	LeanPlane_GetClosest_m31305D784A9F385A4030A64D6145AB7E9E7B1848,
	LeanPlane_TryRaycast_m082ED056209FFFD090AE7F6331C2341D56EEC608,
	LeanPlane_GetClosest_mF04997CFE9B18AEEA1DF3C31E19EFFB4DDB278F2,
	LeanPlane_RayToPlane_mBD9C38BE30C7D51157BEAE1270C63709A453D8BB,
	LeanPlane__ctor_m6136ABF83F1F703CF82F2CA908BD8962CE681CEA,
	LeanRoll_set_Angle_mE887D61B151551F0514A8AE3CA243E87A8910B8C,
	LeanRoll_get_Angle_mEAE28B2DBAAB93691705B724E3B560E3C2C4BDE2,
	LeanRoll_set_Clamp_m0907C0FC8FA63A2DF12B97E2125C24B70CBB6E18,
	LeanRoll_get_Clamp_m88E823DC225A2981FC017909BCAA471849F5FB31,
	LeanRoll_set_ClampMin_mACCED40B2FC690691CF51DE4E658DF62845D5D46,
	LeanRoll_get_ClampMin_m6EE98C23D42A1A116B549C27E9A9CDAAFED17169,
	LeanRoll_set_ClampMax_m9087288BFB7F016D5AE8C16D77CAD2FE92E3D9D1,
	LeanRoll_get_ClampMax_mE4BDF0B55CF54A9DD139E23EA5E376A113C6CE45,
	LeanRoll_set_Damping_m18C84B8122542D69247EB83C97C3D0A7FDA57CCA,
	LeanRoll_get_Damping_mC449B549B4AF7D89E4456883D6FCB5BAA2CA4C84,
	LeanRoll_IncrementAngle_m3F6D6F82CDF3857CC56A3B3BB08EFBEA7BAC6375,
	LeanRoll_DecrementAngle_mF980FD369DC11AF3E44E8FC7176A74C776C01AA7,
	LeanRoll_RotateToDelta_mDB7CFA88C6866D016C8867A84F9EDBD7663A4062,
	LeanRoll_SnapToTarget_m38DE0B392D53013FAF766B9975F04E3BCA9226F1,
	LeanRoll_Start_m9AC4D7BB13467134AF2E8C78F60FBB04966DF729,
	LeanRoll_Update_m05A1404797C5EE0BB5F3A6A3AD72A3520E5CD354,
	LeanRoll__ctor_m241DAF9B4D59659AEF203451CDF3B63E0BA24977,
	LeanSelect_set_DeselectWithNothing_m96D5EAE0EEB615F298F2644201276366A760536C,
	LeanSelect_get_DeselectWithNothing_m6E5FC30F06BAE82F950E1A6705665BCC659082D5,
	LeanSelect_set_Limit_m1DA527E7B5FF03948AD4B561A0F5FB5031C04964,
	LeanSelect_get_Limit_m47EA6BE60E037C5C994F01A81C728C0D0D01FECB,
	LeanSelect_set_MaxSelectables_m2E43A08FDDE829DAC1E44349ED6809ECDB2DF329,
	LeanSelect_get_MaxSelectables_m1A6011E15712E78F14DC334A12E411EEFC7FFFC8,
	LeanSelect_set_Reselect_mA5CAFC19DB17E24C7BBD30039066CE18ABBE22C8,
	LeanSelect_get_Reselect_m21BB1AFD8981E47B83CEF1170EEB8032AFE562BB,
	LeanSelect_get_Selectables_m0541739B28A2D6DE8DDB291B67F36C3D4BB7255C,
	LeanSelect_get_OnSelected_m3C80CDF2BB1C018C1FD67A55D148EDA9E9FFC907,
	LeanSelect_get_OnDeselected_m533685C0474A82E4171383017FCB2EF3F6FB3EC8,
	LeanSelect_get_OnNothing_m3A87EBDF988FCC05BAC09023CF229676FD836D88,
	LeanSelect_add_OnAnySelected_mCBC3335FADE43EE74713529033E1862D9A4FA7CB,
	LeanSelect_remove_OnAnySelected_mE81C8107A509B588D2844CFB9D9DAE50930405EE,
	LeanSelect_add_OnAnyDeselected_m8D59CD367791FED7F05E552087DEAAA38EAE5D9B,
	LeanSelect_remove_OnAnyDeselected_m92803FDD673B11B87F9C54B31A277648D8A08C83,
	LeanSelect_IsSelected_m882CF8026A3B01FE1F1A5374536481252A04FC16,
	LeanSelect_Select_mB2CE2E0E829BA535BBDB80F7BC38054655C5D150,
	LeanSelect_Deselect_mD29FDB8833FDBCE9CD91833CBE489E52D001C628,
	LeanSelect_TrySelect_mAA0CABFECC04FE41D594CFF3B1D944E738AE35E9,
	LeanSelect_TryReselect_m2F88C4B33A2F49E72EAC9FF4321E905D10A06D2D,
	LeanSelect_TryDeselect_m908988308BE386DB0EE1E3FC7D7C465CDBBEBB5F,
	LeanSelect_TryDeselect_m1F043BCFE0AF219FE3D252F4F5E63F7BD25E455D,
	LeanSelect_DeselectAll_mFE5E913DADC65729C87BE27A69D04658F3785E18,
	LeanSelect_Cull_m74A83CB52DDCA433C22E4F9830F22A8E20A078F2,
	LeanSelect_OnEnable_m3C1CA4EABCE9D1BEE4D216FF5421E07A393E46D2,
	LeanSelect_OnDisable_m89F60E7601E09C57440F0411CEEE9EDD563D9334,
	LeanSelect_OnDestroy_mAA479854F63C0D809850EC48D625DBCC75CDC04E,
	LeanSelect__ctor_m1D98D8A3CDA79B09257B6715C3D6A127D0FB09B9,
	LeanSelect__cctor_m16B8DE2064E0D0191A004953151CAFD300A627A4,
	LeanSelectableEvent__ctor_mF85F656B71FB189CE41973B9F90133305E4889DF,
	LeanSelectable_set_SelfSelected_m3B119374E382EB78103F159BFC08C1DDB8220186,
	LeanSelectable_get_SelfSelected_m092BB429EE8A9983CEB84B287CAFCE436DE4EF75,
	LeanSelectable_get_OnSelected_mEE84613146ACD47032B93CB4E8988F526A15988B,
	LeanSelectable_get_OnDeselected_m055BD9B88E71CAB5E6C4D429CA271308F56EBC29,
	LeanSelectable_add_OnAnyEnabled_mE8C99B3F6FEDF172E77B2BDAB39FD1FE08D91BF8,
	LeanSelectable_remove_OnAnyEnabled_mD48231C5388141EEEA0A98289D82D46759507577,
	LeanSelectable_add_OnAnyDisabled_m7D9AF4A62DC7189BEA8AD97B3B0E09AEF07224B8,
	LeanSelectable_remove_OnAnyDisabled_m978AF768489C7EE61BC56590D1E359146315D87B,
	LeanSelectable_add_OnAnySelected_m06916055558D3230F3988C01A8097171E7A39248,
	LeanSelectable_remove_OnAnySelected_m9F438F7C15015AEC827AB0C6BCD00A208FAB71E3,
	LeanSelectable_add_OnAnyDeselected_mFA9DE7F64AB71F46D2AAA77E58486D57EDB507B2,
	LeanSelectable_remove_OnAnyDeselected_m5B2A4C22F2850FDCED2C7A808D26CBE3BDD8C35D,
	LeanSelectable_get_SelectedCount_m68F30EB52466B6A0AB9F60449D8E9713457B3940,
	LeanSelectable_get_IsSelected_m55508746D046549000EB2085D5EA859B5ED855CB,
	LeanSelectable_get_IsSelectedCount_m91D6757A7652F1D8AA7CCC847DD6021915783E77,
	LeanSelectable_Deselect_mCB84D48D0F71C8345F70321595197109042D2A27,
	LeanSelectable_DeselectAll_m4D43396ED0D131513830F2A49E7742F009F4BE08,
	LeanSelectable_InvokeOnSelected_m3C81335F17D41C2F14CEBF38F31B275E59D6FAE9,
	LeanSelectable_InvokeOnDeslected_m6B165447EB743395D368821C3E598822DFDEA7DF,
	LeanSelectable_OnEnable_m2BECBBA774A3DA623EDB2C6482D65882947FA6FD,
	LeanSelectable_OnDisable_m59F3853E5EFDA75B2E35DA0E37F18E2C4E736680,
	LeanSelectable_OnDestroy_mFE969691889D2B97EC1CE32A980EEFAA9E244CCD,
	LeanSelectable__ctor_m64F3EFA449DC2206C65BFD65AFB8A5C0B2B0E738,
	LeanSelectable__cctor_m0DB2F5F487EE2D3579644575756DA358869D5286,
	LeanSelectEvent__ctor_m298B432186C5138B38B93C71D379689F44C0B204,
	LeanSelectableBehaviour_get_Selectable_mDA4E928E7FE887A93CF29BBC0EDD11483D52B97E,
	LeanSelectableBehaviour_Register_m59BCA4108A01A335DA517DC30A64C29ADB64DB80,
	LeanSelectableBehaviour_Register_mDADF1B6384CBA28AC26D2161AA5831FC1D7B9747,
	LeanSelectableBehaviour_Unregister_m21AA701DA1065421693A87AA7CD10DB32AD9318C,
	LeanSelectableBehaviour_OnEnable_m11C1074ED7800B1AE7FF3355ED6F6B564EF91D9D,
	LeanSelectableBehaviour_Start_mCC6D17C6D1520EA1C4BAF1C4F3BB0801ECE7402B,
	LeanSelectableBehaviour_OnDisable_mFDF5E11937776F863480234CA5DB2103F0CEDCE5,
	LeanSelectableBehaviour_OnSelected_m4037909C4E9F254A345795C9351B21884FF52248,
	LeanSelectableBehaviour_OnDeselected_m6D0284D8BDCE6F3596579DB616EB621060A10039,
	LeanSelectableBehaviour__ctor_m24001ACBC3695134BE8306864960F29F6A4573B9,
	LeanSelectableGraphicColor_set_DefaultColor_m04E4A634432FEA2A06EA9D4A206A923C7235C8A8,
	LeanSelectableGraphicColor_get_DefaultColor_m471842FCD7B965642321981E8443ECE91D3A92B9,
	LeanSelectableGraphicColor_set_SelectedColor_mF266529928AD6E74DD5AAC127B8B66B4C853F084,
	LeanSelectableGraphicColor_get_SelectedColor_m1FC1925B3DEBA9C0DDADF948E09E70AF2CDBAE7B,
	LeanSelectableGraphicColor_OnSelected_m105017121C7C20B4B3A407FA3E8EC0BD99F2A696,
	LeanSelectableGraphicColor_OnDeselected_mBBEF2C2E5D4B9CF53858B4723E8D466B92379BFF,
	LeanSelectableGraphicColor_UpdateColor_m8C4B31A043489DD4DA3BFC8DAA3CC0F985E3FE41,
	LeanSelectableGraphicColor__ctor_m3B890E4A02B1C0E7EC5969CA311D4CC7F03434B1,
	LeanSelectableRendererColor_set_DefaultColor_m1CCCA7BD2A7E89B727F6EBEF14240BFB0553DAB7,
	LeanSelectableRendererColor_get_DefaultColor_mBBA833F9D97DD57E02CA62F5CB3E06ACCD6292EF,
	LeanSelectableRendererColor_set_SelectedColor_mDED68B9DAD22DF8C010784056B6CB8EE59489E0C,
	LeanSelectableRendererColor_get_SelectedColor_m1789A269D339ADEE19BC55F124E9CA04FF33C81B,
	LeanSelectableRendererColor_OnSelected_mBDD16192639BDC6D919A7124D386F89C4B9218F5,
	LeanSelectableRendererColor_OnDeselected_mADD2E795E00EA66A0CBA84232DB469B563BB71F4,
	LeanSelectableRendererColor_Start_mB4C768F2029D6F8D4EB2B2424CD86B1D51095818,
	LeanSelectableRendererColor_UpdateColor_mBB4BFB9173F24C7C758D291DC6AAFA77F91F484C,
	LeanSelectableRendererColor__ctor_m514753D2E70569C0B05EE8674786B76B4CA8E846,
	LeanSelectableSpriteRendererColor_set_DefaultColor_m276192DD1E9B19CD0799180F6BB9247572F4232B,
	LeanSelectableSpriteRendererColor_get_DefaultColor_m8D2CBDCC7577986F67D9C04DD9F11D40EF76BD0D,
	LeanSelectableSpriteRendererColor_set_SelectedColor_mD61014AF5613A92CDE8668CAD70DB68C02A3ADF1,
	LeanSelectableSpriteRendererColor_get_SelectedColor_m49805D8A311F4B734CA86528C2BAE5437452E806,
	LeanSelectableSpriteRendererColor_OnSelected_mA0482CB6294A989841715178F2BC601F49C77477,
	LeanSelectableSpriteRendererColor_OnDeselected_m7C8E1081624ABE0358F0CB6B357E7694A431D98A,
	LeanSelectableSpriteRendererColor_UpdateColor_mB959E44683EB3D85A96956DB84A4AEB79A124D2B,
	LeanSelectableSpriteRendererColor__ctor_mD0A401E22773FF1503E454D73F1E7A2B9E57BC2E,
	LeanSpawn_set_Prefab_m9EF621030E54E351A4BA12F6A3F02B3CBA712EDC,
	LeanSpawn_get_Prefab_mB50202E0832F14C4C9A0599EE005812CA96B7424,
	LeanSpawn_set_DefaultPosition_mC45A0DC850B31A783E27971EE2539D7EC218BD3C,
	LeanSpawn_get_DefaultPosition_m977F92325E8630DF382313785A9565D373C1E887,
	LeanSpawn_set_DefaultRotation_m3332F942D2F460104B36D1F039571557661982C1,
	LeanSpawn_get_DefaultRotation_m71C99E2B15F8D895EE8055F9E3D87A2B8D76732F,
	LeanSpawn_Spawn_m4B2658D62A3E17E4D8EC41B5606010E05445CE16,
	LeanSpawn_Spawn_mB6149967EADA16BBA46AD9B5580384B79ED8E5FD,
	LeanSpawn__ctor_mAD61599D9F51F60C2A90BB4EDA5813A6CB7BEF17,
	LeanCommon_Hermite_m933D6F37B112788EA496FEEC7663E6553B0CC3F3,
	LeanCommon_HermiteInterpolate_m18965FC0DCD5D7A0F11486C2497BCDCD4CC205B2,
	LeanScreenQuery__ctor_m59065C77ACB27AC2072C31D92B4D418EC9C60A85,
	LeanScreenQuery__ctor_m96FE813B432186035779FED836415F1E1E16B23A,
	LeanScreenQuery_ChangeLayers_m0FE445BF645A2E5B913D4ABE847CCE70C1F87D25,
	LeanScreenQuery_RevertLayers_mC14E59AC2206E7FA05F7BC48F400DCC67858FA48,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	LeanScreenQuery_GetClosestRaycastHitsIndex_mCA37AA7178583AABC833765E4D9434EBA5BC91E5,
	LeanScreenQuery_DoRaycast3D_m79EEB1F96BF3BB4CC4BCB70E9F1667E4CA7D993E,
	LeanScreenQuery_DoRaycast2D_m57CB1F6A5464D8D88EB1E5E83DA7BA15BAB854B7,
	LeanScreenQuery_DoRaycastUI_m82C6B1269710BCB6377C4F1D25B4B4351DB89B99,
	LeanScreenQuery__cctor_m14B930C96207D6A19AB70FBC4871869A6005184D,
};
extern void Axis__ctor_m37815A1FAA240248DCE733A7077C418505123BA1_AdjustorThunk (void);
extern void Axis_GetValue_m4A7707296DE15BA3B328FC217F4885769C2F6F72_AdjustorThunk (void);
extern void Trigger__ctor_m92E19461A441EB3009409F51BE4B904019FE1A4D_AdjustorThunk (void);
extern void Trigger_WentDown_mECBF1F265D4863FBB00BA61D66AC3E2085501AC4_AdjustorThunk (void);
extern void Trigger_IsDown_mDFDD741E21AEAA27CC3D0B9E259157BE5E9F1660_AdjustorThunk (void);
extern void Trigger_WentUp_m20E4670475591BEF55514EB2B956EFCB09D00B97_AdjustorThunk (void);
extern void LeanScreenDepth__ctor_m317000555909FB83237D72C83E4CE97B9BD82BE3_AdjustorThunk (void);
extern void LeanScreenDepth_Convert_m7A9AD8A2702E65EAEC8757784C7796C0F54A77FB_AdjustorThunk (void);
extern void LeanScreenDepth_ConvertDelta_m2832CD0C407CBCFDFAB2BE1B8D91C2380397F299_AdjustorThunk (void);
extern void LeanScreenDepth_TryConvert_m37BE63E86D3A1DA403AEA64BEDD802C3B5A883A6_AdjustorThunk (void);
extern void LeanScreenQuery__ctor_m59065C77ACB27AC2072C31D92B4D418EC9C60A85_AdjustorThunk (void);
extern void LeanScreenQuery__ctor_m96FE813B432186035779FED836415F1E1E16B23A_AdjustorThunk (void);
extern void LeanScreenQuery_DoRaycast3D_m79EEB1F96BF3BB4CC4BCB70E9F1667E4CA7D993E_AdjustorThunk (void);
extern void LeanScreenQuery_DoRaycast2D_m57CB1F6A5464D8D88EB1E5E83DA7BA15BAB854B7_AdjustorThunk (void);
extern void LeanScreenQuery_DoRaycastUI_m82C6B1269710BCB6377C4F1D25B4B4351DB89B99_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[15] = 
{
	{ 0x0600009C, Axis__ctor_m37815A1FAA240248DCE733A7077C418505123BA1_AdjustorThunk },
	{ 0x0600009D, Axis_GetValue_m4A7707296DE15BA3B328FC217F4885769C2F6F72_AdjustorThunk },
	{ 0x0600009E, Trigger__ctor_m92E19461A441EB3009409F51BE4B904019FE1A4D_AdjustorThunk },
	{ 0x0600009F, Trigger_WentDown_mECBF1F265D4863FBB00BA61D66AC3E2085501AC4_AdjustorThunk },
	{ 0x060000A0, Trigger_IsDown_mDFDD741E21AEAA27CC3D0B9E259157BE5E9F1660_AdjustorThunk },
	{ 0x060000A1, Trigger_WentUp_m20E4670475591BEF55514EB2B956EFCB09D00B97_AdjustorThunk },
	{ 0x06000200, LeanScreenDepth__ctor_m317000555909FB83237D72C83E4CE97B9BD82BE3_AdjustorThunk },
	{ 0x06000201, LeanScreenDepth_Convert_m7A9AD8A2702E65EAEC8757784C7796C0F54A77FB_AdjustorThunk },
	{ 0x06000202, LeanScreenDepth_ConvertDelta_m2832CD0C407CBCFDFAB2BE1B8D91C2380397F299_AdjustorThunk },
	{ 0x06000203, LeanScreenDepth_TryConvert_m37BE63E86D3A1DA403AEA64BEDD802C3B5A883A6_AdjustorThunk },
	{ 0x060003D9, LeanScreenQuery__ctor_m59065C77ACB27AC2072C31D92B4D418EC9C60A85_AdjustorThunk },
	{ 0x060003DA, LeanScreenQuery__ctor_m96FE813B432186035779FED836415F1E1E16B23A_AdjustorThunk },
	{ 0x060003E4, LeanScreenQuery_DoRaycast3D_m79EEB1F96BF3BB4CC4BCB70E9F1667E4CA7D993E_AdjustorThunk },
	{ 0x060003E5, LeanScreenQuery_DoRaycast2D_m57CB1F6A5464D8D88EB1E5E83DA7BA15BAB854B7_AdjustorThunk },
	{ 0x060003E6, LeanScreenQuery_DoRaycastUI_m82C6B1269710BCB6377C4F1D25B4B4351DB89B99_AdjustorThunk },
};
static const int32_t s_InvokerIndices[999] = 
{
	2859,
	3421,
	2957,
	3520,
	2957,
	3520,
	3019,
	3589,
	3019,
	3589,
	3019,
	3589,
	3576,
	3576,
	3576,
	2859,
	3576,
	3576,
	3576,
	2859,
	3421,
	2957,
	3520,
	2957,
	3520,
	3019,
	3589,
	3019,
	3589,
	3019,
	3589,
	3576,
	3576,
	3576,
	3576,
	3576,
	2859,
	3421,
	2957,
	3520,
	3019,
	3589,
	3019,
	3589,
	3576,
	3576,
	3576,
	3576,
	3576,
	2859,
	3421,
	2859,
	3421,
	2859,
	3421,
	2859,
	3421,
	2859,
	3421,
	2859,
	3421,
	2859,
	3421,
	2859,
	3421,
	3576,
	3576,
	3576,
	3576,
	3576,
	3576,
	3576,
	3576,
	2906,
	3462,
	2925,
	3483,
	2925,
	3483,
	2906,
	3462,
	3576,
	3576,
	2925,
	5429,
	5429,
	5355,
	3576,
	2925,
	3483,
	2925,
	3483,
	2925,
	3483,
	2862,
	3423,
	2925,
	3483,
	3576,
	3576,
	3483,
	3576,
	2906,
	3462,
	3576,
	3576,
	3576,
	2906,
	3462,
	2925,
	3483,
	2957,
	3520,
	2859,
	3421,
	2859,
	3421,
	2906,
	3462,
	3000,
	3568,
	3000,
	3568,
	3576,
	3576,
	3576,
	3576,
	2912,
	3468,
	5359,
	5359,
	5359,
	5359,
	5359,
	5359,
	5437,
	5453,
	4810,
	4707,
	4847,
	5336,
	5336,
	5336,
	5336,
	5263,
	5466,
	3576,
	2582,
	564,
	3938,
	3718,
	4882,
	4461,
	3576,
	5466,
	17,
	2634,
	802,
	2080,
	2080,
	1001,
	0,
	0,
	0,
	0,
	3576,
	3576,
	3520,
	2701,
	3576,
	2957,
	3520,
	2957,
	3520,
	2957,
	3520,
	2957,
	3520,
	3483,
	3576,
	2957,
	3576,
	3000,
	3568,
	2906,
	3462,
	3576,
	3576,
	3576,
	0,
	3576,
	3576,
	0,
	5416,
	5437,
	5437,
	3576,
	3576,
	3576,
	5466,
	3576,
	3483,
	3483,
	3576,
	5359,
	5359,
	5359,
	5359,
	5466,
	0,
	0,
	4654,
	4826,
	5341,
	5077,
	5466,
	5355,
	5466,
	4312,
	5091,
	5265,
	5091,
	0,
	4881,
	5091,
	5265,
	5091,
	5265,
	5271,
	5359,
	5466,
	5359,
	4975,
	5359,
	5359,
	4975,
	4975,
	4975,
	3760,
	4826,
	4826,
	4826,
	0,
	4142,
	3712,
	0,
	5265,
	5116,
	4881,
	4737,
	5265,
	5116,
	4881,
	4462,
	5270,
	4764,
	4881,
	3919,
	5466,
	3576,
	2925,
	2925,
	1662,
	1662,
	5429,
	3946,
	5463,
	5074,
	5074,
	5074,
	5074,
	5074,
	5074,
	5453,
	5416,
	5416,
	2925,
	3483,
	2925,
	3483,
	2906,
	3462,
	2906,
	3462,
	3483,
	3421,
	5429,
	5429,
	5074,
	5074,
	5074,
	5074,
	3576,
	3483,
	3576,
	3000,
	3568,
	2957,
	3520,
	2957,
	3520,
	2957,
	3520,
	2957,
	3520,
	3576,
	3576,
	3576,
	3576,
	2925,
	2925,
	2925,
	2925,
	2925,
	2925,
	3576,
	2957,
	3520,
	2957,
	3520,
	2957,
	3520,
	3576,
	2925,
	2925,
	3576,
	3576,
	3576,
	3576,
	2925,
	3483,
	2906,
	3462,
	2957,
	3520,
	2862,
	3423,
	2862,
	3423,
	2925,
	2925,
	3576,
	3576,
	3576,
	3576,
	3576,
	893,
	2925,
	3576,
	3576,
	2925,
	3483,
	2957,
	3520,
	2957,
	3520,
	2957,
	3520,
	2925,
	2925,
	3576,
	3576,
	3576,
	2998,
	2998,
	3576,
	0,
	0,
	0,
	0,
	0,
	0,
	3576,
	2859,
	3421,
	2906,
	3462,
	2925,
	3483,
	3483,
	3483,
	3483,
	3576,
	3576,
	3576,
	2080,
	2925,
	2925,
	2080,
	3576,
	3576,
	3576,
	3576,
	2859,
	279,
	2925,
	2925,
	2925,
	3576,
	2580,
	5077,
	2859,
	3421,
	2859,
	3421,
	2925,
	3483,
	3483,
	3483,
	3483,
	3576,
	3576,
	3576,
	2925,
	3576,
	3576,
	3576,
	3576,
	2859,
	3421,
	2859,
	3421,
	2925,
	3483,
	3576,
	3576,
	3576,
	2925,
	3576,
	2859,
	3421,
	2859,
	3421,
	2925,
	3483,
	2906,
	3462,
	2906,
	3462,
	3483,
	3483,
	3483,
	3483,
	3576,
	3576,
	3576,
	2925,
	3576,
	3576,
	3576,
	3576,
	3576,
	2859,
	3421,
	3576,
	3576,
	2080,
	2925,
	2925,
	3576,
	2859,
	3421,
	2859,
	3421,
	2859,
	3421,
	2859,
	3421,
	2859,
	3421,
	2859,
	3421,
	2925,
	3483,
	3483,
	2906,
	3462,
	2957,
	3520,
	3483,
	3483,
	3483,
	3483,
	3483,
	3483,
	3576,
	3576,
	3576,
	2925,
	3576,
	3576,
	3576,
	3576,
	3576,
	3576,
	2925,
	3483,
	2859,
	3421,
	2957,
	3520,
	2957,
	3520,
	2925,
	2925,
	3576,
	3576,
	3576,
	1666,
	1666,
	3576,
	845,
	778,
	529,
	327,
	0,
	4666,
	5466,
	2906,
	3462,
	3483,
	3483,
	3483,
	3483,
	5359,
	5359,
	3483,
	3483,
	2925,
	4126,
	5214,
	2080,
	4570,
	3576,
	3576,
	3421,
	2925,
	3576,
	3576,
	3576,
	3483,
	3576,
	2925,
	3576,
	3576,
	3576,
	3576,
	2925,
	1637,
	1637,
	2925,
	3576,
	2859,
	3421,
	3483,
	5359,
	5359,
	2925,
	2925,
	1648,
	1637,
	3576,
	2080,
	1637,
	3576,
	3576,
	2957,
	3520,
	2957,
	3520,
	3483,
	2906,
	3462,
	2906,
	3462,
	2957,
	3520,
	3483,
	3483,
	3483,
	3483,
	3483,
	3483,
	2166,
	903,
	3576,
	3576,
	3576,
	3576,
	3576,
	3576,
	2906,
	3462,
	2906,
	3462,
	2906,
	3462,
	2925,
	3483,
	3576,
	3576,
	3576,
	3576,
	3576,
	2925,
	3483,
	2859,
	3421,
	2957,
	3520,
	2925,
	2925,
	3576,
	3576,
	3576,
	1666,
	1666,
	2957,
	2957,
	3576,
	3000,
	3568,
	2906,
	3462,
	2957,
	3520,
	2925,
	2925,
	3576,
	3576,
	3576,
	3576,
	3421,
	3520,
	3421,
	3421,
	3421,
	3566,
	3566,
	3566,
	3566,
	3566,
	3566,
	2701,
	3520,
	2605,
	2605,
	2701,
	2701,
	2701,
	777,
	2636,
	2636,
	2636,
	2636,
	2636,
	1294,
	2636,
	1294,
	2636,
	2636,
	2636,
	2636,
	2636,
	2636,
	1309,
	1309,
	1309,
	1309,
	777,
	2906,
	3576,
	3576,
	5463,
	5336,
	4663,
	5463,
	5336,
	4663,
	5463,
	5336,
	4663,
	5463,
	5336,
	4663,
	5463,
	5336,
	4663,
	4928,
	4486,
	4007,
	5453,
	5263,
	4878,
	4296,
	5453,
	5263,
	4878,
	4296,
	5453,
	5263,
	4878,
	4296,
	5453,
	5263,
	4878,
	4296,
	5453,
	5263,
	4878,
	4296,
	5453,
	5263,
	4878,
	4296,
	5265,
	4877,
	3826,
	5265,
	4877,
	3826,
	5453,
	5263,
	4461,
	4010,
	5453,
	5263,
	4461,
	4010,
	1309,
	5437,
	4295,
	3998,
	4773,
	3576,
	5466,
	5359,
	5359,
	5359,
	5359,
	5359,
	5359,
	5359,
	5359,
	5359,
	5359,
	5359,
	5359,
	5359,
	5359,
	5359,
	5359,
	5359,
	5359,
	2925,
	2925,
	2957,
	3520,
	5453,
	2957,
	3520,
	5453,
	2906,
	3462,
	5429,
	2912,
	3468,
	5432,
	2859,
	3421,
	2859,
	3421,
	2859,
	3421,
	2859,
	3421,
	2859,
	3421,
	2859,
	3421,
	2957,
	3520,
	2957,
	3520,
	5437,
	5453,
	5453,
	5416,
	4667,
	5437,
	5088,
	5224,
	4848,
	4125,
	4583,
	3576,
	3576,
	3576,
	3576,
	3576,
	1663,
	2957,
	2957,
	3576,
	3576,
	503,
	2582,
	2428,
	3576,
	5466,
	2906,
	3462,
	2925,
	3483,
	2957,
	3520,
	3576,
	3576,
	3576,
	2925,
	3483,
	3483,
	2925,
	1637,
	2906,
	1487,
	2957,
	1665,
	2998,
	1688,
	3000,
	1692,
	3001,
	1693,
	1664,
	1525,
	2925,
	1637,
	3576,
	3576,
	3462,
	2428,
	2705,
	1307,
	1307,
	1692,
	190,
	698,
	188,
	689,
	342,
	779,
	776,
	1290,
	1136,
	273,
	3576,
	3576,
	3576,
	5466,
	1311,
	343,
	1308,
	4020,
	3576,
	2957,
	3520,
	2859,
	3421,
	2957,
	3520,
	2957,
	3520,
	2957,
	3520,
	2957,
	2957,
	2998,
	3576,
	3576,
	3576,
	3576,
	2859,
	3421,
	2906,
	3462,
	2906,
	3462,
	2906,
	3462,
	3483,
	3483,
	3483,
	3483,
	5359,
	5359,
	5359,
	5359,
	2080,
	2925,
	2925,
	2080,
	2080,
	2080,
	2058,
	3576,
	2906,
	3576,
	3576,
	3576,
	3576,
	5466,
	3576,
	2859,
	3421,
	3483,
	3483,
	5359,
	5359,
	5359,
	5359,
	5359,
	5359,
	5359,
	5359,
	3462,
	3421,
	5429,
	3576,
	5466,
	2925,
	2925,
	3576,
	3576,
	3576,
	3576,
	5466,
	3576,
	3483,
	3576,
	2925,
	3576,
	3576,
	3576,
	3576,
	2925,
	2925,
	3576,
	2862,
	3423,
	2862,
	3423,
	2925,
	2925,
	3576,
	3576,
	2862,
	3423,
	2862,
	3423,
	2925,
	2925,
	3576,
	3576,
	3576,
	2862,
	3423,
	2862,
	3423,
	2925,
	2925,
	3576,
	3576,
	2925,
	3483,
	2906,
	3462,
	2906,
	3462,
	3576,
	3000,
	3576,
	3938,
	3718,
	2906,
	1493,
	4556,
	5466,
	0,
	0,
	0,
	0,
	0,
	0,
	5153,
	315,
	315,
	639,
	5466,
};
static const Il2CppTokenRangePair s_rgctxIndices[22] = 
{
	{ 0x060000A2, { 0, 7 } },
	{ 0x060000A3, { 7, 6 } },
	{ 0x060000A4, { 13, 8 } },
	{ 0x060000A5, { 21, 3 } },
	{ 0x060000D2, { 24, 3 } },
	{ 0x060000D3, { 27, 1 } },
	{ 0x060000DF, { 28, 5 } },
	{ 0x060000F3, { 33, 1 } },
	{ 0x060000F6, { 34, 3 } },
	{ 0x0600016F, { 37, 4 } },
	{ 0x06000170, { 41, 4 } },
	{ 0x06000171, { 45, 7 } },
	{ 0x06000172, { 52, 6 } },
	{ 0x06000173, { 58, 4 } },
	{ 0x06000174, { 62, 7 } },
	{ 0x06000204, { 69, 3 } },
	{ 0x060003DD, { 72, 1 } },
	{ 0x060003DE, { 73, 1 } },
	{ 0x060003DF, { 74, 4 } },
	{ 0x060003E0, { 78, 2 } },
	{ 0x060003E1, { 80, 2 } },
	{ 0x060003E2, { 82, 2 } },
};
extern const uint32_t g_rgctx_List_1_t9D5226C09539DCFE6F17980B2CA80246A767D71B;
extern const uint32_t g_rgctx_List_1_GetEnumerator_m11BC00E3B94F5DB96A1DC725C3F805DCA16A3DE3;
extern const uint32_t g_rgctx_Enumerator_get_Current_m69B86CD1EB610BBB35286EC5E147DBF647791D0C;
extern const uint32_t g_rgctx_T_t0C181BCE5EED2D591C73325C810382694033733B;
extern const uint32_t g_rgctx_Enumerator_MoveNext_mA21E18CED0004EB349F92DBDEA5E006E5F72F4C6;
extern const uint32_t g_rgctx_Enumerator_tE49C91A74F705644F08B09D1CB5A49EEE9B905B3;
extern const Il2CppRGCTXConstrainedData g_rgctx_Enumerator_tE49C91A74F705644F08B09D1CB5A49EEE9B905B3_IDisposable_Dispose_m3C902735BE731EE30AC1185E7AEF6ACE7A9D9CC7;
extern const uint32_t g_rgctx_Link_Find_TisT_t6832FED987DCE0C89C04A83EF186035EF9C33529_m97563FC7EDB2FD284C7E2DEEB78E2F33CFA61EC1;
extern const uint32_t g_rgctx_T_t6832FED987DCE0C89C04A83EF186035EF9C33529;
extern const uint32_t g_rgctx_List_1_t6EA554F03B518C8B043AB80D1212A1598EE1B71D;
extern const uint32_t g_rgctx_List_1__ctor_m4E2F9C7CC0952B7F1CBDB471CFE8E0A294F39584;
extern const uint32_t g_rgctx_Activator_CreateInstance_TisT_t6832FED987DCE0C89C04A83EF186035EF9C33529_m19C88AF574A71C93D7C8E8447FA1305DE11A2577;
extern const uint32_t g_rgctx_List_1_Add_m3F8B933715E4A4A4E5307D79CD960AA377FC4140;
extern const uint32_t g_rgctx_List_1_tFB4490DBAB4EFFD4006F713CC448FE4C27C0AF1E;
extern const uint32_t g_rgctx_List_1_GetEnumerator_m3769AD723DBDDA37B64EFC8B1293796B1715630E;
extern const uint32_t g_rgctx_Enumerator_get_Current_mBE0BB255C9F5F96B899793909A4F3FE5174C5377;
extern const uint32_t g_rgctx_T_t29B68F7EE86934AA36549B2A5B00371C54DFD897;
extern const uint32_t g_rgctx_Enumerator_MoveNext_m8732D41B144FBF575A921AA00D4EA16E23FDDD3F;
extern const uint32_t g_rgctx_Enumerator_tAFB989EE0F6D38C61A944B74FFC3B36672E2BFFF;
extern const Il2CppRGCTXConstrainedData g_rgctx_Enumerator_tAFB989EE0F6D38C61A944B74FFC3B36672E2BFFF_IDisposable_Dispose_m3C902735BE731EE30AC1185E7AEF6ACE7A9D9CC7;
extern const uint32_t g_rgctx_List_1_Clear_mEDEBB4A20BA424496C55206C9D31676EBB1384C8;
extern const uint32_t g_rgctx_T_tA38F61C0E6F11818D1CB5408059D7A1344B08635;
extern const uint32_t g_rgctx_List_1_tE82603D39C58B31C9BEE715279825DB0FDED59BC;
extern const uint32_t g_rgctx_List_1_Remove_mA80BE859BDB4369EBD4E6190E75F07AAD6E6C239;
extern const uint32_t g_rgctx_GameObject_GetComponent_TisT_tEC189863069A09F0DE623682D9BA09F16CFFB888_mB8D8B744B48690B28E21D0044616164F6E41FCCA;
extern const uint32_t g_rgctx_T_tEC189863069A09F0DE623682D9BA09F16CFFB888;
extern const uint32_t g_rgctx_CwHelper_AddComponent_TisT_tEC189863069A09F0DE623682D9BA09F16CFFB888_mB47104665F5E2328583B835EE20CFB878A51716E;
extern const uint32_t g_rgctx_GameObject_AddComponent_TisT_t7CB447CEA53FBEBFD427F2B303590A3D85F87143_m30B2A54A79EF10721547999EE40DC3A22F30ABDC;
extern const uint32_t g_rgctx_List_1_t82EC5530ED72B06125E9E92978658D2A21077E41;
extern const uint32_t g_rgctx_List_1_get_Count_m79F4D32D685EDD8F8386052E90E50888EB562EA2;
extern const uint32_t g_rgctx_List_1_RemoveRange_m65028C69635AD71188FE8021BF048CDEA255C733;
extern const uint32_t g_rgctx_List_1_set_Capacity_m6DF3442D9A9A067DFB899B3374B8A19DADBF2756;
extern const uint32_t g_rgctx_List_1_Add_m7E0321CCA9B3C31B3A6F9FCF1212316EE8AA1292;
extern const uint32_t g_rgctx_T_t54D3A915C10255852017076CAD645DD40F6A55A0;
extern const uint32_t g_rgctx_T_tB80BBD53C8EE38ADE31A6F90C4654A478BC6B8F2;
extern const uint32_t g_rgctx_GameObject_AddComponent_TisT_tB80BBD53C8EE38ADE31A6F90C4654A478BC6B8F2_mF9A399748E1A72C54930CAA11EC88FDFCB79A43A;
extern const uint32_t g_rgctx_T_tB80BBD53C8EE38ADE31A6F90C4654A478BC6B8F2;
extern const uint32_t g_rgctx_List_1_tF837CB0D235AEF9DAC706BEBBBC138851F6AFFF2;
extern const uint32_t g_rgctx_List_1_get_Count_m14EB754694642F605DB566C458C4E55544C9879B;
extern const uint32_t g_rgctx_List_1_get_Item_m14970D45CBED1F82D5591F64526D19EA51AAFAE0;
extern const uint32_t g_rgctx_T_tCC0B1655D736A7D1C2A0E5D1676098117092A94C;
extern const uint32_t g_rgctx_List_1_t48257785605EAAF9E58E0B082062DC31D2A21525;
extern const uint32_t g_rgctx_List_1_get_Count_m0A3CC3FBFDA56CB058C8FFC05A1754DDD54DBAF4;
extern const uint32_t g_rgctx_List_1_get_Item_m500BFFC96020526DE840DB3E662CB5BF100D0972;
extern const uint32_t g_rgctx_T_t78611A33058D5025311A23616D78CE0B040FFE99;
extern const uint32_t g_rgctx_List_1_t1DD57A4B701FEB82C591892ADA62D1E0AE5BB057;
extern const uint32_t g_rgctx_List_1_get_Count_mF41B8585912C2E945554A5D7674F736AAF73C1FC;
extern const uint32_t g_rgctx_List_1_get_Item_m195B116C6D16F7CD2BA9B125216954559DA09D86;
extern const uint32_t g_rgctx_T_t337CCD0F3829FDEBA3C54C70EFEE3EC8A743FD68;
extern const uint32_t g_rgctx_List_1_RemoveAt_m93B47E1E1D642722D055B6DB9431A949D046825B;
extern const uint32_t g_rgctx_Stack_1_t8B13CD080E3B8076F22748D84A77F5268B571145;
extern const uint32_t g_rgctx_Stack_1_Push_m4517936A62D87CB253A137D5E41A359ECA1CBDAC;
extern const uint32_t g_rgctx_List_1_t453195EF2077CD349F148CA8413C8A7967AC3E2B;
extern const uint32_t g_rgctx_List_1_get_Count_m6AD43FB68A17451FC76EF7BE18B4C1958FAA4452;
extern const uint32_t g_rgctx_List_1_get_Item_m1EF4E7103387DDE2728AA2BC441D606B350C2E62;
extern const uint32_t g_rgctx_Stack_1_tDCBB42471D165F332622A82366EAC7FCDBA0FEAB;
extern const uint32_t g_rgctx_Stack_1_Push_mDEAAA1B27E41EF8354835202FF78BB65CC4CCAB9;
extern const uint32_t g_rgctx_List_1_Clear_m6A9A4B9FE049AA4306BB697B918B6CC51CE5D5BB;
extern const uint32_t g_rgctx_List_1_t3C0DFD8812592894A652E0E9BED327AA39EB642E;
extern const uint32_t g_rgctx_List_1_get_Count_mD7FE631CFE8BB49E829822184382801CDD5A7606;
extern const uint32_t g_rgctx_List_1_get_Item_m7C31953EA2B5622F5C0C3E5BDBD630D9252CC9BD;
extern const uint32_t g_rgctx_T_tC514A105ABCAB98B1C47C0B1E22EB96CF0C96CA1;
extern const uint32_t g_rgctx_List_1_tAD1CD954071596F86579DA42E05D58633CBA911D;
extern const uint32_t g_rgctx_List_1__ctor_m25276695E32DB82CE13AF3B87C2B61F1FB6510CB;
extern const uint32_t g_rgctx_List_1_get_Count_m12D59112CB3B05F7B7A27F1852693C1E0097FF63;
extern const uint32_t g_rgctx_List_1_get_Item_mECFB84630533D18BEA2EEBF34061BBF9E4285A71;
extern const uint32_t g_rgctx_T_tD34B7E4DA8E218FFBBDE375D21267F473DF45F3B;
extern const uint32_t g_rgctx_Activator_CreateInstance_TisT_tD34B7E4DA8E218FFBBDE375D21267F473DF45F3B_mA3D7DE56D082728979C3FCB13D66143E4B7A9B0C;
extern const uint32_t g_rgctx_List_1_Add_mEFC54421F237238CA8A477A600FA3252D6F9FD97;
extern const uint32_t g_rgctx_T_tB4DD857F4AA856F2C514141EAC1B86B272CE5C5D;
extern const uint32_t g_rgctx_GameObject_GetComponentInParent_TisT_tB4DD857F4AA856F2C514141EAC1B86B272CE5C5D_m7C24D95971C2BB01075A5B8AD47C5F1BCDAA8204;
extern const uint32_t g_rgctx_Object_FindObjectOfType_TisT_tB4DD857F4AA856F2C514141EAC1B86B272CE5C5D_m4C93CE64584F34BF1E9F761AC5F50DFA4660CDA8;
extern const uint32_t g_rgctx_LeanScreenQuery_TryQuery_TisT_t65FBEF80A81DA8E283E7DE065D4E00633CD8D60D_mF4F6C06155DC2410BA99DC980459D762DBAB6962;
extern const uint32_t g_rgctx_LeanScreenQuery_TryResult_TisT_tC4D399A737A6D9D5784BB1EEDE747379A5D4ED1B_m3F33700555D96AF4A6B5E085130265A48A171AFE;
extern const uint32_t g_rgctx_LeanScreenQuery_TryGetComponent_TisT_t6C912E89D45C47254CA356C121BB1F0DE5F05D34_mE6CA9D82F29A5CAF6714399068FA06E72D280A7B;
extern const uint32_t g_rgctx_LeanScreenQuery_TryGetComponentInParent_TisT_t6C912E89D45C47254CA356C121BB1F0DE5F05D34_mBBB6608B84C4B20C3E94FDC7F16F78E35530CC38;
extern const uint32_t g_rgctx_LeanScreenQuery_TryGetComponentInChildren_TisT_t6C912E89D45C47254CA356C121BB1F0DE5F05D34_m90537D1B9BDF2A9676B1E41E924987F86DE098AA;
extern const uint32_t g_rgctx_T_t6C912E89D45C47254CA356C121BB1F0DE5F05D34;
extern const uint32_t g_rgctx_Component_GetComponent_TisT_t76D96A5C0143AC1F038AD1E39B9DA20AAE6BDD3E_m0E43A495066B10C1CAE57CBB4600B99E2D01EBDB;
extern const uint32_t g_rgctx_T_t76D96A5C0143AC1F038AD1E39B9DA20AAE6BDD3E;
extern const uint32_t g_rgctx_LeanScreenQuery_TryGetComponent_TisT_t9065364841F9AB0E85F66D0244559308FF11A8B3_mD37FAE726611835CC36697B5FF08AEB8D1513A15;
extern const uint32_t g_rgctx_LeanScreenQuery_TryGetComponentInParent_TisT_t9065364841F9AB0E85F66D0244559308FF11A8B3_mAA9469D6C61CE9F394371421B037C92CC2C64C03;
extern const uint32_t g_rgctx_LeanScreenQuery_TryGetComponent_TisT_t6E558AA8DF649B5677E45B8C9350B7871541AE1C_m21C7840B31EDC59BE3E6A0F3DB6546DD82CAB0EB;
extern const uint32_t g_rgctx_LeanScreenQuery_TryGetComponentInParent_TisT_t6E558AA8DF649B5677E45B8C9350B7871541AE1C_mEF818870987FFF342E9334E348FDE65664FEBD7B;
static const Il2CppRGCTXDefinition s_rgctxValues[84] = 
{
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_List_1_t9D5226C09539DCFE6F17980B2CA80246A767D71B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_GetEnumerator_m11BC00E3B94F5DB96A1DC725C3F805DCA16A3DE3 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Enumerator_get_Current_m69B86CD1EB610BBB35286EC5E147DBF647791D0C },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t0C181BCE5EED2D591C73325C810382694033733B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Enumerator_MoveNext_mA21E18CED0004EB349F92DBDEA5E006E5F72F4C6 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Enumerator_tE49C91A74F705644F08B09D1CB5A49EEE9B905B3 },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_Enumerator_tE49C91A74F705644F08B09D1CB5A49EEE9B905B3_IDisposable_Dispose_m3C902735BE731EE30AC1185E7AEF6ACE7A9D9CC7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Link_Find_TisT_t6832FED987DCE0C89C04A83EF186035EF9C33529_m97563FC7EDB2FD284C7E2DEEB78E2F33CFA61EC1 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t6832FED987DCE0C89C04A83EF186035EF9C33529 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_List_1_t6EA554F03B518C8B043AB80D1212A1598EE1B71D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1__ctor_m4E2F9C7CC0952B7F1CBDB471CFE8E0A294F39584 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Activator_CreateInstance_TisT_t6832FED987DCE0C89C04A83EF186035EF9C33529_m19C88AF574A71C93D7C8E8447FA1305DE11A2577 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_Add_m3F8B933715E4A4A4E5307D79CD960AA377FC4140 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_List_1_tFB4490DBAB4EFFD4006F713CC448FE4C27C0AF1E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_GetEnumerator_m3769AD723DBDDA37B64EFC8B1293796B1715630E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Enumerator_get_Current_mBE0BB255C9F5F96B899793909A4F3FE5174C5377 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t29B68F7EE86934AA36549B2A5B00371C54DFD897 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Enumerator_MoveNext_m8732D41B144FBF575A921AA00D4EA16E23FDDD3F },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Enumerator_tAFB989EE0F6D38C61A944B74FFC3B36672E2BFFF },
	{ (Il2CppRGCTXDataType)5, (const void *)&g_rgctx_Enumerator_tAFB989EE0F6D38C61A944B74FFC3B36672E2BFFF_IDisposable_Dispose_m3C902735BE731EE30AC1185E7AEF6ACE7A9D9CC7 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_Clear_mEDEBB4A20BA424496C55206C9D31676EBB1384C8 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_tA38F61C0E6F11818D1CB5408059D7A1344B08635 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_List_1_tE82603D39C58B31C9BEE715279825DB0FDED59BC },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_Remove_mA80BE859BDB4369EBD4E6190E75F07AAD6E6C239 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_GameObject_GetComponent_TisT_tEC189863069A09F0DE623682D9BA09F16CFFB888_mB8D8B744B48690B28E21D0044616164F6E41FCCA },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_tEC189863069A09F0DE623682D9BA09F16CFFB888 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_CwHelper_AddComponent_TisT_tEC189863069A09F0DE623682D9BA09F16CFFB888_mB47104665F5E2328583B835EE20CFB878A51716E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_GameObject_AddComponent_TisT_t7CB447CEA53FBEBFD427F2B303590A3D85F87143_m30B2A54A79EF10721547999EE40DC3A22F30ABDC },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_List_1_t82EC5530ED72B06125E9E92978658D2A21077E41 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_get_Count_m79F4D32D685EDD8F8386052E90E50888EB562EA2 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_RemoveRange_m65028C69635AD71188FE8021BF048CDEA255C733 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_set_Capacity_m6DF3442D9A9A067DFB899B3374B8A19DADBF2756 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_Add_m7E0321CCA9B3C31B3A6F9FCF1212316EE8AA1292 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t54D3A915C10255852017076CAD645DD40F6A55A0 },
	{ (Il2CppRGCTXDataType)1, (const void *)&g_rgctx_T_tB80BBD53C8EE38ADE31A6F90C4654A478BC6B8F2 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_GameObject_AddComponent_TisT_tB80BBD53C8EE38ADE31A6F90C4654A478BC6B8F2_mF9A399748E1A72C54930CAA11EC88FDFCB79A43A },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_tB80BBD53C8EE38ADE31A6F90C4654A478BC6B8F2 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_List_1_tF837CB0D235AEF9DAC706BEBBBC138851F6AFFF2 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_get_Count_m14EB754694642F605DB566C458C4E55544C9879B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_get_Item_m14970D45CBED1F82D5591F64526D19EA51AAFAE0 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_tCC0B1655D736A7D1C2A0E5D1676098117092A94C },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_List_1_t48257785605EAAF9E58E0B082062DC31D2A21525 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_get_Count_m0A3CC3FBFDA56CB058C8FFC05A1754DDD54DBAF4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_get_Item_m500BFFC96020526DE840DB3E662CB5BF100D0972 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t78611A33058D5025311A23616D78CE0B040FFE99 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_List_1_t1DD57A4B701FEB82C591892ADA62D1E0AE5BB057 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_get_Count_mF41B8585912C2E945554A5D7674F736AAF73C1FC },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_get_Item_m195B116C6D16F7CD2BA9B125216954559DA09D86 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t337CCD0F3829FDEBA3C54C70EFEE3EC8A743FD68 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_RemoveAt_m93B47E1E1D642722D055B6DB9431A949D046825B },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Stack_1_t8B13CD080E3B8076F22748D84A77F5268B571145 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Stack_1_Push_m4517936A62D87CB253A137D5E41A359ECA1CBDAC },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_List_1_t453195EF2077CD349F148CA8413C8A7967AC3E2B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_get_Count_m6AD43FB68A17451FC76EF7BE18B4C1958FAA4452 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_get_Item_m1EF4E7103387DDE2728AA2BC441D606B350C2E62 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Stack_1_tDCBB42471D165F332622A82366EAC7FCDBA0FEAB },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Stack_1_Push_mDEAAA1B27E41EF8354835202FF78BB65CC4CCAB9 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_Clear_m6A9A4B9FE049AA4306BB697B918B6CC51CE5D5BB },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_List_1_t3C0DFD8812592894A652E0E9BED327AA39EB642E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_get_Count_mD7FE631CFE8BB49E829822184382801CDD5A7606 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_get_Item_m7C31953EA2B5622F5C0C3E5BDBD630D9252CC9BD },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_tC514A105ABCAB98B1C47C0B1E22EB96CF0C96CA1 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_List_1_tAD1CD954071596F86579DA42E05D58633CBA911D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1__ctor_m25276695E32DB82CE13AF3B87C2B61F1FB6510CB },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_get_Count_m12D59112CB3B05F7B7A27F1852693C1E0097FF63 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_get_Item_mECFB84630533D18BEA2EEBF34061BBF9E4285A71 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_tD34B7E4DA8E218FFBBDE375D21267F473DF45F3B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Activator_CreateInstance_TisT_tD34B7E4DA8E218FFBBDE375D21267F473DF45F3B_mA3D7DE56D082728979C3FCB13D66143E4B7A9B0C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_Add_mEFC54421F237238CA8A477A600FA3252D6F9FD97 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_tB4DD857F4AA856F2C514141EAC1B86B272CE5C5D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_GameObject_GetComponentInParent_TisT_tB4DD857F4AA856F2C514141EAC1B86B272CE5C5D_m7C24D95971C2BB01075A5B8AD47C5F1BCDAA8204 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Object_FindObjectOfType_TisT_tB4DD857F4AA856F2C514141EAC1B86B272CE5C5D_m4C93CE64584F34BF1E9F761AC5F50DFA4660CDA8 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_LeanScreenQuery_TryQuery_TisT_t65FBEF80A81DA8E283E7DE065D4E00633CD8D60D_mF4F6C06155DC2410BA99DC980459D762DBAB6962 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_LeanScreenQuery_TryResult_TisT_tC4D399A737A6D9D5784BB1EEDE747379A5D4ED1B_m3F33700555D96AF4A6B5E085130265A48A171AFE },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_LeanScreenQuery_TryGetComponent_TisT_t6C912E89D45C47254CA356C121BB1F0DE5F05D34_mE6CA9D82F29A5CAF6714399068FA06E72D280A7B },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_LeanScreenQuery_TryGetComponentInParent_TisT_t6C912E89D45C47254CA356C121BB1F0DE5F05D34_mBBB6608B84C4B20C3E94FDC7F16F78E35530CC38 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_LeanScreenQuery_TryGetComponentInChildren_TisT_t6C912E89D45C47254CA356C121BB1F0DE5F05D34_m90537D1B9BDF2A9676B1E41E924987F86DE098AA },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t6C912E89D45C47254CA356C121BB1F0DE5F05D34 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Component_GetComponent_TisT_t76D96A5C0143AC1F038AD1E39B9DA20AAE6BDD3E_m0E43A495066B10C1CAE57CBB4600B99E2D01EBDB },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t76D96A5C0143AC1F038AD1E39B9DA20AAE6BDD3E },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_LeanScreenQuery_TryGetComponent_TisT_t9065364841F9AB0E85F66D0244559308FF11A8B3_mD37FAE726611835CC36697B5FF08AEB8D1513A15 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_LeanScreenQuery_TryGetComponentInParent_TisT_t9065364841F9AB0E85F66D0244559308FF11A8B3_mAA9469D6C61CE9F394371421B037C92CC2C64C03 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_LeanScreenQuery_TryGetComponent_TisT_t6E558AA8DF649B5677E45B8C9350B7871541AE1C_m21C7840B31EDC59BE3E6A0F3DB6546DD82CAB0EB },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_LeanScreenQuery_TryGetComponentInParent_TisT_t6E558AA8DF649B5677E45B8C9350B7871541AE1C_mEF818870987FFF342E9334E348FDE65664FEBD7B },
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharpU2Dfirstpass_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpU2Dfirstpass_CodeGenModule = 
{
	"Assembly-CSharp-firstpass.dll",
	999,
	s_methodPointers,
	15,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	22,
	s_rgctxIndices,
	84,
	s_rgctxValues,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
